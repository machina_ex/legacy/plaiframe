/**
 * Enable timing in plaiframe Editor. Trigger next based on time.
 * 
 * @copyright Lasse Marburg 2020
 * @license GPLv3
 * @module time/time
 */

const plugin = require('../plugin.js')
var schema = require('./schema.json')

var timer = []

class Time extends plugin.Plugin {
  constructor() {
    super(schema)
    this.name = "time"
  }
  
  setup(config) {
    super.setup(config)
    return schema
  }
  
  cue(data, session) {
    return new Promise((resolve, reject) => {
      for(let item in data) {
        switch(item) {
          case "time":
            if(data.time.hasOwnProperty("timeout")) {
              session.variables.review(data.time.timeout)
              .then(result => {
                data.time.timeout = result
                data.time["index"] = timer.length
                data.time.session = session.name
                let timeout = new Timeout(data.time, session.callback, session.date)
                timer.push(timeout)
                return resolve(timeout.cancel.bind(timeout))
              })
              .catch(err => {
                return reject(err)
              })
            }
            break
        }
      }
    })
  }
  
  command(input) {
    switch(input[0]) {
      case "list":
        for(let t of timer) {
          log.info(this.name + " " + t.index, t.secondsLeft() + " seconds until: " + t.session + "." + t.next)
        }
        break
      default:
        super.command(input)
        break
    }
    
  }
  
  /*
  * var dt = new Date()
  * setMinutes & getMinutes & getMilliseconds & getUTCMilliseconds
  */
}

class Timer {
	constructor(props) {
		Object.assign(this, props)
	}

	done() {
		if(this.hasOwnProperty("next"))
		{
			log.info(this.name, "Done. Next: " + this.next)
			this.callback(this.next)
		} else {
			log.error(this.name, "has no next to dispatch")
		}
	}
  
  /**
  * from https://stackoverflow.com/questions/9640266/convert-hhmmss-string-to-seconds-only-in-javascript/9640417#9640417
  */
  hmsToSeconds(str) {
    var p = str.split(':'),
        s = 0, m = 1;

    while (p.length > 0) {
        s += m * parseInt(p.pop(), 10);
        m *= 60;
    }

    return s;
}
}

class Timeout extends Timer {
	constructor(properties, callback, start_date) {
		super(properties)
    this.name = "timeout"
		this.callback = callback
    this.start_date = start_date
    // log.debug(this.name, "cue happened at" + start_date.toISOString())
    
    this.timeout = this.hmsToSeconds(this.timeout)
    
    if(this.secondsLeft() < this.timeout) {
      log.debug(this.name, "restart " + this.timeout + " seconds timeout with " + this.secondsLeft() + " seconds left to go")
    } else {
      log.debug(this.name, "start " + this.timeout + " seconds timeout")
    }

		this.function = setTimeout(this.done.bind(this), this.timeout*1000-this.timeElapsed())
	}
  
  secondsLeft() {
    let time_since = new Date(Date.now() - this.start_date)
    return Math.round((this.timeout*1000-time_since)/1000)
  }
  
  timeElapsed() {
    return new Date(Date.now() - this.start_date)
  }
  
	cancel() {
    if(this.secondsLeft()) {
      log.debug(this.name, "cancel " + this.timeout + " seconds timeout. " + this.secondsLeft() + " seconds before it was done.")
    }
    timer.splice(this.index, 1)
    for(let i = 0; i < timer.length; i++) {
      timer[i].index = i
    }
		clearTimeout(this.function)
	}
}

module.exports.Plugin = Time
