/**
* helper module for a plugin module file
*
* contains classes that, when extended, enable your plugin module with useful functions
*
* Even if you don't use the Plugin or Collection class, their documentation exemplifies how plugins are build.
*
* @module plugin
*/

/**
* provides basic functions for plai plugins and helps to integrate plugins into plaiframe.
*
* Redefine abstract functions to extend your plugin.
*
* @param {Object} schema - js object notation schema. Defines the plugins necessities for
* settings, cue properties and, if plugin uses collections, creating new items
*/
class Plugin {
  constructor(schema) {
    if (schema) {
      this.schema = Object.deepClone(schema)
    } else {
      this.schema = {
        settings: {},
        cue: {}
      }
    }

  }

  /**
  *
  * Is called once after the plugin is inserted or restored by the game.
  * 
  * If the schema defines collections, setup creates object representations of the collection and 
  * inserts new collections to the database.
  *
  * @param {Object} config - Information and interfaces from and about game.
  * @param {Object} config.game - access to game variables and functions that required this module 
  * @param {string} config.game.name - game name
  * @param {EventEmitter} config.game.event - allows to dispatch game events.
  * @param {Object} config.game.changes - change listeners for this game database
  * @param {EventEmitter} config.game.status - event hub for relevant game incidents or changes
  * @param {function} config.game.alert - Forward alerts to game editor webview (SweetAlert object style) {@link https://plaitools.gitlab.io/documentation/plaiframe/module-game-Game.html#alert Game.alert}
  * @param {function} config.game.addCollection - Create a new custom database collection to the game database {@link https://plaitools.gitlab.io/documentation/plaiframe/module-game-Game.html#addCollection Game.addCollection}
  * @param {function} config.game.deleteSession - remove an existing session from the game {@link https://plaitools.gitlab.io/documentation/plaiframe/module-game-Game.html#deleteSession Game.deleteSession}
  * @param {function} config.game.createSession - launch a new session {@link https://plaitools.gitlab.io/documentation/plaiframe/module-game-Game.html#createSession Game.creatSession}
  * @param {Object} config.settings - settings that where stored previously using the editor
  * @param {Object} config.db - Game Databse collection connector
  * @param {Object} config.app - Express app instance
  * @param {Object} config.socket - socket.io namespace routed to /<pluginname>
  * @param {string} config.url - external webaddress as entered by game designer directing to node server
  * @param {string} config.name - this plugins name
  * @param {array} config.custom_collections - this games accessible collections
  *
  * @param {Object} [options] - aditional specifications on setup
  * @param {boolean} [options.item_collections] - set to true to allow custom collections
  *
  * @returns {Promise|Object} the plugin schema in it's current state.
  */
  setup(config, options) {
    return new Promise((resolve, reject) => {
      Object.assign(this, config)
      
      this.item_collections = false
      
      if(options) {
        if(options.hasOwnProperty("item_collections")) {
          this.item_collections = options.item_collections
        }
      }
      
      let collections_created = []
      
      if (this.schema.hasOwnProperty("collections")) {
        for (let collection in this.schema.collections) {
          this.schema.collections[collection]["properties"]["sessions"] = {
            "type":"array",
            "propertyOrder":3000,
            "readOnly":true,
            "options": {
              "hidden":true,
              "collapsed": true,
              "disable_array_add": true,
              "disable_array_delete": true,
              "disable_array_delete_all_rows":true,
              "disable_array_delete_last_row":true,
              "disable_array_reorder":true
            },
            "items":{
              "type":"object",
              "options": {
                "disable_edit_json": true,
                "disable_properties": true
              },
              "title":"session",
              "properties":{
                "_id":{
                  "type":"string",
                  "readOnly": true
                },
                "reference":{
                  "type":"string",
                  "readOnly": true
                }
              }
            }
          }
          collections_created.push(this.game.addCollection(collection))
          /*
          if (!this.db.hasOwnProperty(collection)) {
            collections_created.push(this.db.addCollection(collection))
          }
          */
        }
      }
      
      Promise.all(collections_created)
      .then(result => {
        let collections_collected = []
        
        if (this.schema.hasOwnProperty("collections")) {
          for (let collection in this.schema.collections) {
            this[collection] = new Collection(this.schema.collections[collection], this.db[collection])
            if(this.item_collections == false) {
              collections_collected.push(this[collection].collectItems())
            }
          }
        }
        
        return Promise.all(collections_collected)
      })
      .then(result => {
        return resolve()
      })
      .catch(error => {
        log.error(this.name, error)
        return reject(error)
      })
    })
  }

  /**
  * Called if a cue was triggered that contains any of this plugins actions.
  * 
  * If a cancel function is returned, the session will store all action data.
  * 
  * If the plaiframe server restarts for any reason, cue is retriggered with all active listeners.
  * 
  * @abstract
  *
  * @param {Object} data - cue data as defined in the cue action
  * @param {Object} session - Information and interfaces of and about the session the cue originates from.
  * @param {string} session.name - name of origin session
  * @param {string} session._id - _id of origin session
  * @param {function} session.callback - dispatches a next cue in origin session
  * @param {EventEmitter} session.event - Allows to emit and receive events in the session scope
  * @param {Variables} session.variables - set of functions to resolve variables in the session scope
  * @param {array} session.path - path list the cue is in
  * @param {string} session.chapter - chapter the cue is in (soon to be deprecated)
  * @param {Date} session.date - date when the cue was dispatched. On server restart when current cue is recalled, this is the original date.
  * @param {string} session.cuename - name of cue
  *
  * @returns {Promise|undefined|string} For child class: Return cancel function if a listener was installed. Return a cuename to trigger next cue. Else return undefined
  */
  cue(data, session) {
    log.warn(this.name, "received cue data but cue function not specified:")
    log.warn(this.name, data)
  }

  /**
  * called when a session wants to cancel a listener that did'nt return a cancel function on installation.
  * 
  * @todo by the date, this function is never called, since listener are forced to return their own function.
  * should this be changed?
  * 
  * @abstract
  * @deprecated
  *
  * @param {Object} listener - all data that was stored about the listener
  */
  cancel(listener) {
    log.warn(this.name, "received cancel command but cancel function not specified:")
    log.warn(this.name, listener)
  }

  /**
  * is called if settings, based on the schema the module provides, were changed
  *
  * @abstract
  *
  * @param {Object} new_settings - changed settings based on settings schema of this plugin
  */
  update(new_settings) {

  }

  /**
  * is called the moment the plugin was disabled in a game
  *
  * @abstract
  *
  * @param {string} config.game - Name of game that required this module
  */
  removed(config) {

  }

  /**
   * is called before node process exits.
   * Can for example be used to stop external processes
   * 
   * @abstract
   *
   * @returns {Promise} - return once everything is set and done to exit
   */
  quit() {
    return new Promise((resolve, reject) => {
      resolve()
    })
  }

  /**
  * handle commands forwarded by game
  *
  * Commands are handed to plugins if they are addressed by name. 
  * Plaiframe will call the `command()` function with whatever is typed after the plugin name.
  * 
  * Commands that match any collection are forwarded to the named collection command function.
  */
  command(input) {
    switch (input[0]) {
      case "collections":
        if (this.schema.hasOwnProperty("collections")) {
          for(let key in this.schema.collections) {
            log.info(this.name, key + " - " + this.schema.collections[key].title + ": " + this.schema.collections[key].description)
          }
        }
        break  
      default:
        if (this.hasOwnProperty(input[0])) {
          let coll = input.shift()
          this[coll].command(input)
        } else if (this.schema.hasOwnProperty("collections")) {
          if (Object.keys(this.schema.collections).length == 1) {
            for (let col in this.schema.collections) {
              this[col].command(input)
            }
          } else {
            log.info(this.name, "No such collection or command '" + input[0] + "'")
          }
        } else {
          log.info(this.name, "No such collection or command '" + input[0] + "'")
        }
        break
    }
  }

  /**
  * write modified schema to plugin document in plugins database
  * 
  * @todo dispatch live update event for gui editor
  * 
  * @param {Object} schema - plugin json schema object
  */
  schemaUpdate(schema) {
    this.schema = schema
    this.db.plugins.set({ name: this.name }, { schema: schema })
  }
}

/**
* provides useful functions for plugins that use collections to create and maintain items.
* 
* @param {Object} schema - containing schemes for collection items
* @param {Object} db_coll - mongodb access to collection in game database
*/
class Collection {
  constructor(schema, db_coll) {
    this.schema = schema
    this.items = {}
    this.db_coll = db_coll
    
    this.name = this.db_coll.name
    
    let coll_change_stream = db_coll.changes({ fullDocument: 'updateLookup' })
    coll_change_stream.on("change", this.review.bind(this))
  }

  /**
  * check db collection for item list and call add function for each record.
  */
  collectItems() {
    return new Promise((resolve, reject) => {
      this.db_coll.find({})
        .then(items => {
          for (let item of items) {
            this.add(item)
          }
          resolve(true)
        })
        .catch(err => {
          log.error(this.name, err)
        })
    })
  }
  
  /**
  * walk through items and look for matching key - value pair
  *
  * @param {string} key - key to look up
  * @param value - value in key to match
  *
  * @returns {Plugin} first item Object that has key:value. undefined if no item was found.
  */
  findItem(key, value) {
    
    for(let item in this.items) {
      if(this.items[item].hasOwnProperty(key)) {
        
        if(this.items[item][key] == value) {
          return this.items[item]
        } else if (key == "_id") {
          if(this.db_coll.compareIDs(this.items[item]._id, value)) {
            return this.items[item]
          }
        }
      }
    }
    log.warn(this.name, "no item found with " + key + ": '" + value + "'")
    return undefined
  }


  /**
  * on change in db collection, check for insert, replace, update and delete operation
  * 
  * - add new item on insert
  * - call item update functions on replace or update
  * - redirect item instance if name was updated
  * - remove item on delete
  *
  * @todo for items that create extra properties in the local object mongo replace leeds to
  * reporting changes that are not actually there
  *
  * @param {Object} change - Mongo Change Event Object or Object that derives from Mongo Change Event. See {@link https://docs.mongodb.com/manual/reference/change-events/#change-stream-output}
  */
  review(change) {
    // log.debug(this.name, change.operationType + " operation in devices collection")
    switch (change.operationType) {
      case 'insert':
        this.add(change.fullDocument)
        break

      case "replace":
      case "update":
        let item = this.findItem("_id", change.documentKey._id)
        
        if(!item) {
          log.error(this.name, "Failed to update. Item missing: " + change.fullDocument.name + " _id: " + change.documentKey._id)
          return
        }
        
        let changes = {updated:{},removed:[]}
        
        if(change.hasOwnProperty("updateDescription")) {
          if(change.updateDescription.hasOwnProperty("updatedFields")) {
            changes.updated = change.updateDescription.updatedFields
          }
          if(change.updateDescription.hasOwnProperty("removedFields")) {
            changes.removed = change.updateDescription.removedFields
          }
        } else {
          changes = Object.changes(item, change.fullDocument)
        }
        changes["document"] = change.fullDocument
        
        if(changes.updated.hasOwnProperty("settings")) {
          if(typeof item.reconnect === "function") {
            log.debug(item.name, "settings changed: ")
            log.debug(item.name, changes.document.settings)
            item.reconnect(changes.document.settings)
          } else {
            log.warn(this.name, item.name + " settings have changed but item does not own a reconnect function to apply them.")
          }
        }
        
        this.update(item.name, changes)
        
        if(item.name != changes.document.name) {
          log.debug(item.name, " name changed to " + changes.document.name)
          this.items[changes.document.name] = item
          item.name = changes.document.name
          delete this.items[item.name]
        }
        
        delete changes.updated.settings
        delete changes.updated.name
        
        if(typeof item.update === "function") {
          item.update(changes.updated)
        } else {
          log.warn(this.name, item.name + " was updated but has no update function.")
        }
        break

      case 'delete':
        for (let item in this.items) {
          if (this.db_coll.compareIDs(this.items[item]._id, change.documentKey._id)) {
            this.remove(item)
            break
          }
        }
        break
    }
  }

  /**
  * add an item of class Item to collection. Redefine this function if you want to add items of custom type/class.
  * 
  * @param {Object} data - initial item data
  */
  add(data) {
    this.items[data.name] = new Item(data, this.db_coll)
    log.info(this.name, "add item: " + data.name)
  }

  /**
  * call item close function and delete item from collection Object
  */
  remove(item) {
    if(typeof this.items[item]["close"] === "function") {
      this.items[item].close()
    } else {
      log.warn(this.name, item + " has no close function.")
    }
    
    delete this.items[item]
    log.debug(this.name, "Item removed: " + item)
  }

  /** 
  * is called if changes happened for one of the items.
  *
  * @abstract 
  *
  * @param {string} item - name of item
  * @param {Object} changes - information about changes
  * @param {Object} changes.updated - elements that have changed
  * @param {Object} [changes.removed] - elements that where removed, if any
  * @param {Object} changes.document - the whole altered document
  */
  update(item, changes) {
    
  }

  /**
  * Get command forwarded from plugin. 
  * 
  * If input matches the name property of an item in the collection, the command is forwarded to the item command function.
  * 
  * If you defined a custom command function you should still call this to forward commands to collection items. Use the super keyword if you extend the Collection class:
  * 
  * `super.command(input)`
  *
  */
  command(input) {
    switch (input[0]) {
      default:
        if (this.items.hasOwnProperty(input[0])) {
          let item = input.shift()
          if (typeof this.items[item].command === "function") {
            this.items[item].command(input)
          } else {
            log.warn(this.name, item + " has no command function")
          }
        } else if (Object.keys(this.items).length == 1) {
          for (let item in this.items) {
            if (typeof this.items[item].command === "function") {
              this.items[item].command(input)
            } else {
              log.warn(this.name, item + " has no command function")
            }
          }
        } else {
          log.info(this.name, "No such item or command '" + input[0] + "'")
        }
        break
    }
  }
}

/**
* provides basic functions for items plai plugins might inherit
*
* @param {Object} data - initial set of item data when item is added to collection
* @param {Object} coll - db collection interface
*/
class Item {
  constructor(data, coll) {
    Object.assign(this, data)
    
    /** access to item element in database */
    this.document = coll.document({_id:data._id})
  }

  /**
  * is called once the Item was removed from collection
  */
  close() {

  }

  /**
  * is called once settings property for this item are changed
  */
  reconnect(settings) {
    this.settings = settings
  }

  /**
  * is called once any property for this item has changed
  */
  update(data) {
    log.debug(this.name, "update")
    log.debug(this.name, data)
    Object.assign(this, data)
  }
  
  
  /**  
   * called with input that is addressing the item. Collection has to forward input to specified item.
   * 
   * @abstract  
   * @param  {array} input command line input forwarded from collection command function
   */   
  command(input) {
    switch (input[0]) {
      default:
        if(this.hasOwnProperty(input[0])) {
          log.info(this.name, this[input[0]])
        } else {
          log.warn(this.name, "doesn't understand " + input[0])
        }
    }
  }


}

module.exports = {
  Plugin: Plugin,
  Collection: Collection,
  Item: Item
}
