/**
* Allows to control basic funcitonalities of plaiframe.
*
* @requires plugin
* @requires medley
*
* @module control/control
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

const plugin = require("../plugin.js")
const schema = require('./schema.json')
const medley = require('./../../medley.js')

/**
 * control this or remote sessions
 *
 * @class SessionControl
 * @extends {plugin.Plugin}
 */
class SessionControl extends plugin.Plugin {
  constructor() {
    super()
  }
  
  /**
   * Session control has no specific config requirements
   *
   * @param {Object} config
   * @returns {Object} - schema
   */
  setup(config) {
    super.setup(config)
    return schema
  }
  
  /**
   * forward cue data to control function
   *
   * @param {Object} data
   * @param {Object} session
   * @returns {Promise} - cue string or undefined returned by control function
   */
  cue(data, session) {
    return new Promise((resolve, reject) => {
      for(let item in data) {
        switch(item) {
          case "session":
            return resolve(this.control(data[item], session))
        }
      }
    })
  }
  
  /**
   *  execute one of the available session commands
   * 
   * - trigger next cue in this session
   * - quit this session (all further actions will fail. Next cues will not be found etc.)
   * - split a new path
   * - join an existing path and cancel its listeners
   * - launch another session
   * - cancel an external session
   *
   * @param {Object} data - see control schema for details
   * @param {Object} session - session context variables and functions
   * @returns {Promise} - next cue string or undefined. See schema for next property in each control action
   */
  control(data, session) {
    return new Promise((resolve, reject) => {
      for(let item in data) {
        switch(item) {
          case "next":
            return resolve(data[item])
          case "quit":
            this.game.deleteSession({"_id":session._id})
              .then(result => {
                return resolve(undefined)
              })
              .catch(err => {
                return reject(err)
              })
              break
          case "split":
            if(Array.isArray(data.split)) {
              for(let split of data.split) {
                this.split(split, session)
              }
            } else {
              this.split(data.split, session)
            }
            
            return resolve(undefined)
          case "join":
            // session.joinPath(data.join) // ignore since join depends on path list
            return resolve(undefined)
          case "launch":
            this.launchSession(data[item],session)
            .then(result => {
              return resolve(result)
            })
            .catch(error => {
              return reject(error)
            })
            break
          case "cancel":
            let session_ref = session.variables.getReference(data[item])
            if(session_ref) {
              this.game.deleteSession(session_ref.query)
              .then(result => {
                return resolve(undefined)
              })
              .catch(err => {
                return reject(err)
              })
            } else {
              return reject("Can not cancel session " + data[item] + ".")
            }
            break
        }
      }
    })
  }

  /**
   * call session splitPath function to split a new path. Create uid pathname if name is not provided.
   * @param {Object} path - path Details, next and name
   * @param {Object} session - session object
   */
  split(path, session) {
    if(path.hasOwnProperty('name')) {
      session.splitPath(path.next, path.name)
    } else {
      session.splitPath(path.next, generateUUID())
    }
  }

  /**
   * create new session
   * 
   * - review and assign arguments
   * - if there is a language property, use config.family and config.language keys to determine level.
   * - if language property does not resolve, defaults to level based on name
   * - create reference in this session referencing the new session
   * - dispatch next (if any) when done
   *
   * @param {Object} data
   * @param {string} data.level - level the session will be based upon
   * @param {Object} [data.arguments] - arguments to forward to new session
   * @param {string} [data.name] - name for new session document
   * @param {Object} session - session context variables and functions
   * @returns {Promise} - next cue string or undefined
   */
  launchSession(data, session) {
    return new Promise((resolve, reject) => {
      let new_session
      let level_query
      session.variables.review(data.level)
      .then(result => {
        if(!result) {
          throw new Error(data.level + " does not resolve to any value")
        }
        data.level = result

        if(data.language) {
          return session.variables.review(data.language)
        }
        return
      })
      .then(lan => {
        if(lan) {
          level_query = {
            "config.family":data.level,
            "config.language":lan
          }
        } else {
          level_query = {
            "name":data.level
          }
        }

        if(data.name) {
          return session.variables.review(data.name)
        }
        return
      })
      .then(name => {
        let args = {}

        for(let arg in data["arguments"]) {
          let arg_ref = session.variables.getReference(data["arguments"][arg])
          if(arg_ref) {
            args[arg] = {query:arg_ref.query, collection:arg_ref.collection}
          } else {
            log.warn(this.name, "couldn't assign argument '" + arg + "' when launching new session")
          }
        }

        if(name) {
          return this.game.createSession({"level":level_query, "arguments":args, "name":name})
        } else {
          return this.game.createSession({"level":level_query, "arguments":args})
        }
      })
      .then(result => {
        new_session = result
        session.variables.store({_id:new_session._id, name:new_session.name, "arguments":new_session.references})
      })
      .then(result => {
        if(data.hasOwnProperty("reference")) {
          return session.variables.review(data.reference)
        }
        return
      })
      .then(reference => {
        if(reference) {
          return session.createReference("sessions", {_id:new_session._id}, reference)
        }
        return
      })
      .then(result => {
        if(data.hasOwnProperty("next")) {
          return resolve(data.next)
        }
        return resolve()
      })
      .catch(error => {
        if(error == "session exists") {
          log.error(this.name, "couldn't launch session with name " + data.name + ". " + data.name + " already exists.")
        }
        return reject(error)
      })
    })
  }
}

module.exports.Plugin = SessionControl
