/**
* control IKEA Tradfri devices like light bulbs.
* Requires at least one IKEA Tradfri Gateway
*
* Work in Progress
*
* @todo almost everything
*
* @requires node-tradfri-client
*
* @module tradfri/tradfri
*/
const tradfri_client = require("node-tradfri-client")

var schema = {
  "settings":{
    "type":"object",
    "title":"Settings",
    "properties":{
      "test":{
        "type":"string",
        "enum":["abc","def","ghi"]
      }
    }
  },
  "cue":{
    "tradfri":{
      "type":"array",
      "items":{
        "type":"object",
        "properties":{}
      }
    }
  },
  "collections":{
    "gateway":{
      "type":"object",
      "title":"Gateways",
      "properties":{
        "settings":{
          "type":"object",
          "required":true,
          "properties":{
            "id":{
              "required":true,
              "type":"string"
            },
            "key":{
              "required":true,
              "type":"string"
            }
          }
        }
      }
    }
  }
}
/**
*
* @param {string} config.game - Name of game that required this module 
* @param {Object} config.collection - Databse collection connector for the tradfri module
*/
function setup(config) {
  log.info("tradfri", "setup config for:")
  log.info("tradfri", config.game)
  return(schema)
}

/**
* called when settings based on the schema the module provides, are changed
*
* @param {Object} new_settings - changed settings based on perferences schema of this plugin
*/
function settings(new_settings) {
  log.info("tradfri", "change settings:")
  log.info("tradfri", new_settings)
}

/**
* handle commands forwarded by game
*/
function command(input) {
  switch(input[0]) {
    case "discover":
    tradfri_client.discoverGateway()
    .then(function(result) {
      log.info("Tradfri", result)
    })
    break
    default:
    log.warn("tradfri", "no function called " + input[0])
    break
  }
}

class Gateway {
  constructor(properties) {
    
  }
}

module.exports = {
  setup:setup,
  command:command,
  settings:settings
}
