/**
* Connect to D-Pro Light Control Software using OSC commands
*
* @requires device
*
* @module d-pro/d-pro
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

const device = require("../devices/device.js")
const plugin = require("../plugin.js")

var schema = {
  "cue": {
  },
  "collections": {
    "shows": {
      "type": "object",
      "title": "D-Pro Show",
      "properties": {
        "settings": {
          "type": "object",
          "description": "set the connection properties for this device",
          "required": true,
          "properties": {
            "port": {
              "type": "number",
              "default": 0,
              "required": true,
              "description": "port the device communicates with the plai framework on. 0 to set random port"
            },
            "ip": {
              "type": "string",
              "description": "the ip address of your device so the framework can send messages to it.",
              "default": "0.0.0.0"
            },
            "framework_port": {
              "type": "number",
              "description": "define a different port the device sends data to the framework on if needed."
            },
            "path": {
              "type": "string",
              "descrption": "prepend an OSC formatted address for every message sent by OSC device"
            }
          }
        }
      }
    }
  }
}



class DPro extends plugin.Plugin {
  constructor() {
    super(schema)
  }

  /**
  * overwrite plugin add function:
  * - add a new Show instance with every added collection
  * - set collection to shows because Show extends from OSCdevice which would otherwise default to device collection for data storage
  * - add show property to cue schema
  *
  * overwrite plugin remove function
  * - remove the show instance
  * - remove its schema properties from the schema
  *
  */
  setup(config) {
    super.setup(config)
      .then(function (result) {
        this.shows.add = function (data) {
          schema.cue[data.name] = {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {}
            },
            "default": [{}]
          }
          data["collection"] = "shows"
          this.shows.items[data.name] = new Show(data, this.db, this.db.shows.document({ "name": data.name }), this.showChanges.bind(this))
        }.bind(this)
        this.shows.remove = function (name) {
          this.shows.items[name].close()
          delete this.shows.items[name]
          delete schema.cue[name]
          this.schemaUpdate(schema)
          log.debug(this.name, "Show removed: " + name)
        }.bind(this)
        this.shows.collectItems()
      }.bind(this))
      .catch(function (error) {
        log.error("D-Pro", error)
      }.bind(this))

    return schema
  }

  cue(data) {
    return new Promise((resolve, reject) => {
      let promises = []
      for (let d in data) {
        if (this.shows.items.hasOwnProperty(d)) {
          promises.push(this.shows.items[d].send(data[d]))
        } else {
          log.error(this.name, "no D-Pro show called " + d)
        }
      }
      Promise.all(promises)
      .then(result => {
        return resolve()
      })
      .catch(err => {
        return reject(err)
      })
    })
  }

  command(input) {
    switch (input[0]) {
      case "shows":
        for (let show in this.shows.items) {
          log.info(this.name, show)
        }
        break
      default:
        super.command(input)
        break
    }
  }

  showChanges(changes, show) {
    for (let change in changes) {
      if (change == "schema") {
        schema.cue[show].items.properties = changes[change]
        this.schemaUpdate(schema)
      }
    }
  }
}

/**
* OSC connection to Enttec D-Pro Light Software
*
* registers mapping from d-pro show and converts it to cue shema for level design
* including all cues, snap etc. the d-pro show has to offer
*
* @param {Object} config - device config data. see device.OSCdevice
* @param {Document} device_doc - mongodb document connector
* @param {function} callback - to report schema changes to d-pro plugin
*/
class Show extends device.OSCdevice {
  constructor(config, db, device_doc, callback) {
    super(config, db)
    this.doc = device_doc
    this.mapping = { "btn": {}, "snap": {}, "fade": {} }
    this.callback = callback

    this.doc.get()
      .then(function (data) {
        if (data.hasOwnProperty('mapping')) {
          this.mapping = data.mapping
          this.schemaUpdate()
        }
      }.bind(this))
      .catch(function (err) {
        log.error(this.name, err)
      }.bind(this))

    /** d-pro show properties */
    this.schema = {
      "btn": {
        "title": "cue",
        "type": "object",
        "description": "Run a cue or cuelist.",
        "properties": {}
      },
      "snap": {
        "title": "snapshot",
        "type": "string",
        "description": "Run a snapshot.",
        "enum": []
      },
      "fade": {
        "title": "fade",
        "type": "object",
        "description": "Run the cue or cuelist with the specified level.",
        "properties": {}
      },
      "reset": {
        "type": "boolean",
        "description": "stop all active cues & cuelists",
        "default": true
      },
      "master": {
        "type": "number",
        "format": "range",
        "minimum": 0.0,
        "maximum": 1.0,
        "step": 0.01,
        "descripton": "set the grand master level"
      },
      "speed": {
        "type": "number",
        "minimum": 0.25,
        "maximum": 4.0,
        "format": "range",
        "step": 0.01,
        "descripton": "set the global speed"
      },
      "tempo": {
        "type": "number",
        "minimum": 30.0,
        "maximum": 480.0,
        "format": "range",
        "step": 1,
        "descrption": "set the global speed using a tempo"
      }
    }

    this.schemaUpdate()
  }

  async send(msg) {
    let snd = {}
    for (let item in msg) {
      switch (item) {
        case 'btn':
          for (let btn in msg[item]) {
            snd[this.mapping.btn[btn]] = msg[item][btn]
          }
          break

        case 'snap':
          snd[this.mapping.snap[msg[item]]] = 1
          break

        case 'fade':
          for (let fade in msg[item]) {
            snd[this.mapping.fade[fade]] = msg[item][fade]
          }
          break

        case 'reset':
          snd['sc/reset'] = 1
          break

        case 'master':
          snd['sc/gm'] = msg[item]
          break

        case 'speed':
          snd['sc/speed'] = msg[item]
          break

        case 'tempo':
          snd['sc/tempo'] = msg[item]
          break
      }
      await super.send(snd)
    }
    return
  }

  /**
  * middle route function to catch and map all label assignments from d-pro
  *
  * remove empty label addresses
  */
  route(osc_msg) {
    if (osc_msg.address.match(/label/g)) {
      let path = osc_msg.address.split('/')
      if (path.length > 2) {
        if (osc_msg.args[0].value == '') {
          if (this.mapping.hasOwnProperty(path[2])) {

            for (let map in this.mapping[path[2]]) {
              if (this.mapping[path[2]][map] == osc_msg.address.replace(/label/g, 'run').substring(1)) {
                delete this.mapping[path[2]][map]
                //log.info(this.name, "remove", osc_msg.address.replace(/label/g, 'run'))
                return null
              }
            }
          }
        } else {
          let addr = osc_msg.address.replace(/label/g, 'run')
          addr = addr.substring(1) // remove leading '/'
          if (this.mapping.hasOwnProperty(path[2])) {

            if (this.mapping[path[2]].hasOwnProperty(osc_msg.args[0].value)) {
              if (this.mapping[path[2]][osc_msg.args[0].value] == addr) {
                return null
              } else {
                log.debug(this.name, "add to mapping:", osc_msg.args[0].value, addr)
                this.mapping[path[2]][osc_msg.args[0].value] = addr
              }
            } else {
              log.debug(this.name, "add to mapping:", osc_msg.args[0].value, addr)
              this.mapping[path[2]][osc_msg.args[0].value] = addr
            }
          }
        }
        this.doc.set({ mapping: this.mapping })
        this.schemaUpdate()
        return null
      }

    } else {
      return super.route(osc_msg)
    }
  }

  schemaUpdate() {
    // BTN
    this.schema.btn.properties = {}
    for (let btn in this.mapping.btn) {
      if (!this.schema.btn.properties.hasOwnProperty(btn)) {
        this.schema.btn.properties[btn] = { "type": "integer", "enum": [0, 1], "default": 1 }
      }
    }
    //SNAP
    this.schema.snap.enum = []
    for (let snap in this.mapping.snap) {
      if (!this.schema.snap.enum.includes(snap)) {
        this.schema.snap.enum.push(snap)
      }
    }
    // FADE
    this.schema.fade.properties = {}
    for (let fade in this.mapping.fade) {
      if (!this.schema.fade.properties.hasOwnProperty(fade)) {
        this.schema.fade.properties[fade] = { "type": "number", "format": "range", "step": 0.01, "minimum": 0.0, "maximum": 1.0, }
      }
    }
    //this.doc.set({"schema":this.schema})
    this.callback({ "schema": this.schema }, this.name)
  }
}

module.exports.Plugin = DPro
