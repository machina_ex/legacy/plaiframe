/**
 * Enable logics in plaiframe Editor. Listen for events or data changes, switch based on conditions etc.
 * 
 * @requires json5
 * @requires plugin
 * @requires events
 * @requires functions
 *
 * @module logic/logic
 * @copyright Lasse Marburg 2020
 * @license GPLv3
 */

const plugin = require('../plugin.js')
var schema = require('./schema.json')
const events = require('events')
const JSON5 = require('json5')
const functions = require('./functions.js')

/**
 * extend cue with switch, listener and event options
 *
 * @class Logic
 * @extends {plugin.Plugin}
 */
class Logic extends plugin.Plugin {
  constructor() {
    super(schema)
    this.name = "logic"
    this.listener = []
    this.event = new events.EventEmitter()
    this.event.setMaxListeners(50)
  }

  /**
   * install change stream listeners for all custom collections and the session collection
   *
   * @param {*} config - game variables, functions, eventemitter etc.
   * @param {Database} config.db - game database connector instance
   * @returns {Object} current logic plugin schema
   */
  setup(config) {
    super.setup(config)

    this.schema["game"] = this.game.name

    this.runtime_collections = []
    if(this.hasOwnProperty("custom_collections")) {
      if(this.custom_collections) {
        this.runtime_collections = this.custom_collections.slice()
      }
    }
    this.runtime_collections.push("sessions")
    
    for(let collection of this.runtime_collections) {
      if(!this.db.hasOwnProperty(collection)) {
        log.error(this.name, collection + " not found")
        break
      }
      let stream = this.db[collection].changes({ fullDocument: 'updateLookup' })
      
      let change_listener = (change) => {
        if(change.operationType != 'delete' && change.operationType != 'invalidate')
        {
          if(change.operationType == 'update') {
            this.event.emit("change", {document:change.fullDocument, updated:change.updateDescription.updatedFields, removed:change.updateDescription.removedFields, collection:this.db[collection]})
            //log.debug(this.name, change.updateDescription.updatedFields, change.updateDescription.removedFields)
          } else {
            this.event.emit("change", {document:change.fullDocument, collection:this.db[collection]})
          }
        }
      }
      
      stream.on("change", change_listener)
    }
    
    this.game.status.on("plugins_loaded", e => {
      this.db.plugins.find({})
      .then((result) => {
        this.mergeEvents(result)

        this.game.changes["plugins"].on("change", change => {
          if(!this.db.plugins.compareIDs(change.documentKey._id, this._id) && change.hasOwnProperty("fullDocument")) {
            this.mergeEvents([change.fullDocument])
          }
        })
      }).catch((err) => {
        log.error(this.name, err)
      })
    })

    return this.schema
  }
  
  /**
  * listen:  
  * returns next cue if there is an instant match.
  * Else it installs listener and returns cancel function.
  *
  * switch:
  * if there is a match or else, next cue is returned to session
  * 
  * event:
  * start listening for specified events. Returns cancel function
  *
  * @returns {Promise} might return a next cue string (switch) or a cancel function (listen and event)
  */
  cue(data, session) {
    return new Promise((resolve, reject) => {
      for(let item in data) {
        switch(item) {
          case "listen":
            let listener = new Listener(data[item], session, this.event, this.db)
            listener.review()
            .then(result => {
              if(result) {
                listener.cancel()
                return resolve(result)
              }
              return resolve(listener.cancel.bind(listener))
            })
            .catch(error => {
              return reject(error)
            })
            break
          case "switch":
            this.switch(data[item], session)
            .then(result => {
              return resolve(result)
            })
            .catch(error => {
              return reject(error)
            })
            break
          case "on":
            let eventlistener = new PEventListener(data[item], session, this.game)
            return resolve(eventlistener.cancel.bind(eventlistener))
          case "dispatch":
            if(data[item]["range"] == "local") {
              this.dispatchEvent(data[item]["event"], data[item]["load"], session.event)
            } else if (data[item]["range"] == "global"){
              this.dispatchEvent(data[item]["event"], data[item]["load"], this.game.event)
            }
            return resolve(undefined)
          case "function":
            if(typeof functions[data.function.name] === "function") {
              session.variables.findAndReplace(data.function["arguments"])
              .then(args => {
                Object.assign(session, {game:this.game, db:this.db})
                log.info(this.name, "call function " + data.function.name + " with args " + args)
                return functions[data.function.name](args, session)
              })
              .then(result => {
                if(data.function.hasOwnProperty("next")) {
                  if(data.function.next) {
                    return resolve(data.function.next)
                  }
                }
                return resolve(undefined)
              })
              .catch(error => {
                return reject(error)
              })
            } else {
              //log.error(this.name, data.function.name + " is not a valid function")
              return reject(data.function.name + " is not a valid function")
            }
            break
        }
      }
    })
  }
  
  /**
   * emit game or session event.
   * 
   * @todo enable event load whenever editor live changes are arranged more properly 
   * and custom events are added to event listener schema
   *
   * @param {string} event
   * @param {*} load
   * @param {EventEmitter} eventemitter
   */
  dispatchEvent(event, load, eventemitter) {
    if(event) {
      log.debug(this.name, "dispatch event: " + event)
      eventemitter.emit(event, load)
    }
  }

  /**
   * walk through all plugin schemas and collect event schemas to merge them in
   * one object.
   * 
   * Truncate event schema properties that are not of type object.
   *
   * append events to `event` property and show event values/load dependend on `event.enum` by using json-schema `dependencies` property
   * 
   * schema uses describedBy link property to modify value enum based on chosen event: https://json-editor.github.io/json-editor/describedby.html
   *
   * @param {array} plugins - all plugins as loaded from database
   * @param {array} [plugins.schema.events] - merge is based on events property in schema
   */
  mergeEvents(plugins) {
    let event_enum = {}
    let event_if = Object.deepClone(this.schema.definitions.if_options)

    event_if.required = false
    event_if["options"] = {
      "dependencies": {
        //"event": [] // uncomment  whenever editor live changes are arranged more properly
        "check_load":true
      }
    }
    this.schema.cue.on.items.properties.next["options"] = {
      "dependencies": {
        //"event": [] // uncomment  whenever editor live changes are arranged more properly
        "check_load":false
      }
    }

    for(let condition of event_if.items.oneOf) {
      condition.properties.value = {
        "type": "string",
        "title": "value",
        "required":true,
        "propertyOrder": 1,
        "watch": {
            "tag": "event_items.event"
        },
        "links": [{
            "rel": "describedBy",
            "href": "#/definitions/{{tag}}"
        }]
      }
    }
    for(let plugin of plugins) {
      if(plugin.hasOwnProperty("schema")) {
        if(plugin.schema.hasOwnProperty("events")) {
          for(let event in plugin.schema.events) {
            event_enum[event] = [...this.schemaToPath(plugin.schema.events[event])]

            if(!this.schema.cue.on.items.properties.event.anyOf[0].enum.includes(event)) {
              this.schema.cue.on.items.properties.event.anyOf[0].enum.push(event)
            }

            if(plugin.schema.events[event].hasOwnProperty("properties")) {
              // event_if.options.dependencies.event.push(event) // uncomment  whenever editor live changes are arranged more properly
              this.schema.definitions[event] = {
                "type": "string",
                "enum":event_enum[event]
              }
            } else if(plugin.schema.events[event].hasOwnProperty("type")) {
              // event_if.options.dependencies.event.push(event) // uncomment  whenever editor live changes are arranged more properly
              this.schema.definitions[event] = {
                "type": "string",
                "enum":[event]
              }
            } else {
              // this.schema.cue.on.items.properties.next.options.dependencies.event.push(event) // uncomment  whenever editor live changes are arranged more properly
            }
          }
        }
      }
    }

    this.schema.cue.on.items.properties.if = event_if
    this.schemaUpdate(this.schema)
  }

  /**
   * generator function that collects all propertynames the schema defines in sorted strings.
   * nested properties are appended to each other using dot notation.
   * e.g.: "first.second.third"
   *
   * @param {Object} obj- 
   * @param {string} [previous=""]
   * 
   * @returns {Array} - list of property path strings
   */
  * schemaToPath(obj, previous = "") {
    for (const [key, value] of Object.entries(obj)) {
      if (value.hasOwnProperty("properties")) {
        if (previous) yield* this.schemaToPath(value, previous + "." + key)
        else yield* this.schemaToPath(value, key)
      } else if (key == "properties") {
        yield* this.schemaToPath(value, previous);
      } else if (typeof value == "object" && !Array.isArray(value)){
        if (previous) yield previous + "." + key
        else yield key
      }
    }
   }
  
  /**
  * dispatches next based on condition. If there is more then one condition matching
  * only the promise resolved with lowest index is returned for the session to dispatch it
  *
  * @returns {Promise} string defining next cue or undefined if none of the conditions matched and no else was defined
  */
  switch(props, session) {
    return new Promise((resolve, reject) => {
      if(props.hasOwnProperty("if")) {
        let condition_promises = []
        for(let if_condition of props["if"]) {
            let condition = new Condition(if_condition, session.variables, this.db)
            condition_promises.push(condition.match())
        }
        Promise.all(condition_promises)
        .then(result => {
          for(let match of result) {
            if(match) {
              return resolve(match)
            }
          }
          if (props.hasOwnProperty("else")) {
            if(props.else.hasOwnProperty("next")) {
              return resolve(props.else.next)
            } else {
              log.warn(this.name, "else is missing a 'next' property")
            }
          } else {
            log.debug(this.name, "no match and no else was defined")
          }
          return resolve(undefined)
        })
        .catch(error => {
          return reject(error)
        })
      }
    })
  }
}

/**
* check differnt types of conditions and dispatch next on a match
*/
class Condition {
  constructor(properties, variables, db) {
    //Object.assign(this, props)
    this.variables = variables
    this.value = properties["value"]
    this.next = properties["next"]
    this.case_sensitive = properties["case_sensitive"]
    this.db = db
    
    if(properties.hasOwnProperty("equals")) {
      this.condition = properties.equals
      this.survey = this.equals
      this.name = "equals"
    } else if(properties.hasOwnProperty("lessThan")) {
      this.condition = properties.lessThan
      this.survey = this.lessThan
      this.name = "less than"
    } else if(properties.hasOwnProperty("greaterThan")) {
      this.condition = properties.greaterThan
      this.survey = this.greaterThan
      this.name = "greater than"
    } else if(properties.hasOwnProperty("contains")) {
      this.condition = properties.contains
      this.survey = this.contains
      this.name = "contains"
    } else if(properties.hasOwnProperty("regex")) {
      this.condition = properties.regex
      this.survey = this.regex
      this.name = "regex"
    } else if (properties.hasOwnProperty("find")) {
      this.condition = properties.find
      this.survey = this.query
      this.name = "query"
    }
  }
  
  /**
  * trigger next cue based on match in one of:
  * - equals
  * - contains
  * - regex
  * - query
  *
  * @param {Object} data - properties that resolve that would return match under certain conditions
  * @param {Object} variables - cue context instance of session Variables Object
  *
  * @returns {Promise} returns a string defining the next cue that condition wants to dipatch or undefined it there is no match
  */
  match() {
    return new Promise((resolve, reject) => {
      this.evaluate()
      .then(result => {
        if(result.value != undefined || this.name == "query") {
          if(Array.isArray(result.condition)) {
            if(result.condition) {
              log.trace(this.name, "compare condition '" + result.condition.join(',').substring(0, 30) + "'... with value '" + result.value + "'")
            }
          } else if (typeof result.condition === "string") {
            log.trace(this.name, "compare condition '" + result.condition.substring(0, 30) + "' with value '" + result.value + "'")
          } else {
            log.trace(this.name, "compare condition '" + JSON.stringify(result.condition) + "' with value '" + result.value + "'")
          }
          
          return this.survey(result.value, result.condition)
        } else {
          return undefined
        }
      })
      .then(result => {
        return resolve(this.feedback(result))
      })
      .catch(error => {
        log.error(this.name, error)
        return reject(error)
      })
    })
  }

  /**
   * evaluate survey result. 
   * 
   * Is seperated from `match()` because messenger `DialogueCondition` requires a more sophisticated handling.
   *
   * @param {Object} result - value returned by survey function
   * @returns {string} - next cue to dispatch or undefined
   */
  feedback(result) {
    if(result) {
      if(this.hasOwnProperty("next")) {
        this.variables.store({match:result})
        return this.next
      } else {
        log.warn(this.name, "no next property defined")
      }
    }
    return undefined
  }
  
  /**
  * review value / condition pair to replace variables if any
  *
  * @returns {Promise} condition and value property with replaced variables if any
  */
  evaluate() {
    return new Promise((resolve, reject) => {
      if(this.value) {
        this.variables.review(this.value)
        .then(result => {
          if(typeof result === "string" || typeof result === "undefined"){
            this.value = result
          } else if (typeof result.toString === "function") {
            this.value = result.toString()
          }
          
          if(this.name == "query") {
            return this.variables.findAndReplace(this.condition)
          } else {
            return this.variables.review(this.condition)
          }
        })
        .then(result => {
          this.condition = result
          return resolve({condition:this.condition, value:this.value})
        })
        .catch(error => {
          log.error(this.name, error)
        })
      } else {
        let promise
        if(this.name == "query") {
          promise = this.variables.findAndReplace(this.condition)
        } else {
          promise = this.variables.review(this.condition)
        }
        
        promise
        .then(result => {
          this.condition = result
          return resolve({condition:this.condition})
        })
        .catch(error => {
          log.error(this.name, error)
        })
      }
      
    })
  }

  caseSensitivity(val) {
    if(!this.case_sensitive) {
      return val.toUpperCase()
    }
    return val
  }
  
  /**
  * check value against condition
  * if value matches condition exactly or  
  * if condition is empty string or  
  * if one item in condition array matches value exactly, return value
  *
  * else return undefined
  *
  * @returns {Promise} the value if it matched the condition or undefined
  */
  equals(value, condition) {
    return new Promise((resolve, reject) => {
      if(typeof condition === "string") {
        if(this.caseSensitivity(value) == this.caseSensitivity(condition) || condition == "") {
          return resolve(value)
        }
      } else if(Array.isArray(condition)) {
        let upper = condition.map(cond => {return this.caseSensitivity(cond)})
        if(upper.includes(this.caseSensitivity(value))) {
          return resolve(value)
        }
      } else {
        log.warn(this.name, "can not resolve condition because condition type is neither string nor array")
      }
      return resolve(undefined)
    })
  }
  
  /**
  * check value against condition
  * if value is Number and less then condition return value
  */
  lessThan(value, condition) {
    return new Promise((resolve, reject) => {
      if(!isNaN(value)) {
        if(!isNaN(condition)) {
          if(condition == "") {
            log.warn(this.name, "empty condition string")
            return resolve(undefined)
          } else if(value == "") {
            log.warn(this.name, "empty value string")
            return resolve(undefined)
          }
          if(Number(value) < Number(condition)) {
            return resolve(value)
          }
        } else if(Array.isArray(condition)) {
          for(let cond of condition) {
            if(!isNaN(cond)) {
              if(condition == "") {
                log.warn(this.name, "contains empty condition string")
              } else if(value == "") {
                log.warn(this.name, "contains empty value string")
              } else if(Number(value) < Number(condition)) {
                return resolve(value)
              }
            } else {
              log.warn(this.name, cond + " is not a Number")
            }
          }
        } else {
          log.warn(this.name, "can not resolve because condition " + condition +" is neither Number nor Array")
          return resolve(undefined)
        }
      } else {
        log.warn(this.name, "can not resolve because value " + value + " is neither Number nor Array")
        return resolve(undefined)
      }
      return resolve(undefined)
    })
  }
  
  /**
  * check value against condition
  * if value is Number and geater then condition return value
  */
  greaterThan(value, condition) {
    return new Promise((resolve, reject) => {
      if(!isNaN(value)) {
        if(!isNaN(condition)) {
          if(condition == "") {
            log.warn(this.name, "empty condition string")
            return resolve(undefined)
          } else if(value == "") {
            log.warn(this.name, "empty value string")
            return resolve(undefined)
          }
          if(Number(value) > Number(condition)) {
            return resolve(value)
          }
        } else if(Array.isArray(condition)) {
          for(let cond of condition) {
            if(!isNaN(cond)) {
              if(condition == "") {
                log.warn(this.name, "contains empty condition string")
              } else if(value == "") {
                log.warn(this.name, "contains empty value string")
              } else if(Number(value) > Number(condition)) {
                return resolve(value)
              }
            } else {
              log.warn(this.name, cond + " is not a Number")
            }
          }
        } else {
          log.warn(this.name, "can not resolve because condition " + condition +" is neither Number nor Array")
          return resolve(undefined)
        }
      } else {
        log.warn(this.name, "can not resolve because value " + value + " is neither Number nor Array")
        return resolve(undefined)
      }
      return resolve(undefined)
    })
  }
  
  /**
  * check value against condition
  * if condition is contained in value or  
  * if condition is empty string or  
  * if one item in condition array is contained in value return condition that matched the value
  *
  * else return undefined
  *
  * @returns {Promise} condition that matched the value or undefined
  */
  contains(value, condition) {
    return new Promise((resolve, reject) => {
      if(typeof condition === "string") {
        if(this.caseSensitivity(value).indexOf(this.caseSensitivity(condition)) >= 0) {
          if(condition == '') {
            return resolve(value)
          }
          return resolve(condition)
        }
      } else if(Array.isArray(condition)) {
        for(let cond of condition) {
          if(this.caseSensitivity(value).indexOf(this.caseSensitivity(cond)) >= 0) {
            return resolve(cond)
          }
        }
      } else {
        log.warn(this.name, "can not resolve condition because condition type is neither string nor array")
      }
      return resolve(undefined)
    })
  }
  
  /**
  * check value against regex
  * if regex matches, resolve with first match
  * if condition is an array, it itreates over all conditions and resolves if/once any of them matches
  *
  * regex is case insensitive (i) and returns after first match (not g / global)
  * 
  * @returns {Promise} part of value that was matching the regex. If none did, undefined
  */
  regex(value, condition) {
    return new Promise((resolve, reject) => {
      if(typeof condition === "string") {
        let res = value.match(new RegExp(condition, "i"))
        if(res) {
          return resolve(res[0])
        }
      } else if(Array.isArray(condition)) {
        for(let cond of condition) {
          let res = value.match(new RegExp(cond, "i"))
          if(res) {
            return resolve(res[0])
          }
        }
      } else {
        log.warn(this.name, "can not resolve condition because condition type is neither string nor array")
      }
      return resolve(undefined)
    })
  }
  
  /**
  * if path and value are defined, true is returned if values are equal.
  * without path and value, true is returned if query returns any document in collection
  *
  * @param {String} condition.collection - what collecton to search in
  * @param {Object} condition.query - mongo find query.
  * @param {String} condition.path - '.' noted path to address distinct property in document returned by query
  * @param {String} value - if condition contains path, compare value with what is in document at path
  */
  query(value, condition) {
    return new Promise((resolve, reject) => {
      if(!this.db.hasOwnProperty(condition.collection)) {
        return reject("Collection " + condition.collection + " not found.")
      }
      
      if(!condition.hasOwnProperty("query")) {
        return reject("Query property missing.")
      }
      
      let find_query = condition.query
      
      if(typeof(condition.query) === "string") {
        if(condition.query[0] == '{') {
          try {
            find_query = JSON5.parse(condition.query)
          } catch(err) {
            if(err instanceof SyntaxError) {
              return reject('syntax error in query "' + condition.query + '"')
            } else {
              log.error(this.name, 'Error when parsing to json:')
              return reject(err)
            }
          }
        } else {
          return reject("Invalid query: " + condition.query)
        }
      }
      
      if(condition.hasOwnProperty("path")) {
        this.db[condition.collection].distinct(condition.path, find_query)
        .then(result => {
          if(!result) {
            return resolve(undefined)
          }
          for(let res of result) {
            log.debug(this.name, "compare " + value + " with " + res)
            if(value == res) {
              return resolve(res[0])
            }
          }
          return resolve(undefined)
        })
        .catch(error => {
          return reject(error)
        })
      } else {
        this.db[condition.collection].find(find_query)
        .then(result => {
          
          if(!result) {
            return resolve(undefined)
          }
          log.trace(this.name, "trigger if " + JSON.stringify(find_query) + " in collection " + condition.collection + " returns 1 or more Documents")
          
          if(value){
            if(result.length == value) {
              return resolve(result.length)
            } else {
              return resolve(undefined)
            }
          } else {
            if(result.length > 0) {
              return resolve(result.length)
            } else {
              return resolve(undefined)
            }
          }
        })
        .catch(error => {
          return reject(error)
        })
      }
    })
  }
}

/**
* creates listener for changes to custom databse collections.
*
* A match immediatly canceles and deactivates (active=false) the listener.
* it is not unlikely that changes to a collection are made at practically the same moment, 
* so there may be unresolved promises in the pipeline even though the event listener was canceled. 
* Thats why any output after a first match is blocked so there are no multiple next cues.
*/
class Listener {
  constructor(props, session, event, db) {
    Object.assign(this, props)
    this.active = true
    this.callback = session.callback
    this.variables = session.variables
    this.cue = session.cue
    this.db = db
    if(props.hasOwnProperty('next')) {
      if(props.next.split('.').length == 1)
      {
        props.next = session.chapter + '.' + props.next
      }
    }
    
    this.name = session._id + " listener"
    
    //this.query = session.variables.parseQuery(props.on)
    
    this.event = event
    
    this.rev = (change) => {
      this.review(change)
      .then(result => {
        if(result) {
          if(this.active) {
            this.active = false
            this.callback(result)
          } else {
            log.debug(this.name, "tried to dispatch next cue but was already canceled")
          }
        }
      })
      .catch(error => {
        log.error(this.name, error)
      })
    }
    
    this.event.on("change", this.rev)
    
    log.info(this.name, "installed.")
  }
  
  /**
  * run match functions to check the changed collections for listener conditions
  * 
  * begins with: ignore changes if change results from storing a listener match in the current cue
  * listener does not cancel quick enough to ignore this change so it would otherwise trigger twice
  *
  * @todo make a more sophisticated pre check what documents or changes to take into account
  * this might also make the above store ignore obsolete
  */
  review(change) {
    return new Promise((resolve, reject) => {
      
      if(change) {
        if(change.collection.name == "sessions") {
          if(change.hasOwnProperty("updated")) {
            if(change.updated.hasOwnProperty(this.cue)) {
              if(change.updated[this.cue].hasOwnProperty("listen")) {
                //log.info("IGNORE UPDATE", change.updated)
                return resolve(undefined)
              }
            }
          }
        }
      }
      
      if(this.hasOwnProperty("if")) {
        let condition_promises = []
        for(let if_condition of this["if"]) {
            let condition = new Condition(if_condition, this.variables, this.db)
            condition_promises.push(condition.match())
        }
        Promise.all(condition_promises)
        .then(result => {
          for(let match of result) {
            if(match) {
              return resolve(match)
            }
          }
          return resolve(undefined)
        })
        .catch(error => {
          return reject(error)
        })
      }
    })
  }
  
  /**
   * remove local db change event listener.
   * reset active flag to surpress multiple triggers of this same event.
   */
  cancel() {
    this.active = false
    this.event.removeListener("change", this.rev)
    log.debug(this.name, "closed. " + this.event.listenerCount("change") + " listeners left open")
  }
}

/**
 * Listen for game and session events and evaluate conditions regarding the event and it's data load.
 * Props will not specify if PEventListener should listen for a game or session event, so it will listen for both.
 * Is canceled on a matching event or by the session context.
 * 
 * @param {Object} properties - porperties as set by level designer
 * @param {string} properties.event - event name
 * @param {array} properties.if - list of conditions to check against (@see {@link Condition} class)
 * @param {Object} properties.else - if event was dispatched but no condition matched
 * @param {string} properties.else.next - cue to dispatch on else
 * @param {Object} session - session context instance
 * @param {Object} game - game context instance
 */
class PEventListener {
  constructor(properties, session, game) {
    this.name = session.name + " EventListener"

    if(properties.hasOwnProperty("if")) {
      for(let if_condition of properties["if"]) {
        if_condition["load"] = if_condition["value"]
      }
    }
    
    Object.assign(this, properties)
    this.game_event = game.event
    this.session_event = session.event
    this.variables = session.variables
    this.callback = session.callback
    
    if(properties.hasOwnProperty("event")) {
      this["event_name"] = properties.event
      this.rev = (args) => {
        this.review(args)
      }
      
      this.game_event.on(this.event_name, this.rev)
      this.session_event.on(this.event_name, this.rev)

      log.info(this.name, "listening on '" + this.event_name + "'")
    } else {
      log.error(session.name, "Can not register Event listener. No event name provided.")
    }
    
  }
  
  /**
   * Check for condition matches with the event arguments
   * Listener value property should specify what to look for providing '.' noted paths
   * e.g.: event value is 'path.to'  
   * then, if args is an object like {path:{to:"some value"}},
   * "some value" will be compared with the condition value and return if there is a match
   * 
   * simply forward args as value to condition if value equals the event name. 
   * That is the case if event has not specified any data paths {@see Logic.mergeEvents} 
   * 
   * dispatch next cue without condition check wherever 'if' property is missing
   * 
   * * @param {*} args - argument load provided with the event. Can be anything
   */
  review(args) {
    if(this.hasOwnProperty("if")) {
      let condition_promises = []
      for(let if_condition of this["if"]) {
        if(if_condition["load"] == this.event_name) {
          if_condition["value"] = args
        }else if(if_condition["load"]) {
          if_condition["value"] = Object.getPath(args, if_condition.load)
        } else {
          if_condition["value"] = args
        }
        log.info("LOAD", if_condition["load"])
        log.info("VALUE", if_condition["value"])
        if(typeof if_condition["value"] !== "undefined" && if_condition["value"] != "") {
          let condition = new Condition(if_condition, this.variables, this.db)
          condition_promises.push(condition.match())
        }
      }
      Promise.all(condition_promises)
      .then(result => {
        for(let match of result) {
          if(match) {
            this.callback(match)
          }
        }
      })
      .catch(error => {
        log.error(this.name, error)
      })
    } else if(this.hasOwnProperty("next")) {
      this.callback(this.next)
    } else {
      log.warn(this.name, "'next' property missing to forward event " + this.event_name)
    }
  }
  
  /**
   * remove game and session event listener
   * 
   */
  cancel() {
    this.game_event.removeListener(this.event_name, this.rev)
    this.session_event.removeListener(this.event_name, this.rev)
    log.debug(this.name, this.event_name + " closed. " + this.game_event.listenerCount(this.event_name) + " game and " + this.session_event.listenerCount(this.event_name) + " session listeners left open")
  }
}

module.exports.Plugin = Logic
module.exports.Condition = Condition
