function setup(data) {
  db = data.db
}

function test(args, refs) {
  return new Promise((resolve, reject) => {
    log.info("test function", "called test function with args " + args)
  })
}

/************************************************************
 * HOMECOMING
 */
async function pendingTickets(args, refs) {
  let ticket_codes = await refs.db.player.distinct('ticket_code', {})

  log.debug("pendingTickets", "check validated codes: " + ticket_codes)

  let schedules = await refs.db.schedule.find({})

  let return_message = ""
  for(let schedule of schedules) {
    let not_validated_codes = schedule.codes.filter(code => { 
      return !ticket_codes.includes(code);  // check if this schedule code doesn't exist in player ticket codes
    })
    let date = new Date(schedule.name).toLocaleString("de-DE")
    return_message += `${date} - ${not_validated_codes}\n`
    log.info("pendingTickets", "Ticket codes not validated: ", date, not_validated_codes)
  }
  return return_message
  
}

 async function hoursUntil(args, refs) {
  let schedule = await refs.variables.review('[[' + args + ']]')
  log.info("hours_Until", schedule)
  let today=new Date()
  let schedule_date = new Date(schedule)

  let one_hour=1000*60*60
  let hours_until = Math.floor((schedule_date.getTime()-today.getTime())/(one_hour))
  log.debug("hours_until", hours_until + " hours left until " + schedule_date.toLocaleString('de-DE'))

  return hours_until
 }

 /**
  * split audiences into 2 parts of the same size.
  * One will play marie - rafale
  * The other will play moritz - rabea
  * 
  * start prolog level for each player
  * 
  * arg 1 ist der Name des Spieltermins
  * arg 2 ist die mindestanzahl an Spielern um ein Spiel mit zwei Protagonisten zu initiieren
  * arg 3 ist die Sprache des Schedule (en/de)
  */
async function splitAudience(args, refs) {
  let player = await refs.db.player.find({schedule:args[0]})
  if(player.length >= args[1]) {
    for(let i = 0; i < player.length; i++) {
      if(i % 2 == 0) {
        log.debug("splitAudience", "Marie/Rafael: " + player[i].name)
        await refs.db.player.set({_id:player[i]._id}, {protagonist:"Marie",influencer:"Rafael",status:"prepare"})
        await refs.game.createSession({level:{'config.family':"prolog",'config.language':args[2]}, "arguments":{Player:{collection:"player", query:{_id:player[i]._id}}, Marie:{collection:'client',query:{first_name:"Marie",language:args[2]}}, Schedule:{collection:'schedule',query:{name:player[i].schedule}}}})
      } else {
        log.debug("splitAudience", "Moritz/Rabea: " + player[i].name)
        await refs.db.player.set({_id:player[i]._id}, {protagonist:"Moritz",influencer:"Rabea",status:"prepare"})
        await refs.game.createSession({level:{'config.family':"prolog",'config.language':args[2]}, "arguments":{Player:{collection:"player", query:{_id:player[i]._id}}, Marie:{collection:'client',query:{first_name:"Moritz",language:args[2]}}, Schedule:{collection:'schedule',query:{name:player[i].schedule}}}})
      }
    }
  } else {
    log.info("splitAudience", "Marie/Rafael: all player in " + args[0])
    await refs.db.player.set({schedule:args[0]}, {protagonist:"Marie",influencer:"Rafael",status:"prepare"})
    for(let i = 0; i < player.length; i++) {
      await refs.game.createSession({level:{'config.family':"prolog",'config.language':args[2]}, "arguments":{Player:{collection:"player", query:{_id:player[i]._id}}, Marie:{collection:'client',query:{first_name:"Marie",language:args[2]}}, Schedule:{collection:'schedule',query:{name:player[i].schedule}}}})
    }
  }
  return
}

async function assignGroups(args, refs) {
  let schedule = args[0]
  let protagonist = args[1]
  let language = args[2]

  let temp = await refs.db.groups.find({name:"template",protagonist:protagonist})
  let template_group = temp[0]
  
  let player = await refs.db.player.find({schedule:schedule, protagonist:protagonist, status:'ready'})

  log.info("assignGroups", player)

  if(player.length < 3) {
    log.warn("assignGroups", player.length + " not enough player with status 'ready' and " + protagonist + " as contact found, to assign group variables for " + schedule + ".")
    return
  } else {
    log.info("assignGroups", "Assign group variables for " + schedule + " for " + player.length + " Player with Protagonist " + protagonist)
  }

  player = shuffle(player)
  log.debug("assignGroups", player)

  let group_index = -1
  let p = 0
  let groups = []

  for(let i = 0; i < player.length; i++) {
    if(player.length % 3 < 2) {
      if (i % 3 == 0 && i <= player.length - 3) {
        group_index ++
        p = 0
        let new_group = await createGroup(refs, schedule, language, template_group, group_index)
        groups.push(new_group)
      }
    } else {
      if(i % 3 == 0 && i < player.length - 8) {
        group_index ++
        p = 0
        let new_group = await createGroup(refs, schedule, language, template_group, group_index)
        groups.push(new_group)
      } else if(i == player.length - 4 || i == player.length - 8) {
        group_index ++
        p = 0
        let new_group = await createGroup(refs, schedule, language, template_group, group_index)
        groups.push(new_group)
      }
    }
    await refs.db.player.set({_id:player[i]._id},{group:template_group.template.names[group_index],player:template_group.template.player[p], status:'playing'})
    p ++
  }

  for(let group of groups) {
    let argu = {}
    for(let p of template_group.template.player) {
      argu[p] = {collection:"player", query:{player:p, group:group, schedule:schedule}}
    }
    argu["Group"] = {collection:"groups", query:{"name":group, "schedule":schedule}}
    argu["Marie"] = {collection:"client", query:{"first_name":protagonist,"language":language}}
    argu["Rafael"] = {collection:"client", query:{"first_name":template_group.influencer,"language":language}}
    await refs.game.createSession({level:{'config.family':template_group.template.first_level,'config.language':language}, "arguments":argu, "name":group + "-" + schedule})
  }
  
  return
}

async function createGroup(refs, schedule, language, template_group, index) {
  let new_group = Object.assign({}, template_group)
  delete new_group._id
  delete new_group.template
  new_group["name"] = template_group.template.names[index]
  new_group.chat.name = template_group.template.chat_names[index]
  new_group.subdomain = template_group.template.subdomains[index]
  new_group["schedule"] = schedule
  new_group["language"] = language
  new_group["delay"] =  '' + ((template_group.template.delay * index) + 1)
  log.debug("createGroup", "Delete Preceeding Group " + new_group["name"])
  await refs.db.groups.c.deleteOne({name:new_group["name"]})
  await refs.db.groups.insert(new_group)
  log.debug("createGroup", "Created new Group " + new_group["name"] + " " + new_group["schedule"])
  return new_group.name
}

 async function deleteDocument(args, refs) {
   let reference = refs.variables.getReference(args)
   log.debug("deleteDocument", "Delete " + args)
   await refs.db[reference.collection].c.deleteOne(reference.query)
 }

 async function moveFile(args, ref) {
  const file = require("../../file.js")
  await file.mv(args[0], args[1])
 }

/************************************************************
 * LOCKDOWN
 */

function getTime(args, ref) {
  return new Promise((resolve, reject) => {
    let today = new Date();
    let time = today.getHours() + ":" + today.getMinutes() // + ":" + today.getSeconds()
    return resolve(time)
  })
}

function getDate(args, ref) {
  return new Promise((resolve, reject) => {
    let today = new Date()
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();
    return resolve(dd + "." + mm + "." + yyyy)
  })
}

function getVacancies(args, refs) {
  return new Promise((resolve, reject) => {
    if(!refs.db.hasOwnProperty("schedule")) {
      return resolve("Da ist was schiefgelaufen, sorry! Bitte probiere es in einigen Stunden erneut.")
    }
    let query = {}
    query['player.' + args[0]] = {'$exists':false}
    query['status'] = {'$ne':'ready'}
    
    refs.db.schedule.sorted(query, {"index":1})
    .then(result => {
      if(result.length) {
        let list = "\n\n"
        for(let res of result) {
          if(res.hasOwnProperty("index")) {
            list += res.text + "\n (" + res.name + ")\n\n"
          }
        }
        return resolve(list)
      } else {
        return resolve("Leider sind nirgends mehr Plätze verfügbar. Sorry!")
      }
    })
    .catch(error => {
      log.error("getVacancies", error)
      return resolve(undefined)
    })
  })
}

async function startTutorial(args, refs) {
  
  let player = await refs.db.player.find({schedule:args[0]})
  for(let p of player) {
    await refs.game.createSession({level:"tutorial", "arguments":{Player:{collection:"player", query:{_id:p._id}}, Chris:{collection:'client',query:{name:args[1]}}}})
  }

  return
}

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
}

/**
 * https://www.w3schools.com/code/tryit.asp?filename=GEC64SSYBJVA
 *
 * @param {*} args
 * @param {*} refs
 */
async function assignGroupsLockdown(args, refs) {
  
  let temp = await refs.db.groups.find({name:"template"})
  let template_group = temp[0]
  let schedule = args[0]
  let chris = args[1]
  let player = await refs.db.player.find({schedule:schedule, telegram_contacts:chris, status:'ready'})

  if(player.length < 3) {
    log.warn("assignGroups", player.length + " not enough player with status 'ready' and " + chris + " as contact found, to assign group variables for " + schedule + ".")
    return
  } else {
    log.info("assignGroups", "Assign group variables for " + schedule + " for " + player.length + " Player.")
  }

  player = shuffle(player)
  log.debug("assignGroups", player)

  let group_index = -1
  let p = 0
  let groups = []

  for(let i = 0; i < player.length; i++) {
    if(player.length % 3 < 2) {
      if (i % 3 == 0 && i <= player.length - 3) {
        group_index ++
        p = 0
        let new_group = await createGroup(refs, schedule, template_group, group_index)
        groups.push(new_group)
      }
    } else {
      if(i % 3 == 0 && i < player.length - 8) {
        group_index ++
        p = 0
        let new_group = await createGroup(refs, schedule, template_group, group_index)
        groups.push(new_group)
      } else if(i == player.length - 4 || i == player.length - 8) {
        group_index ++
        p = 0
        let new_group = await createGroup(refs, schedule, template_group, group_index)
        groups.push(new_group)
      }
    }
    await refs.db.player.set({_id:player[i]._id},{group:template_group.template.names[group_index],player:template_group.template.player[p], status:'playing'})
    p ++
  }

  for(let group of groups) {
    let args = {}
    for(let p of template_group.template.player) {
      args[p] = {collection:"player", query:{player:p, group:group, schedule:schedule}}
    }
    args["Group"] = {collection:"groups", query:{name:group, schedule:schedule}}
    await refs.game.createSession({level:template_group.template.first_level, "arguments":args, name:group + "-" + schedule})
  }
  
  return
}

/**
 * local function called from assignGroups
 *
 * @param {*} coll
 * @param {*} schedule
 * @param {*} template_group
 * @param {*} index
 */
async function createGroupLockdown(refs, schedule, template_group, index) {
  let new_group = Object.assign({}, template_group)
  delete new_group._id
  delete new_group.template
  new_group["name"] = template_group.template.names[index]
  new_group.chat.name = template_group.template.chat_names[index]
  new_group["schedule"] = schedule
  new_group["delay"] =  '' + ((template_group.template.delay * index) + 1)
  await refs.db.groups.insert(new_group)

  return new_group.name
}

/******************************************************
* PATROL und SIGN HERE Archiv
***************************************************/

/**
* returns the number of milliseconds between midnight of January 1, 1970 and now
*/
function timestamp(args, refs) {
  return new Promise((resolve, reject) => {
    log.info("timestamp function", "called timestamp function with args " + args)
    const now = new Date()
    const millis = now.getTime()
    resolve(millis)
  })
}

/**
* returns place object for associated story and stage 
*
* @param {string} args.0 - story name
* @param {string} args.1 - stage number
*/
function getPlace(args, refs) {
  return new Promise((resolve, reject) => {
    // convert argument from string to number if possible
    //const _stage = (args[1] != "" && Number(args[1])) ? Number(args[1]) : args[1]
    // search place associated with stage and story
    refs.db.places.find({ story: args[0], stage: String(args[1]) }, { _id: 0 })
      .then(result => {
        // throw error if no place found in mongo db
        if (!result.length) {
          reject(new Error(`getPlace function: no place for ${args[0]} ${args[1]}`));
        }
        return resolve(result[0])
      })
      .catch(error => {
        log.error("getPlace function", error)
        return resolve(undefined)
      })
  })
}

/**
* assigns intro and corresponding place to player 
* TODO: auf räume prüfen, nicht auf vergebene intors/storys
*
* @param {string} args - player
*/
function assignIntro(args, refs) {
  return new Promise((resolve, reject) => {
    let _player, _intro;
    refs.variables.get(args)
      .then(result => {
        _player = result
        // count how often each intro is assigned (exclude current player)
        return refs.db.player.c.aggregate([
          { $match: { $and: [{ story: { $in: ["intro_1", "intro_2", "intro_3"] } }] } },
          { $group: { _id: '$story', "count": { $sum: 1 } } },
          { $sort: { "count": 1 } }
        ]).toArray()
      })
      .then(result => {
        // zurück zum ursprünglichen intro, wenn man rausgeflogen ist
        if (["intro_1", "intro_2", "intro_3"].includes(_player.story)) {
          _intro = { _id: _player.story };
          return getPlace([_intro["_id"], "1"], refs)
        }

        // if all 3 intros are assigned return the least assigned
        if (result.length == 3) { _intro = result[0]; }
        // if not all intros are assigned, check wich one is not and return
        else {
          const assigned = result.map(a => a._id);
          if (!assigned.includes("intro_1")) { _intro = { _id: "intro_1" }; }
          else if (!assigned.includes("intro_2")) { _intro = { _id: "intro_2" }; }
          else if (!assigned.includes("intro_3")) { _intro = { _id: "intro_3" }; }
          else { reject(new Error(`assign intro function: found no intro`)); }
        }

        // return place for assigned intro
        return getPlace([_intro["_id"], "1"], refs)
      })
      .then(result => {
        const data = {
          story: _intro._id, stage: "1", status: "enqueued",
          place: result,
          timestamp: new Date().getTime()
        };
        return refs.db.player.set({ _id: _player._id }, data);
      })
      .then(result => {
        return resolve("done")
      })
      .catch(error => {
        log.error("assignIntro function", error)
        return resolve(undefined)
      })
  })
}


/**
* checks if player is the only one enqueued for room and if so writes waiting number 
*
* @param {string} args - player
*/
function checkQueue(args, refs) {
  return new Promise((resolve, reject) => {
    let _player;
    refs.variables.get(args)
      .then(result => {
        _player = result
        return refs.db.player.find({
          $and:
            [
              { "place.id": _player.place.id },
              { $or: [{ status: "enqueued" }, { status: "called" }] }
            ]
        })
      })
      .then(result => {
        if (result.length == 1) { // if player is the only one
          return refs.db.player.set({ _id: _player._id }, { "place.waiting_number": 0 });
        } else {
          return
        }
      })
      .then(result => {
        return resolve("done")
      })
      .catch(error => {
        log.error("assignIntro function", error)
        return resolve(undefined)
      })
  })
}

/**
* updates every waiting number for players enqueued for room
* @param {string} args - place id
*/
function updateQueue(args, refs) {
  return new Promise((resolve, reject) => {
    log.info("updateQueue function", "called with args " + args)
    return refs.db.player.c.find({
      $and:
        [
          { "place.id": args },
          { $or: [{ status: "enqueued" }, { status: "called" }] },
          { type: { $ne: "performer" } }
        ]
    }).sort({ timestamp: 1 }).toArray()
      .then(result => {
        let updates = [];
        if (result.length > 0) {
          for (let index = 0; index < result.length; index++) {
            updates.push(refs.db.player.set({ _id: result[index][["_id"]] }, { "place.waiting_number": index }));
          }
        }
        Promise.all(updates)
      })
      .then(result => {
        return resolve("done")
      })
      .catch(error => {
        log.error("assignIntro function", error)
        return resolve(undefined)
      })
  })
}

/**
* assigns story and corresponding place to player 
*
* @param {string} args - player
*/
function assignStory(args, refs) {
  return new Promise((resolve, reject) => {
    let _player, _story;
    refs.variables.get(args)
      .then(result => {
        _player = result

        // count how often each story is assigned (exclude current player)
        return refs.db.player.c.aggregate([
          { $match: { $and: [{ story: { $in: ["story_1", "story_2", "story_3"] } }, { _id: { $ne: _player._id } }] } },
          { $group: { _id: '$story', "count": { $sum: 1 } } },
          { $sort: { "count": 1 } }
        ]).toArray()
      })
      .then(result => {

        // result auffüllen mit noch nicht zugewiesenen storys
        if (result.length != 3) {
          ["story_1", "story_2", "story_3"].forEach(story => {
            if (!result.some(element => element._id === story))
              result.unshift({ _id: story, count: 0 });
          });
        }

        // log.info("ASSIGN STORY RESULTS:", result)

        let options = ["story_1", "story_2", "story_3"];

        // entferne mögliche storys, falls ein player sie schon gespielt hat
        if ('cFlags' in _player) {
          if (_player.cFlags.includes("Story_1_C"))
            options = options.filter(item => item !== "story_1")
          if (_player.cFlags.includes("Story_2_C"))
            options = options.filter(item => item !== "story_2")
          if (_player.cFlags.includes("Story_3_C"))
            options = options.filter(item => item !== "story_3")
        }

        // zur ersten story gehen die spieler zu einer anderen raum als ihr intro
        if (_player.history.length == 2) {
          if ((_player.history.includes("intro_1")))
            options = options.filter(item => item !== "story_1")
          if ((_player.history.includes("intro_2")))
            options = options.filter(item => item !== "story_2")
          if ((_player.history.includes("intro_3")))
            options = options.filter(item => item !== "story_3")
        }

        // log.info("ASSIGN STORY AFTER FLAG FILTER OPTIONS:", options)

        if (options.length == 0) {
          log.error("ASSIGN STORY", "Player hat alle Storys durchgespielt und muss vllt Task C machen")
        }

        // die options liste mit der vergebene storys liste vergleichen und die erste auswählen
        for (let index = 0; index < result.length; index++) {
          if (options.includes(result[index]._id)) {
            _story = { _id: result[index]._id }
            break;
          }
        }

        if (!_story._id) {
          log.error("ASSIGN STORY", "keine story zuweisbar, das sollte nicht passieren!!! wähle zufällige aus")
          _story = { _id: options[Math.floor(Math.random() * options.length)] }
        }

        log.info("ASSIGNED NEW STORY:", _story["_id"])

        // return place for assigned intro
        return getPlace([_story["_id"], "1"], refs)
      })
      .then(result => {
        const data = {
          story: _story._id, stage: "1", status: "enqueued",
          place: result,
          timestamp: new Date().getTime()
        };
        return refs.db.player.set({ _id: _player._id }, data);
      })
      .then(result => {
        return resolve("done")
      })
      .catch(error => {
        log.error("assignStory function", error)
        return resolve(undefined)
      })
  })
}

/**
* checks if player is the only one enqueued for room and if so writes waiting number 
*
* @param {string} args - player
*/
function getReader(args, refs) {
  return new Promise((resolve, reject) => {
    let _player;
    refs.variables.get(args[0])
      .then(result => {
        _player = result
        return refs.db.device.find({
          $and: [
            { "status.card_id": { $in: _player.card_id } },
            { name: { $in: ["Room_1_Reader", "Room_2_Reader", "Room_3_Reader", "Room_4_Reader", "Room_5_Reader", "Room_6_Reader", "Room_7_Reader"] } }
          ]
        })
      })
      .then(result => {
        //log.info("RESULT", result)
        if (!result.length) {
          log.error("getReader function", "no player found " + result)
        }
        return resolve(result[0]["name"])
      })
      .catch(error => {
        log.error("getReader function", error)
        return resolve(undefined)
      })
  })
}

/**
* BIN ICH DIE ERSTE SPIELER*IN UND IST Eine WEITERE MITSPIELER*INNEN MIT DER cFlag VORHANDEN (dann schreibe ich in ihr document, dass sie beim nächstenmal mit mir spielen)
*
*/
function assignTaskC(args, refs) {
  return new Promise((resolve, reject) => {
    let _player, _say, _place, _story;
    refs.variables.get(args[0])
      .then(result => {
        _player = result;
        _story = _player.cFlags[_player.cFlags.length - 1]; // TODO: eigentlich alle möglichen kominationen

        return refs.db.player.find({
          cFlags: _story,
          _id: { $ne: _player._id },
          history: { $ne: _story },
          c_task: { $exists: false }
        })
      })
      .then(result => {
        // log.info("assignTaskC", result)
        if (result.length == 0) {
          return resolve(undefined)
        }
        else {
          if (_story == 'Story_1_C') {
            _say = `${_player.lastname}, ${_player.firstname} und ${result[0].firstname}, ${result[0].lastname} bitte in Raum 5, ${_player.lastname}, ${_player.firstname} und ${result[0].firstname}, ${result[0].lastname}  bitte in Raum 5, Valerie Mohnhaupt bitte begeben sie sich in Raum 5, Valerie Mohnhaupt bitte in Raum 5 zur Gruppenaufgabe`
            _place = { id: 'Room_5' }
          }
          else if (_story == 'Story_2_C') {
            _say = `${_player.lastname}, ${_player.firstname} und ${result[0].firstname}, ${result[0].lastname}  bitte in Raum 2, ${_player.lastname}, ${_player.firstname} und ${result[0].firstname}, ${result[0].lastname}  bitte in Raum 2, Bea Gerber bitte begeben sie sich in Raum 2, Bea Gerber bitte in Raum 2 zur Gruppenaufgabe`
            _place = { id: 'Room_2' }
          }
          else if (_story == 'Story_3_C') {
            _say = `${_player.lastname}, ${_player.firstname} und ${result[0].firstname}, ${result[0].lastname}  bitte in Raum 7, ${_player.lastname}, ${_player.firstname} und ${result[0].firstname}, ${result[0].lastname}  bitte in Raum 7, Leon Walther bitte begeben sie sich in Raum 7, Leon Walther bitte in Raum 7 zur Gruppenaufgabe`
            _place = { id: 'Room_7' }
          }
          return refs.db.player.set({ '_id': result[0]._id }, { 'c_task': { assigned: 'player_2', story: _story, say: _say, place: _place } })
        }
      })
      .then(result => {
        // log.info("assignTaskC", result)
        if (!result) {
          return resolve(undefined)
        }
        else {
          return refs.db.player.set({ '_id': _player._id }, { 'c_task': { assigned: 'player_1', story: _story, place: _place } })
        }
      })
      .then(result => {
        if (!result) {
          return resolve('no')
        }
        else {
          return resolve('player_1')
        }
      })
      .catch(error => {
        log.error("assignTaskC function", error)
        return resolve(undefined)
      })
  })
}


/**
* returns database entry in places document based on a player property (e.g. area)
*/
function getLocationText(args, refs) {
  return new Promise((resolve, reject) => {
    let name = "getLocationText"
    let player = {}
    refs.variables.get(args[0])
      .then(result => {
        player = result
        return refs.db.data.find({ name: "places" })
      })
      .then(result => {
        if (result.length) {
          if (result[0].hasOwnProperty(args[1])) {
            location = result[0][args[1]]
          } else {
            log.error(name, args[1] + " not found in places document")
          }
        } else {
          log.error(name, "places document not found")
          return resolve(undefined)
        }
        if (player) {
          if (player.hasOwnProperty(args[2])) {
            return resolve(location[player[args[2]]])
          } else {
            log.error(name, player._id + " has no property " + args[2])
            return resolve(undefined)
          }
        } else {
          log.error(name, "reference not found: " + args[0])
          return resolve(undefined)
        }
      })
      .catch(error => {
        log.error(this.name, error)
        return resolve(undefined)
      })
  })
}

/**
*
* create a link to the login for the player that fills the login textfields with his/her telnumber and key
*/
function getLoginLink(args, refs) {
  return new Promise((resolve, reject) => {
    refs.variables.get(args[0])
      .then(player => {
        if (player) {
          let telnumber = player.telnumber.replace("+", "%2B")
          return resolve("https://patrol-security.de/angestellte/account/" + player.ticket_code + telnumber + "/")
        } else {
          return resolve(undefined)
        }
      })
      .catch(error => {
        log.error(this.name, error)
        return resolve(undefined)
      })
  })
}

async function recountInstances(args, refs) {
  let sessions = await refs.db.sessions.find('level',{})
  let session_level = []
  for(session of sessions) {
    session_level.push(session.level)
  }
  log.debug('recountInstances', session_level)
  var level_instances = {}
  for(var i=0;i<session_level.length;i++) {
      if(!level_instances.hasOwnProperty(session_level[i])) {
        level_instances[session_level[i]] = 0
      }
      level_instances[session_level[i]] += 1
  }
  log.debug('recountInstances', "set level instances to " + JSON.stringify(level_instances))

  await refs.db.level.set({},{'instances':0})
  for(let level in level_instances) {
    await refs.db.level.set({name:level},{'instances':level_instances[level]})
  }
}

/**
* cancel sessions based on player reference
*
* @param {string} args - player reference name
*/
function cancelPlayerSessions(args, refs) {
  return new Promise((resolve, reject) => {
    let name = "cancelPlayerSessions"

    let reference = refs.variables.getReference(args)

    if (!reference) {
      return reject(name + " " + args + " is not a valid reference")
    }

    let query = reference.query

    let collection = "player"
    let key = 'sessions._id'
    let value = query.sessions._id

    //log.info("REFERENCE", reference)

    let match = {}
    //match['player.sessions._id'] = query.sessions._id
    match[collection + '.' + key] = value

    // log.info("MATCH", match)

    if (!refs.db.hasOwnProperty(collection)) {
      //log.error(name, collection + " is not an existing collection")
      return reject(name + ": " + collection + " is not an existing collection")
    }

    refs.db.sessions.c.aggregate([{
      $lookup:
      {
        from: collection,
        localField: "_id",
        foreignField: "sessions._id",
        as: collection
      }
    },
    {
      $match: match
    },
    { $project: { _id: 1 } }
    ]).toArray()
      .then(result => {
        if (!result.length) {
          log.warn(name, key + ":" + value + " did not return any results in " + collection)
          return undefined
        }
        let object_ids = []
        for (let res of result) {
          if (!refs._id.equals(res._id)) {
            object_ids.push(res._id)
          }
        }
        if (object_ids.length) {
          log.info(name, "cancel sessions with references to " + JSON.stringify(match))
          log.info(name, object_ids)
          return refs.game.deleteSession({ _id: { $in: object_ids } })
        } else {
          log.info(name, "no session to delete.")
        }
        return undefined
      })
      .then(result => {
        return resolve(undefined)
      })
      .catch(error => {
        return reject(error)
      })
  })
}

/**
* cancel sessions based on references
*
* @param {string} args.0 - name of collection to find references in
* @param {string} args.1 - key part of query that finds documents in that collection
* @param {string} args.2 - value part of query that finds documents in that collection
*/
function cancelSessions(args, refs) {
  return new Promise((resolve, reject) => {
    let name = "cancelSessions"
    let collection = args[0]
    let key = args[1]
    let value = args[2]

    let match = {}
    match[collection + '.' + key] = value

    if (!refs.db.hasOwnProperty(collection)) {
      //log.error(name, collection + " is not an existing collection")
      return reject(name + ": " + collection + " is not an existing collection")
    }

    refs.db.sessions.c.aggregate([{
      $lookup:
      {
        from: collection,
        localField: "_id",
        foreignField: "sessions._id",
        as: collection
      }
    },
    {
      $match: match
    },
    { $project: { _id: 1 } }
    ]).toArray()
      .then(result => {
        if (!result.length) {
          log.warn(name, key + ":" + value + " did not return any results in " + collection)
          return resolve(undefined)
        }
        let object_ids = []
        for (let res of result) {
          object_ids.push(res._id)
        }
        log.info(name, "cancel sessions with references to " + JSON.stringify(match))
        log.info(name, object_ids)
        return refs.game.deleteSession({ _id: { $in: object_ids } })
      })
      .catch(error => {
        return reject(error)
      })

    /*
    refs.db[collection].find(reference_query)
    .then(result => {
      if(!result.length) {
        log.warn(name, JSON.stringify(reference_query) + " did not return any results in " + collection)
        return resolve(undefined)
      }
      
      
    })
    .catch(error => {
      log.error(this.name, error)
    })
    */
  })
}

/**
* takes a cuename as argument and returns the time that passed since in the format hh:mm:ss
*/
function since(args, refs) {
  return new Promise((resolve, reject) => {
    since_cue = args[0]
    refs.db.sessions.find({ _id: refs.session })
      .then(result => {
        if (result.length) {
          if (result[0].hasOwnProperty(since_cue)) {
            let t = new Date(Date.now() - new Date(result[0][since_cue].date))
            let date_since = t.toTimeString()
            date_since = date_since.split(' ')[0]
            return resolve(date_since)
          } else {
            log.error("since function", "no such cue " + since_cue)
            return resolve(undefined)
          }
        } else {
          return resolve(undefined)
        }
      })
      .catch(error => {
        log.error(this.name, error)
      })
  })
}

/**
* checks if all roles of level are assigned to players
*/
function attendance(args, refs) {
  return new Promise((resolve, reject) => {
    let assigned;
    refs.db.player.distinct('sessions', { 'sessions._id': refs.session })
      .then(result => {
        assigned = result.reduce((results, item) => {
          if (item._id.equals(refs.session)) results.push(item.reference)
          return results
        }, []);
        return refs.db.sessions.distinct('config.player.name', { '_id': refs.session })
      })
      .then(cast => {
        for (let role of cast) {
          if (!assigned.includes(role)) {
            // log.info(this.name, `${refs.session} ${role} not assigned`);
            return resolve(false);
          }
        }
        // log.info(this.name, `${refs.session} all roles ${cast} assigned`)
        return resolve(true);
      })
      .catch(error => {
        log.error(this.name, error)
        return reject(error)
      })
  })
}


/**
* takes a cuename as argument and returns the time that passed since in the format hh:mm:ss
*/
function left(args, refs) {
  return new Promise((resolve, reject) => {
    if (args.length != 2) {
      log.error("left function", "takes exactly two arguments")
      return resolve(undefined)
    }
    since_cue = args[0]
    total_time = new Date(Number(args[1]) * 1000)
    refs.db.sessions.find({ _id: refs.session })
      .then(result => {
        if (result.length) {
          if (result[0].hasOwnProperty(since_cue)) {
            let t = new Date(Date.now() - new Date(result[0][since_cue].date))
            t = new Date(total_time - t)
            //t = total_time - t
            let date_since = t.toTimeString()
            date_since = date_since.split(' ')[0]
            return resolve(date_since)
          } else {
            log.error("since function", "no such cue " + since_cue)
            return resolve(undefined)
          }
        } else {
          return resolve(undefined)
        }
      })
      .catch(error => {
        log.error(this.name, error)
      })
  })
}

/**
 * Launch a session for each player
 * 
 * @param {string[]} args - level (0) to launch, key (1) and property (2) of player to launch level for
 * @param {Object} refs - plaiframe namespace
 * @returns {Promise<int>} - number of sessions launched
 */
async function launchMany(args, refs) {
  let query = {}
  if(args[1] && args[2]) {
    query[args[1]] = args[2]
  }
  
  let player = await refs.db.player.find(query)
  log.debug("launchMany", `launch ${args[0]} for ${player.length} player. Query: ${JSON.stringify(query)}`)
  for(let p of player) {
    await refs.game.createSession({level:args[0], "arguments":{Player:{collection:"player", query:{_id:p._id}}}})
  }
  
  return player.length
}

/**
 * Set player Area to area in schedule that is least crowded
 * 
 * @param {string} args - player var name (Player)
 * @param {*} refs 
 */
async function setArea(arg, refs) {
  let player = await refs.variables.get(arg)
  let player_a1 = await refs.db.player.find({schedule:player.schedule, area:1})
  let player_a2 = await refs.db.player.find({schedule:player.schedule, area:2})
  if(player_a1.length <= player_a2.length) {
    var area = 1
  } else {
    var area = 2
  }
  await refs.db.player.set({_id:player._id}, {area:area})

  log.debug("setArea", `Player ${player.name} schedule ${player.schedule} located to area ${area}`)
}
/**
* assign values for where training starts and in which area
* assign data from assignment docuemnt in data depending on the timeslot (timecode part of registration)
*/
function startValuesDeprecated(args, refs) {
  return new Promise((resolve, reject) => {
    var reference = refs.variables.getReference(args[0])
    if (!reference) {
      log.error(name, "coldnt resolve player reference " + args[0])
      return resolve(undefined)
    }

    var player = {}
    var assignments = {}
    var ass = {}
    var timeslot = ""

    refs.db.player.find(reference.query)
      .then(result => {
        if (!result.length) {
          log.error(name, "no player found by query: " + reference.query)
          return resolve(undefined)
        }
        player = result[0]

        return refs.db.data.find({ name: "assignments" })
      })
      .then(result => {
        timeslot = player.key.substring(6, 9)

        assignments = result[0]
        ass = assignments[timeslot]

        let start_values = { 'training.location': assignments.startpoints[ass.startpoint - 1], area: ass.area, timeslot: timeslot }
        log.info("startValues function", "assigning startvalues to " + player.telnumber + " " + player.name)
        log.info("startValues function", start_values)
        return refs.db.player.set(reference.query, start_values)
      })
      .then(result => {
        let rotate = {}

        if (ass.area == 1) {
          rotate[timeslot + '.area'] = 2

          if (ass.startpoint == 1) {
            rotate[timeslot + '.startpoint'] = 3
          } else if (ass.startpoint == 2) {
            rotate[timeslot + '.startpoint'] = 4
          }
        } else {
          rotate[timeslot + '.area'] = 1

          if (ass.startpoint == 3) {
            rotate[timeslot + '.startpoint'] = 2
          } else if (ass.startpoint == 4) {
            rotate[timeslot + '.startpoint'] = 1
          }
        }

        refs.db.data.set({ name: "assignments" }, rotate)
      })
      .then(result => {
        return resolve("done")
      })
      .catch(error => {
        log.error(this.name, error)
        return resolve(undefined)
      })
  })
}

/**
* move important data about recent/current job to central property so master level can see how to proceed
*
@* @returns {Promise} amount of jobs the player finished
*/
function jobData(args, refs) {
  return new Promise((resolve, reject) => {
    let name = "nextJob function"
    reference = refs.variables.getReference(args[0])
    if (!reference) {
      log.error(name, "coldnt resolve player reference " + args[0])
      return resolve(undefined)
    }

    var player = {}
    var finished = 0

    refs.db.player.find(reference.query)
      .then(result => {
        if (!result.length) {
          log.error(name, "no player found by query: " + reference.query)
          return resolve(undefined)
        }
        player = result[0]

        let data = {}
        // Overwrite job_data property with status etc of current job
        if (player.hasOwnProperty('current_job')) {
          if (player.hasOwnProperty(player.current_job)) {
            data = { job_data: player[player.current_job] }
          } else {
            log.error("jodData function", "no status values for current job in " + player.name + " " + player.telnumber)
            return resolve(undefined)
          }
        } else {
          log.error("jodData function", "current job property missing in " + player.name + " " + player.telnumber)
          return resolve(undefined)
        }

        // count finished jobs and write number to job data
        // Also calculate average score
        if (player.hasOwnProperty('finished_jobs')) {
          finished = player.finished_jobs.length

          if (finished > 1) {
            let avg_score = player.score / (finished - 1)
            data['score_average'] = Math.round(avg_score * 100) / 100
            if (player.language == "en") {
              data['score_average_formatted'] = data['score_average'].toLocaleString('en-GB')
            } else if (player.language == "de") {
              data['score_average_formatted'] = data['score_average'].toLocaleString('de-DE')
            }
          }
        }
        data.job_data['finished'] = finished

        return refs.db.player.set(reference.query, data)
      })
      .then(result => {
        if (result) {
          return resolve(finished)
        } else {
          log.error("jodData function", "something is missing to resolve job data")
          return resolve(0)
        }
      })
      .catch(error => {
        log.error(this.name, error)
      })
  })
}

/**
*
*/
function cooperation(args, refs) {
  return new Promise((resolve, reject) => {
    let name = "coopJob function"
    var reference = refs.variables.getReference(args[0])
    if (!reference) {
      log.error(name, "couldn't resolve player reference " + args[0])
      return resolve(undefined)
    }

    var player = {}
    var partner = {}
    var coop_job = ""

    return refs.db.player.find(reference.query)
      .then(result => {
        if (!result.length) {
          log.error(name, "no player found by query: " + JSON.stringify(reference.query))
          return undefined
        }

        player = result[0]

        let query = {
          name: { $not: { $in: player.finished_jobs } }, // player has not played level (obligatory)
          family: { $not: { $in: player.finished_jobs } }, // player has not played any level of the family (obligatory)
          'config.area': player.area, // level is in same area the player is in (obligatory)
          'config.type': 'cooperative', // obligatory
          'config.language': player.language, // obligatory
          //'config.group': { $ne: player.progress }, // obligatory
          // 'config.stage':player.stage, // level is of same stage the player is in
          $expr: { $lt: ["$instances", "$config.maxSessions"] } // find only levels where max number of players is not reached. Uses aggregate expression: https://docs.mongodb.com/manual/reference/operator/query/expr/
        }

        return refs.db.level.find(query)
      })
      .then(result => {
        if (!result) {
          return undefined
        }
        if (!result.length) {
          return undefined
        }
        coop_job = result[0].name
        log.debug(name, "Found free cooperation job: " + coop_job)
        return refs.db.player.find({ 'cooperation.status': 'available', area: player.area, language: player.language, _id: { $ne: player._id } })
      })
      .then(result => {
        if (!result) {
          return resolve(undefined)
        }
        if (!result.length) {
          return resolve(undefined)
        }
        log.debug(name, "Found partner " + result[0].name + " to play cooperation job " + coop_job + " with " + player.name)
        return resolve(arrangeCooperation(player, result[0], coop_job, args, refs))
      })
      .catch(error => {
        log.error(this.name, error)
        return resolve("error")
      })
  })
}

function arrangeCooperation(player, partner, coop_job, args, refs) {
  return new Promise((resolve, reject) => {
    refs.game_event.emit('create', {
      level: coop_job, player: { _id: player._id }, role: args[1], callback: session_id => {
        refs.db.player.set({ _id: partner._id }, { cooperation: { status: 'enqueued', session: session_id } })
          .then(result => {
            return refs.db.player.set({ _id: player._id }, { cooperation: { status: 'running', session: session_id, level: coop_job } })
          })
          .then(result => {
            return resolve(undefined)
          })
          .catch(error => {
            return reject(error)
          })
      }
    })
  })
}

/**
* go through rules defining what job the player is ought to play next based on  current parameters.
*
* @todo abfragen z.b. nach area ob im 2. oder 3. job rick miller
*
* @returns {Promise} name of job the player should be assigned to next
*/
function nextJob(args, refs) {
  return new Promise((resolve, reject) => {
    let name = "nextJob function"
    reference = refs.variables.getReference(args[0])
    if (!reference) {
      log.error(name, "coldnt resolve player reference " + args[0])
      return resolve(undefined)
    }

    var player = {}

    refs.db.player.find(reference.query)
      .then(result => {
        if (!result.length) {
          log.error(name, "no player found by query: " + reference.query)
          return resolve(undefined)
        }
        player = result[0]

        if (player.finished_jobs.length == 2) {
          if (player.area == 1) {
            if (player.language == "en") {
              return resolve("wtf_A1_EN")
            }
            return resolve("wtf_A1")
          } else {
            if (player.language == "en") {
              return resolve("wtf_A2_EN")
            }
            return resolve("wtf_A2")
          }
        } else if (player.finished_jobs.length == 3) {
          if (player.area == 1) {
            if (player.language == "en") {
              return resolve("carwatch_A1_EN")
            }
            return resolve("carwatch_A1")
          } else {
            if (player.language == "en") {
              return resolve("carwatch_A2_EN")
            }
            return resolve("carwatch_A2")
          }
        } else if (player.finished_jobs.length == 5) {
          if (player.language == "en") {
            return resolve("stabbing_EN")
          }
          return resolve("stabbing")
        }


        let query = {
          name: { $not: { $in: player.finished_jobs } }, // player has not played level (obligatory)
          'config.family': { $not: { $in: player.finished_jobs } }, // player has not played any level of the family (obligatory)
          'config.area': player.area, // level is in same area the player is in (obligatory)
          'config.type': 'job', // obligatory
          'config.language': player.language, // obligatory
          //'config.group': { $ne: player.progress }, // obligatory
          // 'config.stage':player.stage, // level is of same stage the player is in
          $expr: { $lt: ["$instances", "$config.maxSessions"] } // find only levels where max number of players is not reached. Uses aggregate expression: https://docs.mongodb.com/manual/reference/operator/query/expr/
        }
        return refs.db.level.c.find(query).sort({ instances: 1, 'config.priority': -1 }).toArray() // sort so level with least players is lowest index
      })
      .then(result => {
        if (!result) {
          return resolve(result)
        }
        if (typeof result === "string") {
          return resolve(result)
        }
        //log.debug("FIRST TRY", result)
        if (result.length) {
          return result[0].name // resolve with level played by the least players
        }

        // try another query allowing lower stage levels and ignoring if the maximum count of players is exceeded. This should always return a level!
        let query = {
          name: { $not: { $in: player.finished_jobs } }, // player has not played level (obligatory)
          'config.family': { $not: { $in: player.finished_jobs } }, // player has not played any level of the family (obligatory)
          //'config.group': { $ne: player.progress }, // obligatory
          'config.type': 'job', // obligatory
          'config.language': player.language, // obligatory
          'config.area': player.area, // level is in same area the player is in (obligatory)
          'config.overload': { $ne: false }
          // 'config.stage':{$lte:player.stage} // allow level with same stage or lower
        }
        return refs.db.level.c.find(query).sort({ instances: 1, 'config.priority': -1 }).toArray() // sort so level with least players is lowest index
      })
      .then(result => {
        if (!result) {
          return resolve(result)
        }
        if (typeof result === "string") {
          return resolve(result)
        }

        if (result.length) {
          log.warn(name, "No vacant level left to assign. Assigning occupied level " + result[0].name)
          return resolve(result[0].name)
        }
        log.error(name, "did not find a suitable level. This should not happen!")
        return resolve("error")
      })
      .catch(error => {
        log.error(this.name, error)
        return resolve("error")
      })
  })
}

module.exports = {
  test: test,
  pendingTickets:pendingTickets,
  hoursUntil:hoursUntil,
  splitAudience:splitAudience,
  moveFile:moveFile,
  deleteDocument:deleteDocument,
  getTime: getTime,
  getDate: getDate,
  startTutorial: startTutorial,
  getVacancies: getVacancies,
  assignGroups: assignGroups,
  timestamp: timestamp,
  getPlace: getPlace,
  assignIntro: assignIntro,
  checkQueue: checkQueue,
  updateQueue: updateQueue,
  assignStory: assignStory,
  getReader: getReader,
  assignTaskC: assignTaskC,
  since: since,
  left: left,
  jobData: jobData,
  launchMany: launchMany,
  setArea: setArea,
  nextJob: nextJob,
  cooperation: cooperation,
  attendance: attendance,
  getLoginLink: getLoginLink,
  getLocationText: getLocationText,
  cancelSessions: cancelSessions,
  cancelPlayerSessions: cancelPlayerSessions,
  recountInstances:recountInstances
}
