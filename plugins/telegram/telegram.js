/**
* Allows sending messages from a telegram bot via requests
*
* Interfaces with python script {@link https://gitlab.com/plaitools/plaiframe/-/blob/master/plugins/telegram/telegram.py telegram.py} that enables access to the python module [Telethon]{@link https://docs.telethon.dev/en/latest/index.html}
* python is run using [Child Process]{@link https://nodejs.org/api/child_process.html} and communicates using http requests
*
* @requires plugin
* @requires client
* @requires logic
*
* @requires file
*
* @module telegram/telegram
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

const file = require('../../file.js')
const logic = require('../logic/logic.js')
const plugin = require('../plugin.js')
const { DesignError } = require('../../errors')
var schema = require('./schema.json')

const { spawn } = require('child_process')

const client = require('./client.js')

/**
 * interface to telegram API using telethon python library through child process
 *
 * @class Telegram
 * @extends {plugin.Plugin}
 */
class Telegram extends plugin.Plugin {
  constructor() {
    super(schema)
  }

  /**
   * if not exitst, create chats collection
   * adjust client and bot add functions
   * run init
   *
   * @param {Object} config
   * @returns {Promise} - resolves with current schema when ready
   */
  setup(config) {
    return new Promise((resolve, reject) => {
      super.setup(config)
        .then(result => {
          return this.game.addCollection("chats")
        })
        .then(result => {
          this.bot.add = data => {
            this.bot.items[data.name] = new client.Bot(data, this.app, this.db.bot.document({ _id: this.db.bot.makeID(data._id) }))
          }

          file.mkdir('./plugins/telegram/sessions')

          this.client.add = data => {
            var url = 'http://localhost:' + this.settings.port + '/' + this.game.name
            data["route"] = '/' + this.game.name + '/telegram'
            this.client.items[data.name] = new client.Client(data, url, this.app, this.db.client, {
              codeRequest:this.codeRequest.bind(this), initialContact:this.initialContact.bind(this)
            })
            this.schema.definitions.telegram_client.enum.push(data.name)
            this.schemaUpdate(this.schema)
          }
          return this.init(config.settings)
        })
        .then(result => {
          return resolve(this.schema)
        })
        .catch(error => {
          // log.error(this.name, error)
          return reject(error)
        })
    })
  }

  /**
   * start python child process and log stdin.
   * reload telegram clients.
   * 
   * @param {Object} settings 
   * @param {number} settings.api_id - telegram API ID
   * @param {string} settings.api_hash - telegram API hash
   * @param {number} [settings.port=5000] - python server port for connection between node and python
   */
  init(settings) {
    return new Promise((resolve, reject) => {
      if (!settings.port) {
        settings.port = 5000
      }
      this.settings = settings
      this.startTelethon(settings)
      .then(result => {
        return this.bot.collectItems()
      })
      .then(result => {
        return resolve(this.client.collectItems())
      })
      .catch(err => {
        return reject(err)
      })
    })
  }

  /**
   * quit and rerun init if settings changed
   * 
   * @param {Object} settings - for setting properties see @see Telegram.init
   * 
   */
  update(settings) {
    if(settings.api_hash != this.settings.api_hash || settings.api_id != this.settings.api_id || settings.port != this.settings.port) {
      log.info(this.name, "reconnecting telethon")
      this.quit()
      .then(result => {
        return this.init(settings)
      })
      .catch(err => {
        log.error(this.name, err)
      })
    }
  }

  codeRequest(req, res) {
    this.game.alert({
      title:"Telegram Login",
      text:"You should have received a login code for your telegram client. Enter the Code and confirm to authenticate plaiframe with your telegram client.",
      content:"input"
    }, response => {
      res.send({code:response})
    })
  }

  /**
   * spawn python script `telegram.py` as child process.
   * 
   * `telegram.py` will host a local server that allows to:
   * - create clients
   * - send messages through clients
   * 
   * each client sends requests back to the plait server on incoming messages etc.
   *
   * @param {Object} settings - api client id and hash to connect to
   * @returns {Promise} - resolves when python script printed to stdout that it's running
   */
  startTelethon(settings) {
    return new Promise((resolve, reject) => {
      if (!Object.hasOwnProperties(settings, ["api_id", "api_hash", "port"])) {
        log.error(this.name, "Settings missing to connect to telethon client API.")
        return resolve()
        //return reject("Settings missing to connect to telethon client API.")
      }
      let properties = Object.assign(
        {
          remote_port: this.port,
          remote_host: "localhost",
          game: this.game.name,
          server_port: settings.port,
          server_host: "localhost"
        }, settings
      )

      this.telethon = spawn("python3", ["./plugins/telegram/telegram.py", JSON.stringify(properties)])
      
      let running = false
      this.telethon.stdout.on('data', (data) => {
        if (data.includes("app is running")) {
          running = true
          return resolve()
        }
        log.trace("telethon", data.toString())
      })

      this.telethon.stderr.on('data', (data) => {
        log.error("telethon", data.toString())
        if(!running) {
          //return reject(data.toString()) // would be nice but the general approach for plugin setup errors has to be rethought first
        }
      })
  
      this.telethon.on('close', (code) => {
        log.info("telethon", `child process exited with code ${code}`)
      })
    })
  }

  /**
   * @deprecated
   * log messages and errors and close message coming from `telegram.py` stdout
   *
   */
  monitorTelethon() {
    //this.telethon.stdout.on('data', (data) => {
    //  log.debug("telethon", data.toString())
    //})

    this.telethon.stderr.on('data', (data) => {
      log.error("telethon", data.toString());
    })

    this.telethon.on('close', (code) => {
      log.info("telethon", `child process exited with code ${code}`);
    })
  }

  /**
   * stop telethon python child process
   */
  quit() {
    return new Promise((resolve, reject) => {
      if (this.hasOwnProperty("telethon")) {
        this.telethon.on('close', (code) => {
          //log.info("telethon", `child process exited with code ${code}`);
          return resolve()
        })
        this.telethon.kill('SIGINT')
      } else {
        return resolve()
      }
    })
  }

  cue(data, session) {
    return new Promise((resolve, reject) => {
      for (let item in data) {
        data[item]["clients"] = this.client.items
        data[item]["variables"] = session.variables
        data[item]["db"] = this.db
        data[item]["callback"] = session.callback
        data[item]["createReference"] = session.createReference
        data[item]["game"] = this.game
        switch (item) {
          case "send_message":
            let msg = new Message(data[item])
            msg.sendMessage(data[item])
              .then(result => {
                return resolve(result)
              })
              .catch(error => {
                return reject(error)
              })
            break
          case "send_file":
            let file = new Message(data[item])
            file.sendFile(data[item])
              .then(result => {
                return resolve(result)
              })
              .catch(error => {
                return reject(error)
              })
            break
          case "send_geo":
            let geo = new Message(data[item])
            geo.sendGeo(data[item])
              .then(result => {
                return resolve(result)
              })
              .catch(error => {
                return reject(error)
              })
            break
          case "dialogue":
            let dialogue = new Dialogue(data[item])
            dialogue.setup()
            .then(result => {
              return resolve(dialogue.cancel.bind(dialogue))
            })
            .catch(err => {
              return reject(err)
            })
            break
          case "create_chat":
            let c_chat = new Chat(data[item])
            c_chat.setup()
            .then(res => {
              return c_chat.create()
            })
            .then(result => {
              return resolve(result)
            })
            .catch(err => {
              return reject(err)
            })
            break
          case "edit_chat":
            let e_chat = new Chat(data[item])
            e_chat.setup()
            .then(res => {
              return e_chat.edit()
            })
            .then(result => {
              return resolve(result)
            })
            .catch(err => {
              return reject(err)
            })
            break
        }
      }
      for (let item in data.telegram) {
        switch (item) {
          case "send":
            this.send(data.telegram.send, session)
              .then(result => {
                return resolve(undefined)
              })
              .catch(error => {
                return reject(error)
              })
            break
        }
      }
    })
  }

  command(input) {
    switch(input[0]) {
      default:
        super.command(input)
        break
    }
  }

  async userExists(peer) {
    let player_list = await this.db.player.find({'telegram.id':peer})
    if(player_list.length) {
      return true
    } else {
      return false
    }
  }

  /**
	* callback executed by client if there is a contact by a user that has no routing registered.
  * adds user with telegram property to player collection if it does not exist.
  *
  * If player with given telegram id (user_id) does not exist create a new player with following specs:
  * 
  * `{telegram:{_id:<user_id>}, telegram_contacts:[]}`
  * 
  * telegram_contacts property will be updated whenever a client interacts with this player.
  * 
  * If player exists and is already part of a session based on the same level, ignore contact.
  * 
  * Otherwise start default level with existing or new player.
  * 
  * If player language is available in default levels, use the language specific version of the level.
  * 
  * 
  * @return {Promise} - Object with `level` name and `player` _id if a default level was started. Otherwise `{ignore:true}`
  * 
  */
  async initialContact(user_id, default_level, client) {
    let level = ""
    let player = {}
    if(default_level.hasOwnProperty("de")) {
      level = default_level["de"]
    } else {
      log.warn(this.name, "when opening default level, no level in 'de' language was found")
    }
    let player_list = await this.db.player.find({'telegram.id':user_id})
    if(player_list.length) {
      player = player_list[0]

      if(player.hasOwnProperty("language")) {
        if(default_level.hasOwnProperty(player.language)) {
          level = default_level[player.language]
        }
      }

      if(player.hasOwnProperty("sessions")) {
        let player_sessions = player.sessions.map(session => {return {_id:session._id}})
        if(player_sessions.length) {
          let sessions = await this.db.sessions.find({$or:player_sessions,level:level})
          if(sessions.length) {
            return({level:level, player:player._id, ignore:true})
          }
        }
      }
      
    } else {
      let result = await this.db.player.insert({'telegram':{id:user_id},'telegram_contacts':[]})
      player._id = result.insertedId
    }

    await this.game.createSession({level:level, "arguments":[{collection:"player", query:{_id:player._id}}]})

    return({level:level, player:player._id, ignore:false})
  }


  /**
  * send text message to channel from bot
  * @todo allow sending to multiple channels when variables.get returns more than one item
  */
  send(properties, session) {
    return new Promise((resolve, reject) => {
      var agent = {}
      var channel = undefined
      if (properties.hasOwnProperty("bot")) {
        if (this.bot.items.hasOwnProperty(properties.bot)) {
          agent = this.bot.items[properties.bot]
        } else {
          throw new DesignError("Not an existing bot: " + properties.bot)
        }
      } else {
        throw new DesignError("No valid bot property found")
      }

      if (properties.hasOwnProperty("to")) {
        session.variables.get(properties.to)
          .then(result => {
            if (result.hasOwnProperty("telegram")) {
              channel = agent.getChat(result.telegram)
              if (!channel) { throw new DesignError("couldn't find chat with " + JSON.stringify(result.telegram)) }
              return session.variables.review(properties.text)
            } else {
              throw new DesignError("refered document " + properties.to + " has no telegram property")
            }
          })
          .then(reviewed_text => {
            return agent.send(channel, reviewed_text)
          })
          .then(res => {
            log.info(this.name, res.result.from.first_name + ' sent: "' + res.result.text + '" to chat ' + res.result.chat.first_name)
            return resolve(undefined)
          })
          .catch(error => {
            return reject(error)
          })
      } else {
        throw new DesignError("'to' property missing")
      }
    })
  }
}

/**
 * temporary session of two members sending 1-n messages/resonses back and forth.
 * 
 * requires an agent that is an existing client and a refernece to a chat document.
 * Referencing a player is possible. The player needs a telegram.id or at least a telnumber property.
 * 
 * after resolving the references in setup:  
 * - this.chat is the chat document object.
 * - this.entity is the chat or player telegram id or telnumber
 *
 * @class Conversation
 */
class Conversation {
  constructor(properties) {
    if (properties.hasOwnProperty("chat")) {
      this.chat_reference = properties.chat
      delete properties.chat
    } else if (properties.hasOwnProperty("to")) {
      this.chat_reference = properties.to
    } else if (properties.hasOwnProperty("from")) {
      this.chat_reference = properties.from
    }

    Object.assign(this, properties)

    this.log_name = "conversation"
  }

  /**
   * identify agent and chat entity. 
   * 
   * agent is oligatory. It has to resolve an existing telegram client.
   * 
   * chat my be a specific group chat document or a player with telegram properties.
   * 
   * From chat document, `this.entity` is created. It is the telegram id found in chat document
   *
   * @returns {Promise} - return when agent and chat are resolved
   */
  async setup() {
    if (this.hasOwnProperty("agent")) {
      if (this.clients.hasOwnProperty(this.agent)) {
        this.client = this.clients[this.agent]
      } else {
        let client = await this.variables.get(this.agent)
        if(client) {
          if(this.clients.hasOwnProperty(client.name)) {
            this.client = this.clients[client.name]
          } else {
            throw new Error("agent " + this.agent + " does not resolve an existing Telegram Client.")
          }
        } else {
          throw new Error("Couldn't resolve agent reference " + this.agent)
        }
      }
    } else {
      throw new Error("conversation is missing agent property")
    }

    if(this.hasOwnProperty("chat_reference")) {
      this.chat = await this.variables.get(this.chat_reference)
      if(this.chat) {
        this.entity = await this.getId(this.chat)
        if(this.entity) {
          return
        } else {
          throw new Error("could not resolve chat reference for document " + this.chat.name + " " + this.chat._id)
        }
      } else {
        throw new Error("chat can not be resolved " + this.chat_reference)
      }
    }

    return
  }

  /**
   * get telegram chat compatible identifier.
   * 
   * if the identified document has a `telegram_contacts` property it is treated as a telegram user.
   * getId will extract as many information as possible from the user and try to add it as a contact as long as that didn't happen before.
   * 
   * Different clients will all access the same telegram property. Adding the clients name to `telegram_contacts` will make sure, 
   * that only clients that did not have contact with the user before will try to add the user to their contacts.
   *
   * @param {Object} document - player or chat document object
   * @returns identifier for telethon chat. undefined if no id could be found
   */
  async getId(document) {
    if (document.hasOwnProperty("telegram")) {
      if(document.hasOwnProperty("telegram_contacts")) {
        if(!document.telegram_contacts.includes(this.client.name)) {
          let user = {}
          if(!document.telegram.phone) {
            user = await this.client.getUser(document.telegram.id)
            await this.db.player.set({_id:document._id}, {'telegram':user})
          } else {
            user = document.telegram
          }
          if(!user.last_name) {
            user["last_name"] = ""
          }
          if(user.phone) {
            let contact = await this.client.addContact(user)
            if(contact.hasOwnProperty('id')) {
              await this.db.player.set({_id:document._id}, {'telegram':contact})
              await this.db.player.push({_id:document._id}, {'telegram_contacts':this.client.name})
            } else {
              log.error(this.name, "Failed to add telegram contact " + document._id + ". Maybe due to user privacy settings.")
              log.error(this.name, user)
            }
          } else {
            log.warn(this.name, "Could not add telegram contact " + document._id + ". Phone property missing. Maybe due to user privacy settings.")
            log.debug(this.name, user)
          }
        }
      }

      if(document.telegram.hasOwnProperty("id")) {
        return document.telegram["id"]
      } else {
        throw "Document with telegram property is missing telegram id. " + document._id
      }
    } else if (document.hasOwnProperty("telnumber") && document.hasOwnProperty("name")) {
      let data = {telnumber:document.telnumber}
      if(document.hasOwnProperty("name")) {
        data["first_name"] = document.name
      } else {
        data["first_name"] = document._id
      }
      if(document.hasOwnProperty("last_name")) {
        data["last_name"] = document.last_name
      } else {
        data["last_name"] = ""
      }
      let result = await this.client.addContact(data)
      await this.db.player.set({_id:document._id}, {'telegram':result})
      await this.db.chats.set({_id:document._id}, {'telegram':result})
      log.info(this.log_name, "add telegram contact for " + document.telnumber)
      log.info(this.log_name, result)
      return result.id
    } else {
      throw "could not resolve chat id for document " + document._id
    }
  }

  sendMessage(content) {
    return new Promise((resolve, reject) => {
      this.variables.review(content.text)
        .then(result => {
          delete content.text
          content["message"] = result
          let properties = Object.assign({
            entity: this.entity
          }, content)
          if("forward" in content) {
            return this.client.forwardMessage(properties)
          } else {
            return this.client.sendMessage(properties)
          }
        })
        .then((result) => {
          log.debug(this.client.name, "Sent message to " + this.entity + ": " + content.message)
          return resolve()
        })
        .catch((err) => {
          return reject(err)
        })
    })
  }

  async sendGeo(content) {
    let lat = await this.variables.review(content.latitude)
    let lon = await this.variables.review(content.longitude)
    await this.client.sendGeo({entity: this.entity, latitude:lat, longitude:lon})
    log.debug(this.client.name, "Send Geo Location " + lon + " " +  lat + " to " + this.entity)
  }

  sendFile(content) {
    return new Promise((resolve, reject) => {
      this.variables.review(content.file)
        .then(result => {
          content.file = this.game.getAssetsPath(result) // "public/files/" + result
          if (content.caption) {
            return this.variables.review(content.caption)
          } else {
            return
          }
        })
        .then((result) => {
          content["caption"] = result
          let properties = Object.assign({
            entity: this.entity
          }, content)
          log.debug(this.client.name, "Send File " + content.file + " to " + this.entity)
          return this.client.sendFile(properties)
        })
        .then(result => {
          return resolve()
        })
        .catch((err) => {
          return reject(err)
        })
    })
  }

  cancel() {

  }
}

/**
 * respond to incoming messages. Dialogue is between 1 client and 1 group chat or player.
 * 
 * Each incoming message is queued to avoid conflicts and race conditions.
 */
class Dialogue extends Conversation {
  constructor(properties) {
    super(properties)

    /** stacks all incoming message data during this conversation */
    this.history = []

    /** queue of unresolved incoming message responses */
    this.queue = new Queue(this.incomingMessage.bind(this))
  }

  async setup() {
    await super.setup()

    if (this.hasOwnProperty("timeout")) {
      log.debug(this.log_name, "next timeout in " + this.timeout.time[0])
      this.timer = setTimeout(this.time.bind(this), this.timeout.time[0] * 1000)
    }

    if(!this.hasOwnProperty("priority")) {
      this.priority = 1
    }

    this.routing = this.client.on('message', { id: this.entity, priority: this.priority }, properties => {
      return new Promise((resolve, reject) => {
        this.queue.put(properties, response => {
          return resolve(response)
        })
      })
    })
  }

  /**
   * compare incoming message with conditions. 
   * 
   * goto else response if there is no match.
   * 
   * cancel the "no answer" timeout responses if there is any type of response in if or else.
   *
   * @param {Object} properties - incoming message properties like text, date, from, to, media etc.
   * @returns {Promise} - resolve with reaction object signaling how to respond to incoming message
   */
  async incomingMessage(properties) {
    this.history.push(properties)

    if (this.hasOwnProperty("if")) {
      for (let if_condition of this["if"]) {
        let condition = new DialogueCondition(if_condition, properties, this.variables, this.db)
        let reaction = await condition.match()
        if(reaction) {
          await this.respond(reaction, properties)
          return true
        }
      }
    }

    if (this.hasOwnProperty("else")) {
      let reaction = DialogueCondition.getReaction(this["else"])
      await this.respond(reaction, properties)
      return true
    } else {
      let content = properties.text
      if("media" in properties) {
        content = properties.media
      }
      log.debug(this.log_name, 'no match on message "' + content + '" from ' + this.entity + " and no else defined")
      return false
    }
  }

  /**
   * Respond according to reaction object. Maybe:
   * - run "next" callback
   * - download media
   * - respond to message
   * - reply to message
   * - forward message to
   * 
   * message and response details are stored to dialogue level variable:
   * **match** store what part of the message triggered next cue
   * **download** store download path and media type
   *
   * @param {Object} reaction - object that stores how to react on incoming message
   * @param {Object} message - message details
   * @returns {Promise} - resolves undfined once all is done
   */
  async respond(reaction, message) {
    clearTimeout(this.timer)
    if("next" in reaction) {
      this.callback(reaction.next)
      message["match"] = reaction.match
    }
    if("download" in reaction) {
      let file = await this.variables.review(reaction.download.path) 
      message["download"] = await this.client.downloadMedia({message:message.id, chat:this.entity, file:file})
      // await this.createReference("chats", {_id:chat_id}, reference)
    }
    await this.variables.store(message)
    if("respond" in reaction) {
      await this.sendMessage({text:reaction.respond})
    }
    if("reply" in reaction) {
      await this.sendMessage({text:reaction.reply, reply_to:message.id})
    }
    if("forward" in reaction) {
      await this.sendMessage({text:reaction.forward, forward:message.id})
    }
  }

  /**
  * time Event dispatched after time elapsed
  * 
  * If there are more timeouts the next timer is started. Otherwise (if any) next cue is dispatched.
  */
  time() {
    let react = DialogueCondition.getReaction(this.timeout)
    if (react.respond) {
      this.sendMessage({text:react.respond})
    }
    if (react.hasOwnProperty("next")) {
      this.callback(react.next)
    }
    if (this.timeout.hasOwnProperty("respond")) {
      if (this.timeout.count + 1 < this.timeout.respond.length) {
        let timespan = this.timeout.time[(this.timeout.count + 1) % this.timeout.time.length]
        log.info(this.log_name, "Next Timeout in " + timespan)
        this.timer = setTimeout(this.time.bind(this), timespan * 1000)
      }
    } else {
      log.warn(this.log_name, "can not install further timeouts. No responses defined.")
    }
  }

  /**
   * - remove this message routing from clients routing list
   * - stop timeout if it is still running
   * - join incomingMessage queue and prevent upcoming queded items
   */
  async cancel() {
    this.client.cancel("message", this.entity, this.routing)
    if(this.hasOwnProperty("timer")) {
      clearTimeout(this.timer)
      log.debug(this.log_name, "timeout canceled")
    }
    await this.queue.cancel()
  }
}

/**
 * allows to find media in message
 * 
 * @param {Object} properties - condition properties
 * @class DialogueCondition
 * @extends {logic.Condition}
 */
class DialogueCondition extends logic.Condition {
  constructor(properties, value_properties, variables, db) {
    super(properties, variables, db)

    this.value = value_properties.text

    if (properties.hasOwnProperty("media")) {
      this.condition = properties.media
      this.survey = this.equals
      this.name = "contains media"
      this.value = value_properties.media
    }
    
    this.properties = properties
  }

  feedback(result) {
    if(result) {
      let react = DialogueCondition.getReaction(this.properties)
      react["match"] = result
      return react
    }
    return undefined
  }

  /**
  * Find out how to react to the current event.
  *
  * returns one of these options:
  * - what message text to send (as response, reply or forward)
  * - to ignore
  * - to dispatch next cue (if there is no respond or if the last respond is called based on count
  *
  * @param {Object} react - stores information about if/how to react that are altered with each call of this function
  * 
  * @returns {Object} - instructions on how to handle current response status.
  */
  static getReaction(react) {
    let return_value = {}

    if(react.hasOwnProperty("download")) {
      return_value["download"] = react.download
    }
    
    if (react.hasOwnProperty("respond")) {
      if (!react.hasOwnProperty("count")) {
        react["count"] = 0
      } else {
        react.count++
      }
      if (react.count >= react.respond.length - 1 && react.hasOwnProperty("next")) {
        //this.callback(react.next)
        if (react.next) {
          return_value["next"] = react.next
        }
      }

      if (react.count < react.respond.length) {
        if (typeof react.respond[react.count] === "string") {
          return_value["respond"] = react.respond[react.count]
        } else if (react.respond[react.count].mode) {
          let mode = react.respond[react.count].mode
          return_value[mode] = react.respond[react.count].text
        } else {
          log.error(this.log_name, "failed to create message response.")
          return ({ type: 'ignore' })
        }
        return (return_value)
      }
      log.debug(this.log_name, "No response left. Ignoring no. " + react.count)
      return ({ type: 'ignore' })
    } else {
      react["count"] = 0
      if (react.hasOwnProperty("next")) {
        //this.callback(react.next)
        return_value["next"] = react.next
      }
      return_value["type"] = "ignore"
      return (return_value)
    }
  }
}

/**
 * @todo in Chat umbenennen
 *
 * @class Chat
 * @extends {Conversation}
 */
class Chat extends Conversation {
  constructor(properties) {
    super(properties)

    this.users = []
    this.user_ids = []
  }

  /**
   * resolve user data for all members.
   * creates two lists: 
   * - `this.users` contains telegram compatible ids of all members
   * - `this.user_ids` contains mongo/nedb ids for of all members
   *
   * @param {String} reference - list of variable references
   * @returns {Promise} - resolves once this.users and this.user_ids lists are created
   */
  async getUsers(reference) {
    for(let member of reference) {
      let user_doc = await this.variables.get(member, true)
      if(user_doc) {
        this.user_ids.push(user_doc._id)
        let user = await this.getId(user_doc)
        if(!this.users.includes(user)) {
          this.users.push(user)
        }
      }
    }
    return
  }

  /**
   * create a group chat or channel via the client and when done edit its properties.
   * 
   * get members from variables. Once created the chat will be referenced by `_id` in each member and the agent that created the chat.
   * 
   * If given, create reference to chat in session it originates from.
   *
   * @param {Object} properties - members, name, etc...
   */
  create() {
    return new Promise((resolve, reject) => {
      let chat_id
      return this.getUsers(this.members)
      .then(result => {
        return this.variables.review(this.name)
      })
      .then(result => {
        this.name = result
        return this.client.createChat({
          title: this.name,
          users: this.users
        })
      })
      .then(result => {
        this.entity = result.id
        let chat = {
          name: this.name,
          title: this.name,
          users: this.users,
          admin: this.client.name,
          telegram: result
        }
        return this.db.chats.insert(chat)
      })
      .then(result => {
        chat_id = result.insertedId
        return this.db.player.push({ "_id": { "$in": this.user_ids } }, { "chats": chat_id })
      })
      .then(result => {
        this.user_ids.push(this.client._id)
        return this.db.client.push({ "_id": { "$in": this.user_ids } }, { "chats": chat_id })
      })
      .then(result => {
        if(this.hasOwnProperty("reference")) {
          return this.variables.review(this.reference)
        }
        return
      })
      .then(reference => {
        if(reference) {
          return this.createReference("chats", {_id:chat_id}, reference)
        }
        return
      })
      .then(result => {
        return this.edit()
      })
      .then(result => {
        return resolve(this.next)
      })
      .catch(err => {
        log.error(this.log_name, err)
        return resolve(undefined)
      })
    })
  }

  /**
   * edit properties of existing chat.
   * 
   * add and remove members and configure user rights
   *
   * @returns {Promise} - next cue string or undefined
   */
  async edit() {
    if (this.hasOwnProperty("add_members")) {
      await this.getUsers(this.add_members)
      await this.addUsers(this.users)
      await this.db.chats.push({ "_id": this.chat._id }, { "users": { "$each": this.users } })
      await this.db.player.push({ "_id": { "$in": this.user_ids } }, { "chats": this.chat._id })
      await this.db.client.push({ "_id": { "$in": this.user_ids } }, { "chats": this.chat._id })
      log.info(this.log_name, this.users.length + " members added to group chat " + this.chat.name)
    }
    if(this.photo) {
      this.photo = await this.variables.review(this.photo)
      this.photo = this.game.getAssetsPath(this.photo)
      await this.client.editChat("photo", {chat_id: this.entity, photo:this.photo})
    }
    if(this.about) {
      this.about = await this.variables.review(this.about)
      await this.client.editChat("about", {peer: this.entity, about:this.about})
    }
    if(this.title) {
      this.title = await this.variables.review(this.title)
      await this.client.editChat("title", {chat_id: this.entity, title:this.title})
      await this.db.chats.set({ "_id": this.chat._id }, {title:this.title})
    }
    if(this.restrictions) {
      let restrictions = Object.assign({peer: this.entity}, this.restrictions)
      await this.client.editChat("restrictions", restrictions)
    }
    if(this.admin) {
      if(this.admin.hasOwnProperty("members")) {
        await this.getUsers(this.admin.members)
        delete this.admin.members
        for(let user of this.users) {
          let admin = Object.assign({entity: this.entity, user:user}, this.admin)
          await this.client.editChat("admin", admin)
        }
      }
    }
    if(this.next) {
      return this.next
    }
    return    
  }

  /**
   * add list of new users to existing chat.
   * call addUser function successively
   * 
   * @param {Array} user - list of telegram compatible user ids
   */
  async addUsers(users) {
    for (const u of users) {
      await this.client.addUser({ chat_id: this.entity, user_id: u, fwd_limit: 42 })
    }
  }
}

/**
 * sends one message from/to conversation members.
 * 
 * @class Message
 * @extends {Conversation}
 */
class Message extends Conversation {
  constructor(properties) {
    super(properties)
  }

  /**
   * 
   *
   * @return {Promise} - resolves with next cue string or undefined. Does not forward catched errors!
   */
  sendMessage() {
    return new Promise((resolve, reject) => {
      super.setup()
        .then((result) => {
          if (Array.isArray(this.player)) {
            log.warn(this.log_name, "Dont forget to implement multiple players in conversations")
            return reject("multiple players in one conversation not allowed")
          } else {
            return super.sendMessage({ text: this.text, pin:this.pin})
          }
        })
        .then(result => {
          return resolve(this.next)
        })
        .catch((err) => {
          log.error(this.log_name, err)
          return resolve(this.next)
          // return reject(err)
        })
    })
  }

  /**
   * 
   *
   * @return {Promise} - resolves with next cue string or undefined. Does not forward catched errors!
   */
  sendGeo() {
    return new Promise((resolve, reject) => {
      super.setup()
        .then((result) => {
          if (Array.isArray(this.player)) {
            log.warn(this.log_name, "Dont forget to implement multiple players in conversations")
            return reject("multiple players in one conversation not allowed")
          } else {
            return super.sendGeo({ longitude: this.longitude, latitude:this.latitude})
          }
        })
        .then(result => {
          return resolve(this.next)
        })
        .catch((err) => {
          log.error(this.log_name, err)
          return resolve(this.next)
          // return reject(err)
        })
    })
  }

  /**
   *
   *
   * @return {Promise} - resolves with next cue string or undefined. Does not forward catched errors!
   */
  sendFile() {
    return new Promise((resolve, reject) => {
      super.setup()
        .then((result) => {
          if (Array.isArray(this.player)) {
            log.warn(this.log_name, "Dont forget to implement multiple players in conversations")
            return reject("multiple players in one conversation not allowed")
          } else {
            let content = {
              file: this.file,
              caption: this.caption
            }
            if (this.format) {
              if (this.format == "document") {
                content["force_document"] = true
              } else if (this.format == "voice") {
                content["voice_note"] = true
              } else if (this.format == "video") {
                content["video_note"] = true
              }
            }
            if(this.pin) {
              content["pin"] = this.pin
            }
            return super.sendFile(content)
          }
        })
        .then(result => {
          resolve(this.next)
        })
        .catch((err) => {
          log.error(this.log_name, err)
          return resolve(this.next)
          // return reject(err)
        })
    })
  }
}

/** connector class to plaiframe */
module.exports.Plugin = Telegram