Telegram
========

Install
--------

The Telegram plugin for plaiframe requires [python 3](https://www.python.org/downloads/) with 3 python packages. I recommend to use the python package manager pip to install them.

If it wasn't included with python 3, [install pip](https://pip.pypa.io/en/stable/installing/) 

If available use aptitude package manager

`$ sudo apt-get install -y python3-pip`

Use pip to install the async flask like python server library [Quart](https://pgjones.gitlab.io/quart/)

`$ pip3 install quart`

Install the python Telegram Client API library [Telethon](https://docs.telethon.dev/en/latest/basic/installation.html)

`$ pip3 install -U telethon --user`

Install the http [requests](https://requests.readthedocs.io/en/master/) library

`$ pip3 install requests`

Create a Client
---------------

Follow this guide on how to create a Telegram development API ID and hash: https://docs.telethon.dev/en/latest/basic/signing-in.html

Add ID and hash to the telegram plugin settings. Now you should be able to create new telegram clients.

I recommend you install each client on a classical Telegram App first. Easiest is to use the [Telegram web app](https://web.telegram.org).

Once you have a client installed, create a telegram client in plaiframe and fill out `telnumber` in settings. Once you save the client, plaiframe will ask you for a login authentification code. You should have received it via sms or in your Telegram App.

Emojis
-------

Telegram uses Apple emojis. To use emoticons when sending messages you can

look them up on this page:

https://emojipedia.org/

or use this page:

https://www.emojicopy.com/

Search the wanted emoji and copy it. Use the `copy` button to copy it, then paste it into the message field.

Same works with the Telegram Desktop or Web Client. Simply write a message with the wanted emoji and copy the message.

You should see a unicode representation or some kind of icon in the textfield. The representation you see depends on the browser you use to run the plai editor.