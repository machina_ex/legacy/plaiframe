import requests
import json
import sys
from quart import Quart, render_template_string, request, jsonify
from telethon.sync import errors
import asyncio

import client

app = Quart(__name__)
app.secret_key = 'djklasnjdIODEohuo82132(("/2j'

class TelegramServer(object):
    def __init__(self, config):
        self.clients = {}
        self.config = config

        #
        # forward request to function of client based on request address.
        #
        @app.route('/%s/<client>/<method>' % config["game"], methods = ['GET', 'POST'])
        async def route(client, method):
            data = await request.get_json()
            print('function call %s for client %s' %(client, method))
            print(data)
            result = await getattr(self.clients[client], method)(data)
            return jsonify(result)

        #
        # create a new telethon telegrram client.
        # 
        # provide request instance with address of plaiframe game.
        #
        # If client with same name exists, disconnect and overwrite.
        #
        @app.route('/%s/createClient' % config["game"], methods = ['GET', 'POST'])
        async def createClient():
            client_config = await request.get_json()

            if "telnumber" not in client_config or "name" not in client_config:
                return jsonify({'error':'can not create telegram client. Property missing'})

            client_config["api_id"] = self.config["api_id"]
            client_config["api_hash"] = self.config["api_hash"]
            print('create Telegram API Client')
            print(client_config)
            request_url = "http://%s:%d/%s/telegram/%s" % (config["remote_host"], config["remote_port"], config["game"], client_config["name"])

            plai_request = PlaiRequest(request_url)
            
            if client_config["name"] in self.clients:
                print("Client %s alread exists. Existing client will be disconnected." % client_config["name"])
                await self.clients[client_config["name"]].disconnect()
            
            self.clients[client_config["name"]] = client.Client(client_config, plai_request)
            

            cl = self.clients[client_config["name"]]

            try:
                connect = await self.clients[client_config["name"]].connect()
            except errors.rpcerrorlist.ApiIdInvalidError as e:
                return jsonify({"error":'Api ID is invalid'})

            print(connect)
            return jsonify(connect)

        #
        # disconnect and delete client
        #
        @app.route('/%s/removeClient' % config["game"], methods = ['GET', 'POST'])
        async def removeClient():
            client_config = await request.get_json()

            if "name" not in client_config:
                return jsonify({'error':'can not remove telegram client. name property missing'})

            if client_config["name"] in self.clients:
                print("Client %s will be disconnected and removed." % client_config["name"])
                await self.clients[client_config["name"]].disconnect()
                self.clients.pop(client_config["name"])
                return jsonify({'remove':'successfull'})
            else:
                return jsonify({'error':'can not remove telegram client. no such client %s' % client_config["name"]})

    def run(self, config):
        print("quart app is running")
        app.run(host=config["server_host"], port=config["server_port"])

class PlaiRequest(object):
    def __init__(self, requestaddress):
        self.address = requestaddress
        print("Make requests to %s" % self.address)

    def request(self, sub_address, data):
        headers = {'Content-type': 'application/json'}
        try:
            request_address = "%s%s" %(self.address, sub_address)
            print("request to %s" % request_address)
            print(data)
            r = requests.get(request_address, json=data, headers=headers)
            
            print(r.json())
            if r.status_code == 200:
                return(r.json())
            else:
                return(False)

        except requests.exceptions.ConnectionError as e:
            print("Network Request Error:\n  %s. \n" % e.args[0])
            return(False)

if __name__ == "__main__":
    argv = sys.argv[1:]
    
    config = {
        "remote_port":8080,
        "remote_host":"localhost",
        "game":"lockdown",
        "server_port":5000,
        "server_host":"0.0.0.0",
        "api_id":1006033,
        "api_hash":'0ee8d07f836686fb78b145da23ca10a5'
    }
    
    if len(argv) == 1:
        config = json.loads(argv[0])
    else:
        print("No args provided. Run with default settings")

    #plait = PlaitRequest("http://%s:%d/%s" % (config["remote_host"], config["remote_port"], config["game"]))
    
    telegram = TelegramServer(config)
    telegram.run(config)