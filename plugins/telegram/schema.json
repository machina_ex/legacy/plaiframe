{
  "settings": {
    "type":"object",
    "title":"Telegram API Settings",
    "properties":{
      "api_id":{
        "type":"number",
        "required":true,
        "description":"create/find your API key on my.telegram.org"
      },
      "api_hash":{
        "type":"string",
        "required":true,
        "description":"create/find your API hash on my.telegram.org"
      },
      "port":{
        "type":"number",
        "default":5000,
        "required":true,
        "description":"every game spawns it's own python process. You must choose a different port for each game tp avoid conflict."
      }
    }
  },
  "collections": {
    "bot": {
      "type": "object",
      "title": "Bot",
      "description": "connection to telegram Bot Request API",
      "properties": {
        "settings": {
          "type": "object",
          "description": "set the connection properties for this Telegram Bot",
          "required": true,
          "properties": {
            "apikey": {
              "type": "string",
              "required": true,
              "description": "find your twilio account token in the twilio user console at the Dashboard in Home. Its the same for all twilio instances that share one user"
            }
          }
        },
        "chats": {
          "type": "object",
          "options": {
            "disable_edit_json": true,
            "disable_properties": true
          },
          "additionalProperties": {
            "type": "object",
            "options": {
              "collapsed": true,
              "disable_edit_json": true,
              "disable_properties": true
            },
            "properties": {
              "id": {
                "type": "string",
                "readonly": true
              },
              "last_name": {
                "type": "string",
                "readonly": true
              },
              "first_name": {
                "type": "string",
                "readonly": true
              },
              "title": {
                "type": "string",
                "readonly": true
              },
              "type": {
                "type": "string",
                "readonly": true
              },
              "all_members_are_administrators": {
                "type": "boolean",
                "readonly": true
              }
            }
          }
        }
      }
    },
    "client": {
      "type": "object",
      "title": "Client",
      "description": "connection to telegram Client API through python library 'telethon'.",
      "properties": {
        "settings": {
          "type": "object",
          "description": "set the connection properties for this Telegram Client",
          "required": true,
          "properties": {
            "telnumber": {
              "type": "string",
              "required": true,
              "description": "The phone number the client is registered by"
            },
            "password": {
              "type":"string",
              "format":"password",
              "description":"enter if client account has password protection"
            },
            "level":{
              "title":"default level",
              "type":"object",
              "format":"table",
              "description":"Define level, that are used, once this Client is contacted and no session registered a routing for the contacting chat.",
              "properties":{
                "de":{
                  "type":"string",
                  "required":true
                },
                "en":{
                  "type":"string"
                }
              }
            },
            "test":{
              "type":"boolean",
              "description":"set true if this is supposed to be a test client"
            }
          }
        },
        "chats": {
          "type": "array",
          "format":"table",
          "options": {
            "collapsed":true,
            "disable_array_add": true,
            "disable_array_delete": true,
            "disable_array_delete_all_rows":true,
            "disable_array_delete_last_row":true,
            "disable_array_reorder":true
          },
          "items":{
            "type":"string",
            "readonly": true
          }
        }
      }

    }
  },
  "definitions":{
    "telegram_client":{
      "propertyOrder":1,
      "type":"string",
      "required":true,
      "enum":[]
    },
    "chat":{
      "anyOf":[
        {
          "type":"string",
          "title":"default",
          "enum":["Groupchat","Player","PlayerA","PlayerB","PlayerC","PlayerD"],
          "description":"role name of player, chat or channel"
        },
        {
          "type":"string",
          "title":"custom",
          "description":"find query or role name of player, chat or channel"
        }
      ]
    },
    "restrictions":{
      "type":"object",
      "description":"limit permissions for non admin group members. Setting a value to true, forbids this option. See https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.chats.ChatMethods.edit_permissions",
      "properties":{
        "view_messages":{
          "type":"boolean",
          "title":"view messages"
        },
        "send_messages":{
          "type":"boolean",
          "title":"send messages"
        },
        "send_media":{
          "type":"boolean",
          "title":"send media"
        },
        "send_stickers":{
          "type":"boolean",
          "title":"send stickers"
        },
        "send_gifs":{
          "type":"boolean",
          "title":"send gifs"
        },
        "send_games":{
          "type":"boolean",
          "title":"send games"
        },
        "send_inline":{
          "type":"boolean",
          "title":"send inline"
        },
        "send_polls":{
          "type":"boolean",
          "title":"send polls"
        },
        "change_info":{
          "type":"boolean",
          "title":"change info"
        },
        "invite_users":{
          "type":"boolean",
          "title":"invite users"
        },
        "pin_messages":{
          "type":"boolean",
          "title":"pin messages"
        }
      }
    },
    "admin":{
      "type":"object",
      "title":"set admin",
      "description":"grant or revoke admin rights for chat members. For small Group chats you can only toggle if member is admin. See https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.chats.ChatMethods.edit_admin",
      "required":["members","is_admin"],
      "properties":{
        "members":{
          "type":"array",
          "items":{
            "type":"string"
          }
        },
        "is_admin":{
          "title":"is admin",
          "type":"boolean"
        },
        "change_info":{
          "type":"boolean"
        },
        "post_messages":{
          "type":"boolean"
        },
        "edit_messages":{
          "type":"boolean"
        },
        "delete_message":{
          "type":"boolean"
        },
        "ban_users":{
          "type":"boolean"
        },
        "invite_users":{
          "type":"boolean"
        },
        "pin_messages":{
          "type":"boolean"
        },
        "add_admins":{
          "type":"boolean"
        },
        "title":{
          "type":"string"
        }
      }
    },
    "lanuages":{
      "type":"string",
      "enum":["da-DK","de-DE","en-AU","en-CA","en-GB","en-IN","en-US","ca-ES","es-ES","es-MX","fi-FI","fr-CA","fr-FR","it-IT","ja-JP","ko-KR","nb-NO","nl-NL","pl-PL","pt-BR","pt-PT","ru-RU","sv-SE","zh-CN","zh-HK","zh-TW"]
    },
    "respond":{
      "type":"array",
      "format":"table",
      "description":"Respond to the incoming message. ",
      "items":{
          "type":"object",
          "properties":{
            "mode":{
              "type":"string",
              "enum":["respond","reply","forward"]
            },
            "text":{
              "type":"string",
              "format":"textarea",
              "content":true,
              "description":"response or reply text or chat to forward to.",
              "required":false
            }
          }
      },
      "default":[""]
    }
  },
  "cue": {
    "send_message":{
      "type":"array",
      "propertyOrder":10,
      "title":"telegram message",
      "default":[{"to":"Player"}],
      "items":{
        "type":"object",
        "required":["agent","to","text"],
        "properties": {
          "agent":{
            "$ref":"#/definitions/telegram_client"
          },
          "to":{"$ref":"#/definitions/chat"},
          "text":{
            "type":"string",
            "content":true,
            "format":"textarea"
          },
          "pin":{
            "type":"boolean",
            "format":"checkbox",
            "description":"ONLY FOR GROUP CHATS. Pin the message and notify all group chat users."
          },
          "next":{
            "type":"string",
            "required":false
          }
        }
      }
    },
    "send_geo":{
      "type":"array",
      "propertyOrder":10,
      "title":"telegram geo",
      "default":[{"to":"Player"}],
      "items":{
        "type":"object",
        "required":["agent","to","longitude", "latitude"],
        "properties": {
          "agent":{
            "$ref":"#/definitions/telegram_client"
          },
          "to":{"$ref":"#/definitions/chat"},
          "latitude":{
            "type":["number","string"]
          },
          "longitude":{
            "type":["number","string"]
          },
          "next":{
            "type":"string",
            "required":false
          }
        }
      }
    },
    "send_file":{
      "type":"array",
      "title":"telegram file",
      "propertyOrder":11,
      "default":[{"to":"Player"}],
      "items":{
        "type":"object",
        "required":["agent","to","file","format"],
        "properties": {
          "agent":{
            "$ref":"#/definitions/telegram_client"
          },
          "to":{"$ref":"#/definitions/chat"},
          "file":{"$ref":"#/definitions/files"},
          "caption":{
            "type":"string",
            "content":true,
            "format":"textarea"
          },
          "format":{
            "type":"string",
            "enum":["auto","document","voice","video"]
          },
          "pin":{
            "type":"boolean",
            "format":"checkbox",
            "description":"ONLY FOR GROUP CHATS. Pin the message and notify all group chat users."
          },
          "next":{
            "type":"string",
            "required":false
          }
        }
      }
    },
    "dialogue":{
      "type":"array",
      "title":"telegram dialogue",
      "propertyOrder":12,
      "listener":true,
      "items":{
        "type":"object",
        "required":["agent","chat"],
        "title":"dialogue",
        "properties": {
          "agent":{
            "$ref":"#/definitions/telegram_client"
          },
          "chat":{"$ref":"#/definitions/chat"},
          "from":{
            "type":"string",
            "description":"Player or Client the message in the chat originates from. If chat is not a group chat or channel this will be ignored."
          },
          "priority":{
            "type":"number",
            "default":1,
            "description":"Ignore this if there is a dialogue with the same members of higher priority in a different path, prefer this otherwise. If priority is the same, latest dialogue is prefered.",
            "enum":[0, 1, 2],
            "options": {
              "enum_titles": [
                "low",
                "normal",
                "high"
              ]
            }
          },
          "if":{
            "type":"array",
            "title":"if",
            "items":{
              "title":"condition",
              "oneOf":[
                {
                  "title":"Equals",
                  "description":"If message equals the or any of the following values",
                  "type":"object",
                  "default":{"equals":""},
                  "additionalProperties": false,
                  "properties":{
                    "equals":{"$ref":"#/definitions/equals"},
                    "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                    "respond":{"$ref":"#/definitions/respond"},
                    "next":{"$ref":"#/definitions/next"}
                  }
                },
                {
                  "title":"Contains",
                  "description":"If message contains the or any of the following values",
                  "type":"object",
                  "default":{"contains":""},
                  "additionalProperties": false,
                  "properties":{
                    "contains":{"$ref":"#/definitions/contains"},
                    "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                    "respond":{"$ref":"#/definitions/respond"},
                    "next":{"$ref":"#/definitions/next"}
                  }
                },
                {
                  "title":"Conatins Media",
                  "description":"If message contains some type of media",
                  "type":"object",
                  "default":{"media":""},
                  "additionalProperties": false,
                  "properties":{
                    "media":{
                      "propertyOrder": 2,
                      "oneOf":[
                        {
                          "title":"contains",
                          "type":"string",
                          "enum":["photo","document","audio","voice","video","video_note","gif","sticker","unknown"],
                          "content":true,
                          "required":true
                        },
                        {
                          "title":"contains any of",
                          "type":"array",
                          "default":["",""],
                          "items":{"type":"string","content":true, "enum":["photo","document","audio","voice","video","video_note","gif","sticker","unknown"]}
                        }
                      ]
                      
                    },
                    "download":{
                      "type":"object",
                      "properties":{
                        "path":{
                          "type":"string",
                          "default":"telegram/",
                          "description":"set path and or filename."
                        },
                        "reference":{
                          "type":"string",
                          "default":"Download",
                          "description":"Set a reference to access download information."
                        }
                      }
                    },
                    "respond":{"$ref":"#/definitions/respond"},
                    "next":{"$ref":"#/definitions/next"}
                  }
                },
                {
                  "title":"Less Than",
                  "description":"If message is value that is less than",
                  "type":"object",
                  "default":{"lessThan":""},
                  "additionalProperties": false,
                  "properties":{
                    "lessThan":{"$ref":"#/definitions/lessThan"},
                    "respond":{"$ref":"#/definitions/respond"},
                    "next":{"$ref":"#/definitions/next"}
                  }
                },
                {
                  "title":"Greater Than",
                  "description":"If message is value that is greater than",
                  "type":"object",
                  "default":{"greaterThan":""},
                  "additionalProperties": false,
                  "properties":{
                    "greaterThan":{"$ref":"#/definitions/greaterThan"},
                    "respond":{"$ref":"#/definitions/respond"},
                    "next":{"$ref":"#/definitions/next"}
                  }
                },
                {
                  "title":"Regular Expression",
                  "description":"If regular expression returns anything when used on incoming message",
                  "type":"object",
                  "default":{"regex":""},
                  "additionalProperties": false,
                  "properties":{
                    "regex":{"$ref":"#/definitions/regex"},
                    "respond":{"$ref":"#/definitions/respond"},
                    "next":{"$ref":"#/definitions/next"}
                  }
                },
                {
                  "title":"Mongo Query",
                  "description":"If incoming message equals value in path of first query document that is returned.",
                  "type":"object",
                  "default":{"find":""},
                  "additionalProperties": false,
                  "properties":{
                    "find":{"$ref":"#/definitions/find"},
                    "respond":{"$ref":"#/definitions/respond"},
                    "next":{"$ref":"#/definitions/next"}
                  }
                }
              ]
            }
          },
          "else":{
            "type":"object",
            "properties":{
              "next":{"type":"string","descripton":"wird ausgelöst, wenn die letzte response verschickt wird."},
              "respond":{"$ref":"#/definitions/respond"},
              "loop":{"type":"number","min":0, "required":false}
            }
          },
          "timeout":{
            "type":"object",
            "properties":{
              "time":{
                "type":"array",
                "required":true,
                "minItems":1,
                "items":{
                  "type":"number",
                  "minimum":0
                }
              },
              "respond":{
                "type":"array",
                "title":"send",
                "format":"table",
                "description":"Send a message on each timeout until there is an incoming message.",
                "items":{
                  "type":"string",
                  "content":true,
                  "format":"textarea",
                  "description":"message text",
                  "required":false
                },
                "default":[""]
              },
              "next":{"type":"string","descripton":"trigger next cue on last timeout."},
              "loop":{"type":"number","min":0, "required":false}
            },
            "default":{"time":0}
          }
        }
      }
    },
    "create_chat":{
      "type":"object",
      "title":"create chat",
      "required":["name","agent","members","type"],
      "properties":{
        "type":{
          "type":"string",
          "enum":["chat","channel"]
        },
        "name":{
          "type":"string"
        },
        "agent":{
          "$ref":"#/definitions/telegram_client"
        },
        "members":{
          "type":"array",
          "description":"reference or query to agent or player",
          "items":{
            "type":"string"
          }
        },
        "photo":{"$ref":"#/definitions/files"},
        "about":{
          "type":"string",
          "description":"group description text"
        },
        "restrictions":{"$ref":"#/definitions/restrictions"},
        "admin":{"$ref":"#/definitions/admin"},
        "reference":{
          "type":"string",
          "description":"Name to reference the group chat or channel from inside this level."
        },
        "next":{
          "type":"string",
          "description":"trigger next once chat was created"
        }
      }
    },
    "edit_chat":{
      "type":"object",
      "title":"edit chat",
      "description":"change group chat settings and add or remove members. The executing Agent has to be group admin",
      "required":["chat","agent"],
      "properties":{
        "agent":{
          "$ref":"#/definitions/telegram_client"
        },
        "chat":{
          "type":"string"
        },
        "title":{
          "type":"string",
          "description":"change group chat title"
        },
        "add_members":{
          "type":"array",
          "title":"add members",
          "format":"table",
          "description":"add members by reference or query to agent or player",
          "items":{
            "type":"string"
          }
        },
        "remove_members":{
          "type":"array",
          "title":"remove members",
          "format":"table",
          "description":"remove members by reference or query to agent or player",
          "items":{
            "type":"string"
          }
        },
        "photo":{"$ref":"#/definitions/files"},
        "about":{
          "type":"string",
          "description":"group description text"
        },
        "restrictions":{"$ref":"#/definitions/restrictions"},
        "admin":{"$ref":"#/definitions/admin"},
        "next":{
          "type":"string",
          "description":"trigger next once chat was edited"
        }
      }
    }
  }
}