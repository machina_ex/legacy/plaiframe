from telethon.sync import TelegramClient, events, utils, errors
from telethon.tl.types import InputPhoneContact
from telethon.tl.types import PeerChat
from telethon.tl.types import ChatBannedRights
from telethon.tl.types import Message
from telethon.tl.types import InputGeoPoint
from telethon.tl.types import MessageMediaGeo
from telethon.tl.functions.contacts import ImportContactsRequest
from telethon.tl.functions.messages import CreateChatRequest
from telethon.tl.functions.messages import AddChatUserRequest
from telethon.tl.functions.messages import EditChatPhotoRequest
from telethon.tl.functions.messages import EditChatAboutRequest
from telethon.tl.functions.messages import EditChatTitleRequest
from telethon.tl.functions.messages import EditChatAdminRequest
from telethon.tl.functions.messages import EditChatDefaultBannedRightsRequest
import datetime

import asyncio
import sys, json, time

#
# extracts the TelegramClient methods that are relevant for plaiframe
#
# using test server is not working. I tried this but had no luck:
# https://docs.telethon.dev/en/latest/developing/test-servers.html
#
class Client(object):
    def __init__(self, config, plai_request):
        #self.api_id = config["api_id"] = 1006033
        #self.config["api_hash"] = '0ee8d07f836686fb78b145da23ca10a5'
        if "test" in config and config["test"]:
            print("create TEST client %s" % config["name"])
            self.client = TelegramClient(None,  config["api_id"], config["api_hash"])
            self.client.session.set_dc('2', '149.154.167.40', 80)
        else:
            print("create client %s, %s" % (config["name"], config["telnumber"]))
            self.client = TelegramClient("./plugins/telegram/sessions/%s" % config["telnumber"].lstrip('+0'),  config["api_id"], config["api_hash"])

        self.config = config
        self.plait = plai_request

        # handle incoming messages
        # 
        # in a former version plaiframe was actually responding using the request response.
        # reaction are obsolte since July, 14th 2020. Plaiframe will always hanldle responses seperately.
        # 
        # Telethon Message Object: https://docs.telethon.dev/en/latest/modules/custom.html#module-telethon.tl.custom.message
        # Telethon Message Event: https://docs.telethon.dev/en/latest/modules/events.html#telethon.events.newmessage.NewMessage
        #
        # Media Messages dont seem to have a from_id. Don't know why but thats why I check it first and use sender_id instead in case.
        #
        # TODO: Use peer_id instead of finding the peer yourself. But first make sure why it says something else then it does in the documentation:
        # https://docs.telethon.dev/en/latest/modules/custom.html#module-telethon.tl.custom.message
        #
        # peer_id seems to be what I want it to be, the id of either the chat message went to or the sender (from) in a private chat. Documentation says its always the sender, weird
        #
        # Maybe I can always use sender_id instead of from_id. But its not in the documentation at the place I would expect so I'm not certain if thats save
        #
        @self.client.on(events.NewMessage(incoming=True))
        async def incoming(event):
            try:
                print('full message: %s' % event.message)
                print('incoming message: %s' % event.message.message)
                #print('From: %s' % event.message.from_id)
                #print('Sender: %s' % event.message.sender_id)
                #print('To: %s' % event.message.to_id)

                # make sure to cache the sender of the message. Does not work for channels! https://docs.telethon.dev/en/latest/modules/custom.html#telethon.tl.custom.sendergetter.SenderGetter.get_sender()
                await event.message.get_sender()

                peer_id = utils.get_peer_id(event.message.to_id)
                peer_type = ""
                peer = 0

                from_id = 0
                if event.message.from_id is None:
                    from_id = event.message.sender_id
                elif isinstance(event.message.from_id, int):
                    from_id = event.message.from_id
                else:
                    from_id = utils.get_peer_id(event.message.from_id, add_mark=False)

                if peer_id > 0:
                    # from_id is no longer of type int with telethon v 1.17 https://docs.telethon.dev/en/latest/misc/changelog.html#channel-comments-and-anonymous-admins-v1-17
                    peer_type = "user"
                    if isinstance(event.message.from_id, int):
                        peer = event.message.from_id
                    else:
                        peer = event.message.sender_id
                elif int(str(peer_id)[:4]) == "-100":
                    peer_type = "channel"
                    peer = utils.get_peer_id(event.message.to_id, add_mark=False)
                else:
                    peer_type = "chat"
                    peer = utils.get_peer_id(event.message.to_id, add_mark=False)

                msg = {
                    "text":event.message.message,
                    "id":event.message.id,
                    "out":event.message.out,
                    "mentioned":event.message.mentioned,
                    "media_unread":event.message.media_unread,
                    "post":event.message.post,
                    "date":event.message.date.isoformat("_","seconds"),
                    "from_id":from_id,
                    "reply_to_msg_id":event.message.reply_to_msg_id,
                    "grouped_id":event.message.grouped_id,
                    "to_id":utils.get_peer_id(event.message.to_id, add_mark=False),
                    "peer_type":peer_type,
                    "peer":peer
                }

                if event.message.media:
                    msg["media"] = "unknown"
                    if event.message.photo:
                        msg["media"] = "photo"
                        file_extension = ".jpg"
                    if event.message.document:
                        msg["media"] = "document"
                    if event.message.audio:
                        msg["media"] = "audio"
                    if event.message.voice:
                        msg["media"] = "voice"
                    if event.message.video:
                        msg["media"] = "video"
                    if event.message.video_note:
                        msg["media"] = "video_note"
                    if event.message.gif:
                        msg["media"] = "gif"
                    if event.message.sticker:
                        msg["media"] = "sticker"

                    print('has media: %s'% msg["media"])

                self.plait.request('/new_message', msg)
                
            except Exception as e:
                print(e)
                return({"error":str(e)})

    #
    # connect and sign in if necessary.  
    # sign in telethon details:
    # https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.auth.AuthMethods.sign_in
    #
    async def connect(self):
        await self.client.connect()

        if await self.client.is_user_authorized():
            print("sign in succesful")
            return {"connect":"successful"}
        elif "telnumber" in self.config:
            if "test" in self.config and self.config["test"]:
                self.client.start(
                    phone=self.config["telnumber"], code_callback=lambda: '22222'
                )
            else:
                await self.client.send_code_request(self.config["telnumber"])
                code_result = self.plait.request('/code_request', {})
                code = code_result["code"]
                # code = input("Enter Login Code\n")
                # res = await plait.request("/login_code", {})
                print("Signing in with " + self.config["telnumber"] + ", " + code)
                
                try:
                    await self.client.sign_in(phone=self.config["telnumber"], code=code)
                    
                except errors.SessionPasswordNeededError as e:
                    await self.client.sign_in(phone=self.config["telnumber"], code=code, password=self.config["password"])
                except Exception as e:
                    print(e)
                
                if await self.client.is_user_authorized():
                    print("sign in succesful")
                    return {"connect":"successful"}
                else:
                    print("Failed to login")
                    print(self.config)
        else:
            print("Failed to sign in. Phone number missing.")
            return {"connect":"failed"}

    async def disconnect(self):
        await self.client.disconnect()
        print("disconnect %s" % self.config["telnumber"])
        return {'disconnect':'successful'}

    #
    # todo: add event to notify if message was read
    # see: https://docs.telethon.dev/en/latest/modules/events.html#telethon.events.messageread.MessageRead
    #
    async def send_message(self, properties):
        try:
            if "entity" in properties:
                pin = properties.pop("pin", False)

                # properties["entity"] = await self.client.get_input_entity(properties["entity"])
                # Type for 2 seconds, then send a message
                async with self.client.action(properties["entity"], 'typing'):
                    await asyncio.sleep(2)
                    message = await self.client.send_message(**properties)
                    
                    if pin:
                        await message.pin(notify=True)
                    return {'message':'sent'}
            else:
                return({'message':'entity missing'})
        except ValueError as e:
            return {'error':str(e)}

    async def forward_messages(self, properties):
        try:
            if "entity" in properties:
                async with self.client.action(properties["entity"], 'typing'):
                    await asyncio.sleep(2)
                    await self.client.forward_messages(**properties)
                    
                    return {'message':'forwarded'}
            else:
                return({'message':'entity missing'})
        except ValueError as e:
            return {'error':str(e)}

    async def send_file(self, properties):
        pin = properties.pop("pin", False)

        message = await self.client.send_file(**properties)

        if pin:
            await message.pin(notify=True)

        return {'file':'sent'}

    async def send_geo(self, properties):
        properties['message'] = Message(id=1,media=MessageMediaGeo(geo=InputGeoPoint(long=properties.pop('longitude', False), lat=properties.pop('latitude', False), accuracy_radius=None)))
        await self.client.send_message(**properties)

        return {'message':'sent'}

    async def download_media(self, properties):
        msg = await self.client.get_messages(properties["chat"], ids=properties['message'])
        # msg["path"] = "telegram/%s_%s%s" % (msg["media"], msg["id"], file_extension)# time.strftime("%Y%m%d-%H%M%S")
        path = await self.client.download_media(message=msg, file="public/%s" % properties["file"])
        print(str(self.get_media_type(msg)) + " download path: " + str(path))
        return {'path':path,'media':self.get_media_type(msg)}

    # obsolete
    async def download_progress_callback(self, current, total):
        print('Downloaded', current, 'out of', total,
            'bytes: {:.2%}'.format(current / total))

    def get_media_type(self, message):
        if message.media:
            if message.photo:
                return "photo"
            if message.document:
                return "document"
            if message.audio:
                return "audio"
            if message.voice:
                return "voice"
            if message.video:
                return "video"
            if message.video_note:
                return "video_note"
            if message.gif:
                return "gif"
            if message.sticker:
                return "sticker"
        return None

    async def get_user(self, properties):
        try:
            user = await self.client.get_entity(properties["id"])
            print(user)
            return self.user_to_json(user)
        except ValueError as e:
            return {'error':str(e)}

    def user_to_json(self, user):
        json_user = {
            "id":user.id,
            "username":user.username,
            "phone":utils.parse_phone(user.phone),
            "first_name":user.first_name,
            "last_name":user.last_name,
            "access_hash":user.access_hash
        }
        return json_user
    #
    # add new user to contacts based on phone number
    # see: https://stackoverflow.com/questions/44597384/add-new-contact-in-api-telegram-python-telethon
    #
    # not sure if replace actually exists
    #
    async def add_contact(self, properties):
        contact = InputPhoneContact(client_id = 0, phone = properties["phone"], first_name=properties["first_name"], last_name=properties["last_name"])
        
        result = await self.client(ImportContactsRequest([contact]))
        print(result)
        if len(result.users):
            return self.user_to_json(result.users[0])
        else:
            return({})

    async def create_chat(self, properties):
        update = await self.client(CreateChatRequest(
            users=properties["users"],
            title=properties["title"]
        ))
        result = {
            "id":update.chats[0].id,
            "title":update.chats[0].title,
            "participants_count":update.chats[0].participants_count,
            "date":update.chats[0].date.isoformat("_","seconds")
        }
        print(result)
        return(result)

    async def add_chat_user(self, properties):
        try:
            update = await self.client(AddChatUserRequest(
                chat_id=properties["chat_id"],
                user_id=properties["user_id"],
                fwd_limit=properties["fwd_limit"]  # Allow the user to see the X last messages
            ))
            result = {
                "id":update.chats[0].id,
                "title":update.chats[0].title,
                "participants_count":update.chats[0].participants_count,
                "date":update.chats[0].date.isoformat("_","seconds")
            }
            print(result)
            return(result)
        except errors.rpcbaseerrors.RPCError as e:
            print(e)
            return {"error":str(e)}
    
    async def edit_chat_admin(self, properties):
        result = await self.client.edit_admin(**properties)
        return({"success":result})

    # telethon client method edit_permissions() seems to only work with channels and supergoups.
    # thats why I use the respective Full API method.
    async def edit_chat_restrictions(self, properties):
        try:
            peer = PeerChat(properties.pop("peer"))
            properties["until_date"] = None
            update = await self.client(EditChatDefaultBannedRightsRequest(
                peer=peer,
                banned_rights=ChatBannedRights(**properties)
            ))
            print(update)
            return(self.update_to_json(update.chats[0]))
        except errors.rpcerrorlist.ChatNotModifiedError as e:
            return({"failed":str(e)})

    async def edit_chat_title(self, properties):
        update = await self.client(EditChatTitleRequest(
            chat_id=properties["chat_id"],
            title=properties["title"]
        ))
        print("Edit chat title %s" % update.chats[0].title)
        return(self.update_to_json(update.chats[0]))
    
    async def edit_chat_about(self, properties):
        try:
            update = await self.client(EditChatAboutRequest(
                peer=PeerChat(properties["peer"]),
                about=properties["about"]
            ))
            print("Edit chat about %d" % update)
            return({"success":update})
        except errors.rpcerrorlist.ChatAboutNotModifiedError as e:
            print(str(e))
            return {"failed":str(e)}

    async def edit_chat_photo(self, properties):
        photo = await self.client.upload_file(properties["photo"])
        update = await self.client(EditChatPhotoRequest(
            chat_id=properties["chat_id"],
            photo=photo
        ))
        print("Edit chat photo for %s to %s" % (update.chats[0].title, properties["photo"]))
        return(self.update_to_json(update.chats[0]))

    def update_to_json(self, update):
        return {
            "id":update.id,
            "title":update.title,
            "participants_count":update.participants_count,
            "date":update.date.isoformat("_","seconds")
        }
        
        

# Command line input
def checkinput():
    newline = input("\n")
    msg = json.loads(newline)

    if "number" in msg:
        print("got number %s" % msg["number"])

if __name__ == "__main__":
    argv = sys.argv[1:]
    
    if len(argv) == 1:
        config = argv[0]
        #resp = {'tach':'nodejs'}
        #print(json.dumps(resp))
        print("stdout: Please enter your phone (or bot token):")
        while True:
            checkinput()
            time.sleep(.100)
        #client = Client(json.loads(config))