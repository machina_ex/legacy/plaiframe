/**
* connections to telethon {@link https://gitlab.com/plaitools/plaiframe/-/blob/master/plugins/telegram/client.py TelegramClient} objects.
* 
* uses axios requests and the plaiframe express app instance to send and receive commands.
*
* @todo replace bot API requests with axios requests
* 
* @requires axios
* @requires plugin
*
* @module telegram/client
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

const axios = require('axios')
const request = require('request')

const Item = require('../plugin.js').Item

/**
 * Base class for communication clients. Organizes routing for incoming events.
 * Stacks routings with callbacks from different sessions and paths and orders what is being forwarded where.
 * 
 * 
 * @param {Object} routing - collection of routes
 *
 * @class Agent
 * 
 */
class Agent extends Item {
  constructor(routing, config, coll) {
    super(config, coll)

    this.routing = {}
    this.pending = {}

    for(let route of routing) {
      this.routing[route] = {}
      this.pending[route] = {}
    }

    /** 
     * identification for route entries. Up count for each new route. Allows for canceling the right entry.
     * Refered to as `route_id` when assigned to entry.
     * */
    this.routing["_id_count"] = 0
  }

  on(type, properties, callback) {
    for (let route in this.routing) {
      if (route == type) {
        let pos = this.addRouting(this.routing[route], this.pending[route], properties, callback)
        log.debug(this.name, "priority " + properties.priority + " " + route + " routing added for " + properties.id + " at position " + pos)
        return this.routing._id_count
      }
    }
    log.error(this.name, "no routing paramter " + type)
    return undefined
  }

  /**
   * add response callback in routing stack of player. Position in stack depends on priority.
   * lower priorities are added behind higher ones. Same priorities are added at front (Last In First Out)
   * 
   * @param {string} route
   * @param {Object} properties
   * @param {string} properties.id - Player identification. What form it has (telnumber, messenger id) depends on the API
   * @param {number} properties.priority - Priority of this routing.
   * @param {function} callback
   * 
   * @returns {number} position in routing stack array
   */
  addRouting(route, pending, properties, callback) {
    if (pending.hasOwnProperty(properties.id)) {
      pending[properties.id](callback)
    }

    this.routing._id_count ++
    log.info(this.name, "add priority " + properties.priority + " routing for " + properties.id + " route id: " + this.routing._id_count)

    let item = { callback: callback, priority: properties.priority, route_id:this.routing._id_count }
    
    if (route.hasOwnProperty(properties.id)) {
      for(let i = 0; i < route[properties.id].length; i++) {
        if (route[properties.id][i].priority <= properties.priority) {
          route[properties.id].splice(i, 0, item)
          return i
        }
      }

      let len = route[properties.id].push(item)
      return len - 1
    } else {
      route[properties.id] = [item]
      return 0
    }
  }
  
  /**
   * deprecated. Might be nice to reinstall if Agent becomes part of messenger classes also parenting twilio
   *
   * @param {*} pending
   * @param {*} id
   * @param {*} data
   */
  addPending(pending, id, data) {
    pending[id] = callback => {
      delete pending[id]
      callback(data)
      .then(react => {
        this.messageResponse(res, react)
      })
      .catch(error => {
        log.error(this.name, error)
      })
    }
  }

  /**
   * cancel routing that was installed previously.
   *
   * @param {string} type - routing type like 'sms', 'call' or 'message'
   * @param {string} id - peer identification like phone number or messenger id
   * @param {number} route_id - routing id as returned by @see Agent.addRouting 
   */
  cancel(type, id, route_id) {
    if(this.routing[type].hasOwnProperty(id)) {
      if(this.routing[type][id].length <= 1) {
        if(this.routing[type][id][0].route_id == route_id) {
          delete this.routing[type][id]
          log.info(this.name, "last " + type + " routing 0 removed for " + id)
        } else {
          log.warn(this.name, "can not cancel " + type + " routing for " + id + ". No routing registered with route id " + route_id)
        }
        return
      }
      for(let i = 0; i < this.routing[type][id].length; i++) {
        if(this.routing[type][id][i].route_id == route_id) {
          this.routing[type][id].splice(i, 1)
          log.info(this.name, type + " routing " + i + " removed for " + id)
          return
        }
      }
			log.warn(this.name, "can not cancel " + type + " routing for " + id + ". No routing registered with route id " + route_id)
		} else {
			log.warn(this.name, "can not cancel " + type + " routing for " + id + ". No routing registered.")
		}
  }

  command(input) {
    switch (input[0]) {
      case "routing":
				if(input.length == 2) {
					for(let rout in this.routing[input[1]]) {
						for(let i = 0; i < this.routing[input[1]][rout].length; i++) {
							log.info(this.name, rout + " " + i +": route id " + this.routing[input[1]][rout][i].route_id + " priority " + this.routing[input[1]][rout][i].priority)
						}
					}
				} else {
					log.info(this.name, this.routing)
				}
				break
			case "cancel":
				if(input.length == 4) {
					this.cancel(input[1], input[2], input[3])
				} else {
					log.info(this.name, "cancel requires arguments type, peer id and route index")
				}
				break
			case "pending":
				log.info(this.name, this.pending)
				break
      default:
        super.command(input)
    }
  }
}

/**
 * @param {string} config.route - base route path (game + telegram)
 */
class Client extends Agent {
  constructor(config, url, app, coll, callbacks) {
    super(["message", "update"], config, coll)
    this.app = app
    this.url = url

    this.callbacks = callbacks

    log.info(this.name, this.settings)
    log.info(this.name, "telegram client at " + this.url)

    this.setRouting()

    this.reconnect(config.settings, true)
  }

  reconnect(settings, init) {
    if(settings.telnumber != this.settings.telnumber || init) {
      if(settings.telnumber) {
        let connect_data = Object.assign({ name: this.name }, settings)

        axios.post(this.url + '/createClient', connect_data)
        .then(response => {
          if(response.data.error) {throw new Error(response.data.error)}
          log.info(this.name, "Telegram Client created")
          log.info(this.name, response.data)
        })
        .catch(err => {
          log.error(this.name, err)
        })
      } else {
        log.warn(this.name, "Telethon API client could not connect: Phone number missing")
      }
      
    }

    this.settings = settings
  }

  close() {
    return new Promise((resolve, reject) => {
      axios.post(this.url + '/removeClient', {name:this.name})
        .then(response => {
          if(response.data.error) {throw new Error(response.data.error)}
          log.info(this.name, "Telegram Client removed")
          log.info(this.name, response.data)
          return resolve()
        })
        .catch(err => {
          return reject(err)
        })
    })
    
  }

  setRouting() {
    this.app.get(this.route + '/' + this.name + '/new_message', this.newMessage.bind(this))
    this.app.get(this.route + '/' + this.name + '/code_request', this.callbacks.codeRequest.bind(this))
  }

  /**
   * callback for incoming message request. forwarded from telethon client event `NewMessage`.
   * Message data is forwarded to the most recent registered routing callback. 
   * If that one does not have a response, message data is forwarded to the next callback etc.
   * 
   * if there is no routing registered and the client has a default level assigned to it:
   * - a callback possibility will be set in pending list so default level can get back to this
   * - default level is initiated
   *
   * @param {Object} req - express request parameter
   * @param {Object} res - express response parameter
   */
  async newMessage(req, res) {
    res.send({"ignore":true})

    let peer = req.body.peer

    let data = req.body

    if (this.routing.message[peer]) {
      for(let response of this.routing.message[peer]) {
        let responded = await response.callback(data)
        if(responded) {
          log.debug(this.name, "responded, skip any further routings")
          break
        }
      }
      //this.routing.message[peer][0].callback(data)
    } else if (this.settings.hasOwnProperty("level") && data.peer_type == "user") {
      log.info(this.name, "no message routing for " + data.peer_type + " " + peer + ": " + data.text)

      this.pending.message[peer] = react_function => {
        delete this.pending.message[peer]
        react_function(data)
      }

      this.callbacks.initialContact(peer, this.settings.level, this.name)
      .then(result => {
        if(result.ignore) {
          log.info(this.name, "ignore message. " + result.player + " is already assigned to a session based on " + result.level)
          this.pending.message[peer](data => {
            return Promise.resolve()
          })
        } else {
          log.info(this.name, "started default level " + result.level + " with player " + result.player)
        }
      })
      .catch(err => {
        log.error(this.name, err)
        if(this.pending.message.hasOwnProperty(peer)) {
          delete this.pending.message[peer]
        }
      })
    } else {
      log.debug(this.name, 'no routing for incoming message from '  + data.peer_type + ' ' + peer + ' and no default level defined: ' + data.text)
    }
    
  }

  requestError(error) {
    if (error.response) {
      // Request made and server responded
      log.error(this.name, error.response.data)
      // log.error(this.name, "status code: " + error.response.status)
      log.error(this.name, error.response.headers)
    } else if (error.request) {
      // The request was made but no response was received
      log.error(this.name, error.request)
    } else {
      // Something happened in setting up the request that triggered an Error
      log.error(this.name, error.message)
    }
  }

  /**
   * request telethon send_message function:
   * https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.send_message
   *
   * @param {Object} data - json formatted parameter for telethon send_message
   * @returns {Promise} - resolves when message was sent
   */
  sendMessage(data) {
    return new Promise((resolve, reject) => {
      axios.post(this.url + '/' + this.name + '/send_message', data)
        .then(response => {
          if (response.data.error) { return reject(response.data.error) }
          log.debug(this.name, response.data)
          return resolve()
        })
        .catch(err => {
          this.requestError(err)
          return reject(err.message)
        })
    })
  }

  /**
   * request telethon forward_message function:
   * https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.forward_messages
   *
   * @param {Object} data - json formatted parameter for telethon forward_messages
   * @returns {Promise} - resolves when message was sent
   */
  forwardMessage(data) {
    return new Promise((resolve, reject) => {
      axios.post(this.url + '/' + this.name + '/forward_messages', data)
        .then(response => {
          if (response.data.error) { return reject(response.data.error) }
          log.info(this.name, response.data)
          return resolve()
        })
        .catch(err => {
          this.requestError(err)
          return reject(err.message)
        })
    })
  }

  /**
   * request telethon send_file function:
   * https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.send_message
   *
   * @param {Object} data - json formatted parameter for telethon send_message
   * @returns {Promise} - resolves when file was sent
   */
  sendFile(data) {
    return new Promise((resolve, reject) => {
      axios.post(this.url + '/' + this.name + '/send_file', data)
        .then(response => {
          if (response.data.error) { return reject(response.data.error) }
          log.info(this.name, response.data)
          return resolve()
        })
        .catch(err => {
          this.requestError(err)
          return reject(err.message)
        })
    })
  }

  /**
   * request telethon send_message function with geo data
   * https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.messages.MessageMethods.send_message
   *
   * @param {Object} data - json formatted parameter for telethon send_message
   * @returns {Promise} - resolves when file was sent
   */
   sendGeo(data) {
    return new Promise((resolve, reject) => {
      axios.post(this.url + '/' + this.name + '/send_geo', data)
        .then(response => {
          if (response.data.error) { return reject(response.data.error) }
          log.info(this.name, response.data)
          return resolve()
        })
        .catch(err => {
          this.requestError(err)
          return reject(err.message)
        })
    })
  }

  /**
   * request telethon download_media function:
   * https://docs.telethon.dev/en/latest/modules/client.html#telethon.client.downloads.DownloadMethods.download_media
   *
   * @param {Object} data - json formatted parameter for telethon download_media
   * @returns {Promise} - resolves when file was sent
   */
  downloadMedia(data) {
    return new Promise((resolve, reject) => {
      axios.post(this.url + '/' + this.name + '/download_media', data)
        .then(response => {
          if (response.data.error) { return reject(response.data.error) }
          log.info(this.name, response.data)
          return resolve(response.data)
        })
        .catch(err => {
          this.requestError(err)
          return reject(err.message)
        })
    })
  }

  /**
   * request telethon Full API CreateChatRequest function:
   * https://tl.telethon.dev/methods/messages/create_chat.html
   *
   * @param {Object} data - json formatted parameter for telethon create_chat
   * @returns {Promise} - resolves when chat was created
   */
  createChat(data) {
    return new Promise((resolve, reject) => {
      if(!data.users.length) {
        return reject("can not create chat. Not enough users")
      }
      if(!data.title) {
        return reject("can not create chat. No title")
      }
      axios.post(this.url + '/' + this.name + '/create_chat', data)
        .then(response => {
          if (response.data.error) { return reject(response.data.error) }
          log.info(this.name, response.data)
          return resolve(response.data)
        })
        .catch(err => {
          this.requestError(err)
          return reject(err.message)
        })
    })
  }

  /**
   * request telethon Full API AddChatUserRequest function:
   * https://tl.telethon.dev/methods/messages/add_chat_user.html
   *
   * @param {Object} data - json formatted parameter for telethon add_chat_user
   * @returns {Promise} - resolves when user was added
   */
  addUser(data) {
    return new Promise((resolve, reject) => {
      axios.post(this.url + '/' + this.name + '/add_chat_user', data)
        .then(response => {
          if (response.data.error) { return reject(response.data.error) }
          log.info(this.name, response.data)
          return resolve(response.data)
        })
        .catch(err => {
          this.requestError(err)
          return reject(err.message)
        })
    })
  }

  /**
   * @todo empty yet
   * request telethon Full API DeleteChatUserRequest function:
   * https://tl.telethon.dev/methods/messages/delete_chat_user.html
   *
   * @param {Object} data - json formatted parameter for telethon delete_chat_user
   * @returns {Promise} - resolves when user was removed
   */
  removeUser(data) {

  }

  /**
   * request the different types of telethon edit group chat functions
   * 
   * - EditChatAboutRequest
   * - EditChatPhotoRequest
   * - EditChatTitleRequest
   * - EditChatDefaultBannedRightsRequest
   * - clien.edit_admin
   *
   * @param {string} chat_property - the group chat property to change: about, photo, title, restrictions or admin
   * @param {Object} data - json formatted parameter for one of the above telethon edit chat methods
   * @returns {Promise} - resolves when chat was edited
   */
  editChat(chat_property, data) {
    return new Promise((resolve, reject) => {
      axios.post(this.url + '/' + this.name + '/edit_chat_' + chat_property, data)
        .then(response => {
          if (response.data.failed) { 
            log.warn(this.name, response.data.failed)
            return resolve(undefined) 
          }
          log.info(this.name, response.data)
          return resolve(response.data)
        })
        .catch(err => {
          this.requestError(err)
          return reject(err.message)
        })
    })
  }

  getUser(id) {
    return new Promise((resolve, reject) => {
      axios.post(this.url + '/' + this.name + '/get_user', {"id":id})
      .then(response => {
        if (response.data.error) { return reject(response.data.error) }
        log.info(this.name, response.data)
        return resolve(response.data)
      })
      .catch(err => {
        this.requestError(err)
          return reject(err.message)
      })
    })
  }

  /**
   * Add a contact to Telegram Client Contacts based on phone number.
   * 
   * @param {Object} data
   * @param {string} data.telnumber - player telnumber
   * @param {string} data.first_name - First Name for contact
   * @param {string} data.last_name - Last Name for contact
   */
  addContact(data) {
    return new Promise((resolve, reject) => {
      //data["phone"] = data["telnumber"]
      delete data["telnumber"]
      if(!data.phone) {
        return reject("can not add contact " + data.first_name + ". phone property missing. ")
      }
      axios.post(this.url + '/' + this.name + '/add_contact', data)
      .then(response => {
        if (response.data.error) { return reject(response.data.error) }
        log.info(this.name, response.data)
        return resolve(response.data)
      })
      .catch(err => {
        this.requestError(err)
        return reject(err.message)
      })
    })
  }

  testAddContact(phone, first_name) {
    
  }

  command(input) {
    switch (input[0]) {
      case "user":
        if(!isNaN(input[1]) && input[1].charAt(0) != '+') {
          this.getUser(Number(input[1]))
          .then((result) => {
            log.info(this.name, result)
          }).catch((err) => {
            log.error(this.name, err)
          })
        } else {
          this.getUser(input[1])
          .then((result) => {
            log.info(this.name, result)
          }).catch((err) => {
            log.error(this.name, err)
          })
        }
        break
      case "add_contact":
        if(input.length == 3) {
          this.addContact({phone:input[1], first_name:input[2], last_name:""})
        } else if (input.length == 2) {
          this.getUser(Number(input[1]))
          .then((result) => {
            result["phone"] = "+" + result["phone"]
            return this.addContact(result)
          })
          .then(result => {
            log.info(this.name, result)
          })
          .catch((err) => {
            log.error(this.name, err)
          })
        }
        
        break
      default:
        super.command(input)
    }
  }
}

/**
* represents a telegram API Bot. api key property allows it to make requests adressing the telegram bot.
* retrieves and stores chats the bot is currently in. 
*/
class Bot {
  constructor(config, app, doc) {
    Object.assign(this, config)

    this.app = app
    this.doc = doc

    if (!this.hasOwnProperty("chats")) {
      this.chats = {}
    }

    this.reconnect(config.settings)
  }

  reconnect(settings) {
    this.settings = settings
    this.settings.url = "https://api.telegram.org/bot" + settings.apikey
    this.settings.method = "GET"

    this.updateChats()
  }

  /**
  * request an telegram Bot API einen text an den chat zu senden
  * https://api.telegram.org/bot[BOT_API_KEY]/sendMessage?chat_id=[MY_CHANNEL_NAME]&text=[MY_MESSAGE_TEXT]
  */
  send(chat_id, message) {
    return new Promise((resolve, reject) => {
      this.request("/sendMessage", { chat_id: chat_id, text: message })
        .then(result => {
          return resolve(result)
        })
        .catch(error => {
          log.error(this.name, error)
          return reject(error)
        })
    })
  }

  /**
    * make request to telegram api
  *
  * @param {string} subaddr - subaddress in the telegram api to define what type of request to make
    */
  request(subaddr, data) {
    return new Promise((resolve, reject) => {
      //log.info(this.name, "request: ")
      //log.info(this.name, data)
      try {
        let options = {
          qs: data,
          url: this.settings.url + subaddr,
          method: this.settings.method
        }
        request(options, (err, response, body) => {
          if (err) { return reject(err) }
          if (typeof body === 'string') {
            try {
              //body = body.replace(/(\r\n|\n|\r)/gm, "")
              //log.info(this.name, body)
              body = JSON.parse(body)
            } catch (err) {
              log.warn(this.name, 'error when parsing string to json.')
              log.warn(this.name, err)
            }
          }
          return resolve(body)
        })
      } catch (err) {
        log.error(this.name, err)
      }
    })
  }

  /**
  * handle response from request
  */
  request_callback(err, response, body) {
    if (err) { log.error(this.name, new Error(err)); return; }
    if (response.hasOwnProperty('statusCode')) {
      if (response.statusCode == 404) {
        log.error(this.name, new Error(e))
      } else {
        log.info(this.name, "response status:", response.statusCode)
      }
    } else {
      log.warn(this.name, "invalid response (status code missing)")
    }
  }

  /**
  * get chat id based on channel first and last name or group chat title
  *
  * @param {string} [data.first_name] - first_name property of telegram user
  * @param {string} [data.last_name] - last_name property of telegram user
  * @param {string} [data.title] - title of group chat
  *
  * @returns {string} chat id if successfull. Else undefined
  */
  getChat(data) {
    if (data.hasOwnProperty("title")) {
      for (let chat in this.chats) {
        if (this.chats[chat].title == data.title) {
          return chat
        }
      }
    } else if (data.hasOwnProperty("first_name")) {
      for (let chat in this.chats) {
        if (this.chats[chat].first_name == data.first_name && this.chats[chat].last_name == data.last_name) {
          return chat
        }
      }
    }
    return (undefined)
  }

  /**
  * pull update object for Bot https://api.telegram.org/bot[BOT_API_KEY]/getUpdates
  */
  updateChats() {
    this.request("/getUpdates", "")
      .then(result => {
        if (result.hasOwnProperty("result")) {
          for (let res of result.result) {
            if (res.hasOwnProperty("message")) {
              if (res.message.hasOwnProperty("chat")) {
                if (!this["chats"].hasOwnProperty(res.message.chat.id)) {
                  log.info(this.name, "new chat acknoledged: ")
                  log.info(this.name, res.message.chat)
                  this["chats"][res.message.chat.id] = res.message.chat
                  let updt = {}
                  let addr = 'chats.' + res.message.chat.id
                  updt[addr] = res.message.chat
                  this.doc.set(updt)
                }
              }
            }
          }
        }

      })
      .catch(error => {
        log.error(this.name, error)
      })
  }

  command(input) {
    switch (input[0]) {
      case "update":
        this.updateChats()
        break
      default:
        log.warn(this.name, "doesn't understand " + input[0])
    }
  }
}

module.exports = {
  Client: Client,
  Bot: Bot
}