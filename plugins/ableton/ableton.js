/**
* Connect plaiframe to Ableton Live using the max for live API
*
* @requires device
*
* @module ableton/ableton
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

const device = require("../devices/device.js")
const plugin = require("../plugin.js")

var schema = {
  "cue": {
  },
  "collections": {
    "liveset": {
      "type": "object",
      "title": "Livesets",
      "properties": {
        "settings": {
          "type": "object",
          "description": "set the connection properties for this liveset",
          "required": true,
          "properties": {
            "port": {
              "type": "number",
              "default": 0,
              "required": true,
              "description": "port on which the liveset communicates with the plai framework. 0 to set random port."
            },
            "ip": {
              "type": "string",
              "description": "the ip address of the computer your liveset runs on, so the framework can send messages to it."
            },
            "framework_port": {
              "type": "number",
              "description": "define a different port where the liveset sends data to the framework if its different from port above."
            }
          }
        }
      }
    }
  }
}

class Ableton extends plugin.Plugin {
  constructor() {
    super(schema)
  }

  /**
  * overwrite plugin add function:
  * - add a new Show instance with every added collection
  * - add show property to cue schema
  *
  * overwrite plugin remove function
  * - remove the show instance
  * - remove its schema properties from the schema
  *
  */
  setup(config) {
    super.setup(config)
      .then(function (result) {
        this.liveset.add = function (data) {
          schema.cue[data.name] = {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {}
            },
            "default": [{}]
          }
          data["collection"] = "liveset"
          this.liveset.items[data.name] = new Liveset(data, this.db, this.db.liveset.document({ "name": data.name }), this.livesetChanges.bind(this))
        }.bind(this)
        this.liveset.remove = function (name) {
          this.liveset.items[name].close()
          delete this.liveset.items[name]
          delete schema.cue[name]
          this.schemaUpdate(schema)
          log.debug(this.name, "Liveset removed: " + name)
        }.bind(this)
        this.liveset.collectItems()
      }.bind(this))
      .catch(function (error) {
        log.error(this.name, error)
      }.bind(this))

    return schema
  }

  cue(data) {
    return new Promise((resolve, reject) => {
      let promises = []
      for (let d in data) {
        if (this.liveset.items.hasOwnProperty(d)) {
          promises.push(this.liveset.items[d].send(data[d]))
        } else {
          log.error(this.name, "no Liveset called " + d)
        }
      }
      Promise.all(promises)
      .then(result => {
        return resolve()
      })
      .catch(err => {
        return reject(err)
      })
    })
    
  }

  command(input) {
    switch (input[0]) {
      case "livesets":
        for (let liveset in this.liveset.items) {
          log.info(this.name, liveset)
        }
        break
      default:
        super.command(input)
        break
    }
  }

  livesetChanges(changes, liveset) {
    for (let change in changes) {
      if (change == "schema") {
        schema.cue[liveset].items.properties = changes[change]
        this.schemaUpdate(schema)
      }
    }
  }
}

/**
* TCP connection to Ableton Live via "plaiforlive" maxforlive plugin
*
* registers mapping from live set and converts it to cue shema for level design
* including all clips, tracks and scenes
*
* @param {Object} config - device config data. see device.OSCdevice
* @param {Document} device_doc - mongodb document connector
* @param {function} callback - to report schema changes to ableton plugin
*/
class Liveset extends device.TCPdevice {
  constructor(config, db, device_doc, callback) {
    super(config, db)
    this.doc = device_doc
    this.mapping = { "clips": {}, "tracks": {}, "scenes": {} }
    this.callback = callback

    this.doc.get()
      .then(function (data) {
        if (data.hasOwnProperty('mapping')) {
          this.mapping = data.mapping
          this.schemaUpdate()
        }
      }.bind(this))
      .catch(function (err) {
        log.error(this.name, err)
      }.bind(this))

    /** liveset cue properties */
    this.schema = {
      "play": {
        "type": "object",
        "properties": {
          "clip": {
            "type": "string"
          },
          "scene": {
            "type": "string"
          }
        }
      },
      "stop": {
        "type": "object",
        "properties": {
          "track": {
            "type": "string"
          },
          "clip": {
            "type": "string"
          }
        }
      },
      "mix": {
        "type": "object",
        "properties": {
          "track": {
            "type": "string",
            "required": true
          },
          "value": {
            "required": true,
            "type": "number",
            "format": "range",
            "minimum": -1,
            "maximum": 1,
            "step": "0.01"
          },
          "device": {
            "required": true,
            "type": "string",
            "enum": [
              "panning",
              "volume",
              "track_activator",
              "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
            ]
          }
        }
      }
    }

    this.callback({ "schema": this.schema }, this.name)
  }

  /**
  * route changes in liveset
  * check for clips, tracks and scenes and add them to cue properties
  */
  handle(message, socket) {
    try {
      message = JSON.parse(message)
    } catch (err) {
      log.warn(this.name, 'error when parsing string to json.')
      log.warn(this.name, err)
    }
    if (message.hasOwnProperty('liveset')) {
      // CLIPS
      this.schema.play.properties.clip['enum'] = []
      this.schema.stop.properties.clip['enum'] = []
      for (let clip in message.liveset.clips) {
        this.schema.play.properties.clip['enum'].push(clip)
        this.schema.stop.properties.clip['enum'].push(clip)
      }

      // TRACKS
      this.schema.stop.properties.track['enum'] = ["master"]
      this.schema.mix.properties.track['enum'] = []
      for (let track in message.liveset.tracks) {
        this.schema.stop.properties.track['enum'].push(track)
        this.schema.mix.properties.track['enum'].push(track)
      }

      // SCENES
      this.schema.play.properties.scene['enum'] = []
      for (let scene in message.liveset.scenes) {
        this.schema.play.properties.scene['enum'].push(scene)
      }
      this.callback({ "schema": this.schema }, this.name)
    } else {
      super.handle(message, socket)
    }
  }
}

module.exports.Plugin = Ableton
