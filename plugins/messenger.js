/**
 * outlines for generic messenger module. Work in Progress
 * @todo almost everything
 * @module messenger
 */

/**
 * 
 */
class Messenger {
  constructor() {

  }
}

/**
 *
 * @class Conversation
 */
class Conversation {
  constructor(properties) {

  }
}

class Dialogue extends Conversation {
  constructor(properties) {
    super(properties)
  }
}

/**
 * 
 * @class Agent
 * 
 */
class Agent {
  constructor() {
  }
}

module.exports = {
  Messenger:Messenger,
  Conversation:Conversation,
  Dialogue:Dialogue,
  Agent:Agent
}