/**
* connect to twilio Account via webhook
*
* @requires twilio
*
* @module twilio/phone
* @copyright Lasse Marburg 2020
* @license GPLv3
*/
const twilio = require("twilio")

/** twilio xml message response generator */
const MessagingResponse = twilio.twiml.MessagingResponse
const VoiceResponse = twilio.twiml.VoiceResponse

/**
* Phone interface Module to connect to twilio API
*
* Twilio Account Webhooks need to be set to HTTP GET
* the phones address routing for incomming call and sms is build like this:
* http://<plai webaddress>:<plai port>/<phone name>/sms
* http://<plai webaddress>:<plai port>/<phone name>/call
* 
* @param {Object} config - has to contain all properties necessary to configure twilio interface
* @param {string} config.name - name for this twilio phone instance. First part for Twilio Request address
* @param {string} config.settings.sid - twilio SID
* @param {string} config.settings.token - twilio Token
* @param {string} config.settings.telnumber - twilio Telnumber
* @param {string} [config.settings.audiofiles='./public/audio/<config.name>'] - path that is prepended to audiofiles for external access by twilio client
* @param {string} config.url - game webaddress and port
* @param {Object} app - Express app forwarded from plai framework
*/
class Phone {
	constructor(config, app)
	{
		Object.assign(this, config)
		this.app = app
		//this.answer_default = answer_default;
		//this.sms_default = sms_default;
		
		/** stores objects identified by telnumber key, that allow forwarding callstatus messages, incoming calls and incoming sms */
		this.routing = {call:{},answer:{},status:{},sms:{}}
		
		/** stores a list of objects identified by telnumber key that allow forwarding incoming events 
		* pending gets a new entry on incoming events that have no routing */
		this.pending = {call:{}, sms:{}}
		
		/** allows to mute outgoing sms to save money or to stop sending in case of emergency */
		this.locked = false
		
		if(config.settings.hasOwnProperty('method')) {
			this.setRouting(this.name, config.settings.method)
		} else {
			this.setRouting(this.name, 'GET')
		}
	
		this.reconnect(config.settings)
	}
	
	/**
	* (re-)create twilio client
	*/
	reconnect(settings) {
		if(!settings.hasOwnProperty('method')) {
			settings["method"] = 'GET'
		}
		this.settings = settings
		
		if(settings.hasOwnProperty('sid') && settings.hasOwnProperty('token')) {
			try {
				this.client = new twilio(settings.sid, settings.token)
				log.info(this.name, 'twilio client connected')
				log.debug(this.name, settings)
			} catch(error) {
				log.error(this.name, "Failed connecting twilio client")
				log.error(this.name, error)
			}
		} else {
			log.error(this.name, "couldn't connect to twilio. Sid and or token missing.")
		}
	}
	
	setRouting(name, method) {
		this.web_routes = {
			'/sms':this.smsIn.bind(this),
			'/call':this.callIn.bind(this),
			'/callstatus':this.callStatus.bind(this)
		}
		this.settings.webaddresses = []
		if(method.toUpperCase() == 'POST') {
			for(let route in this.web_routes) {
				this.app.post('/' + this.game_name + '/' + name + route, this.web_routes[route])
				this.settings.webaddresses.push('/' + this.game_name + '/' + name + route)
			}
		} else {
			for(let route in this.web_routes) {
				this.app.get('/' + this.game_name + '/' + name + route, this.web_routes[route])
				this.settings.webaddresses.push('/' + this.game_name + '/' + name + route)
			}
		}
	}
	
	removeRouting(name) {
		this.app._router.stack.forEach(function(routing, i, routes) {
			if(routing.route) {
				for(let route in this.web_routes) {
					if(routing.route.hasOwnProperty('path')) {
						// log.debug(0, "check " + routing.route.path + " against " + route)
						if(routing.route.path == '/' + name + route) {
							routes.splice(i, 1)
							log.info(0, 'express route removed: ' + routing.route.path)
						}
					}
				}
			}
		}.bind(this))
	}
	
	/**
	* remove express routings
	* not sure if twilio client also needs to or can be closed
	*/
	close() {
		this.removeRouting(this.name)
	}
	
	/**
	* handles sms in requests from twilio
	*
	* uses request.query to evaluate what message came from whom
	* if 'from' property is a registered number, forward to registered callback
	*
	* the callbacks registered are in a stack per telnumber. smsIn always uses the most recent entry, which is on index 0
	* routings are added on array bottom see {@link Phone.registerSmsIn} 
	*
	* if query is empty, try body. Usefull for test requests with curl or via browser url
	*
	* If there is no routing registered for the telnumber and if the Phone has a default level defined sms in is pending.  
	* That means a callback is stored in pending object for this telnumber and allows the next routing registered for this number to imediatly reply
	*/
	smsIn(req, res) {
		/*
		log.info("HEADER", req.headers)
		log.info("QUERY", req.query)
		log.info("BODY", req.body)
		
		log.info("msg", req.query.Body)
		*/
		let content = {}
		if(req.query.hasOwnProperty('From')) {
			content = req.query
		} else if (req.body.hasOwnProperty('From')) {
			content = req.body
		} else {
			log.error(this.name, 'invalid request to /sms/' + this.name)
			log.error(this.name, req.headers)
			res.status(400)
			res.send('invalid request')
			return
		}
		
		if(this.routing.sms[content.From]) {
			this.routing.sms[content.From][0].route(content.From, content.Body)
			.then(reaction => {
				this.messageResponse(res, reaction)
				return 
			})
			.catch(error => {
				log.error(this.name, error)
			})
		} else if (this.settings.hasOwnProperty("level")) {
			log.info(this.name, "incoming sms from unregistered telnumber " + content.From)
			this.foreignContact(content.From, this.settings.level)
			this.pending.sms[content.From] = function(args) {
				delete this.pending.sms[content.From]
				args.route(content.From, content.Body)
				.then(react => {
					this.messageResponse(res, react)
				})
				.catch(error => {
					log.error(this.name, error)
				})
			}.bind(this)
		} else {
			log.warn(this.name, 'no routing for incomming message from ' + content.From +  ' and no default level defined.')
			log.warn(this.name, content.Body)
			
			this.emptyResponse(res)
		}
	}
	
	/**
	* handle incomming call requests from twilio
	*/
	callIn(req, res) {
		let content = {}
		if(req.query.hasOwnProperty('From')) {
			content = req.query
		} else if (req.body.hasOwnProperty('From')) {
			content = req.body
		} else {
			log.error(this.name, 'invalid request to /call/' + this.name)
			log.error(this.name, req.headers)
			res.status(400)
			res.send('invalid request')
			return
		}
		
		if(this.routing.answer[content.From]) {
			let reaction = this.routing.answer[content.From][0].route(content.From)
			return this.voiceResponse(res, reaction)
		} else if (this.settings.hasOwnProperty("level")) {
			log.info(this.name, "incoming call from unregistered telnumber " + content.From)
			this.foreignContact(content.From, this.settings.level)
			this.pending.call[content.From] = function(args) {
				delete this.pending.call[content.From]
				let react = args.route(content.From)
				this.voiceResponse(res, react)
			}.bind(this)
		} else {
			log.warn(this.name, 'no routing for incomming call from ' + content.From +  ' and no default level defined.')
			
			this.voiceResponse(res, {type:"reject",reason:"busy"})
		}
	}
	
	/**
	* handle call status requests from twilio
	*
	* clean up pending queue if theres something left
	*
	* check for routing and get reaction from callbacks accordingly
	*/
	callStatus(req, res) {
		let content = {}
		if(req.query.hasOwnProperty('From')) {
			content = req.query
		} else if (req.body.hasOwnProperty('From')) {
			content = req.body
		} else {
			log.error(this.name, 'invalid request to /callstatus/' + this.name)
			log.error(this.name, req.headers)
			res.status(400)
			res.send('invalid request')
			return
		}
		
		let telnumber = ""
		if(content.Direction == "inbound") {
			telnumber = content.From
		} else if(content.Direction == "outbound" || content.Direction == "outbound-api") {
			telnumber = content.To
		}
		content['telnumber'] = telnumber
		
		log.debug(this.name, content.Direction + " Call Status " + content.CallStatus + " " + telnumber)
		
		if(content.CallStatus == "completed") {
			if(content.hasOwnProperty("CallDuration")) {
				if(content.CallDuration < 2) {
					content.CallStatus = "interrupted"
				}
			}
			if(this.pending.hasOwnProperty(telnumber)) {
				delete this.pending[telnumber]
			}
		}
		
		if(this.routing.status[telnumber]) {
			let reaction = this.routing.status[telnumber].route(content.CallStatus, content)
			return this.voiceResponse(res, reaction)
		} else {
			log.warn(this.name, 'no routing for call status change from ' + content.From)
		}
		
		this.respond(res, 'OK')
	}
	
	respond(res, response) {
		// Close response message
		res.writeHead(200, {
        'Content-Type':'text/xml'
    });
    res.end(response.toString());
	}
	
	emptyResponse(res) {
		this.respond(res, new MessagingResponse)
	}
	
	messageResponse(res, reaction) {
		
		const twiml = new MessagingResponse()
		
		switch(reaction.type) {
			case 'respond':
			if(this.locked) {
				log.warn(this.name, "Try responding with '" + reaction.text + " but sending sms is locked.")
			} else {
				twiml.message(reaction.text)
			}
			
			break
			case 'ignore':
			break
		}
		
		this.respond(res, twiml)
	}
	
	voiceResponse(res, reaction) {
		const twiml = new VoiceResponse()
		
		switch(reaction.type) {
			case "say":
				twiml.say({voice:reaction.voice, language:reaction.language}, reaction.say)
				break
			case "play":
				twiml.play(this.files + '/' + reaction.play)
				break
			case "sayplay":
				for(let answered of reaction.answered) {
					if(answered.hasOwnProperty("say")) {
						let voice = this.settings.voice
						if(answered.hasOwnProperty("voice")) {
							voice = answered.voice
						}
						let language = this.settings.language
						if(answered.hasOwnProperty("language")) {
							language = answered.language
						}
						twiml.say({voice:voice, language:language}, answered.say)
						log.debug(this.name, "Say: " + answered.say + " (" + voice + ", " + language + ")")
					} else if(answered.hasOwnProperty("play")) {
						twiml.play(this.files + '/' + answered.play)
						log.debug(this.name, "Play: " + this.files + '/' + answered.play)
					}
				}
				break
			case "flow":
				// see https://www.twilio.com/docs/voice/twiml/redirect
				log.debug(this.name, "redirecting to Studio Flow " + reaction.flowname + " " + reaction.sid + " account: " + reaction.account)
				twiml.redirect({method: 'POST'}, 'https://webhooks.twilio.com/v1/Accounts/' + reaction.account + '/Flows/'+ reaction.sid)
				break
			case "reject":
				twiml.reject({reason: reaction.reason})
				break
			case "hangup":
				twiml.hangup()
				break
			case "ignore":
				break
			default:
				twiml.reject({reason: "busy"})
				break
		}
		
		this.respond(res, twiml)
	}
	
	/**
	* Send sms
	* @param {String} telnumber number to send to
	* @param {String} text text of message to send
	* @param {function} callback function to call when done sending
	*/
	sendSms(telnumber, text, callback)
	{
		if(this.locked) {
			log.warn(this.name, "Try sending '" + text + "' to " + telnumber + " but sending sms is locked.")
			callback(undefined, {body:"not send",to:"to nobody"})
			return
		}
		
		this.client.messages.create({
			to:telnumber,
			from:this.settings.telnumber,
			body:text
		}, callback)
	}
	
	/**
	* call out
	* See Twilio Reference here:
	* https://www.twilio.com/docs/api/voice/making-calls#url-parameter
	* Example for Status Callbacks:
	* https://www.twilio.com/docs/api/voice/making-calls#make-a-call-and-monitor-progress-events
	* @param {String} number - telnumber to call
	* @param {String} properties.file - path/to/audiofile to play when call established
	* @param {function} properties.established - is called when call was established (in a closed handler this would be done internally)
	* @param {function} properties.interrupt - is called when call couldnt be established
	* @param {function} properties.ended - is called with duration when established call is finished
	*/
	call(telnumber)
	{
		log.info(this.name, 'make call to ' + telnumber)
		
		/*
		log.info(this.name, {
			url: this.url,
			to:telnumber,
			from:this.settings.telnumber,
			method:this.settings.method,
			statusCallback:this.url + '/' + this.name + '/callstatus',
	    statusCallbackMethod:this.settings.method,
	    statusCallbackEvent:["completed"]
			//statusCallbackEvent:["initiated", "ringing", "answered", "completed"]
		})
		*/
		this.client.calls.create({
			to:telnumber,
			from:this.settings.telnumber,
			url:this.url + '/' + this.name + '/callstatus',
			method:this.settings.method,
			statusCallback:this.url + '/' + this.name + '/callstatus',
	    statusCallbackMethod:this.settings.method,
			statusCallbackEvent:["initiated", "ringing", "answered", "completed"]
		})
		.then(response => {
			log.trace(this.name, response)
		})
		.catch(err => {
			log.error(this.name, err)
		})
	}
	
	registerCallStatus(telnumber, properties) {
		this.routing.status[telnumber] = properties
		log.debug(this.name, "call status change listener registered for " + telnumber)
		log.debug(this.name, properties)
	}
	
	cancelCallStatus(telnumber) {
		if(this.routing.status.hasOwnProperty(telnumber)) {
			delete this.routing.status[telnumber]
			log.debug(this.name, "call status change routing removed for " + telnumber)
		} else {
			log.warn(this.name, 'can not cancel call status routing for ' + telnumber + '. No routing registered.')
		}
	}
	
	/**
	* add response for telnumber on incomming call.
	* if there is already a routing registered for telnumber and path, overwrite. (Though this case should never occur)
	* stack callin routing for same number. On callin event the most recent registered routing is used
	* store path to allow canceling the right routing {@link Phone.cancelCallIn}
	*
	* if there is an incomming call request pending for the telnumber, respond to it
	*
	* @param {string} telnumber - telnumber of client may be calling
	* @param {Array} path - path the cue is in that registers callin
	* @param {Object} callback - functions to call on incoming callin events. Is called on incomming call for telnumber
	*/
	registerCallIn(telnumber, path, priority, callback) {
		if(this.pending.call.hasOwnProperty(telnumber)) {
			this.pending.call[telnumber]({route:callback})
		}
		
		if(this.routing.answer.hasOwnProperty(telnumber)) {
			for(let i = 0; i < this.routing.answer[telnumber].length; i++) {
				if(this.routing.answer[telnumber][i].path.equals(path)) {
					this.routing.answer[telnumber][i] = {path:path,route:callback}
					log.info(this.name, "callin routing " + i + " updated for " + telnumber + " path: " + path)
					return
				}
			}
			let pos = 0
			if(priority == "high") {
				this.routing.answer[telnumber].unshift({path:path,route:callback})
			} else {
				pos = this.routing.answer[telnumber].push({path:path,route:callback})
			}
			log.info(this.name, "callin routing added for " + telnumber + " path: " + path + ", position: " + pos)
		} else {
			this.routing.answer[telnumber] = []
			this.routing.answer[telnumber].unshift({path:path,route:callback})
			log.info(this.name, "first callin routing added for " + telnumber + " path: " + path)
		}
	}
	
	/**
	* cancel callin routing for telnumber and path
	*/
	cancelCallIn(telnumber, path) {
		if(path == "all") {
			delete this.routing.answer[telnumber]
		}
		if(this.routing.answer.hasOwnProperty(telnumber)) {
			for(let i = 0; i < this.routing.answer[telnumber].length; i++) {
				if(this.routing.answer[telnumber][i].path.equals(path)) {
					if(this.routing.answer[telnumber].length <= 1) {
						delete this.routing.answer[telnumber]
						log.info(this.name, "last callin routing " + i + " removed for " + telnumber + " path: " + path)
					} else {
						this.routing.answer[telnumber].splice(i, 1)
						log.info(this.name, "callin routing " + i + " removed for " + telnumber + " path: " + path)
					}
					return
				}
			}
			log.warn(this.name, 'can not cancel callin routing for ' + telnumber + ' path: ' + path + '. No routing registered.')
			//delete this.routing.sms[telnumber]
			//log.debug(this.name, "sms in routing removed for " + telnumber + " path: " path)
		} else {
			log.warn(this.name, 'can not cancel callin routing for ' + telnumber + '. No routing registered.')
		}
		
		/*
		if(this.routing.answer.hasOwnProperty(telnumber)) {
			delete this.routing.answer[telnumber]
			log.debug(this.name, "call in routing removed for " + telnumber)
		} else {
			log.warn(this.name, 'can not cancel call_in routing for ' + telnumber + '. No routing registered.')
		}
		*/
	}
	
	/**
	* add response for telnumber on incomming sms.
	* if there is already a routing registered for telnumber and path, overwrite. (Though this case should never occur)
	* stack sms routing for same number. On smsin event the most recent registered routing is used
	* store path to allow canceling the right routing {@link Phone.cancelSmsIn}
	*
	* if there is an incomming sms request pending for the telnumber, respond to it
	*
	* @param {string} telnumber - telnumber of client may be sending sms
	* @param {Array} path - path the cue is in that registers smsin
	* @param {Object} callback - functions to call on incoming sms events. Is called on incomming sms for telnumber
	*/
	registerSmsIn(telnumber, path, priority, callback) {
		if(this.pending.sms.hasOwnProperty(telnumber)) {
			this.pending.sms[telnumber]({route:callback})
		}
		if(this.routing.sms.hasOwnProperty(telnumber)) {
			for(let i = 0; i < this.routing.sms[telnumber].length; i++) {
				if(this.routing.sms[telnumber][i].path.equals(path)) {
					this.routing.sms[telnumber][i] = {path:path,route:callback}
					log.info(this.name, "smsin routing " + i + " updated for " + telnumber + " path: " + path)
					return
				}
			}
			let pos = 0
			if(priority == "high") {
				this.routing.sms[telnumber].unshift({path:path,route:callback})
			} else {
				pos = this.routing.sms[telnumber].push({path:path,route:callback})
			}
			log.info(this.name, "smsin routing added for " + telnumber + " path: " + path + ", position: " + pos)
		} else {
			this.routing.sms[telnumber] = []
			this.routing.sms[telnumber].unshift({path:path,route:callback})
			log.info(this.name, "first smsin routing added for " + telnumber + " path: " + path)
		}
	}
	
	/**
	* cancel sms in routing for telnumber and path
	*/
	cancelSmsIn(telnumber, path) {
		if(path == "all") {
			delete this.routing.sms[telnumber]
		}
		if(this.routing.sms.hasOwnProperty(telnumber)) {
			for(let i = 0; i < this.routing.sms[telnumber].length; i++) {
				if(this.routing.sms[telnumber][i].path.equals(path)) {
					if(this.routing.sms[telnumber].length <= 1) {
						delete this.routing.sms[telnumber]
						log.info(this.name, "last smsin routing " + i + " removed for " + telnumber + " path: " + path)
					} else {
						this.routing.sms[telnumber].splice(i, 1)
						log.info(this.name, "smsin routing " + i + " removed for " + telnumber + " path: " + path)
					}
					
					return
				}
			}
			log.warn(this.name, 'can not cancel smsin routing for ' + telnumber + ' path: ' + path + '. No routing registered.')
			//delete this.routing.sms[telnumber]
			//log.debug(this.name, "sms in routing removed for " + telnumber + " path: " path)
		} else {
			log.warn(this.name, 'can not cancel smsin routing for ' + telnumber + '. No routing registered.')
		}
	}
	
	command(input) {
		switch(input[0]) {
			case "routing":
				if(input.length == 2) {
					for(let rout in this.routing[input[1]]) {
						for(let i = 0; i < this.routing[input[1]][rout].length; i++) {
							log.info(this.name, rout + " " + i +": " + this.routing[input[1]][rout][i].path)
						}
					}
				} else {
					log.info(this.name, this.routing)
				}
				break
			case "cancel":
				if(input.length == 3) {
					switch(input[1]) {
						case "sms":
							this.cancelSmsIn(input[2], "all")
							break
							
						case "answer":
							this.cancelCallIn(input[2], "all")
							break
						default:
							log.info(this.name, "invalid argument for cancel: " + input[1])
					}
				} else {
					log.info(this.name, "cancel requires arguments 'sms' or 'answer' and telnumber")
				}
				break
			case "pending":
				log.info(this.name, this.pending)
				break
			case "lock":
				this.locked = true
				log.info(this.name, "locked")
				break
			case "unlock":
				this.locked = false
				log.info(this.name, "unlocked")
				break
			default:
				log.warn(this.name, "Unknown command " + input[0])
		}
  }
}

/**
* I implemented this Array prototype addon from
* https://stackoverflow.com/questions/7837456/how-to-compare-arrays-in-javascript/14853974#14853974
*
* without modifications
*
* Usage:
* ``` javascript
* [1, 2, [3, 4]].equals([1, 2, [3, 2]]) === false;
* [1, "2,3"].equals([1, 2, 3]) === false;
* [1, 2, [3, 4]].equals([1, 2, [3, 4]]) === true;
* [1, 2, 1, 2].equals([1, 2, 1, 2]) === true;
* ```
*/

// Warn if overriding existing method
if(Array.prototype.equals)
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;       
        }           
        else if (this[i] != array[i]) { 
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;   
        }           
    }       
    return true;
}
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});

module.exports = {
	Phone:Phone
}
