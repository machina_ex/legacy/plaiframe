/**
*
* simple sound plugin based on howler package. Basic funcionality.
* Start and Stop soundfiles on cue.
*
* @requires plugin
* @requires file
*
* @module sound/sound
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

var player = require('play-sound')(opts = {})
const file = require('../../file.js')
const plugin = require('../plugin.js')
var schema = require('./schema.json')


class Sound extends plugin.Plugin {
  constructor() {
    super(schema)
    this.audio = {}
    this.name = "sound"
    this.path = "./plugins/sound/"
  }
  
  setup(config) {
    super.setup(config)
    this.update(config.settings)
    return schema
  }
  
  update(settings) {
    if(settings.hasOwnProperty("path")) {
      this.path = settings.path
    }
    
    this.schema.cue.sound.properties.play.oneOf[0]["enum"] = file.ls(this.path, [".mp3",".wav"])
    this.schemaUpdate(schema)
  }
  
  cue(data, session) {
    return new Promise((resolve, reject) => {
      for(let item in data) {
        switch(item) {
          case "sound":
            if(data.sound.hasOwnProperty("play")) {
              session.variables.review(data.sound.play)
              .then(result => {
                data.sound.play = result
                let playlist = {tracks:[data.sound.play],name:"single file"}
                
                if(this.playlist.items[data.sound.play]) {
                  playlist = this.playlist.items[data.sound.play]
                }
                
                this.audio[session.cue] = {canceled: false, progress: 0, next:data.sound["next"], loop:data.sound["loop"]}
                
                this.play(playlist, session, this.audio[session.cue])
                
                if(data.sound.hasOwnProperty("next")) {
                  return resolve( () => {
                    this.audio[session.cue]["canceled"] = true
                  })
                }
                
                return resolve(undefined)
              })
              .catch(error => {
                return reject(error)
              })
            }
            if(data.sound.hasOwnProperty("stop")) {
              if(this.audio.hasOwnProperty(data.sound.stop)) {
                this.audio[data.sound.stop]["process"].kill()
                return resolve(undefined)
              } else if (data.sound.stop == "all") {
                for(let a in this.audio) {
                  this.audio[a]["process"].kill()
                }
                return resolve(undefined)
              } else {
                return reject("no such cue to stop audio: " + data.sound.stop)
              }
            }
        }
      }
    })
  }
  
  play(playlist, session, options) {
    let audiofile = playlist.tracks[options.progress]
    options.progress ++
    log.debug(this.name, "play sound " + this.path + audiofile)
    this.audio[session.cue]["process"] = player.play(this.path + audiofile, err => {
      if (err) return reject(err)
      if(!options.canceled) {
        log.debug(this.name, "done playing " + audiofile)
        
        if(playlist.tracks.length == options.progress) {
          log.debug(this.name, "playlist finished: " + playlist.name)
          if (options.loop) {
            log.debug(this.name, "Play " + options.loop + " more times.")
            options.loop --
            options.progress = 0
            
            this.play(playlist, session, options)
          } else if (options.next) {
            session.callback(options.next)
          }
        } else {
          this.play(playlist, session, options)
        }
        
      } else {
        log.debug(this.name, "done playing " + audiofile + ". followcue canceled")
      }
    })
  }
}

module.exports.Plugin = Sound
