/**
 * Organize access to plaiframe data collections
 * 
 * @requires plugin
 * @requires json5
 * @requires file
 * 
 * @module data/data
 * @copyright Lasse Marburg 2020
 * @license GPLv3
 */

const plugin = require('../plugin.js')
var schema = require('./schema.json')
const JSON5 = require('json5')
const file = require('../../file.js')


/**
* install request API for all custom collections the game contains
*/
class Data extends plugin.Plugin {
  constructor() {
    super(schema)
    this.name = "data"
    this.collection_apis = []
  }

  /**
  * create request API for each custom database
  * 
  * collect files in file directory and watch for changes.
  */
  setup(config) {
    return new Promise((resolve, reject) => {
      if (config.hasOwnProperty("custom_collections")) {
        if (config.custom_collections) {
          for (let collection of config.custom_collections) {
            if (!config.db.hasOwnProperty(collection)) {
              log.error(this.name, collection + " not found")
              break
            }
            this.collection_apis.push(new CollectionAPI(config.db[collection], config.app, config.game.name))
  
            if(config.db.type == "nedb") {
              this.schema.collections[collection] = {
                "type":"object",
                "properties":{
                  "telegram":{
                    "type":"object",
                    "options":{
                      "disable_edit_json":true,
                      "disable_properties":true,
                    },
                    "properties":{
                      "id":{
                        "type":"number",
                        "readOnly": true
                      },
                      "first_name":{
                        "type":"string",
                        "readOnly": true
                      },
                      "last_name":{
                        "type":"string",
                        "readOnly": true
                      },
                      "access_hash":{
                        "type":"string",
                        "readOnly": true
                      },
                      "phone":{
                        "type":"string",
                        "readOnly": true
                      },
                      "username":{
                        "anyOf":[{
                          "type":"string",
                          "readOnly": true
                        },{
                          "type":"null",
                          "readOnly": true
                        }]
                        
                      },
                      "initial":{
                        "type":"boolean",
                        "readOnly":true
                      }
                    }
                  },
                  "telegram_contacts":{
                    "type":"array",
                    "items":{
                      "type":"string"
                    }
                  }
                }
              }
            }
          }
        }
      }
      super.setup(config)
      .then(result => {
        log.debug(this.name, "load assets filenames from " + this.game.assets)
        file.mkdir(this.game.assets)

        this.collectFiles(this.game.assets)

        file.watchDir(this.game.assets, (e, file) => {
          this.collectFiles(this.game.assets)
          log.debug(this.name, "change in file directory: " + e + " " + file)
          this.schemaUpdate(this.schema)
        })
        
        return resolve(this.schema)
      })
      .catch(err => {
        return reject(err)
      })
    })
  }

  collectFiles(path) {
    this.schema.definitions.files.anyOf[0].enum = []

    let files = file.ls(path, ".")

    for(let f of files) {
      this.schema.definitions.files.anyOf[0].enum.push(f)
    }

    let dirs = file.ls(path, "directories")

    for(let dir of dirs) {
      let files = file.ls(path + "/" + dir, ".")
      for(let fi of files) {
        this.schema.definitions.files.anyOf[0].enum.push(dir + "/" + fi)
      }
    }
  }

  cue(data, session) {
    return new Promise((resolve, reject) => {
      for (let item in data) {
        switch (item) {
          case "store":
            this.store(data[item], session)
              .then(result => {
                return resolve(result) // always undefined
              })
              .catch(error => {
                return reject(error)
              })
            break
          case "load":
            this.load(data[item], session)
            .then(result => {
              return resolve(result) // always undefined
            })
            .catch(error => {
              return reject(error)
            })
        }   
      }
    })
  }

  store(properties, session) {
    return new Promise((resolve, reject) => {
      let name = session.name + " store"
      if (properties.hasOwnProperty("update")) {
        var ref = session.variables.getReference(properties.document)

        session.variables.review(properties.update)
          .then(result => {
            try {
              var update = JSON5.parse(result)
            } catch (err) {
              log.error(name, "couldn't store " + result + ". Not a valid JSON")
              throw new Error(err)
            }
            log.info(name, "Store " + result + " to " + ref.collection + " " + JSON.stringify(ref.query))
            return this.db[ref.collection].update(ref.query, update)
          })
          .then(res => {
            log.debug(name, " matched: " + res.matchedCount + ", modiified: " + res.modifiedCount)
            return resolve(undefined)
          })
          .catch(error => {
            log.error(name, error)
            return resolve(undefined)
          })
      } else if (properties.hasOwnProperty("set")) {
        var ref = session.variables.getReference(properties.document)
        var store_promises = []
        for (let key in properties.set) {
          store_promises.push(this.setValue(key, properties.set[key], ref, session))
        }
        Promise.all(store_promises)
          .then(result => {
            let modified = 0
            let matched = 0
            for (let res of result) {
              modified += res.modifiedCount
              matched += res.matchedCount
            }
            log.debug(name, "changed " + modified + " of " + matched + " entries")
            return resolve(undefined)
          })
          .catch(error => {
            log.error(this.name, error)
            return resolve(undefined)
          })
      } else {
        log.warn(name, "Properties missing store action")
        return resolve(undefined)
      }
    })
  }

  /**
   * create new reference to current session in an external document.
   * 
   * document can than be accessed during session through name reference.
   *
   * @param {Object} properties
   * @param {Object} session
   * @returns {Promise} - returns next cue (if any) once reference was created
   */
  load(properties, session) {
    return new Promise((resolve, reject) => {
      if(!Object.hasOwnProperties(properties, ["collection", "query", "name"])) {
        return reject("can not load reference. collection, query or name property missing.")
      }

      session.variables.review(properties.query)
      .then(result => {
        return session.createReference(properties["collection"], result, properties["name"])
      })
      .then(result => {
        if(properties.next) {
          return resolve(properties.next)
        }
        return resolve()
      })
      .catch(err => {
        return reject(err)
      })
    })
  }

  setValue(key, value, ref, session) {
    return new Promise((resolve, reject) => {
      session.variables.review(value)
        .then(result => {
          let update = {}
          update[key] = result
          return this.db[ref.collection].set(ref.query, update)
        })
        .then(res => {
          return resolve(res)
        })
        .catch(error => {
          log.error("set value", error)
          return resolve(undefined)
        })
    })
  }
}

/**
* creates express app routing to enable requests to interface with mongodb collection
* read only access on game url
* read/write access on secured /data subaddress
*
* allows [Cross-origin resource sharing](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
*
* @param {Object} collection - mongodb collection interface
* @param {Object} app - express app as forwarded form index.js
* @param {string} game - game name to create game specific routing (/<game>/<collection>)
*/
class CollectionAPI {
  constructor(collection, app, game) {
    this.name = collection.name + "API"
    this.collection = collection
    
    if(collection.name != "player") {
      log.info(this.name, "Create API routing at /" + game + '/' + collection.name)
      app.get('/' + game + '/' + collection.name, this.incommingReadOnly.bind(this))
    }

    log.info(this.name, "Create API routing at /" + game + '/data/' + collection.name)
    app.get('/' + game + '/data/' + collection.name, this.incomming.bind(this))
    
  }

  incommingReadOnly(req, res) {
    this.incomming(req, res, "read")
  }

  /**
  * deal with requests
  */
  incomming(req, res, mode) {
    //log.info(this.name, req.headers.host)
    let prom
    if (!Object.isEmpty(req.body)) {
      log.info(this.name, req.body)
      prom = this.handle(req.body, mode)
    } else if (!Object.isEmpty(req.query)) {
      prom = this.handle(req.query, mode)
    } else {
      log.warn(this.name, "Request with no body or query data. Headers:")
      log.warn(this.name, req.headers)
      log.warn(this.name, req.query)
      log.warn(this.name, req.body)
      throw new Error("content missing")
    }
    prom.then(result => {
      res.status(200)
      res.send(result)
    })
    .catch(error => {
      res.status(400)
      res.send(error)
      log.warn(this.name, error)
    })
  }

  /**
  * Interface with mongo collection
  * 
  * get documents or update database
  * 
  * allows update with
  * $set (set), 
  * $push (push) 
  * or $pull (pull) operator
  *
  * @param {Object} data - request data from body or query
  */
  handle(data, mode) {
    return new Promise((resolve, reject) => {
      log.debug(this.name, data)
      if (data.hasOwnProperty("set") && data.hasOwnProperty("key") && mode != "read") {
        let query = {}
        query[data.key] = data.set
        delete data.set
        delete data.key

        for (let d in data) {
          if (data[d][0] == '{') {
            try {
              data[d] = JSON5.parse(data[d])
            } catch (err) {
              if (err instanceof SyntaxError) {
                log.warn(this.name, 'syntax error in query "' + data[d] + '"')
              } else {
                log.error(this.name, 'Error when parsing to json:')
                log.error(this.name, err)
              }
              return reject("error when parsing json: " + err.message)
            }
          }
        }

        return resolve(this.collection.set(query, data))
      } else if (data.hasOwnProperty("push") && mode != "read") {
        let query = {}
        query[data.key] = data.push
        delete data.push
        delete data.key

        return resolve(this.collection.push(query, data))
      } else if (data.hasOwnProperty("pull")) {
        let query = {}
        query[data.key] = data.pull
        delete data.pull
        delete data.key

        return resolve(this.collection.remove(query, data))
      } else if (data.hasOwnProperty("get")) {
        var key = data.get
        delete data.get
        for(let d in data) {
          if(data[d] == "undefined") {
            data[d] = undefined
          }
        }
        this.collection.find(data)
          .then(result => {
            for (let res of result) {
              if (res.hasOwnProperty(key)) {
                return resolve(res[key])
              } else if (key == "all") {
                return resolve(res)
              } else {
                return reject("no such key: " + key)
              }
            }
            return reject("not found")
          })
      } else if (data.hasOwnProperty("getMany")) {
        var key = data.getMany
        delete data.getMany
        for(let d in data) {
          if(data[d] == "undefined") {
            data[d] = undefined
          }
        }
        this.collection.find(data)
          .then(result => {
            if (result.length) {
              let ret_val = []
              for (let res of result) {
                if (res.hasOwnProperty(key)) {
                  ret_val.push(res[key])
                } else if (key == "all") {
                  ret_val.push(res)
                } else {
                  ret_val.push(undefined)
                }
              }
              return resolve(ret_val)
            } else {
              return reject("not found")
            }
          })
      } else if(mode == "read") {
        return reject("no write access")
      } else {
        return reject("property missing")
      }
    })
  }
}

module.exports.Plugin = Data
