/**
*
* This plugin summons many typical communication protocols. It allows to connect create different types of connections and to address them in levels.
* It adds a send action to cue creation that allows to send messages to all previously created devices.
* It allows to switch or listen based on the state of the created devices.
*
* References JSON Meta Schema from json-editor node module to allow creating device schemas:
* node_modules/@json-editor/json-editor/docs/meta_schema.json
*
* @requires device
*
* @module devices/devices
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

const device = require("./device.js")
const plugin = require("../plugin.js")
const meta_schema = require("../../node_modules/@json-editor/json-editor/docs/meta_schema.json")

var schema = require("./schema.json")


class Devices extends plugin.Plugin {
  constructor() {
    super(schema)
  }
  
  /**
  * overwrite plugin add function:
  * - add a new Device instance with every added collection
  * - add device schema to cue send schema
  * - modify device item update function to make a schema update
  *
  * Note: `this.device` refers to the device items collection instance that holds all device instances.
  * `device` on the other hand is the device.js module
  *
  * inserts meta_schema in device "schema" property
  *
  * @todo meta schema creates unexpected validation errors.
  */
  setup(config) {
    return new Promise((resolve, reject) => {
      this.update(config.settings)
      
      super.setup(config)
      .then(result => {
        this.device.add = data => {
          if(data.schema) {
            this.schema.cue.send.items.properties[data.name] = data.schema
            this.schema.events[data.name] = data.schema
          } else {
            this.schema.cue.send.items.properties[data.name] = {}
            this.schema.events[data.name] = {}
          }
          
          data["startDefaultLevel"] = this.startDefaultLevel.bind(this)
          
          this.device.items[data.name] = device.getDevice(data, this.db, this.app, this.game.event)
          
          this.schemaUpdate(this.schema)
        }
        
        this.device.update = (item, changes) => {
          if(changes.updated.hasOwnProperty("settings")) {
            if(changes.updated.settings.interface != this.device.items[item].settings.interface) {
              log.info(this.name, "interface changed for '" + item + "'. Recreating device")
              this.device.items[item] = device.getDevice(changes.document, this.db, this.app, this.game.event)
            }
          }
          if(changes.updated.hasOwnProperty("schema")) {
            this.schema.cue.send.items.properties[item] = changes.updated.schema
            this.schema.events[item] = changes.updated.schema
            log.info(this.name, "schema changed for '" + item + "'. Updating")
            this.schemaUpdate(this.schema)
          }
        }
        
        return this.device.collectItems()
      })
      .then(collected => {
        return device.getPorts()
      })
      .then(ports => {
        log.info(this.name, ports)
        this.schema.collections.device.properties.settings.oneOf[4].properties.port.enum = ports.serial
        
        this.schema.collections.device["definitions"] = meta_schema.definitions
        
        device.setVirtualMidiPort(this.name)
        
        resolve(this.schema)
      })
      .catch(function(error){
        log.error(this.name, error)
        reject(error)
      }.bind(this))
    })
  }
  
  update(settings) {
    if(settings.hasOwnProperty("midi")) {
      if(settings.midi.active) {
        device.activateMidi()
      } else {
        device.deactivateMidi()
      }
    }
  }
  
  /**
  * use device send function if cue contains send property to forward messages
  * 
  * if device send returns a value its stored in session document.
  *
  * @todo wait for device send to return before returning cue
  */
  async cue(data, session) {
    for(let item in data) {
      switch(item) {
        case "send":
        let result = await session.variables.findAndReplace(data[item])
        for(let dev in result) {
          let response = await this.device.items[dev].send(result[dev])
          if(response) {
            let st = {}
            st[dev] = response
            await session.variables.store(st)
          }
        }
        return
      }
    }
  }
  
  /**
	* start a new level
  */
  startDefaultLevel(player_query, default_level) {
    
    this.db.player.find(player_query)
    .then(res => {
      if(!res.length) {
        return this.db.player.insert(player_query)
      } else {
        return
      }
    })
    .then(res => {
      log.info(this.name, "start default level " + default_level + " with player " + JSON.stringify(player_query))
      this.event.emit("create", {level:default_level, player:player_query})
    })
    .catch(err => {
      log.error(this.name, err)
    })
  }
  
  command(input) {
    switch(input[0]) {
      case "list":
      for(let device in this.device.items) {
        log.info(this.name, device)
      }
      break
      case "ports":
      device.getPorts()
      .then(ports => {
        log.info(this.name, ports)
      })
      .catch(function(error){
        log.error(this.name, error)
      }.bind(this))
      break
      case "midi":
      if(input[1] == "on") {
        device.activateMidi()
        log.info(this.name, "please restart plaiframe to reactivate midi")
      } else if(input[1] == "off") {
        device.deactivateMidi()
      }
      break
      default:
      super.command(input)
      break
    }
  }
  
  deviceChanges(changes, device) {
    for(let change in changes) {
      if(change == "schema") {
        schema.cue[device].items.properties = changes[change]
        this.schemaUpdate(schema)
      }
    }
  }
}

module.exports.Plugin = Devices
