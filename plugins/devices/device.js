/**
 * 
 * Diverse Types of Device connectors
 * 
 * @requires medley
 * @requires node-midi
 * @requires serialport
 * @requires net
 * @requires axios
 * @requires dgram
 * @requires json5
 *
 * @todo add stencils and stencils collection like in the old days. See editor.js
 *
 * @module devices/device
 * @copyright Lasse Marburg 2020
 * @license GPLv3
 */


try {
  var osc = require('osc-min')
} catch (err) {
  var osc = null
  console.log("running without osc module")
}

try {
  var SerialPort = require('serialport')
  var Readline = require('@serialport/parser-readline')
} catch (err) {
  var SerialPort = null
  console.log("running without serial module")
}

const medley = require('../../medley.js')
const dgram = require('dgram')
const net = require('net')
const axios = require('axios')

const JSON5 = require('json5')
const { isArray } = require('jquery')

var midi = null
var virtual_midi_port = ''

/**
* midi makes trouble on some machines so it needs to be activated manually
*
*/
function activateMidi() {
  try {
    midi = require('midi')
    /** global var that holds name of virtual midi port. Keeps it to one virtual midi port per game */
    virtual_midi_port = ''
    log.info("devices", "Midi is active.")
  } catch (err) {
    midi = null
    log.info("devices", "Midi is inactive.")
  }
}

function deactivateMidi() {
  midi = null
  log.info("devices", "Midi is inactive.")
}

/**
* define a name for the virtual midi in and out ports
*
* @param {string} vmp - name for virtual midi port
*/
function setVirtualMidiPort(vmp) {
  virtual_midi_port = vmp
}

/**
* enlist all available port names. (Serial, Midi)
* prepend a possible virtual midi port based on game name
*
* @param {string} game - name of game that is requesting ports
*
* @returns {Promise} object with array for each serial and midi portnames. Arrays are empty [] if no valid pors are found
*/
function getPorts() {
  return new Promise(function (resolve, reject) {
    var ports = {}
    if (midi) {
      input = new midi.input()
      ports["midi_input"] = [virtual_midi_port]

      for (let i = 0; i < input.getPortCount(); i++) {
        if (input.getPortName(i) != virtual_midi_port) {
          ports.midi_input.push(input.getPortName(i))
        }
      }

      output = new midi.output()
      ports["midi_output"] = [virtual_midi_port]

      for (let i = 0; i < output.getPortCount(); i++) {
        if (output.getPortName(i) != virtual_midi_port) {
          ports.midi_output.push(output.getPortName(i))
        }
      }
    }

    if (SerialPort) {
      ports["serial"] = []

      SerialPort.list(function (err, serial_ports) {
        //if(err) {return reject(err)}
        if(Array.isArray(serial_ports)) {
          for (port of serial_ports) {
            if (port.serialNumber) {
              ports.serial.push(port.comName)
            }
          }
        }
        resolve(ports)
      })
    } else {
      resolve(ports)
    }
  })
}

/**
* create and return device based on interface as specified in device settings
* defaults to device collection for data storage
*
* @param {Object} data - data set the device will be based on
* @param {Object} db - interface to Database
* @param {Object} app - express app to allow http requests and routing for webdevice
* @param {EventEmitter} game_event - event emitter that originated in game class instance
*
* @returns {Device} instance of one of the device classes
*/
function getDevice(data, db, app, game_event) {
  if (!data.hasOwnProperty("collection")) {
    data["collection"] = "device"
  }
  switch (data.settings.interface.toUpperCase()) {
    case "UDP":
      return new UDPdevice(data, db, game_event)
      break

    case "OSC":
      return new OSCdevice(data, db, game_event)
      break

    case "TCP":
      return new TCPdevice(data, db, game_event)
      break

    case "HTTP":
      return new Webdevice(data, db, game_event, app, "http://")
      break

    case "HTTPS":
      return new Webdevice(data, db, game_event, app, "https://")
      break

    case "SERIAL":
      return new Serialdevice(data, db, game_event)
      break

    case "MIDI":
      return new Mididevice(data, db, game_event)
      break

    default:
      log.error("getDevice", "no such interface " + data.settings.interface)
      return undefined
      break
  }
}

/**
* Has Access to write incomming messages to DB.
* Devices should define their own [send]{@link Device#send} function and redirect incomming
* messages as JSON or JSON fromatted string to the [status update]{@link Device#statusUpdate} function.
*
* @param {Object} config - contains all device specifications
* @param {string} config.name - name identifier for Device
* @param {string} config.interface - connection protocoll that is used for communication
* @param {Object} config.settings - specification of interface parameters
* @param {Object} config.properties - properties the device can send and/or receive
* @param {Collection} db - instance of Database connection
* @param {EventEmitter} game_event - to dispatch events for the whole game
*/
class Device {
  constructor(config, db, game_event) {
    Object.assign(this, config)
    this.db = db
    this.game_event = game_event

    log.debug(0, "Device created: " + config.name)
  }

  /**
  * validate incomming message and update device status in DB
  * 
	* objects are appended to the status variable in dot notation ('key1.key2') so they
  * don't overwrite existing variables
  * 
  * single values (string, number) will overwrite all existing values!
  *
  * Fire game event where event name is the device name and message is the event load
  *
  * message may contain special keys (preceding lowdash)
  * 
  * - _player
  * status is saved to specified player document. Mongo find query in player collection
  * `_player:{<key>:<value>}`
  * 
  * @todo
	*				- more keywords
	*				"_get": allow to retrieve status data from device
  *       "_set": allow to set status/player properties directly (on first level)
  *       "_launch": start a default level
  * 
  * rewrite default level launch maybe using keyword. Always run default level with device document as first argument.
  *
  * @param {Object|string} message - json formatted message
  */
  statusUpdate(message) {
    if (!message) { return }
    if (typeof message === 'string') {
      try {
        message = JSON5.parse(message)
      } catch (err) {
        log.warn(this.name, 'error when parsing string to json. Will save data as single string, overwriting existing data.')
        //log.debug(this.name, err)
      }
    }

    if (this.hasOwnProperty("default")) {
      if (this.default.hasOwnProperty("reference") && this.default.hasOwnProperty("level")) {
        for (let key in message) {
          if (this.default.reference == key) {
            let player_query = {}
            player_query[key] = message[key]
            
            this.db.player.find(player_query)
            .then(result => {
              if(result.length > 0) {
                this.startDefaultLevel(player_query, this.default.level)
              } else {
                log.warn(this.name, "try starting default session but no matching player found.")
              }
            })
            .catch(error => {
              log.error(this.name, error)
            })

            continue
          }
        }
      } else {
        log.warn(this.name, "try starting default level but reference or level is missing.")
      }
    }

    if (message.hasOwnProperty('_player')) {
      var collection = "player"
      var property = this.name
      if (typeof message._player === 'string') {
        var target = { "name": message._player }
      } else {
        var target = message._player
      }
      delete message._player
    } else {
      var collection = this.collection
      var property = "status"
      var target = { '_id': this._id }
    }

    let data = {}

    if (typeof message === 'number' || typeof message === 'string') {
      data[property] = message
      this.single_value = true
    } else if (this.single_value) {
      data[property] = message
      this.single_value = false
    } else {
      let path = ''
      for (let key in message) {
        let path = property + '.' + key
        data[path] = message[key]
      }
    }

    /*
    log.debug(this.name, target)
    log.debug(this.name, "document in collection " + collection + " set: ")
    log.debug(this.name, data)
    */

    this.db[collection].set(target, data)
    .then(result => {
      this.game_event.emit(this.name, message)
    })
    .catch(err => {
      log.error(this.name, err)
    })
  }

  /**
  * change connection properties and restart server/connection if needed
  * all device types that can change connection properties (meaning all, I guess) have to have this
  * @abstract
  * @param {Object} settings - (Updated) device settings
  */
  reconnect(settings) {
    log.error(this.name, 'No reconnect function defined for "' + this.constructor.name + '"! Tried to update: ')
    log.error(this.name, settings)
  }

  /**
  * send data to device.
  *
  * @abstract
  * @param {Object} data - data to send.
  */
  async send(data) {
    log.error(this.name, 'No send function defined for "' + this.constructor.name + '"! Tried to send: ')
    log.error(this.name, data)
    return
  }
  
  /**
  * store current device settings to device document on database
  */
  storeSettings() {
    this.db.device.set({_id:this._id}, {settings:this.settings})
  }
  
  /**
   * on changes in this devices document
   * @abstract
   */
  update(updt) {
    //
  }
  
  /**
  * @todo add reconnect command
  */
  command(input) {
    switch (input[0]) {
      case "send":
        try {
          this.send(JSON5.parse(input[1]))
        } catch (err) {
          log.warn(this.name, 'Can not send message. Error when parsing string to json.')
          log.warn(this.name, input[1])
        }
        break
      case "settings":
        log.info(this.name, this.settings)
        break
      case "reconnect":
        this.reconnect(this.settings)
        break
      default:
        log.warn(this.name, 'no such command: ' + input[0])
        break
    }
  }
}

/**
* UDP network interface
* send and receive UDP messages
*
* based on dgram module
* best understood with help of:
* {@link https://www.hacksparrow.com/node-js-udp-server-and-client-example.html}
*
* @param {string} config.name - name identifier for this device
* @param {number} config.settings.port - port to communicate with device. Device and framework port are identical if framework_port is not provided.
* @param {number} [config.settings.framework_port] - distinct port to listen for messages from this UDP device
* @param {string} config.settings.ip - ip of device. Where to send messages to from plai framework
*/
class UDPdevice extends Device {
  constructor(config, db, game_event) {
    super(config, db, game_event)

    this.reconnect(config.settings)
  }

  /**
  * start udp server to listen for messages
  * if framework_port is not defined, device object listens on port for incomming messages from device
  *
  * @param {Object} settings - new setting properties. See class doc for setting properties.
  */
  reconnect(settings) {
    this.settings = settings

    if (!settings.hasOwnProperty('framework_port')) {
      this.settings['framework_port'] = this.settings.port
    }

    if (this.hasOwnProperty('server')) {
      this.server.close(this.startServer.bind(this))
    } else {
      this.startServer()
    }

  }

  startServer() {
    this.server = dgram.createSocket('udp4')

    this.server.on('listening', () => {
      var address = this.server.address()
      log.debug(this.name, 'listening on ' + address.address + ':' + address.port)
    })

    this.server.on('message', (message, remote) => {

      this.handle(message, remote)
    })

    this.server.on('error', err => {
      if (err.code == 'EADDRINUSE') {
        log.error(this.name, 'framework port already in use')
        log.error(this.name, err.message)
      } else {
        log.error(err)
      }
    })

    this.server.bind(this.settings.framework_port, '0.0.0.0')
  }

  handle(message, remote) {
    log.debug(this.name, remote.address + ':' + remote.port + ' - ' + message)
    this.statusUpdate(message.toString())
    //log.debug(this.name, remote.address + ':' + remote.port +' - ' + message)
  }

  async send(msg) {
    if (this.settings.hasOwnProperty('ip')) {
      let json_msg = msg
      if (!Buffer.isBuffer(msg)) {
        msg = new Buffer(JSON.stringify(msg))
      }

      var client = dgram.createSocket('udp4')

      client.send(msg, 0, msg.length, this.settings.port, this.settings.ip, (err, bytes) => {
        if (err) {
          log.error(this.name, err)
        }
        log.debug(this.name, 'send ' + JSON.stringify(json_msg) + ' to ' + this.settings.ip + ':' + this.settings.port)
        client.close()
        return
      })
    } else {
      log.error(this.name, 'try to send message but no ip was defined.')
      return
    }
    
  }

  close() {
    if (this.hasOwnProperty('server')) {
      this.server.close()
    }
  }
}

/**
* OSC/UDP network interface
* send and receive OSC messages over UDP connection
*
* based on osc-min package
* {@link https://www.npmjs.com/package/osc-min}
*
* @param {string} config.name - name identifier for this device
* @param {number} config.settings.port - port to communicate with device. Device and framework port are identical if framework_port is not provided.
* @param {number} [config.settings.framework_port] - distinct port to listen for messages from this UDP device
* @param {string} config.settings.ip - ip of device. Where to send messages to from plai framework
* @param {string} config.settings.path - device OSC path that is prepended to message OSC path
*/
class OSCdevice extends UDPdevice {
  constructor(config, db, game_event) {
    super(config, db, game_event)
  }

  /**
  * send OSC formatted message. For every (nested) property a seperate message is send.
  */
  async send(msg) {
    const osc_msg = [...this.toOSC(msg)]

    for (let o of osc_msg) {
      if (o.hasOwnProperty('addr') && o.hasOwnProperty('args')) {
        if (this.settings.hasOwnProperty("path")) {
          o.addr = this.settings.path + o.addr
        }
        log.info(this.name, o)
        let buf = osc.toBuffer({
          address: o.addr,
          args: o.args
        })

        await super.send(buf)
      }
    }
    return
  }

  /**
  * itereate through object and convert it to osc compatible message
  * e.g.: {one:{two:{three:4}}} becomes {address:"/one/two/three", args 4}
  * 
  * created with help of Jonas Wilms:
  * https://stackoverflow.com/questions/54634869/convert-json-to-osc-address-and-arguments/54635002#54635002
  */
  * toOSC(obj, previous = "") {
    for (const [key, value] of Object.entries(obj)) {
      if (typeof value !== "object" || Array.isArray(value)) {
        yield { addr: previous + "/" + key, args: value };
      } else {
        yield* this.toOSC(value, previous + "/" + key);
      }
    }
  }

  route(osc_msg) {
    let osc_data = {}
    let path = osc_msg.address.replace(/\//g, '.')
    path = path.slice(1)
    if (osc_msg.args.length > 1) {
      let values = []
      for (let arg of osc_msg.args) {
        values.push(arg.value)
      }
      osc_data[path] = values
    } else {
      let value = osc_msg.args[0].value
      osc_data[path] = value
    }

    return osc_data
  }

  /**
  * cast incomming OSC messages to a json format and update Database with incomming
  * function is called by UDP event handler and overwrites UDPDevice.handle() function
  */
  handle(message, remote) {
    if (Buffer.isBuffer(message)) {
      try {
        message = osc.fromBuffer(message)
        let data = this.route(message)
        //log.debug(this.name, "routed incomming OSC msg: ")
        //log.debug(this.name, data)
        this.statusUpdate(data)
      } catch (error) {
        log.warn(this.name, "invalid OSC packet")
        log.warn(this.name, error)
        return
      }
    } else {
      log.warn(this.name, "Incomming msg is not a Buffer. Type: " + typeof (message))
    }
  }
}



/**
* TCP network interface
* send and receive TCP messages
*
* based on net module
* best understood with help of:
* {@link https://www.hacksparrow.com/tcp-socket-programming-in-node-js.html}
*
* @param {string} config.name - name identifier for this device
* @param {number} config.settings.port - port to communicate with device. Device and framework port are identical if framework_port is not provided.
* @param {number} [config.settings.framework_port] - distinct port to listen for messages from this UDP device
* @param {string} config.settings.ip - ip of device. Where to send messages to from plai framework
*/
class TCPdevice extends Device {
  constructor(config, db, game_event) {
    super(config, db, game_event)

    this.reconnect(config.settings)
  }

  /**
  * start tcp server to listen for messages
  * if framework_port is not defined, device object listens on port for incomming messages from device
  *
  * @param {Object} settings - new setting properties. See class doc for setting properties.
  */
  reconnect(settings) {
    this.settings = settings

    if (!settings.hasOwnProperty('framework_port')) {
      this.settings['framework_port'] = this.settings.port
    }

    if (this.hasOwnProperty('server')) {
      if (this.server.listening) {
        this.server.close(this.startServer.bind(this))
      } else {
        log.warn(this.name, "tcp server was not running properly. Try to Reconnect none the less.")
        this.startServer()
      }
    } else {
      this.startServer()
    }

  }

  startServer() {
    this.server = net.createServer()

    this.server.listen({ port: this.settings.framework_port, host: '0.0.0.0' }, function () {
      log.debug(this.name, 'listening on ' + this.server.address().address + ':' + this.server.address().port)
    }.bind(this))

    this.server.on('error', function (err) {
      switch (err.code) {
        case 'EADDRINUSE':
          log.error(this.name, 'Port ' + err.port + ' already in use!')
          break

        default:
          log.error(this.name, err)
      }
    })

    this.server.on('connection', function (sock) {
      log.debug(this.name, 'connected ' + sock.remoteAddress + ':' + sock.remotePort)

      sock.on('data', function (data) {
        //log.debug(this.name, 'incomming ' + sock.remoteAddress + ': ' + data)
        this.handle(data.toString(), sock)
        // this.statusUpdate(data.toString())
        // Write the data back to the socket, the client will receive it as data from the server
        // sock.write('Response "' + data + '"');
      }.bind(this))

      sock.on('close', function (data) {
        log.debug(this.name, 'closed ' + sock.remoteAddress + ':' + sock.remotePort);
      }.bind(this))
    }.bind(this))
  }

  /**
  * default handle function. Overwrite if you want to deal with incomming messages from child class
  * 
  */
  handle(message, socket) {
    log.debug(this.name, 'incomming ' + socket.remoteAddress + ': ' + message)
    this.statusUpdate(message)
  }

  send(msg) {
    if (this.settings.hasOwnProperty('ip')) {
      var client = new net.Socket()

      client.connect(this.settings.port, this.settings.ip, () => {
        log.debug(this.name + ' send', msg)
        client.write(JSON.stringify(msg))
        client.destroy()
      })

      client.on('data', (data)  => {
        log.debug(this.name + ' sent', data)
        client.destroy()
      })

      client.on('close', () => {
        log.debug(this.name, 'closed')
        return
      })

      client.on('error', (err) => {
        log.error(this.name, 'couldn`t send tcp message to ' + err.address + ':' + err.port + ': ')
        log.error(this.name, msg)
        switch (err.code) {
          case 'ECONNREFUSED':
            log.error(this.name, 'Connection was REFUSED')
            break

          case 'EHOSTUNREACH':
            log.error(this.name, 'unable to REACH HOST')
            break

          case 'EHOSTDOWN':
            log.error(this.name, 'HOST is DOWN')
            break

          case 'ETIMEDOUT':
            log.error(this.name, 'connection TIMED OUT')
            break

          default:
            log.error(this.name, err)
        }
        return
      })
    } else {
      log.error(this.name, 'try to send message but no ip was defined.')
      return
    }
  }

  close() {
    if (this.hasOwnProperty('server')) {
      if (this.server.listening) {
        this.server.close()
      }
    }
  }
}

/**
* http request interface.
* Handles requests to the device and allows to make remote requests
*
* @param {string} config.name - name identifier for this device
* @param {string} config.settings.url - url or ip of device with port and path
* @param {string} config.settings.host - url or ip of device
* @param {string} config.settings.path - path to device on host
* @param {string} [config.settings.route=config.name] - route path for requests from device
* @param {string} [config.settings.method] - POST or GET (default)
* @param {number} [config.settings.port] - path to device on host if not 8080
* @param {Object} express_app - access to express app object
*/
class Webdevice extends Device {
  constructor(config, db, game_event, express_app, protocol) {
    super(config, db, game_event)
    this.app = express_app
    this.json = true

    this.protocol = protocol

    this.settings = this.buildDefaults(this.settings)

    if (config.settings.hasOwnProperty('method')) {
      if (config.settings.method.toUpperCase() == 'POST') {
        this.app.post(config.settings.route, this.handle.bind(this))
      } else {
        this.app.get(config.settings.route, this.handle.bind(this))
      }
    } else {
      this.settings.method = "GET"
      this.app.get(config.settings.route, this.handle.bind(this))
    }
    
    this.storeSettings()
    
    log.debug(this.name, "connected:")
    log.debug(this.name, config.settings)
  }

  /**
  * if no routing is specified, create routing from name.
  * make sure that spaces in name are replaced if any
  *
  * if no url or uri specified, create form host and port.
  *
  * @param {Object} settings - current settings
  *
  * @returns {Object} settings with adapted defaults.
  */
  buildDefaults(settings) {
    if (!settings.hasOwnProperty('route')) {
      let name_convert_spaces = this.name.replace(/ /g, '%20')
      settings.route = '/' + name_convert_spaces
    }
    
    if (settings.hasOwnProperty('host') && settings.hasOwnProperty('port')) {
      let port = ""
      if(settings.port != 80) {
        port = ":" + settings.port
      }
      if (settings.hasOwnProperty('path')) {
        settings.url = this.protocol + settings.host + port + settings.path
      } else {
        settings.url = this.protocol + settings.host + port
      }
    } else if (!settings.hasOwnProperty("url") && !settings.hasOwnProperty("uri")) {
      log.error(this.name, 'Can not create webdevice url. Host and/or Port property missing')
    }
    
    return (settings)
  }

	/**
	* reconnect http device
	* set outgoing request properties
	* open routing on new route path
	* close previous route path. With help from:
	* https://stackoverflow.com/questions/10378690/remove-route-mappings-in-nodejs-express/28369539#28369539
	*
  * @param {Object} settings - new setting properties. See class doc for setting properties.
  */
  reconnect(settings) {
    settings = this.buildDefaults(settings)
    if (settings.route != this.settings.route) {
      this.removeRoute(this.settings.route)
      if (settings.hasOwnProperty('method')) {
        if (settings.method.toUpperCase() == 'POST') {
          this.app.post(settings.route, this.handle.bind(this))
        } else {
          this.app.get(settings.route, this.handle.bind(this))
        }
      } else {
        this.settings.method = "GET"
        this.app.get(settings.route, this.handle.bind(this))
      }
    }

    this.settings = settings
    
    this.storeSettings()
    
    log.debug(this.name, "reconnected:")
    log.debug(this.name, this.settings)
  }

  removeRoute(path) {
    log.debug(this.name, "remove route: " + path)
    this.app._router.stack.forEach(function (routing, i, routes) {
      if (routing.route) {
        if (routing.route.hasOwnProperty('path')) {
          //log.debug(0, "check " + routing.route.path)
          if (routing.route.path == path) {
            routes.splice(i, 1)
            log.info(0, 'express route removed: ' + routing.route.path)
          }
        }
      }
    })
  }

  /**
  * deal with requests to this http device
  */
  handle(req, res) {
    try {
      if (!Object.isEmpty(req.body)) {
        log.info(this.name, req.body)
        this.statusUpdate(req.body)
      } else if (!Object.isEmpty(req.query)) {
        if (req.query.hasOwnProperty("data")) {
          log.info(this.name, req.query.data)
          this.statusUpdate(req.query.data)
        } else {
          log.info(this.name, req.query)
          this.statusUpdate(req.query)
        }
      } else {
        log.warn(this.name, "Request with no body or query data. Headers:")
        log.warn(this.name, req.headers)
        log.warn(this.name, req.query)
        log.warn(this.name, req.body)
        //log.warn(this.name, req)
      }
      res.status(200)
      res.send("OK")
    } catch (e) {
      log.error(this.name, new Error(e))
    }
  }

	/**
  * send json to device. If device knows endpoints and data has endpoint in properties, prepend endpoint to request url.
  * @returns {*} if request returns with data, data is returned. If there is more than one request, latest requests data is returned.
	*/
  async send(data) {
    let return_value = {}
    if("endpoints" in this.settings) {
      for(let d in data) {
        if(this.settings.endpoints.includes(d)) {
          let response = await this.request(data[d], d)
          delete data[d]
          let response_data = this.getData(response)
          if(response_data) {
            this.statusUpdate(response_data)
            return_value = response_data
          }
        }
      }
    }

    if(data && !Object.isEmpty(data)) {
      let response = await this.request(data)
      let response_data = this.getData(response)
      if(response_data) {
        this.statusUpdate(response_data)
        return_value = response_data
      }
    }
    return return_value
  }

  /**
	* make request to device
	*/
  request(data, path="") {
    return new Promise((resolve, reject) => {
      let options = {
        method:this.settings.method,
        url:this.settings.url + path
      }

      if(this.settings.method.toUpperCase() == "GET") {
        if (typeof data === 'string') {
          options["params"] = JSON5.parse(data)
        } else {
          options["params"] = data
        }
      } else {
        if (typeof data === 'string') {
          options["data"] = JSON5.parse(data)
        } else {
          options["data"] = data
        }
      }

      log.info(this.name, options.method + " request: ")
      log.info(this.name, options.url + "?data='" + JSON.stringify(data) + "'")

      axios(options)
      .then(response => {
        if (response.data.error) { return reject(response.data.error) }
        return resolve(response)
      })
      .catch(err => {
        this.requestError(err)
        return reject(err.message)
      })
    })
  }

  requestError(error) {
    if (error.response) {
      // Request made and server responded
      log.error(this.name, error.response.data)
      // log.error(this.name, "status code: " + error.response.status)
      log.trace(this.name, error.response.headers)
    } else if (error.request) {
      // The request was made but no response was received
      log.trace(this.name, error.request)
    } else {
      // Something happened in setting up the request that triggered an Error
      // log.error(this.name, error.message)
    }
  }

	/**
	* make request to device
	*/
  requestDeprecated(data, path="") {
    return new Promise((resolve, reject) => {
      if (typeof message === 'string') {

      } else {
        data = JSON.stringify(data)
      }
      
      try {
        this.settings.body = data + "\n"
        this.settings.query = "data=" + data
        this.settings.headers = { "content-type": "application/json" }
        let options = Object.assign(this.settings, {url:this.settings.url + path})

        log.info(this.name, "request: ")
        log.info(this.name, options.url + "?data='" + data + "'")

        let req = request(options, (err, response, body) => {
          if (err) { log.error(this.name, new Error(err)); return reject(err); }
          if (response.hasOwnProperty('statusCode')) {
            if (response.statusCode == 404) {
              log.error(this.name, "Status 404")
              //return reject("Request returned with status 404")
              return reject(response)
            } else {
              log.info(this.name, "response status:", response.statusCode)
              return resolve(response)
            }
          } else {
            log.warn(this.name, "invalid response (status code missing)")
            return resolve(response)
          }
        })
      } catch (err) {
        log.error(this.name, err)
      }
    })
  }
  
  /**
   * look into request to find any json data in body or query
   *
   * @param {Object} req - http request object
   * @returns {Object} - data that was found in body or query part. undefined if none was found.
   */
  getData(req) {
    if(!Object.isEmpty(req.data)) {
      log.info(this.name, req.data)
      return(req.data)
    } else if (!Object.isEmpty(req.body)) {
      log.info(this.name, req.body)
      return(req.body)
    } else if (!Object.isEmpty(req.query)) {
      if (req.query.hasOwnProperty("data")) {
        log.info(this.name, req.query.data)
        return(req.query.data)
      } else {
        log.info(this.name, req.query)
        return(req.query)
      }
    } else {
      log.warn(this.name, "Request with no body or query data. Headers:")
      log.warn(this.name, req.headers)
      // log.warn(this.name, req.query)
      // log.warn(this.name, req.body)
      //log.warn(this.name, req)
      return
    }
  }

	/**
	* stop routing
	*/
  close() {
    this.removeRoute(this.settings.route)
  }
}

/**
* serial interface
* serialport module API:
* https://serialport.io/docs/api-stream
*
* @param {Object} config - configuration properties for this serial device
* @param {Object} config.settings - Serial connection properties
* @param {Object} config.settings.port - serial port
* @param {Object} [config.settings.baud=115200] - Serial connection baud rate
*/
class Serialdevice extends Device {
  constructor(config, db, game_event) {
    if (SerialPort) {
      super(config, db, game_event)
      this.reconnect(config.settings)
    } else {
      super(config, db, game_event)
      log.error(0, "Can't create Serial Device. serialport module not installed.")
    }
  }

  /**
  * close (if open) and open Serial connection.
  * @param {Object} settings - new setting properties. See class doc for setting properties.
  *
  */
  reconnect(settings) {
    if (this.hasOwnProperty('conn')) {
      if (this.conn.isOpen) {
        this.conn.close()
      }
    }

    if (!settings.hasOwnProperty('baud')) {
      settings.baud = 115200
    }

    this.settings = settings

    this.conn = new SerialPort(settings.port, { baudRate: settings.baud }, function (err) {
      if (err) {
        log.error(this.name, 'tried connecting ' + settings.port)
        log.error(this.name, err.message)

        this.conn.on('open', function () {
          log.info(this.name, 'port is now open')
        }.bind(this))
      } else {
        this.connected()
      }
    }.bind(this))
  }

  connected() {
    const parser = this.conn.pipe(new Readline({ delimiter: '\r\n' }))
    parser.on('data', function (message) {
      log.debug(this.name, message.toString());
      this.statusUpdate(message)
    }.bind(this));
  }

  async send(msg) {
    let message = JSON.stringify(msg)
    this.conn.write(message + "\n", (err) => {
      if (err) {
        log.error(this.name, 'tried to send ' + message)
        log.error(this.name, err.message)
      } else {
        log.debug(this.name + ' send', message)
      }
      return
    })
  }

  close() {
    if (this.hasOwnProperty('conn')) {
      if (this.conn.isOpen) {
        this.conn.close()
      }
    }
  }
}

/**
* midi interface
* uses:
* https://www.npmjs.com/package/midi
*
*/
class Mididevice extends Device {
  constructor(config, db, game_event) {
    if (midi) {
      super(config, db, game_event)
      this.reconnect(config.settings)
    } else {
      log.error(0, "Can't create Midi Device. node-midi module not installed.")
    }
  }

  /**
  * close (if open) and open Midi input and output
  * route incomming midi by channel except its 0 (any)
  * create virtual Port if input or output port is the vitual port name dafined by game
  *
  * @param {Object} settings - new setting properties. See class doc for setting properties.
  *
  */
  reconnect(settings) {
    if (this.hasOwnProperty('input')) {
      this.input.closePort()
    }

    this.settings = settings

    this.input = new midi.input()

    this.input.on('message', function (deltaTime, message) {
      let status = {}
      if (message[0] < 144) {
        status.channel = message[0] - 127
        log.debug(this.name, 'note off ' + message + ' time: ' + deltaTime)
      } else if (message[0] < 160) {
        status.channel = message[0] - 143
        log.debug(this.name, 'note on ' + message + ' time: ' + deltaTime)
      } else {
        log.warn(this.name, 'unknown midi status: ' + message[0])
        return
      }

      status.pitch = message[1]
      status.velocity = message[2]
      status.note = message[1] + ' ' + message[2]

      if (this.settings.channel == status.channel || this.settings.channel == 0) {
        this.statusUpdate(status)
      }
    }.bind(this))

    // Connect Input
    let connected = false

    if (this.settings.input_port == virtual_midi_port) {
      this.input.openVirtualPort(this.settings.input_port)
      log.info(this.name, 'connected midi input on virtual port: ' + this.settings.input_port)
      connected = true
    } else {
      for (let i = 0; i < this.input.getPortCount(); i++) {
        if (this.settings.input_port == this.input.getPortName(i)) {
          this.input.openPort(i)
          log.info(this.name, 'connected midi input on port: ' + this.input.getPortName(i))
          connected = true
        }
      }
    }


    if (!connected) {
      log.info(this.name, 'could not connect. no such midi input port: ' + this.settings.input_port)
    }

    // Connect Output
    if (this.hasOwnProperty('output')) {
      this.output.closePort()
    }

    this.output = new midi.output()

    connected = false
    if (this.settings.output_port == virtual_midi_port) {
      this.output.openVirtualPort(this.settings.output_port)
      log.info(this.name, 'connected midi output on virtual port: ' + this.settings.output_port)
      connected = true
    } else {
      for (let i = 0; i < this.output.getPortCount(); i++) {
        if (this.settings.output_port == this.output.getPortName(i)) {
          this.output.openPort(i)
          log.info(this.name, 'connected midi output on port: ' + this.output.getPortName(i))
          connected = true
        }
      }
    }


    if (!connected) {
      log.info(this.name, 'could not connect. no such midi output port: ' + this.settings.output_port)
    }
  }

  /**
  * send note on midi message based on either:
  * a blank (' ') seperated string with two numbers (pitch and velocity)
  * a number for each pitch and velocity
  */
  async send(msg) {
    let channel = this.settings.channel + 143
    if (msg.hasOwnProperty("channel")) {
      channel = msg.channel + 143
    }

    let snd = [channel, 0, 0]

    if (msg.hasOwnProperty("note")) {
      msg = msg.note.split(" ")
      snd[1] = msg[0]

      if (msg.length == 2) {
        snd[2] = msg[1]
      }
    }
    if (msg.hasOwnProperty("pitch")) {
      snd[1] = msg.pitch
    }
    if (msg.hasOwnProperty("velocity")) {
      snd[2] = msg.velocity
    }

    this.output.sendMessage(snd)
    log.debug(this.name, 'send', snd[0], snd[1], snd[2])

    return
  }

  close() {
    if (this.hasOwnProperty('output')) {
      this.output.closePort()
    }
    if (this.hasOwnProperty('input')) {
      this.input.closePort()
    }
  }
}

module.exports = {
  getDevice: getDevice,
  midi: midi,
  activateMidi:activateMidi,
  deactivateMidi:deactivateMidi,
  getPorts: getPorts,
  Device: Device,
  UDPdevice: UDPdevice,
  OSCdevice: OSCdevice,
  osc: osc,
  TCPdevice: TCPdevice,
  Webdevice: Webdevice,
  Serialdevice: Serialdevice,
  Mididevice: Mididevice,
  setVirtualMidiPort: setVirtualMidiPort
}
