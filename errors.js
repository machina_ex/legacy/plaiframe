/**
* plaitools specific errors
*
* 
* @module errors
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

/**
* errors that occur if there is something wrong with the json data generated when creating plai games
*/
class DesignError extends Error {
  constructor(...params) {
    super(...params)
    
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DesignError)
    }
  }
}

module.exports = {
  DesignError  
}
