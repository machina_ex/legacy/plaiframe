/**  
 * @file control
 * @copyright Lasse Marburg 2020
 * @license GPLv3 
 */

//const Swal = swal;
control = {collections:{},document_ready:false}

socket.on('connect', function(data){
  console.log("control connected")

  socket.get('game','name')
	.then(function(title) {
		$("#title").text(title + " control")
    document.title = title + " control"
    
    main = control
    control.game = title
    
    $('#home').parent().attr("href","/ui", "_self")
    $('#editor').parent().attr("href","/ui/" + title + "/editor", "_self")
    
    /** url routing instance 
    * @global */
    routing = new Routing("/ui/"+ title + "/control")
    
    control.init()

    control.collections.sessions = new Sessions()
    control.collections.sessions.update()
    .then(result => {
      while(!control.document_ready) {}
      
      routing.review()
    })
    .catch(error => {
      console.error(error)
    })
	})
  
  //showSessions()
})

$( document ).ready(function() {
  console.log("ready")
  control.document_ready = true
})

/**
* contorl interface container
*/
class Control {
  constructor() {
    this.collections = {}
    this.document_ready = false
  }
  
  /**
  * detach all elements from content DOM
  */
  clearContent() {
    $('#editor_div').children().detach()
    $('#content').html($('#editor_div'))
  }

  /**
   * on socket connect.  
   * clean up navbar/remove elements
   */
  init() {
    $(".collection_menu").remove()
  }
}

/** control instance 
* @global */
control = new Control()

/**
* archive function. Check it out one last time then remove it
* @todo: REMOVE
* @deprecated
*/
function showSessions() {
  console.log("overview")
  $('#content').html($(''))
  
  $('#content').prepend($('<h2>Active Sessions</h2>'))
  
  let $sessions = $('<table class="collection_list"><th>ID</th><th>Level</th><th>Player</th><th>Slot</th><th>Cue</th><th>Since</th><th></th><th></th></table>')
  
  $sessions.appendTo('#content')
  var player = []
  socket.get('find', {'player':{}})
  .then(result => {
    player = result
    return socket.get('find', {'sessions':{}})
  })
  .then(result => {
    console.log(result)
    if(result.length) {
      for(let session of result) {
        let s_player = ""
        for(let plyr of player) {
          if(plyr.hasOwnProperty('sessions')) {
            if(plyr.sessions.length) {
              for(let p_session of plyr.sessions) {
                if(p_session._id == session._id) {
                  s_player += plyr.name + " " + plyr.telnumber
                  var timeslot = plyr.timeslot
                }
              }
            }
          }
        }
        let cues = ""
        for(let k in session) {
          if(session[k]) {
            if(session[k].hasOwnProperty('date')) {
              cues = k + " (" + Math.round((new Date(Date.now() - new Date(session[k].date)))/60000) + " min.) "
            } else if(session[k].hasOwnProperty('smsin')) {
              if(session[k].smsin.hasOwnProperty('match')) {
                cues = k + " ('" + session[k].smsin.match + "')"
              } else {
                cues = k + " ('" + session[k].smsin.text.substring(0, 32) + "')"
              }
            } else if(session[k].hasOwnProperty('logic')) {
              cues = k + " (" + session[k].logic.match + ")"
            }
          }
        }
        //<i class="far fa-times-circle"></i>
        let $del = $('<td class="icon"><i class="fa fa-times tableIcon"></i></td>')
        $del.on('click', e => {
          confirmation("Are you sure you want to cancel session " + session._id + " based on level " + session.level)
          .then(result => {
            if(result) {
              socket.get('session', {cmd:'delete',session:{_id:session._id}})
              showSessions()
            }
          })
          .catch(error => {
            log.error(this.name, error)
          })
        })
        
        $('<tr id="' + session._id + '"><td>' + session._id + '</td><td>' + session.level + '</td><td>' + s_player + '</td><td>' + timeslot + '</td><td>' + cues + '</td><td>' + Math.round((new Date(Date.now() - new Date(session.date)))/60000) + ' min.</td><td class="icon"><a href="' + session.name + '#list_view"><i class="fa fa-play-circle tableIcon"></i></a></td></tr>').appendTo($sessions)
        $('#' + session._id).append($del)
      }
    }
  })
}
