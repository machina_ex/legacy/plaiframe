/**
* @file session
* Refers to a session document.
* opens Control on openDocument
* @copyright Lasse Marburg 2020
* @license GPLv3
* 
*/
class Sessions extends Collection {
  constructor() {
    super("sessions", {
      schema:{title:"Sessions"},
      sort_by:"date",
      overview:{
        header:["name","_id","level","cue","idle","launch"],
        options:["control","cancel"]
      }
    })
    
    this.open_synonym = "control"
    
    this.addOption("launch", "fa-rocket", e => {
      Sessions.launch()
    })
    
    this.overview.addOption("cancel","fa-times", session => {
      confirmation("Are you sure you want to cancel session " + session._id + " based on level " + session.level)
      .then(result => {
        if(result) {
          return socket.get('session', {cmd:'delete',session:{_id:session._id}})
        }
        return
      })
      .then(result => {
        if(result) {
          console.log("session " + result)
        } else {
          console.log("cancel delete session")
        }
      })
      .catch(error => {
        log.error(this.name, error)
      })
    })
    
    this.overview.addOpenDocumentOption("control","fa-play-circle")
    
    socket.on('session', data => {
      console.log("server session message: " + data)
      switch(data) {
        case "created":
        case "deleted":
        this.update()
        $('#content').trigger('update')
        break
      }
    })
  }

  /**
   * merge references to session documents to append them to session overview
   * 
   * also show recent cue and since when the session runs
   *
   * @param {Object} documents - session documents as forwarded from Overview.update function
   */
  additionalOverviewData(documents) {
    return new Promise((resolve, reject) => {
      let ref_promises = []
      for(let doc of documents) {
        let launch_date = new Date(doc.date)
        doc["recent_cue"] = {date:new Date(0), name:""}
        for(let property in doc) {
          if(doc[property].date) {
            if(new Date(doc[property].date) > doc.recent_cue.date) {
              doc.recent_cue = {name:property, date:new Date(doc[property].date)}
            }
          }
        }

        
        doc["cue"] = doc["recent_cue"].name
        doc["idle"] = this.secondsToHMS(Math.floor((Date.now() - doc["recent_cue"].date) / 60000))
        doc["launch"] = this.secondsToHMS(Math.floor((Date.now() - launch_date) / 60000))

        if(doc.hasOwnProperty('date')) {
          let tzoffset = (launch_date).getTimezoneOffset() * 60000 //offset in milliseconds
          doc.date = (new Date(launch_date - tzoffset)).toISOString().slice(0, -1).replace('T', ' ').slice(0, -4)
        }
        ref_promises.push(socket.get('session', {cmd:"get_references",_id:doc._id}))
      }
      Promise.all(ref_promises)
      .then(result => {
        result.forEach((res, i) => {
          let refs = {}
          for(let ref in res) {
            if(!this.overview.properties.includes(ref)) {
              this.overview.properties.splice(-4, 0, ref,)
            }
            refs[ref] = res[ref][0].name
          }
          Object.assign(documents[i], refs)
        })
        console.log(documents)
        return resolve(documents)
      })
      
    })
  }

  secondsToHMS(s) {
    var h = Math.floor(s/3600); //Get whole hours
    s -= h*3600;
    var m = Math.floor(s/60); //Get remaining minutes
    s -= m*60;
    return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
  }

  
  
  /**
  * open the session control page
  */
  openDocument(data) {
    return new Promise((resolve, reject) => {
      socket.get('find', {level:{name:data.level}})
      .then(result => {
        console.log("Open Session Control " + result[0].name)
        console.log(result[0])
        let session_control = new SessionControl(result[0], data)
        return resolve("done")
      })
      .catch(error => {
        return reject(error)
      })
    })
  }
  
  /**
  * alert with session launch options
  * open control session page when loaded
  *
  * @param {string} [level] - name of level the session will be based on. If none, user will be prompted to choose level.
  */
  static launch(level) {
    var cont = document.createElement('div')
    
    let data = {}
    let player_ident = ""
    let player_label = ""
    
    let $level = $('<select class="swal_form_content">')
    var $name = $('<input class="swal_form_content">')
    var $assign = $('<form id="assign_form" class="swal_form_content">')
    
    let target = "_self"
    
    cont.append("Set session name")
    $name.appendTo(cont)
    
    if(!level){
      var lvl_promise = socket.get('find', { 'level': {} })
    } else {
      target = "_blank"
      var lvl_promise = Promise.resolve()
    }
    
    lvl_promise
    .then(result => {
      if(result) {
        $name.val("new_session")
        for (let level of result) {
          $("<option/>").val(level.name).text(level.name).appendTo($level)
        }
        cont.append("Choose level session is based on")
        $level.appendTo(cont)
      } else {
        $name.val( level + "_session")
        cont.append("Create new Session based on '" + level + "'")
      }
      return socket.get('find', { 'player': {} })
    })
    .then(result => {
      if (Array.isArray(result)) {
        for (let plyr of result) {
          plyr._identification = plyr._id
          player_ident = "_id"
          player_label = plyr._id
          
          if(plyr.hasOwnProperty("telegram")) {
            player_label = plyr.telegram.id
          } else if(plyr.hasOwnProperty("telnumber")) {
            player_label = plyr.telnumber
          }
          
          // let $check_option = $('<label for="' + plyr.card_id + '">' + plyr.name + '</label><input type="checkbox" value="' + plyr.card_id[0] + '" id="' + plyr.card_id + '"><br>').appendTo($assign)
          let $check_option = $('<label for="' + plyr._identification + '">'+ plyr.name + ' ' + player_label + '</label><input type="checkbox" value="'+plyr._identification+'" id="' + plyr._identification + '"><br>').appendTo($assign)
          // let $check_option = $('<div><input type="checkbox" value="'+plyr.telnumber+'">' + plyr.name + ' ' + plyr.telnumber + '</div>').appendTo($assign)
          if (result.length == 1) {
            // if there is only one option check it by default
            $check_option.children().prop('checked', true)
          }
        }
        cont.append("\nSelect Players that will be assigned to session")
        $assign.appendTo(cont)
      }
      
      return Swal({
        content: cont,
        className: 'swal_autosize',
        text: "Launch Session",
        buttons: {
          cancel: true,
          confirm: true,
        },
      })
    })
    .then(value => {
      if (value) {
        data = { cmd: 'create', session: $name.val()}
        
        var assigned_player = $assign.children('input[type="checkbox"]:checked').map(function () {
          let ret = {collection:"player", query:{}}
          ret.query[player_ident] = $(this).val()
          
          return ret
        }).get()
        
        if(level) {
          data["level"] = level
        } else {
          data["level"] = $level.val()
        }
        if(assigned_player.length) {
          data["arguments"] = assigned_player
        }
        console.log(data)
        return socket.get("session", data)
      } else {
        return Promise.reject("canceled")
      }
    })
    .then(result => {
      if (result == "exists") {
        return confirmation($name.val() + " already exists. Overwrite existing Session?")
      } else if (result == "created") {
        window.open("/ui/" + main.game + "/control/sessions/" + $name.val(), target)
        console.log("Session " + $name.val() + " created")
        return Promise.reject("created")
      }
    })
    .then(result => {
      if (result) {
        data.cmd = 'overwrite'
        return socket.get("session", data)
      } else {
        this.launch(level)  // reopen formula
      }
    })
    .then(result => {
      if (result == "created") {
        console.log("Session " + $name.val() + " created")
        window.open("/ui/" + main.game + "/control/sessions/" + $name.val(), target)
      }
    })
    .catch(err => {
      if (err != "canceled" && err != "created") {
        console.error(err)
      } else if(!level){
        routing.undo()
      }
    })
  }
}

/**
* Create cue buttons and arrange them in list, table or show view
*
* start in list view on reload
*
* visualize if cues are triggered by user or automatically
*
* in show view:
* trigger next tab on tab key
* trigger next cue on arrow key
*/
class SessionControl {
  constructor(level_data, session_data) {
    this.level = level_data
    this.name = session_data.name
    this.data = session_data
    
    this.cues = {chapter:{buttons:[],bars:[],tabs:[]}}
    
    this.view = ""
    this.$current_tab = null
    
    this.connectServer()
    
    $(window).bind('keydown', event => {
      console.log("Key pressed: " + event.key)
      switch(event.key) {
        case "ArrowRight":
          console.log("next cue")
          console.log($("#page_div").children(".triggered.cuebar").length)
          if(!$("#page_div").children(".triggered.cuebar").length) {
            $("#page_div").children(".cuebar").first().children(".cue").trigger('click')
          } else {
            $("#page_div").children(".triggered.cuebar").next().children(".cue").trigger('click')
          }
          break
        case "Tab":
          console.log("switch tab")
          if($("#page_div").children(".pages_menu").children(".cue.selected").next().index() == -1) {
            $("#page_div").children(".pages_menu").children(".cue").first().trigger('click')
          } else {
            $("#page_div").children(".pages_menu").children(".cue.selected").next().trigger('click')
          }
          break
      }
    })
    
    main.clearContent()
    
    let $title = $('<h2>' + this.name + ' (' + this.level.name + ')</h2>')
    $('#content').prepend($title)
    
    this.addOption("list", "fa-th-list", this.listView.bind(this))
    this.addOption("table", "fa-th", this.tableView.bind(this))
    this.addOption("show", "fa-gamepad", this.showView.bind(this))
    
    let $edit = $('<button title="open ' + this.level.name + ' level editor" class="option_toggle"><i class="fas fa-wrench"></i></button>')
    $edit.on("click", e => {
      window.open("/ui/" + main.game + "/editor/level/" + this.level.name)
    })
    $('#content').prepend($edit)
    
    this.update(level_data, session_data)
    this.listView()
    $('#content').children("[title|='list view']").addClass("option_selected")
  }
  
  /**
  * add buttons to toggle view and to access this levels editor
  */
  addOption(view, icon, callback) {
    let $view = $('<button title="' + view + ' view" class="option_toggle"><i class="fas ' + icon + '"></i></button>')
  
    routing.add({path:"/sessions/" + this.name, hash:view, link:$view}, e => {
      $("#content").children(".session_control").detach()
      callback()
      $(".option_toggle").removeClass("option_selected")
      $view.addClass("option_selected")
    })
    
    $('#content').prepend($view)
  }
  
  /**
  * connect to framework session Object
  * listen for cues being triggered
  */
  connectServer() {
    this.socket = io(namespace + "/sessions/" + this.name)
    console.log('session connecting to socket namespace: ' + namespace + "/sessions/" + this.name)
    
    this.socket.get = (key, property) => {
    	return new Promise((resolve, reject) => {
    		if(!this.socket.is_connected('get data')){reject()}
    		let uuid = generateUUID()
    		let send = {data:property,id:uuid}
    		this.socket.emit(key, send)
    		this.socket.on(uuid, function(data) {
    			if(data) {
    				resolve(data)
    			} else {
    				console.error("No return value for '" + key + " "+ property + "'")
    			}
    			this.socket.removeAllListeners(key)
    		})
    	})
    }
    
    this.socket.is_connected = cmd => {
    	if(!this.socket.connected) {
    		Swal({
    			title: "Can't " + cmd,
    			text: "no connection to server!",
    			icon: "warning",
    			buttons: false
    		})
    	}
    	return this.socket.connected
    }
    
    this.socket.on("connect", data => {
      console.log("session connected: " + this.name)
    })
    
    this.socket.on('cue', cue => {
      this.stopAllTimer()
      $('.triggered').removeClass('triggered')
      for(let chapter in this.cues) {
        this.cues[chapter].bars.forEach(bar => bar.$.removeClass("triggered")) 
      }
      console.log("server message cue: " + cue.chapter + "." + cue.name)
      console.log(this.cues[cue.chapter].bars)
      for(let bar of this.cues[cue.chapter].bars) {
        if(bar.name == cue.name) {
          bar.startTimer()
          bar.$.addClass("triggered")
        }
      }
      
      for(let tab of this.cues.chapter.tabs) {
        if(tab.name == cue.chapter + "." + cue.name) {
          if(this.view == "show") {
            tab.$.trigger('click')
          } else {
            this.$current_tab = tab.$
          }
        }
      }
    })
  }
  
  /**
  * create required cue buttons
  *
  * @param {Object} level_data - level document json
  * @param {Object} session_data - session document json
  */
  update(level_data, session_data) {
    console.log("update")
    this.level = level_data
    this.name = session_data.name
    this.data = session_data
    
    for(let chapter in this.level['chapter']) {
      this.cues[chapter] = {buttons:[],bars:[],tabs:[]}
      
      for(let cue of this.level.chapter[chapter]) {
        let cue_button = new CueButton(cue, chapter, this.dispatch.bind(this))
        let cue_bar = new CueBar(cue_button)
        this.cues[chapter].buttons.push(cue_button)
        this.cues[chapter].bars.push(cue_bar)
      }
    }
    for(let [tab, cuelist] of Object.entries(this.level.control.pages)) {
      let tab_button = new TabButton(tab, cuelist, this.cues, this)
      this.cues.chapter.tabs.push(tab_button)
    }
    if(this.cues.chapter.tabs.length) {
      this.$current_tab = this.cues.chapter.tabs[0].$
    }
  }
  
  /**
  * toggle list view.
  * show all buttons (left) with comments (right) in one list
  */
  listView() {
    this.view = "list"
    console.log("List View")
    for(let chapter in this.cues) {
      var $chapter_div = $('<div class="cuelist session_control"></div>')
      var $chapter_title = $('<h3>'+chapter+'</h3>')
      $chapter_title.appendTo($chapter_div)
      
      for(let cue_bar of this.cues[chapter].bars) {
        cue_bar.assemble()
        cue_bar.$.appendTo($chapter_div)
      }
      $chapter_div.appendTo($('#content'))
    }
  }
  
  /**
  * toggle table view
  * show all buttons without comment in a grid.
  */
  tableView() {
    this.view = "table"
    console.log("Table View")
    $('#content').find('.cue').detach()
    for(let chapter in this.cues) {
      var $chapter_div = $('<div class="cuetable session_control"></div>')
      var $chapter_title = $('<h3>'+chapter+'</h3>')
      $chapter_title.appendTo($chapter_div)
      
      for(let cue_button of this.cues[chapter].buttons) {
        cue_button.$.appendTo($chapter_div)
      }
      $chapter_div.appendTo($('#content'))
    }
  }
  
  /**
  * show view is based on the pages property in level. Cues are arranged accordingly
  *
  */
  showView() {
    this.view = "show"
    if(!this.cues.chapter.tabs.length) {
      Swal({
        className: 'swal_autosize',
        icon:"warning",
        title:"Show view not available",
        text: "Please use the level editor to arange a show control view."
      })
      console.warn("No Show view available")
      return
    }
    console.log("Show View")
    
    var $page_div = $('<div class="cuelist session_control" id="page_div"></div>')
    var $pages_menu = $('<div class="pages_menu"></div>')
    
    for(let tab of this.cues.chapter.tabs) {
      $pages_menu.append(tab.$)
    }
    
    $page_div.append($pages_menu)
    $('#content').append($page_div)
    
    this.$current_tab.trigger("click")
  }
  
  /**
  * send cue to be dispatched to server
  *
  * @param {string} chapter - name of chapter the cue originates from
  * @param {string} cuename - name of cue to be dispatched
  */
  dispatch(chapter, cuename) {
    console.log("dispatch", chapter,cuename)
    this.socket.get('cue', {chapter:chapter, cue:cuename})
    
    for(let chapter in this.cues) {
      this.cues[chapter].bars.forEach(bar => bar.$.removeClass("selected")) 
      this.cues[chapter].buttons.forEach(button => button.$.removeClass("selected")) 
    }
    this.stopAllTimer()
  }
  
  /**
  * stop any timer that is running
  */
  stopAllTimer() {
    for(let chapter in this.cues) {
      this.cues[chapter].bars.forEach(bar => bar.stopTimer()) 
    }
  }
}

/**
* A Tab that opens a cuelist on click. Is also triggered by clicking corresponding cue.
*
* @param {string} name - button name
* @param {Array} list - list of cues that are summarized under this tab
* @param {Array} cue_bars - list of {@link CueBar} Objects summarized under this tab
* @param {Object} session_control - session control namespace
*/
function TabButton(name, list, cue_bars, session_control) {
  this.$ = $('<button class="cue" id="'+name+'_tab">' + name + '</button>')
  this.name = name
  
  console.log("Create Page Button " + name)
  
  this.$.click( () => {
    console.log("Tab Click")
    session_control.$current_tab = this.$
    $("#page_div").children('.cuebar').detach()
    for(let cue of list) {
      cue = cue.split('.')
      let chapter = cue[0]
      cue = cue [1]
      
      for(let cue_bar of cue_bars[chapter].bars) {
        if(cue_bar.name == cue) {
          cue_bar.assemble()
          $("#page_div").append(cue_bar.$)
        }
      }
    }
    
    $('.pages_menu .cue').removeClass('selected')
    this.$.addClass('selected')
  })
}

/**
*  Button that triggers a cue on click
*
* @param {Object} cue - cue properties the button is based on
* @param {string} chapter - name of chapter the cue originates from
* @param {function} callback - called on click
*/
function CueButton(cue, chapter, callback)
{
  Object.assign(this, cue)
	this.$ = $('<button class="cue" id="'+chapter+'.'+cue.name+'">' + cue.name + '</button>')
  if(cue.hasOwnProperty('style')) {
    this.$.addClass(cue.style)
  }
  if(cue.comment) {
    this.$.attr('title', chapter + '.' + cue.name + ' - ' + cue.comment)
  } else {
    this.$.attr('title', chapter + '.' + cue.name)
  }
	
  this.$.on("click", e => {
    callback(chapter, cue.name)
    this.$.addClass('selected')
  })
}

/**
* Contains a cue (left) and its comment (right)
*
* @param {CueButton} button - button object the bar is wrapped around
*/
function CueBar(button) {
  this.$ = $('<div class="cuebar"></div>')
  this.$timer = $('<div class="timer">0:00</div>')
  
  this.$timer.hide()
  
  this.name = button.name
  
  if(!button.comment) {
    button.comment = '-'
  }
  
  var $textfield = $('<div class="comment">' + button.comment + '</div>')
  
  button.$.on("click", e => {
    console.log("click " + e.target)
    this.$.addClass('selected')
  })
  
  this.assemble = () => {
    button.$.appendTo(this.$)
    this.$.append(this.$timer)
    this.$.append($textfield)
  }
  
  this.tvar
  this.sec
  this.min
  
  this.startTimer = () => {
    this.$timer.show()
    clearInterval(this.tvar)
    this.tvar = setInterval(this.cueTimer, 1000)
    this.sec = 0
    this.min = 0
    this.$timer.text(this.min + ':0' + this.sec)
  }
  
  this.cueTimer = () => {
    this.sec++
    if(this.sec > 59) {
      this.sec = 0
      this.min++
    }
    if(this.sec < 10) {
      this.$timer.text(this.min + ':0' + this.sec)
    } else {
      this.$timer.text(this.min + ':' + this.sec)
    }
  }
  
  this.stopTimer = () => {
    clearInterval(this.tvar)
  }
}
