/**
* @file filemanager and editor for level and plugins
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

/** form style editor using json-editor
* @global  */
var form = new Form()
var connected = false

socket.on('connect', function (data) {
  if(connected) {
    console.log("editor reconnected")
    editor.assignUser()
    routing.review()
    return
  }
  
  console.log("editor connected")

  socket.get('game', 'name')
  .then(title => {
    $("#title").text(title + " editor")
    document.title = title + " editor"
    
    //$('#home').parent().attr("href","../../ui", "_self")
    $('#home').parent().attr("href","/ui", "_self")
    $('#control').parent().attr("href","/ui/" + title + "/control/sessions#list", "_self")
    
    main = editor
    editor.game = title
    
    routing = new Routing("/ui/"+ title + "/editor")
    
    editor.init()
    
    editor.collections.level = new Level()
    
    editor.collections.plugins = new Plugins()
    
    return editor.collections.plugins.init()
  })
  .then(result => {
    return editor.updateAll()
  })
  .then(result => {
    return editor.assignUser()
  })
  .then(result => {
    while(!editor.document_ready) {}
    connected = true
    editor.ready()
  })
  .catch(error => {
    console.error(error)
  })
})

socket.on('schema', function (data) {
  for (let plugin in data) {
    if (data[plugin].hasOwnProperty('cue')) {
      editor.collections.level.append(plugin, data[plugin])
    }
  }
})

socket.on('lock', args => {
  editor.lock(args)
})

socket.on('unlock', args => {
  editor.unlock(args)
  routing.review()
})

$(document).ready(() => {
  editor.document_ready = true
})

/**
* specify key shortcuts
* - ctrl+s to save currently open editor
*
*/
$(window).bind('keydown', function (event) {
  if (event.ctrlKey || event.metaKey) {
    switch (String.fromCharCode(event.which).toLowerCase()) {
      case 's':
        event.preventDefault()
        editor.save()
        break
    }
  }
})

window.onbeforeunload = function (event) {
  if (form.changed) {
    event.returnValue = "You have unsaved changes."
  }
}

/**
* Expands Navbar with Level and Plugins and their collections.
* Allows to open, save, close etc.
*
*/
class Editor {
  constructor() {
    this.file_menu = new FileMenu()
    this.collections = {}
    this.document_ready = false
    this.user = {}
  }
  
  /**
   * on socket connect.  
   * clean up navbar/remove elements
   */
  init() {
    $(".collection_menu").remove()
    
    $('#editor_user').on('click', e => {
      console.log("click")
      socket.get('rooms', '')
      .then(msg => {
        console.log(msg)
        let usage = "Current users editing or viewing documents\n\n"
        for(let room in msg) {
          if(Object.keys(msg[room]).length) {
            usage += room.toString() + ":\n"
            for(let id in msg[room]) {
              usage += "- " + msg[room][id].name + " (" + msg[room][id].browser + ') \n'
            }
            usage += "\n"
          }
        }
        Swal({
          title: "Editor User",
          text: usage
        })
      })
    })

    this.unlock()
  }

  /**
   * get user details from server and store them in editor user variable
   * 
   * user was defined on login. If there is no login user is empty
   */
  assignUser() {
    /*
    return new Promise((resolve, reject) => {
      if (typeof(Storage) !== "undefined") {
        if(!localStorage.user) {
          inputPrompt("Bitte gib deinen Namen ein", "", input => {
            localStorage.user = input
            return resolve()
          })
        } else {
          return resolve()
        }
      } else {
        return resolve()
      }
    })
    */
  return new Promise((resolve, reject) => {
    console.log("get user")
    socket.get('user', '')
    .then(result => {
      console.log(result)
      this.user = result
      $("#username").html(this.user.name)
      return resolve()
    })
   })
  }

  /**
  * is called if:
  * 
  * - socket connected  
  * - all collections are populated  
  * - document ready event fired
  */
  ready() {
    console.log("editor ready")
    this.file_menu.disable()
    routing.review()
  }

  /**
   * disable current editor and saving.
   * 
   * Show notification and toggle lock icon to closed.
   *
   * @param {string} msg - notification message to be displayed in alert
   */
  lock(msg) {
    if(!form.locked) {
      form.locked = true
      plaigraph.lock()
      this.file_menu.config({save:false, delete:false})
      console.log("Editor locked")
      $('.lock').addClass('closed').removeClass('open')
      $('.lock').children('.lock').removeClass('fa-lock-open').addClass('fa-lock')
      $('.lock').off('click')
      $('.lock').on('click', e => {
        Swal({
          title: "Editor locked",
          text: msg,
          icon: "warning"
        })
      })
      Swal({
        title: "Editor locked",
        text: msg,
        icon: "warning"
      })
    }
  }

  /**
   * enable current editor and saving.
   * 
   * Show success notification and toggle lock icon to open.
   */
  unlock() {
    if(form.locked) {
      form.locked = false
      plaigraph.unlock()
      this.file_menu.config({save:true, delete:true})
      console.log("Editor unlocked")
      $('.lock').removeClass('closed').addClass('open')
      $('.lock').children('lock').removeClass('fa-lock').addClass('fa-lock-open')
      $('.lock').off('click')
      
      Swal({
        title: "Editor unlocked",
        text: "Editing and saving is enabled",
        icon: "success",
        buttons: false,
        timer: 2500
      })
    }
  }
  
  /**
  * populte all dynamic lists in navbar
  * - level
  * - sessions
  * - plugins
  *
  */
  updateAll() {
    return new Promise((resolve, reject) => {
      var populate_collections = []
      for (let collection in this.collections) {
        populate_collections.push(this.collections[collection].update())
      }
      Promise.all(populate_collections)
      .then(result => {
        console.log("all updated")
        return resolve(result)
      })
      .catch(error => {
        return reject(error)
      })
    })
  }
  
  /**
  * call collections remove function and delete collection instance.
  *
  * @param {string} name - collection name
  */
  removeCollection(name) {
    this.collections[name].$collection_menu.remove()
    delete this.collections[name]
    console.log("collection removed: " + name)
  }
  
  /**
  * REset/Clear form editor and remove current content from DOM.
  */
  closeContent() {
    form.clear()
      .then(confirmed => {
        if (confirmed) {
          this.file_menu.disable()
          this.clearContent()
          // $('#content').html($('#editor_div'))
        } else {
          routing.undo()
        }
      })
  }
  
  /**
  * Clear content container.
  */
  clearContent() {
    $('#editor_div').children().detach()
    $('#content').html($('#editor_div'))
  }
  
  /**
  * call remove function of current content collection.
  * clear content.
  */
  deleteContent() {
    console.log(form)
    this.collections[form.collection].removeDocument(form.title, form.id)
    .then(result => {
      if(result) {
        form.discard()
        this.file_menu.disable()
        this.clearContent()
        routing.show('#empty')
      }
    })
    .catch(error => {
      console.error(error)
    })
  }
  
  /**
  * call socket.save() function with current form content
  * refresh dynamic gui elements and set editor content to unchanged
  * if content was not in database yet, assign auto created ID
  */
  save() {
    if(this.file_menu.getState("save")) {
      socket.save(form.getData(), form.collection)
        .then(result => {
          if (result) {
            if (result.hasOwnProperty('insertedId')) {
              form.assignID(result.insertedId)
            }
            routing.show("/" + form.collection + "/" + form.title + "#edit")
            editor.file_menu.enable()
            form.changed = false
            this.collections[form.collection].update()
            .then(result => {
              $("#content").trigger("save")
              document.cookie = "status=saved"
            })
            .catch(err => {
              console.error(err)
            })
          }
        })
    } else {
      Swal({
        text: "Saving is disabled.",
        icon: "warning",
        buttons: false,
        timer: 2000
      })
    }
  }
  
  /**
  * open file explorer and provide json style (including linebreaks) textfile
  *
  * parts from:
  * https://stackoverflow.com/questions/3665115/create-a-file-in-memory-for-user-to-download-not-through-server/18197341#18197341
  *
  * removes _id property before download for better (re-)import
  * 
  * @param {string} filename - name of file that will be downloaded
  * @param {Object} [data] - json data to be downloaded as a file. If not provided current content is downloaded
  */
  download(filename, data) {
    if (!data) {
      data = form.getData()
    }

    let download_data = Object.assign({}, data)

    if (download_data.hasOwnProperty('_id')) {
      delete download_data._id
    }
    let json_data = JSON.stringify(download_data, null, 2)
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(json_data));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
  
  /**
  * open file input and
  * get JSON data from chosen file
  *
  * @returns {Promise} json parsed data from file
  */
  import() {
    return new Promise(function (resolve, reject) {
      var $input = $('<input type="file" id="fileinput" />')
      $input.trigger('click')
      $input.on('change', function (e) {
        var fileReader = new FileReader()
        fileReader.onload = function () {
          var data = fileReader.result
          console.log(data)
          try {
            let js_data = JSON.parse(data)
            return resolve(js_data)
          } catch (e) {
            Swal({
              title: "Import failed:",
              text: String(e),
              icon: "warning"
            })
            return reject(e)
          }
        }
        fileReader.onabort = function () {
          console.log("abort")
          //return resolve()
        }
        fileReader.onloadend = function () {
          console.log("end")
          //return resolve()
        }
        fileReader.onerror = function () {
          console.log("error")
          //return resolve()
        }
        fileReader.onloadstart = function () {
          console.log("start")
        }
        fileReader.onprogress = function () {
          console.log("progress")
        }
        fileReader.readAsText($input.prop('files')[0])
      })
    })
  }
}

/**
* Left most navbar element (File) with options to manipulate current editor content.
*
*/
class FileMenu {
  constructor() {
    this.$save = $("#file_save")
    this.$save_as = $("#file_save_as")
    this.$download = $("#file_download")
    this.$schema_download = $("#file_schema_download")
    //this.$close = $("#file_close")
    this.$delete = $("#file_delete")
  }
  
  /**
  * set all file options to enabled
  */
  enable() {
    this.config({save:!form.locked,save_as:true,download:true,schema_download:true,close:true,delete:!form.locked})
  }
  
  /**
  * set all file options to disabled
  */
  disable() {
    this.config({save:false,save_as:false,download:false,schema_download:false,close:false,delete:false})
  }
  
  /**
  * enable or disable specific options
  *
  * @param {Object} options - add properties that are supposed to be enabled (true) or disabled (false)
  * @param {Object} options.save - enable (true) or disable (false) save option
  * @param {Object} options.save_as - enable (true) or disable (false) save_as option
  * @param {Object} options.download - enable (true) or disable (false) download option
  * @param {Object} options.schema_download - enable (true) or disable (false) schema_download option
  * @param {Object} options.delete - enable (true) or disable (false) delete option
  */
  config(options) {
    if(options.hasOwnProperty("save")) {
      this.toggle(this.$save, options.save, event => {
        editor.save()
      })
    }
    if(options.hasOwnProperty("save_as")) {
      this.toggle(this.$save_as, options.save_as, event => {
        inputPrompt("Enter new name for document", form.title + "_copy", function (doc_name) {
          if (doc_name) {
            socket.save(form.getData(), form.collection, doc_name)
              .then(result => {
                if (result) {
                  editor.collections[form.collection].update()
                  routing.open("/" + form.collection + "/" + doc_name + "#edit")
                }
              })
          }
        })
      })
    }
    if(options.hasOwnProperty("download")) {
      this.toggle(this.$download, options.download, event => {
        inputPrompt("Enter Filename", form.title + ".json", function (doc_name) {
          if (doc_name) {
            editor.download(doc_name)
          }
        })
      })
    }
    if(options.hasOwnProperty("schema_download")) {
      this.toggle(this.$schema_download, options.schema_download, event => {
        inputPrompt("Enter Filename for schema", form.title + "_schema.json", function (doc_name) {
          if (doc_name) {
            editor.download(doc_name, editor.collections[form.collection].getSchema())
          }
        })
      })
    }
    
    /*
    if(options.hasOwnProperty("close")) {
      this.toggle(this.$close, options.close, () => {
        editor.routing.add({hash:"empty", link:$("#close")}, e => {
          editor.closeContent()
        })
      })
    }
    */
    if(options.hasOwnProperty("delete")) {
      this.toggle(this.$delete, options.delete, event => {
        editor.deleteContent()
      })
    }
  }
  
  /**
  * add or remove click function from option element
  */
  toggle($elem, state, callback) {
    if(state) {
      if($elem.hasClass('inactive')) {
        $elem.addClass('active').removeClass('inactive')
        $elem.click(callback)
      }
    } else {
      if($elem.hasClass('active')) {
        $elem.removeClass('active').addClass('inactive').off('click')
      }
    }
  }
  
  /**
  * get current state of option
  *
  * @returns {boolean} true if option is enable, false if option is disabled
  */
  getState(elem) {
    return $("#file_" + elem).hasClass('active')
  }
}

/** global editor element */
var editor = new Editor()
