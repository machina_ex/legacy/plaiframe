/**
* @file make types of plait database collections accessible with web form
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

/**
* add a menu item that gives access to document editors for a databse collections
* Extend this class for any collection type that has special rewuirements.
*
* overview has following options:
*
* ### edit
* button next to each element to open document settings
*
* ### delete
* button next to each element to remove document from database
* 
* ### add
* show button underneath the table to create or add new document
*
* @param {string} name - Name of Database collection. All editable plai collections have to have a name property
* @param {Object} setings - customize the collection
* @param {Object} settings.schema - the json schema the editor form will be based upon
* @param {string} [settings.sort_by="modified"] - upon what property to sort list items by default
* @param {array} settings.overview.header - list of possible column names (string). Columns are shown in this order
* @param {array} settings.overview.options - list of default options added to overview.
*/
class Collection {
  constructor(name, settings) {
    
    this.title = name
    this.name = name
    if(settings.sort_by) {
      this.sort_by = settings.sort_by
    } else {
      this.sort_by = "modified"
    }
    
    this.open_synonym = "edit"
    
    this.options = {}
    
    routing.add(this.name, e => {
      //routing.show(this.name + "#list")
    })
    
    this.max_list_length = 7
    
    if(settings.schema) {
      this.schema = settings.schema
      if(this.schema.hasOwnProperty('title')) {
        this.title = this.schema.title
      }
      
      if(!this.schema.hasOwnProperty("properties")) {
        this.schema.properties = {}
      }
      
      this.schema.properties['_id'] = {
        "type":"string",
        "readonly":true,
        "required":true
      }
      if(!this.schema.properties.hasOwnProperty('name')) {
        this.schema.properties['name'] = {
          "type":"string",
          "readonly":true,
          "required":true
        }
      }
      this.schema.properties['modified'] = {
        "type":"string",
        "readonly":true,
        "required":true
      }
    }
    
    this.$collection_menu = $(
      `<div class="dropdown collection_menu" id="${name}_collection_menu">
        <a class="nostyle">
          <button class="dropbtn" id="${name}_menu_btn">${this.title}</button>
        </a>
      </div>`
    )
    
    this.$menu_items = $(`<div class="dropdown-content" id="${name}_menu"><hr></div>`)
    this.$collection_menu.append(this.$menu_items)
    $('#menubar').append(this.$collection_menu)
    
    if(settings.overview) {
      this.overview = new Overview({name:this.name,update:this.update.bind(this),removeDocument:this.removeDocument.bind(this)}, settings.overview.header, this.additionalOverviewData.bind(this))
      
      routing.add({path:this.name, hash:"list", link:this.$collection_menu.children("a")}, e => {
        this.showOverview(this.sort_by)
      })
    }
  }
  
  /**
  * get current cue schema
  */
  getSchema() {
    return this.schema
  }
  
  /**
  * re populate collection
  */
  update() {
    return this.populate()
  }

  /**
   * merge additional documents to append them to collection overview
   *
   * @param {Object} documents - collection documents as forwarded from Overview.update function
   * @abstract
   */
  additionalOverviewData(documents) {
    return new Promise((resolve, reject) => {
      return resolve(documents)
    })
  }
  
  /**
  * call find query in database and add collect results.
  * results are added to navbar until max elements is reached.
  * 
  * clear the menu beforehand
  */
  populate() {
    return new Promise((resolve, reject) => {
      this.$menu_items.find(".menu_collection_item").remove()
      
      let query = {}
      query[this.name] = {}
      query["sort"] = {}
      query.sort[this.sort_by] = -1
      socket.get('sorted', query)
        .then(data => {
          for (let i = 0; i < data.length; i++) {
            if (i < this.max_list_length) {
              this.addDocument(data[i], true)
            } else if (i == this.max_list_length) {
              this.addDocument(data[i], false)
              let $more = $('<a class="active menu_collection_item">...</a>')
              this.$menu_items.children(".menu_collection_item").last().after($more)
              routing.add({path:this.name, hash:"list", link:$more})
            } else {
              this.addDocument(data[i], false)
            }
          }
          return resolve("done")
        })
    })
  }
  
  /**
  * add link to open document in navbar and create routing
  */
  addDocument(doc, visible) {
    routing.add({path:this.name + "/" + doc.name}, e => {
      return this.getDocument(doc.name, doc._id)
    })
    routing.add({path:this.name + "/" + doc.name, hash:this.open_synonym, symbolic:true}, e => {
      return this.getDocument(doc.name, doc._id)
    })
    
    if(visible) {
      let $button = $('<a class="active menu_collection_item">' + doc.name + '</a>')
      
      if(this.$menu_items.children(".menu_collection_item").length == 0) {
        this.$menu_items.prepend($button)
      } else {
        this.$menu_items.children(".menu_collection_item").last().after($button)
      }
      
      routing.add({path:this.name + "/" + doc.name, hash:this.open_synonym,link:$button}, e => {
        return this.getDocument(doc.name, doc._id)
      })
    }
  }
  
  /**
  * append option beneath navbar dropdown menu and above list overview
  *
  * @param {string} option - Will be part of option id and # reference. Will be displayed with uppercase first letter..
  * @param {function} callback - function that is called when menu option is clicked
  */
  addOption(option, icon, callback) {
    this.options[option] = {icon:icon}
    
    let $option = $(`<a  id="${this.name}_${option}">${option.charAt(0).toUpperCase() + option.slice(1)}</a>`)
    this.$menu_items.append($option)
    $option.addClass('active')
    routing.add({path:this.name,hash:option,link:$option}, callback)
  }
  
  /**
  * appends a `create new item` option that calls newDocument function on successfull click event
  */
  addCreateOption() {
    this.addOption("create", "fa-plus", event => {
      inputPrompt("Type name for new " + this.title, "new_" + this.name, custom_name => {
        if(custom_name) {
          this.newDocument(custom_name)
        } else {
          routing.undo()
        }
      })
    })
  }
  
  /**
  * appends an import item option that opens file dialog and calls openDocument function on successfull open event
  */
  addImportOption() {
    this.addOption("import", "fa-upload", event => {
      main.import()
      .then(data => {
        if(data) {
          this.openDocument(data, true)
        } else {
          routing.undo()
        }
      })
      .catch(error => {
        console.error(error)
        routing.undo()
      })
    })
  }
  
  /**
  * tell server to create a new document.
  * Is called by create option.
  */
  newDocument(doc_name) {
    var find_query = {}
    find_query[this.name] = {"name":doc_name}
    socket.get('find',find_query)
  	.then(data => {
      console.log("New " + this.title)
      console.log(data)
  		if(data.length) {
        confirmation(doc_name + " already exists. Save will create duplicate.")
        .then(confirmed => {
          if(confirmed) {
            this.openDocument({"name":doc_name}, true)
          } else {
            routing.undo()
          }
        })
      } else {
        this.openDocument({"name":doc_name}, true)
      }
  	})
  }
  
  /**
  * find document in database
  *
  * @param {string} name - document name
  * @param {string} [_id] - document _id if known
  */
  getDocument(name, _id) {
    return new Promise((resolve, reject) => {
      let query = {}
      if(_id) {
        query[this.name] = { "_id": _id }
      } else {
        query[this.name] = { "name": name }
      }
      socket.get('find', query)
        .then(result => {
          console.log("Open Database Content " + this.name + " " + name)
          console.log(result[0])
          return this.openDocument(result[0])
        })
        .then(result => {
          return resolve(result)
        })
        .catch(error => {
          return reject(error)
        })
    })
  }
  
  /**
  * show editor with document data and allow for it to be edited.
  *
  * @param {Object} data - document data
  * @param {boolean} is_new - true if document was not yet saved to server because it was just created or imported
  */
  openDocument(data, is_new) {
    return new Promise((resolve, reject) => {
      form.replace(data, this.name)
      .then(result => {
        if(result) {
          main.clearContent()
          if(main.file_menu) {
            main.file_menu.enable()
          }
          
          let title = form.editableTitle(data.name, 'h2')
          
          title.on('edited', function(e, param) {
            form.setCurrent('name',param.new_title)
            form.title = param.new_title
            form.changed = true
          })
          
          if(is_new) {
            form.changed = true
            if(main.file_menu) {
              main.file_menu.config({delete:false})
            }
          }
          
          $('#content').prepend(title)
          
          form.load(this.schema)
          form.show(data.name)
        }
        return resolve("done")
      })
    })
  }
  
  /**
  * remove document from collection here and from server.
  * Prompts confirmation
  *
  * @param {string} name - document name
  * @param {_id} _id - document _id
  */
  removeDocument(name, _id) {
    return new Promise((resolve, reject) => {
      if(!_id) {
        console.error("can not remove " + name + ". No valid _id." )
        return resolve(false)
      }
      confirmation("Are you sure you want to delete " + name + " (" + _id + ")")
        .then(result => {
          if (result) {
            return socket.remove(this.name, { _id: _id })
          }
          return
        })
        .then(result => {
          console.log(result)
          if(result) {
            return this.update()
          } else {
            return
          }
        })
        .then(result => {
          if(result) {
            return resolve(true)
          } else {
            return resolve(false)
          }
        })
        .catch(error => {
          console.error(error)
        })
    })
  }
  
  /**
  * 2D table showing all collection items with preselected properties
  */
  showOverview(sort_by) {
    if(main.file_menu) {
      main.file_menu.disable()
    }
    
    main.clearContent()
    
    $('#content').prepend($('<h2>'+this.title+'</h2>'))
    
    this.overview.update(sort_by)
    .then(result => {
      for(let option in this.options) {
        let $option = $('<button title="' + option + ' ' + this.name + '" class="option_toggle"><i class="fas ' + this.options[option].icon + '"></i></button>')
        routing.add({path:this.name, hash:option, link:$option})
        
        $('#content').prepend($option)
      }
      $('#content').on('update', e => {
        console.log("Content Update")
        this.overview.update()
      })
    })
    .catch(error => {
      console.error(error)
    })
  }
}

/**
* creates a 2D table from a database collection.
*
* @param {string} collection.name - name of collection that will be fetched from database
* @param {Array} properties - properties that are supposed to be table columns header
* @param {function} additionalData - call during update to potentially assign more information to list
*/
class Overview {
  constructor(collection, properties, additionalData) {
    console.log("create " + name + " overview")
    this.sort_order = -1
    this.collection = collection

    this.additionalData = additionalData
    
    this.options = {}//options
    this.properties = properties
    this.header = []
    
    this.sorted_by = ""
    
    this.$table = $('<table class="collection_list" id="' + this.collection.name + '_overview"></table>')
  }
  
  /**
  * fetch sorted list from database.
  * toggle sort order.
  *
  * @param {string} sort_by - property based on which the list will be sorted
  */
  update(sort_by) {
    return new Promise((resolve, reject) => {
      this.$table.empty()
      if(!sort_by) {
        sort_by = this.sorted_by
      }
      if (sort_by != this.sorted_by) {
        this.sort_order = -1
        this.sorted_by = sort_by
      } 
      let query = {'sort':{}}
      query.sort[sort_by] = this.sort_order
      
      query[this.collection.name] = {}
      
      socket.get('sorted', query)
      .then(result => {
        if(typeof this.additionalData === 'function') {
          return this.additionalData(result)
        }
        return result
      })
      .then(result => {
        if(result.length) {
          this.header = this.getHeaderList(result)
          this.addHeader(this.header)
          
          for(let row of result) {
            this.addRow(row)
          }
          
          this.$table.appendTo('#content')
          
          if(this.sort_order == 1) {
            this.sort_order = -1
            $('#sort_by_' + sort_by.replace('.','_')).append('<i class="fas fa-arrow-down"></i>')
          } else {
            this.sort_order = 1
            
            $('#sort_by_' + sort_by.replace('.','_')).append('<i class="fas fa-arrow-up"></i>')
          }
          
          return resolve("done")
        } else {
          return resolve("empty")
        }
      })
      .catch(error => {
        return reject(error)
      })
    })
  }
  
  /**
  * add row to table based on document
  */
  addRow(row) {
    let $row = $('<tr class="overview_row" id="' + row._id + '">')
    
    for(let head of this.header) {
      let content = Object.getPath(row, head)
      if(!content) {
        content = ""
      }
      if(typeof content === "object" && !Array.isArray(content)) {
        content = JSON.stringify(content)
      }
      $row.append($('<td>' + content + '</td>'))
    }
    
    for(let option in this.options) {
      $row.append(this.options[option].create(row, $row))
    }
    
    this.$table.append($row)
  }
  
  /**
  * append option to each document in list
  *
  * @param {string} option - Will be part of option id and # reference. Will be displayed with uppercase first letter..
  * @param {function} callback - function that is called when menu option is clicked
  */
  addOption(option, icon, callback) {
    let create = (row, $row) => {
      let $option = $('<td class="icon" title="' + option + ' ' + row.name + '"><i class="fa ' + icon + ' tableIcon"></i></td>')
      
      $option.on('click', e => {
        callback(row)
      })
      
      return $option
    }
    
    this.options[option] = {create:create}
  }
  
  /**
  * add a delete button to each row that calls the collecitons removeDocument function with the documents name and _id.
  */
  addDeleteOption() {
    this.addOption("delete", "fa-times", row => {
      console.log(row)
      this.collection.removeDocument(row.name, row._id)
      .then(result => {
        if(result) {
          this.update()
        }
      })
      .catch(error => {
        console.error(error)
      })
    })
  }
  
  /**
   * open document on click on row.
   * 
   * adds an open button to each row that opens the selected document
   * 
   * @param {String} title - hash name for opening document (e.g. #open or #edit)
   * @param {String} icon - fontawesome icon name for buton (e.g. fa-wrench)
   */
  addOpenDocumentOption(title, icon) {
    let create = (row, $row) => {
      let $open = $('<td class="icon" title="' + title + ' ' + row.name + '"><i class="fa ' + icon + ' tableIcon"></i></td>')
      
      routing.add({path:this.collection.name + "/" + row.name, hash:title,link:$open})
      routing.add({path:this.collection.name + "/" + row.name, hash:title,link:$row, exclude_icons:true})
      
      return $open
    }
    
    this.options[title] = {create:create}
  }
  
  /**
  * Get a list of visible headers based on the properties that are supposed to be displayed and what
  * properties are actually in the data.
  * sort them based on the overview properties list
  * expand objects on the first level if they appear in the properties list and sort the subproperties by name.
  */
  getHeaderList(data) {
    let header = []
    for(let prop of this.properties) {
      let nested = []
      for(let item of data) {
        if(item.hasOwnProperty(prop)) {
          if(typeof item[prop] === "object" && !Array.isArray(item[prop])) {
            for(let key in item[prop]) {
              if(!nested.includes(prop + "." + key)) {
                nested.push(prop + "." + key)
              }
            }
          }
          if(!header.includes(prop)) {
            header.push(prop)
          }
        }
      }
      if(nested.length) {
        nested.sort()
        header.splice(header.indexOf(prop), 1, ...nested)
      }
    }
    console.log(header)
    return header
  }
  
  /**
  * add selected header and options to table
  */
  addHeader(header) {
    for(let head of header) {
      let $head = $('<th class="overview_column_title" title="' + head + '" id="sort_by_' + head.replace('.','_') + '">' + head.split('.').pop() + '</th>')
      $head.on("click", e => {
        this.update(head)
      })
      this.$table.append($head)
    }
    
    for(let option in this.options) {
      let $option = $('<th class="overview_column_option">' + option + '</th>')
      this.$table.append($option)
    }
  }
}

/**
* Based on plugins collection. Connects to server plugin functions and allows to add, remove etc. plugins.
*
*/
class Plugins extends Collection {
  constructor() {
    super("plugins", {
      schema:{title:"Plugins"},
      overview:{
        header:["name","_id","settings","modified"]
      }
    })
    
    this.overview.addOpenDocumentOption("edit","fa-wrench")
    this.overview.addDeleteOption()
    
    this.addOption("add", "fa-plus", e => {
      console.log("Add Plugin")
      
      var $plugin = $('<select class="swal_form_content">')
      
      socket.get("plugin",{list:"disabled"})
      .then(result => {
        console.log("Available Plugins: " + result)
        var content = document.createElement('div')
        
        for (let option of result) {
          $("<option/>").val(option).text(option).appendTo($plugin)
        }
        
        content.append("Choose Plugin you want to enable")
        $plugin.appendTo(content)

        return Swal("Add Plugin", {
          content: content,
          buttons: {
            cancel: true,
            confirm: true,
          },
        })
      })
      .then(value => {
        if(value && $plugin.val()) {
          return socket.get("plugin",{"add":$plugin.val()})
        }
        return
      })
      .then(value => {
        if(value) { return this.update() }
        return
      })
      .then(value => {
        if(value) { 
          routing.open("/plugins/" + $plugin.val() + "#edit") 
        } else {
          routing.undo()
        }
      })
      .catch(error => {
        console.error(error)
      })
    })
  }
  
  /**
  * pull all plugins and their schemes from server
  * 
  * if plugin has items, add collection to navbar
  *
  * @returns {Promise} "done" when done
  */
  init() {
    return new Promise((resolve, reject) => {
      socket.get('find', { 'plugins': {} })
        .then(plugins => {
          if (plugins.length) {
            for (let plugin of plugins) {
              editor.collections.level.append(plugin.name, plugin.schema)
              this[plugin.name] = []
              if (plugin.schema.hasOwnProperty('collections')) {
                for (let coll in plugin.schema.collections) {
                  this[plugin.name].push(coll)
                  let coll_object = new Collection(coll, {
                    schema:plugin.schema.collections[coll],
                    overview:{
                      header:["name","_id","settings","default","modified"]
                    }
                  })
                  coll_object.addCreateOption()
                  coll_object.addImportOption()
                  coll_object.overview.addOpenDocumentOption("edit","fa-wrench")
                  coll_object.overview.addDeleteOption()
                  
                  editor.collections[coll] = coll_object
                }
              }
            }
          }
          return resolve("done")
        })
        .catch(error => {
          return reject(error)
        })
    })
  }
  
  /**
  * open plugin settings without delete or save_as option
  */
  openDocument(data) {
    return new Promise((resolve, reject) => {
      form.replace(data, 'plugins')
      .then(result => {
        if (result) {

          editor.file_menu.config({delete:false,save_as:false,save:true,download:true})
          editor.clearContent()
          //$('#content').html($('#editor_div'))

          form.load(data.schema.settings, "settings")
          form.show("settings")

        } else {
          routing.undo()
        }
        return resolve("done")
      })
    })
    
  }
  
  /**
  * when removing a plugin, check for collections the plugin inherits and remove them as well
  */
  removeDocument(name, _id) {
    return new Promise((resolve, reject) => {
      super.removeDocument(name, _id)
      .then(result => {
        if(result) {
          if(this[name].length) {
            for (let coll of this[name]) {
              editor.removeCollection(coll)
            }
          }
        }
        return resolve(result)
      })
      .catch(error => {
        return reject(error)
      })
    })
  }
}
