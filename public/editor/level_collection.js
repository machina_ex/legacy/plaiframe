/**
* Creates elements for level editing.
* 
* the Level Collection class provides the basic cue schema. Cue schemas from plugins are merged into it.
*
* The config schema is as well defined in this class.
*
* @file level_collection
* @copyright Lasse Marburg 2020
* @license GPLv3
*/
class Level extends Collection {
  constructor(data) {
    super("level", {
      schema:{title:"Level"}, 
      overview:{
        header:["name","_id","config","instances","modified"]
      }
    })

    /** cue schema */
    this.cue = {
      "title": "Cue",
      "headerTemplate": "{{ i1 }} {{ self.path }} {{ self.name }}",
      "type": "object",
      "id": "a_cue",
      "definitions":{},
      "default":{"color":"teal"},
      "properties": {
        "name": {
          "required":true,
          "type": "string",
          "description": "Cue name",
          "propertyOrder": 1
        },
        "color":{
          "type":"string",
          "enum":["teal","tomato","peru","sapphireblue","pearlypurple","orangeyellow","dimgray"],
          "propertyOrder": 1,
          "required":false
        },
        "path": {
          "required":true,
          "readOnly":true,
          "propertyOrder":2002,
          "type":"array",
          "options":{
            "hidden":true
          },
          "items":{
            "type":"string"
          }
        },
        "comment": {
          "type":"string",
          "propertyOrder": 1
        },
        "view": {
          "type": "object",
          "propertyOrder":2001,
          "readOnly": true,
          "required": false,
          "options": {
            "hidden": true
          },
          "descripton": "settings for jointjs view of cue",
          "properties": {
            "id": {
              "type": "string"
            },
            "position": {
              "type": "object",
              "properties": {
                "x": {
                  "type": "number"
                },
                "y": {
                  "type": "number"
                }
              }
            },
            "style":{
              "type":"string",
              "propertyOrder": 3,
              "descripton":"add css style to the cue to give it the looks"
            }
          }
        }
      }
    }
    
    /** chapter schema */
    this.chapter = {
        "title":"|",
        "type":"array",
        "format":"tabs",
        "definitions":{},
        "items":this.cue
      }
    
    /** level schema */
    this.level = {
      "_id": {
        "type":"string",
        "readonly":true
      },
      "name": {
        "type":"string",
        "required":true
      },
      "chapter": {
        "type":"object",
        //"format":"categories",
        //"basicCategoryTitle": "One",
        "properties":this.chapter
      },
      "config": {
        "type":"object",
        "properties":{
          "arguments":{
            "type":"object",
            "description":"add arguments that should be assigned when launching session of this level.",
            "additionalProperties":{
              "type":"object",
              "properties":{
                "collection":{
                  "type":"string",
                  "description":"source collection of default document"
                },
                "query":{
                  "oneOf":[
                    {
                      "type":"string",
                      "description":"mongo find query to default document(s). E.g. {'name':'document name'}",
                      "required":true
                    },
                    {
                      "type":"object",
                      "description":"mongo find query to default documents(s).",
                      "required":true
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "control": {
        "type":"object",
        "properties": {
          "pages":{
            "type":"object",
            "properties":{},
            "additionalProperties": {
              "type":"array",
              "items":{
                "type":"string"
              }
            }
          }
        }
      }
    }
    
    /** store plugins to embedd their cue properties */
    this.plugins = {}
    
    /** store cues to allow for autocomplete */
    this.cuelist = []
    
    this.$launch = $('<button title="Launch Session" class="option_toggle"><i class="fas fa-rocket"></i></button>')
    this.$control_layout = $('<button title="Open Control Layout" class="option_toggle"><i class="fas fa-edit"></i></button>')
    this.$config = $('<button title="Open Level Config" class="option_toggle"><i class="fas fa-cog"></i></button>')
    this.$table_view = $('<button title="Show List View" class="option_toggle"><i class="fas fa-list"></i></button>')
    this.$graph_view = $('<button title="Show Flow View" class="option_toggle option_selected"><i class="fas fa-project-diagram"></i></button>')
    
    this.addCreateOption()
    this.addImportOption()
    
    this.overview.addOpenDocumentOption("edit","fa-wrench")
    this.overview.addOption("launch", "fa-rocket", row => {
      Sessions.launch(row.name)
    })
    this.overview.addDeleteOption()
  }
  
  /**
  * add plugins cue properties and definitions to level schema
  *
  * replace previous cue properties and definitions
  * 
  * for cue in order to use it in graph editor and for whole chapter in order to use it for table view
  */
  append(plugin, schema) {
    if(this.plugins.hasOwnProperty(plugin)) {
      for(let t_prop in this.plugins[plugin].cue) {
        delete this.cue.properties[t_prop]
      }
      for(let t_def in this.plugins[plugin].definitions) {
        delete this.cue.definitions[t_def]

        delete this.chapter.definitions[t_def]
      }
    }
    
    if(schema.hasOwnProperty("definitions")) {
      Object.assign(this.cue.definitions, schema.definitions)

      Object.assign(this.chapter.definitions, schema.definitions)
    }
    
    Object.assign(this.cue.properties, schema.cue)
    
    this.plugins[plugin] = schema
  }
  
  /**
  * get current cue schema
  */
  getSchema() {
    return this.cue
  }
  
  /**
  * open leveleditor in main window
  *
  * @param {Object} data - json formatted level data
  * @param {string} data.name - name var is required
  * @param {boolean} [is_new=false] - if content is already stored in database.
  */
  openDocument(data, is_new, view) {
    return new Promise((resolve, reject) => {
      if(!data.hasOwnProperty("chapter")) {
        data.chapter = { chapter: [{ name: "START", "path": ["main"] }, {name: "QUIT", "view": {"position": {"x": 200,"y": 200}}, session:[{"quit":true}]}] }
      }
      if(!data.hasOwnProperty("config")) {
        data.config = {}
      }
      if(!data.hasOwnProperty("control")) {
        data.control = { pages: {} }
      }
      
      this.setCues(data.chapter)
      form.replace(data, 'level')
        .then(result => {
          if (result) {

            editor.file_menu.enable()
            editor.clearContent()
            // $('#content').html($('#editor_div'))
            
            //var tabs = form.tabrow('chapter', this.chapter, "graph")
            form.load(this.chapter, "chapter.chapter")
            form.showGraph("chapter.chapter") // GRAPH Edit
            
            //$('#content').prepend(tabs)
            
            if(is_new) {
              this.routeOptionsMenu("/level", data.name)
              
              $("#content").on("save", e => {
                this.routeOptionsMenu("/level/" + data.name, data.name)
              })
              
              form.changed = true
              editor.file_menu.config({delete:false})
            } else {
              this.routeOptionsMenu("/level/" + data.name, data.name)
            }
            
            let $sub_menu = $('<h2></h2>')
            $sub_menu.prepend(this.$graph_view)
            $sub_menu.prepend(this.$table_view)
            $sub_menu.prepend(this.$config)
            $sub_menu.prepend(this.$control_layout)
            $sub_menu.prepend(this.$launch)
            
            $(".option_toggle").removeClass("option_selected")
            this.$graph_view.addClass("option_selected")
            
            setGraphSchema(this.cue)

            var $title = form.editableTitle(data.name, 'div')
            $title.on('edited', function (e, param) {
              form.title = param.new_title
              form.changed = true
            })
            $sub_menu.prepend($title)
            $('#content').prepend($sub_menu)
          } else {
            routing.undo()
          }
          return resolve("done")
        })
    })
  }
  
  /**
  * create click routings for level options
  *
  * - launch: launch a session based on the selected level
  * - control_layout: switch to cue control editing page
  * - config: switch to level config editor
  * - table: switch to table view of level elements
  * - graph: switch to node graphical view of level elements
  */
  routeOptionsMenu(path, name) {
    routing.add({path:path, hash:"launch", link:this.$launch}, e => {
      Sessions.launch(name)
      $(".option_toggle").removeClass("option_selected")
      this.$launch.addClass("option_selected")
    })
    
    routing.add({path:path, hash:"control_layout", link:this.$control_layout}, e => {
      this.showControlLayout()
      $(".option_toggle").removeClass("option_selected")
      this.$control_layout.addClass("option_selected")
    })
    
    routing.add({path:path, hash:"config", link:this.$config}, e => {
      this.showConfig()
      $(".option_toggle").removeClass("option_selected")
      this.$config.addClass("option_selected")
    })

    routing.add({path:path, hash:"table", link:this.$table_view}, e => {
      form.show('chapter.chapter')
      $(".option_toggle").removeClass("option_selected")
      this.$table_view.addClass("option_selected")
    })
    
    routing.add({path:path, hash:"graph", link:this.$graph_view, symbolic:true}, e => {
      form.showGraph('chapter.chapter')
      $(".option_toggle").removeClass("option_selected")
      this.$graph_view.addClass("option_selected")
    })
  }
  
  /**
  * show config of the currently open level
  */
  showConfig() {
    form.load(this.level.config, 'config')
    form.show('config')
  }
  
  /**
  * show control page layout of the currently open level
  */
  showControlLayout() {
    form.load(this.level.control.properties.pages, 'control.pages')
    form.show('control.pages')
  }
  
  setCues(cues) {
    this.cuelist = []

    for(let chapter in cues) {
      for(let cue of cues[chapter]) {
        this.cuelist.push(chapter + '.' + cue.name)
      }
    }
    
    // removed because it conflicts once default cue properties are removed:
    /*
    this.cue.properties.dispatch.items['enum'] = this.cuelist
    this.cue.properties.listen.items.properties.followcue['enum'] = this.cuelist
    this.cue.properties.listen.items.properties.else['enum'] = this.cuelist
    this.level.control.properties.pages.properties = {}
    */
    for(let cue of this.cuelist) {
      this.level.control.properties.pages.properties[cue] = {
        "type":"array",
        "format":"table",
        "items":{
          "type":"string",
          "title":"cue",
          "enum":this.cuelist
        }
      }
    }
  }
  
}
