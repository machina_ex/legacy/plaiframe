/**
* @file Game Overview. Allows to create and manage plai games
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

const Swal = swal;

socket.on('connect', function(data) {
  console.log("get games")
  socket.get('games', {})
  .then(function(result) {
    if(result.length) {
      $('#game_table').children().not('#create').remove()
      for(let game of result) {
        addGame(game)
      }
    }
  })
})

$( document ).ready(function() {
  $('#create').prop('title', 'create game')
  $('#create').click(createGame)
})

/**
* create a game representing square button with edit, control and delete icon
*
* @todo add copy functionality
*
* @param {string} name - game name
*/
function addGame(name) {
  var $game_node = $('<a id="' + name + '" class="square" title="' + name + '" href="/ui/' + name + '/editor"></a>')
  var $title = $('<div class="square_content">' + name + '</div>')
  var $edit = $('<a title="Edit" href="/ui/' + name + '/editor"><i class="fa fa-wrench squareIcon"></i></a>')
  var $ctrl = $('<a title="Control" href="/ui/' + name + '/control/sessions#list"><i class="fa fa-play-circle squareIcon"></i>')
  var $copy = $('<i title="Copy" class="fa fa-copy squareIcon"></i>')
	var $del = $('<i title="Delete" class="fa fa-trash-alt squareIcon"></i>')

  $title.prepend('<br><br>')
  $title.append('<br><br>')
  $title.append($edit)
  $title.append($ctrl)
  $title.append($del)
  $('.squareIcon').fadeTo(1,0)
  $game_node.append($title)
  
  $del.click(function() {
    confirmation("Are you sure you want to remove " + name + "?")
    .then(function(confirmed){
      if(confirmed) {
        socket.get('unload', {"title":name})
        .then(function(deleted) {
          if(deleted) {
            removeGame(name)
          }
        })
      }
    })
    return false
  })

  $copy.click(function() {
    inputPrompt("Enter name for duplicate of " + name, name + "_copy", function(copy_title) {
      if(copy_title) {

      }
    })
  })

  $game_node.mouseenter(function() {
    $('.squareIcon').fadeTo(100,1)
  })
  $game_node.mouseleave(function() {
    $('.squareIcon').fadeTo(100,0)
  })
  $('#game_table').prepend($game_node)
}

/**
* open name input and template dropdown
* create game on confirmation then add game squareIcon
*/
function createGame() {
  var content = document.createElement('div')
  var $name = $('<input class="swal_form_content" value="new_game">')

  var $template = $('<select title="Template" class="swal_form_content">')
  for (let option of ["basic","installation","multiplayer","open-world"]) {
    $("<option/>").val(option).text(option).appendTo($template)
  }
  
  content.append("Set the new games name")
  $name.appendTo(content)
  content.append("Choose a template.")
  $template.appendTo(content)

  Swal({
    content: content,
    className:'swal_autosize',
    text:"Create Game",
    buttons: {
      cancel: true,
      confirm: true,
    },
  })
  .then((value) => {
    if(value && $name.val() && $template.val()) {
      socket.get('create', {"title":$name.val(),"template":$template.val()})
      .then(function(result) {
        console.log($name.val() + " " + result)
        addGame($name.val())
      })
    }
  })
}

/**
* remove game squareIcon
*
* @param {string} name - game name
*/
function removeGame(name) {
  $('#' + name).remove()
  console.log(name + " removed")
}
