/**
* @file create socket connection
* defines collection of functions to communicate with server application
* socket is a global variable so hat there is only one connection per session
* @copyright Lasse Marburg 2020
* @license GPLv3
*
*/


//var socket = io.connect(location.hostname + ":" + location.port)

/** determine namespace name for spcket connection
* @global
*/
namespace = window.location.pathname.slice(0, window.location.pathname.indexOf(page_name) + page_name.length)

/** connect to node app
* @namespace
*/
socket = io(namespace, { query: `browser=${identifyBrowser()}`}) // &user=${localStorage.user}

console.log('socket connecting to namespace: ' + namespace)

socket.getNamespace = function(page_name) {
	return window.location.pathname.slice(0, window.location.pathname.indexOf(page_name) + page_name.length)
}

/**
* display alert
*/
socket.on('alert',function(data, uuid) {
	if(data && uuid) {
		Swal(data)
		.then(result => {
			console.log("alert returns: " + result + " to " + uuid)
			let ret = {
				data: result,
				id: uuid
			}
			socket.emit('alert_response', ret)
		})
	}
})

/**
* show warning on disconnect
*/
socket.on('disconnect', function(data){
	Swal({
		icon:"warning",
		text:"Server disconnected. Consider downloading unsaved data"
	})
})

/**
*
* on connection established
* - close disconnect warning
* - add functions to socket variable
*/
socket.on('connect', function(data){
	console.log("server connected")

	if(Swal.getState().isOpen) {
		Swal.close()
	}
})

socket.command = function(key, property) {
	return new Promise(function(resolve, reject) {
		socket.get(key, property)
		.then(function(response) {
			if(response) {
				resolve(true)
			} else {
				reject(response)
			}
		})
	})
}

/**
* show alert when client not connected
*
* @returns {boolean} true if client connected, false if not. (same as socket.connected)
*/
socket.is_connected = function(cmd) {
	if(!socket.connected) {
		Swal({
			title: "Can't " + cmd,
			text: "no connection to server!",
			icon: "warning",
			buttons: false
		})
	}
	return socket.connected
}

/**
* example: call find method on database collection 'level' and query a name
* `get('find',{'level':{'name':'somename'}})`
*
* @param {string} key - address where or what to get stuff from
* @param {Object} property - specify your request
*
* @returns {Promise} response data. Will in case return empty data!
*/
socket.get = function (key, property) {
	return new Promise(function(resolve, reject) {
		if(!socket.is_connected('get data')){reject()}
		let uuid = generateUUID()
		let send = {data:property,id:uuid}
		socket.emit(key, send)
		socket.on(uuid, function(data) {
			if(data) {
				resolve(data)
			} else {
				console.warn("Empty return value for '" + key + " "+ property + "'")
				resolve(data)
			}
			socket.removeAllListeners(key)
		})
	})
}

/**
* call update function on database content
*
* @param {string} collection - database collection (view or table) to change
* @param {Object} query - query to address database document
* @param {Object} data - dataset to replace document(s) with
*/
socket.update = function(collection, query, data) {
	return new Promise(function(resolve, reject) {
		let send = {id:generateUUID()}
		let d = new Date()
		//data["modified"] = new Date(Date.now())// d.toUTCString()
		send['collection'] = collection
		send['query'] = query
		send['data'] = data
		console.log(send)
		socket.emit('replace', send)
		socket.on(send.id, function(result) {
			resolve(result)
		})
	})
	
}

/**
* call insert function on database content
*
* @param {string} collection - database collection (view or table) to change
* @param {Object} data - dataset to replace document(s) with
*/
socket.insert = function(collection, data) {
	return new Promise(function(resolve, reject) {
		let send = {id:generateUUID()}
		send['collection'] = collection
		send['data'] = data
		console.log(send)
		socket.emit('insert', send)
		socket.on(send.id, function(result) {
			console.log("inserted:")
			console.log(result)
			return resolve(result)
		})
	})
	
}

/**
* call slice/delete function on database content
*
* @param {string} collection - database collection (view or table) to change
* @param {Object} query - query to address document to be removed
*/
socket.remove = function(collection, query) {
	return new Promise((resolve, reject) => {
		let send = {id:generateUUID()}
		send['collection'] = collection
		send['query'] = query
		console.log(send)
		socket.emit('remove', send)
		socket.on(send.id, function(result) {
			console.log("removed:")
			console.log(result)
			return resolve(result)
		})
	})
	
}

/**
* write documents (back) to database
* to replace a database entry it is dependend on _id property
*
* @param {Object} data - docuemnt to update or insert
* @param {string} data.name - so far data
* @param {string} collection - name of collection to save data to
* @param {string} [docname] - save under a different name
*
* @returns {Promise} true if successful
*/
socket.save = function(save_data, collection, doc_name) {
  return new Promise(function(resolve, reject) {
		if(!socket.is_connected('save file')){reject()}
		save_data["modified"] = new Date(Date.now())// d.toUTCString()
		let data = Object.assign({}, save_data)
    console.log("save:")
    console.log(data)
    if(doc_name) {
      data['name'] = doc_name
      socket.insert(collection, data)
			.then(function(result) {
				resolve( socket.saveResult(data, result) )
			})
    } else if (!data._id) {
      socket.insert(collection, data)
			.then(function(result) {
				resolve( socket.saveResult(data, result) )
			})
    } else {
      socket.update(collection, {'_id':data._id}, data)
			.then(function(result) {
				resolve( socket.saveResult(data, result) )
			})
    }
  });
}

/**
* check for errors in save result
* calls success or fail function accordingly
*
* @param {Object} data - data set that was tried to be stored
* @param {Object} result - result that was returned by server on save process
*
* @returns {*} false if there was an error in the result, else the result object itself
*/
socket.saveResult = function(data, result) {
	if(result.hasOwnProperty('writeError')) {
		socket.saveFailed(data, result.writeErrors)
		return false
	} else if (result.hasOwnProperty('writeConcernError')) {
		socket.saveFailed(data, result.writeConcernError)
		return false
	} else {
		socket.saveSuccessfull(data)
		return result
	}
}

/**
* show save was successful alert
*
* @param {Object} data - alert shows data.name if any
*/
socket.saveSuccessfull = function(data) {
	let title = ''
	if(data.hasOwnProperty('name')) {
		title = data.name
	} else {
		title = 'unnamed file'
	}

	Swal({
		text: title + " saved",
		icon: "success",
		buttons: false,
		timer: 1000
	})
}

/**
* show save failed alert with error message
*
* @param {Object} data - alert shows data.name if any
* @param {string} error - error message
*/
socket.saveFailed = function(data, error) {
	let name = ''
	if(data.hasOwnProperty('name')) {
		name = data.name
	} else {
		name = 'unnamed file'
	}
	
	Swal({
		title: "Failed to save " + name,
		text: error,
		icon: "warning",
		buttons: false
	})
}
