/**
* @file url_routing
* organise routing based on url
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

/**
* allows to store reactions that are triggered on url changes.
* callbacks are stored sorted using the subaddress they are bound to
*
* uses window popstate event to listen for url changes
*
* holds its own history to allow to go back to previous pages even if only the hash changed
*
* @param {string} base_url - subaddress under which the Routing instance starts
*/
class Routing {
  constructor(base_url) {
    /** local history */
    this.history = []
    
    /** stores all callbacks that where registered under a certain path */
    this.routes = {}
    
    this.base_url = base_url
    console.log("Address routing enabled for " + base_url)
    
    /*
    $(window).on('hashchange', () => {
      console.log("hashchange")
      this.review()
    })
    */
    
    $(window).on('popstate', (e) => {
      console.log("popstate event")
      console.log(e)
      this.review()
    })
  }
  
  /**
  * Store a new callback under a specific path and/or hash or add hashes and link elements to existing callbacks.
  * 
  * you may connect the path and/or hash to a DOM element.  
  * for `<a>` elements href property is added.  
  * for all others a click handler is added to the element.
  *
  * If you provide different callbacks for hash and path, they are called successively if url includes the hash. The hash callback will only be run once the path callback returns. The callback can also return a promise.
  * 
  * Add `symbolic:true` to args if you want to prevent the hash callback from being run after path callback. For example if they do the same thing.
  * See review {@link Routing#review} function for details.
  *
  * @param {Object} args - specify what routing to add
  * @param {string} args.path - subadress that will be routed
  * @param {string} [args.hash] - using hash, callback and link will be bound to hash and path
  * @param {Object} [args.link] - DOM element that will change the url on click
  * @param {boolean} [args.symbolic=undefined] - if set true will prevent triggering content twice if hash is synonym to address
  * @param {boolean} [args.exclude_icons] - prevent link from firing if there is a button on top that was hit when clicking
  * @param {function} [callback] - if provided creates a new routing. Is called once url matches path and/or hash. May return Promise.
  */
  add(args, callback) {
    let route, hash, path//, routing
    if(args.path && args.hash) {
      if(args.path[0] != '/') {args.path = '/' + args.path}
      if(this.routes.hasOwnProperty(args.path)) {
        if(args.hash[0] != '#') {args.hash = '#' + args.hash}
        route = args.path + args.hash
        path = args.path
        hash = args.hash
      } else {
        console.error("Can't add hash " + args.hash + " to path " + args.path + ". Path was not added to Routing.")
      }
    } else if (args.path) {
      if(args.path[0] != '/') {args.path = '/' + args.path}
      route = args.path
      path = args.path
    } else if(args.hash) {
      if(args.hash[0] != '#') {args.hash = '#' + args.hash}
      route = args.hash
      hash = args.hash
      path = "/"
    } else {
      if(args[0] != '/') {args = '/' + args}
      route = args
      path = args
    }
    
    if(!this.routes.hasOwnProperty(path)) {
      this.routes[path] = {hash:{}}
    }
    
    if(typeof callback === "function") {
      if(hash) {
        if(this.routes[path].hash.hasOwnProperty(hash)) {
          this.routes[path].hash[hash]["call"] = callback
        } else {
          this.routes[path].hash[hash] = {call: callback}
        }
        if(args.symbolic) {
          this.routes[path].hash[hash]["symbolic"] = args.symbolic
        }
      } else {
        this.routes[path]["call"] = callback
      }
    } else if (this.routes.hasOwnProperty(path)) {
      if(hash) {
        if(this.routes[path].hash.hasOwnProperty(hash)) {
          callback = this.routes[path].hash[hash].call
        }
      } else if(this.routes[path].hasOwnProperty("call")) {
        callback = this.routes[path].call
      }
    }
    
    if(args.hasOwnProperty("link")) { 
      if(args.link.is("a")) {
        args.link.attr("href", this.base_url + route)
      } else {
        args.link.click(e => {
          return new Promise((resolve, reject) => {
            if(args.exclude_icons) {
              if ($(e.target).is('.icon') || $(e.target).is('i')) {
                e.preventDefault();
                return;
              }
            }
            this.show(route)
            this.record()
            if(typeof callback === "function") {
              return Promise.resolve(callback(e))
            } else {
              console.error("no callback bound to link element with route to " + route)
            }
          })
        })
      }
    }
  }
  
  /**
  * show and call a url.  
  * basically window.open relative to base_url
  *
  * @param {string} url - url to be opened
  * @param {string} [target="_self"] - in own window or new window ...
  */
  open(url, target) {
    if(!target) {target = "_self"}
    window.open(this.base_url + url, target)
  }
  
  /**
  * display a url without triggering any callback that was associated with {@link Routing#add}.  
  * yet store url in browser history
  *
  * @param {string} args.path - url path to show
  * @param {Object} args.hash - hash to show
  */
  show(args) {
    let url = ""
    if(args.path && args.hash) { 
      if(args.hash[0] != '#') {args.hash = '#' + args.hash}
      if(args.path[0] != '/') {args.path = '/' + args.path}
      url = args.path + args.hash
    } else if (args.path) {
      if(args.path[0] != '/') {args.path = '/' + args.path}
      url = args.path
    } else {
      if(args[0] != '/') {args = '/' + args}
      url = args
    }
    history.pushState({base_url:this.base_url}, "", this.base_url + url)
  }
  
  /**
  * check url and, if any, runs callbacks associated with url path and/or hash.
  * 
  * if there is a callback for both path and hash, path callback runs first until it returns. Then hash callback follows except it's symbolic.
  * 
  * records url to history
  */
  review() {
    this.record()
    
    for(let route in this.routes) {
      let path = this.base_url + route.replace(/ /g, '%20')
      
      if(path == window.location.pathname) {
        if(typeof this.routes[route]["call"] === 'function') {
          Promise.resolve(this.routes[route]["call"]({target:this.routes[route]["link"]}))
          .then(result => {
            if(this.routes[route]["hash"].hasOwnProperty(window.location.hash)) {
              if(this.routes[route]["hash"][window.location.hash].hasOwnProperty("call") && !this.routes[route]["hash"][window.location.hash]["symbolic"]) {
                this.routes[route]["hash"][window.location.hash]["call"]({target:this.routes[route]["hash"][window.location.hash]["link"]})
              }
            }
          })
          .catch(error => {
            console.error(error)
          })
        } else if(this.routes[route]["hash"].hasOwnProperty(window.location.hash)) {
          if(this.routes[route]["hash"][window.location.hash].hasOwnProperty("call")) {
            this.routes[route]["hash"][window.location.hash]["call"]({target:this.routes[route]["hash"][window.location.hash]["link"]})
          }
        } else {
          console.error("no valid functio assigned to " + route)
        }
      }
    }
  }
  
  /**
  * jump to previous url
  */
  undo() {
    console.log("undo")
    console.log(this.history)
    if(this.history.length > 1) {
      this.history.pop()
      this.show(this.history[this.history.length-1])
    } else {
      window.history.back()
    }
  }
  
  /**
  * store current url in local history list
  */
  record() {
    let window_path = window.location.pathname.substring(window.location.pathname.indexOf(this.base_url) + this.base_url.length)
    this.history.push(window_path + window.location.hash)
    console.log("history push: " + window_path + window.location.hash)
  }
  
}
