/** 
 * @file config
 * @copyright Benedikt Kaffai 2020
 * @license GPLv3
 */

class gLevel {
    constructor(options, graph) {
        this.name = options.name;
        this.schema = options.schema;
        this.cues = [];
        this.graph = graph;
    }
    print() {
        console.log(this.cues);
    }
    export() {
        this.updatePath();

        let file = {
            "name": "registration",
            "chapter": { "chapter": [] },
            "config": {
                "player": [
                    {
                        "name": "Player"
                    }
                ]
            }
        };
        this.cues.forEach(cue => { file.chapter.chapter.push(cue.getData()); });
        return JSON.stringify(file, null, 2);
    }
    getValues() {
        let values = []
        this.cues.forEach(cue => { values.push(cue.getData()); });
        return values
    }

    // this gets called, when flow view is first loaded
    import(level) {      
        this.graph.clear();

        this.name = level.name;
        this.cues = [];
        level.forEach(data => {
            let position = {
                x: parseInt(50),
                y: parseInt(50)
            };
            if (data.hasOwnProperty('view')) {
                if (data.view.hasOwnProperty('position')) {
                    position = {
                        x: parseInt(data.view.position.x),
                        y: parseInt(data.view.position.y)
                    };
                }
            }
            // console.log(`GRAPH: erstelle den Cue: ${data.name}`);
            let cue = new Cue(data, position);
            this.cues.push(cue);
            cue.create();
        });

        // console.log(`GRAPH: zeichne alle ${this.cues.length} Cues `);
        const actionsCells = [].concat(...this.cues.map(c => c.getActionsCells()));
        const cueCells = this.cues.map(c => c.cell);
        graph.addCells(actionsCells.concat(cueCells));
        this.cues.forEach(c => { c.cell.fitEmbeds(c.style) });

        // console.log(`GRAPH: zeichne Verbindungen `);
        this.renderLinks()
        this.updatePath();
    }
    // cue data, position of cue and bool if path for level should be updated
    addCue(data, position) {
        let cue = new Cue(data, position);
        this.cues.push(cue);
        cue.create();
        cue.render();
        this.linkCue(cue.name);
        this.updatePath();
        return cue;
    }
    /**
    * creates all links from and to this cue 
    * if targets of outgoing links don't exist they are skipped
    */
   renderLinks(){
        let cells = [].concat(...this.cues.map(currentCue => {
            return currentCue.getActionsCells().filter(cell => {
                for(let item of cell.attributes.ports.items){                    
                    if (item.group == 'next') return true
                }
                return false;                
            })
        }));
        let links = [];         
        cells.forEach(cell => {
            const ports = cell.attributes.ports.items;
            for (let port of ports) {
                if (port.group == 'next') {
                    let target = this.getCue(port.attrs.info.target);
                    if (target) {
                        let link = new joint.shapes.standard.Link({
                            source: { id: cell.id, port: port.id },
                            target: { id: target.cell.id, port: 'trigger' },
                            router: {
                                name: 'manhattan',
                                args: {
                                    startDirections: ['right'],
                                    endDirections: ['left'],
                                    // step: 10,
                                    // maximumLoops: 200
                                }
                            },
                            connector: { name: 'rounded', radius: 5 },
                            attrs: { line: { stroke: `rgba(85,98,112, .6)` } }
                        });
                        links.push(link);                
                    }
                }
            }
        });
        this.graph.addCells(links);
   }
    linkCue(cueName){
        // console.log(`GRAPH: render ${cueName} links started`);
        // get all cells from other cues connected to this cue and this cues cells
        let cells = [].concat(...this.cues.map(currentCue => {
            return currentCue.getActionsCells().filter(cell => {
                for(let item of cell.attributes.ports.items){
                    if (currentCue.name === cueName || 
                        item.attrs.info.target === cueName) return true
                }
                return false;                
            })
        }));
       
        cells.forEach(cell => {
            const ports = cell.attributes.ports.items;
            for (let port of ports) {
                if (port.group == 'next') {
                    let target = this.getCue(port.attrs.info.target);
                    if (target) {
                        let link = new joint.shapes.standard.Link({
                            source: { id: cell.id, port: port.id },
                            target: { id: target.cell.id, port: 'trigger' },
                            router: {
                                name: 'manhattan',
                                args: {
                                    startDirections: ['right'],
                                    endDirections: ['left'],
                                    // step: 10,
                                    // maximumLoops: 200
                                }
                            },
                            connector: { name: 'rounded', radius: 5 },
                            attrs: { line: { stroke: `rgba(85,98,112, .6)` } }
                        });
                        // link.addTo(this.graph);
                        this.graph.addCell(link)                
                    }
                }
            }
        });
        // console.log(`GRAPH: render ${cueName} links finished`);
    }
    removeLinks() {
        this.graph.getLinks().forEach(link => {
            this.graph.getCell(link.id).remove();
        })
    }
    removeCue(name) {
        let index = this.cues.findIndex(cue => cue.name == name);
        let cue = this.cues[index];
        graph.getCell(cue.cell.id).remove()
        this.cues.splice(index, 1);
        this.updatePath();
    }
    getCue(name) {
        for (let cue of this.cues) {
            if (cue.name.toUpperCase() == name.toUpperCase()) {
                return cue;
            }
        }
        return false;
    }
    getCueByKey(keyString, value) {
        for (let cue of this.cues) {
            let keyArray = keyString.split('.');
            let result = cue;
            for (let key of keyArray) {
                try {
                    result = result[key];
                } catch (error) {
                    break;
                }
            }
            if (result == value) return cue;
        }
        return false;
    }
    /*
    Die Pfad listen der Nodes entwickeln sich über folgende Regeln.

    a) Der erste Node (START) hat einen pfad eintrag in der Liste (main)
    b) Nodes erben jeweils die Pfad Liste des Vorangehenden nodes, der per next angeschlossen ist
    c) Hat ein Node mehrere Vorgänger erhält er die zusammengeführten Pfad listen der Vorgänger. Gleiche einträge werden zusammengefasst, sodass es keine multiplikate in der Liste gibt (merge)
    d) Wenn eine kürzere Liste der Vorgänger Nodes in einer längeren Liste eines Vorgänger nodes enthalten ist, wird die längere Liste dem Node Pfad nicht hinzugefügt. Das verhindert rekursive Pfade, wenn Nodes auf vorgänger nodes weisen.
    e) Enthält ein Node join, wird die Pfad Liste des node der beendet werden soll in die Pfadliste aufgenommen wie in c).
    f) Enthält ein vorangehender Node split, wird ein neuer, bisher ungenutzter Eintrag zur Pfad liste hinzugefügt. Ggf. kann der Eintrag in split spezifiziert werden.
    */
    updatePath(parent=this.getCue('START'), visited = []) {
        if (parent.name === 'START') {
            this.cues.forEach(c => c.data.path = []); // remove all old paths
            this.getCue('START').data.path = ['main']; // Regel a)
        }

        if (!parent) return console.error("no parent to update path");

        // check if current cue (parent) has "join" or "split" and do specific actions
        for (const item in parent.data.session) {
            const property = parent.data.session[item];
            
            if (property.hasOwnProperty("join"))
                parent.addPath(property["join"]); // Regel e)

            if (property.hasOwnProperty("split")) {
                if (Array.isArray(property["split"])) { // 
                    property["split"].forEach(s => {
                        const child = this.getCue(s.next);
                        if (child) child.addPath(s.name); // Regel b)
                    })
                }
                else { // else block follows the old schema and could be removed
                const child = this.getCue(property["split"].next);
                if (child) child.addPath(property["split"].name); // Regel b)
                }
            }

        }

        // from parent walk through every child and its children and transmit paths
        const ancestors = [...visited]; // Teil von Regel d)
        for (const name of [...new Set(Cue.getNext(parent.data, "next"))]) {
            visited = [...ancestors]; // Teil von Regel d). overwrite visited when we jump back from children of child
            const child = this.getCue(name);
            if (child && child != parent && !ancestors.includes(name)) { // Regel d)
                child.addPath(parent.data.path); // Regel c)
                visited.push(name);
                this.updatePath(child, visited);
            }
        }

    }

}