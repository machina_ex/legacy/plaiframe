/** 
 * @file view
 * @copyright Benedikt Kaffai 2020
 * @license GPLv3
 */

// register custom element definitions under name 'plaiframe'
joint.shapes.plaiframe = {};

joint.shapes.plaiframe.CUE = joint.shapes.standard.HeaderedRectangle.extend(
    {
        defaults: joint.util.deepSupplement(
            {
                type: 'plaiframe.Cue',
                size: { width: 100, height: 100 },
                attrs: {
                    body: {
                        fill: 'rgba(255, 255, 255, 0.02)',
                        stroke: "rgba(50, 195, 185, .9)",
                        strokeWidth: 1.5
                    },
                    header: {
                        // fill: 'rgba(48, 208, 198, 0.7)',
                        fill: "rgba(50, 195, 185, .9)",
                        stroke: "rgba(50, 195, 185, .9)",
                        strokeWidth: 1.5
                    },
                    headerText: { text: 'CUENAME', fontWeight: 'bold' },
                    bodyText: { text: '' },

                },
                ports: {
                    groups: {
                        trigger: {
                            z: 0,
                            position: 'left',
                            markup: [{
                                tagName: 'circle',
                                selector: 'port'
                            }],
                            attrs: {
                                text: { fill: 'black' },
                                port: {
                                    magnet: 'passive',
                                    fill: 'white',
                                    stroke: 'black',
                                    strokeWidth: 1,
                                    r: 5,
                                    // width: 40,
                                    // height: 20,
                                    // x: -20,
                                    // y: -10
                                }
                            }
                        }
                    },
                    items: [
                        {
                            id: 'trigger',
                            group: 'trigger',
                        }
                    ]
                },
            },
            joint.shapes.standard.HeaderedRectangle.prototype.defaults)
    }
);

joint.shapes.plaiframe.ACTION = joint.shapes.standard.HeaderedRectangle.extend(
    {
        defaults: joint.util.deepSupplement(
            {
                type: 'plaiframe.Action',
                size: { width: 275, height: 125 },
                attrs: {
                    body: {
                        fill: `rgba(250, 253, 253, .3)`,
                        stroke: `rgba(85,98,112, .6)`,
                        strokeWidth: .5
                    },
                    header: {
                        // fill: 'white',
                        fill: `rgba(241, 243, 244, .9)`,
                        stroke: `rgba(85,98,112, .6)`,
                        strokeWidth: .5
                    },
                    headerText: { text: 'ACTIONTYPE', fill:`rgba(44, 46, 46, 1)` },
                    bodyText: { text: '', fill:`rgba(44, 46, 46, 1)`   }
                },
                ports: {
                    groups: {
                        next: {
                            z: 0,
                            position: 'right',
                            markup: [{
                                tagName: 'circle',
                                selector: 'port'
                            }],
                            label: {
                                position: {
                                    name: 'outside'
                                }
                            },
                            attrs: {
                                text: { fill: 'black', text: '' },
                                port: {
                                    magnet: 'passive',
                                    fill: 'white',
                                    stroke: `rgba(85,98,112, .6)`,
                                    strokeWidth: 1,
                                    r: 5,
                                }
                            }
                        }
                    },
                    items: []
                },
            },
            joint.shapes.standard.HeaderedRectangle.prototype.defaults)
    }
);

joint.shapes.plaiframe.COMMENT = joint.shapes.standard.HeaderedRectangle.extend(
    {
        defaults: joint.util.deepSupplement(
            {
                type: 'plaiframe.COMMENT',
                size: { width: 275, height: 125 },
                attrs: {
                    body: {
                        fill: `rgba(240, 240, 240, .3)`,
                        stroke: `rgba(85,98,112, .6)`,
                        strokeWidth: .5
                    },
                    header: {
                        fill: `rgba(85,98,112, .3)`,
                        stroke: `rgba(85,98,112, .3)`,
                        strokeWidth: .5
                    },
                    headerText: { text: 'KOMMENTAR', fontWeight: 'bold' },
                    bodyText: {  text: '', fontSize: 14}
                },
                ports: {
                    groups: {
                    },
                    items: [
                    ]
                },
            },
            joint.shapes.standard.HeaderedRectangle.prototype.defaults)
    }
);

joint.shapes.plaiframe.TIMER = joint.shapes.standard.HeaderedRectangle.extend(
    {
        defaults: joint.util.deepSupplement(
            {
                type: 'plaiframe.TIMER',
                size: { width: 275, height: 30 },
                attrs: {
                    body: {
                        fill: `rgba(250, 253, 253, .3)`,
                        stroke: `rgba(85,98,112, .6)`,
                        strokeWidth: .5
                    },
                    header: {
                        fill: `rgba(241, 243, 244, .9)`,
                        stroke: `rgba(85,98,112, .6)`,
                        strokeWidth: .5
                    },
                    headerText: { text: '' },
                    bodyText: { text: '' }
                },
                ports: {
                    groups: {
                        next: {
                            z: 0,
                            position: 'right',
                            markup: [{
                                tagName: 'circle',
                                selector: 'port'
                            }],
                            label: {
                                position: {
                                    name: 'outside'
                                }
                            },
                            attrs: {
                                text: { fill: `rgba(85,98,112, .6)`, text: '' },
                                port: {
                                    magnet: 'passive',
                                    fill: 'white',
                                    stroke: `rgba(85,98,112, .6)`,
                                    strokeWidth: 1,
                                    r: 5,
                                }
                            }
                        }
                    },
                    items: []
                },
            },
            joint.shapes.standard.HeaderedRectangle.prototype.defaults)
    }
);