/** 
 * @file plaigraph
 * @copyright Benedikt Kaffai 2020
 * @license GPLv3
 */


////////////////////////////////////////////////////
// Global Variables and Settings
////////////////////////////////////////////////////
const Swal2 = swal;
var plaigraph = {}
console.log("load graphic node UI")

plaigraph.config = gConfig;
plaigraph.form = Object.assign(plaigraph.config.jsoneditor, { schema: plaigraph.config.schema });

function setGraphSchema(schema) {
    console.log(schema)
    plaigraph.form.schema = schema
}

$paper_container = $('<section id="paper-container"></section>')
$paper_mainview = $('<div id="paper-mainview"></div>')
$paper_miniview = $('<div id="paper-miniview"></div>')

$paper_container.append($paper_mainview)
$paper_container.append($paper_miniview)


const graph = new joint.dia.Graph;
const paper = new joint.dia.Paper({
    el: $paper_mainview.get(),  
    model: graph,
    width: plaigraph.config.paper.width,
    height: plaigraph.config.paper.height,
    gridSize: 10,
    drawGrid: true,
    background: {
        color: 'rgb(252, 252, 252)'
    },
    interactive: function (cellView, method) {
        return cellView
    }
});
paper.scale(0.9);
const minimap = new joint.dia.Paper(
    {
        el: $paper_miniview.get(), 
        model: graph,
        width: plaigraph.config.paper.width * plaigraph.config.paper.scaling,
        height: plaigraph.config.paper.height * plaigraph.config.paper.scaling,
        gridSize: 1,
        background: {
            color: 'rgba(150, 150, 150, 0.1)'
        },
        interactive: false
    });
minimap.scale(plaigraph.config.paper.scaling);

plaigraph.level = new gLevel('NEWLEVEL', graph);

// plaigraph.init = function () {
//     plaigraph.config.cues.forEach(cue => {
//         plaigraph.level.addCue(cue.content, cue.position);
//         plaigraph.level.renderLinks();
//     });
// }

plaigraph.lock = function() {
    plaigraph["locked"] = true
}

plaigraph.unlock = function() {
    plaigraph["locked"] = false
}

////////////////////////////////////////////////////
// Events
////////////////////////////////////////////////////

// doubleclick to create or update Cues
paper.on('blank:pointerdblclick', (evt, x, y) => {
    if(plaigraph.locked) {return}
    openForm({ x: x, y: y }, false);
});
paper.on('cell:pointerdblclick', (cell, x, y) => {
    let cue = plaigraph.level.getCueByKey('cell.id', cell.model.id);
    openForm(cue.position, cue);
});


//  update position of cells on pointerup event 
paper.on('element:pointerup', function (cell, evt, x, y) {
    if(plaigraph.locked) {return}
    let cue = plaigraph.level.getCueByKey('cell.id', cell.model.id);
    if (cue) {
        cue.update();
        $paper_container.trigger('change')
    }
});

// events to allow dragging of jointjs canvas 
const papercontainer = $paper_container.get(0) 
paper.on('blank:pointerdown', (evt) => {
    plaigraph.config.dragging.panning = true;
    plaigraph.config.dragging.x = evt.pageX;
    plaigraph.config.dragging.y = evt.pageY;
    $('body').css('cursor', 'move');
});
document.addEventListener('mouseup', (evt) => {
    plaigraph.config.dragging.panning = false;
    $('body').css('cursor', 'default');
}, false);
papercontainer.addEventListener('mousemove', function (evt) {
    if (plaigraph.config.dragging.panning) {
        let $this = $(this);
        let mousePos = plaigraph.config.dragging
        $this.scrollLeft($this.scrollLeft() + mousePos.x - evt.pageX);
        $this.scrollTop($this.scrollTop() + mousePos.y - evt.pageY);
        mousePos.x = evt.pageX;
        mousePos.y = evt.pageY;
    }
}, false);

////////////////////////////////////////////////////
// Callbacks
////////////////////////////////////////////////////

function openForm(position, cue) {
    let editor; // local reference to json editor object
    Swal2.fire({
        onOpen: (div) => {
            document.getElementsByClassName("swal2-actions")[0].style.zIndex = "0"; 
            footer = document.getElementsByClassName("swal2-footer")[0];
            cueDelete = footer.getElementsByTagName("button")[0];
            cueCopy = footer.getElementsByTagName("button")[1];
            cueDelete.onclick = function (event) {
                const cuename = event.target.dataset.cuename;
                // ask before cue/action deletion
                Swal2.fire({
                    type: 'error',
                    title: 'CUE LÖSCHEN',
                    text: 'Willst du diesen Cue wirklich löschen? Oder nur die Änderungen verwerfen.',
                    showCloseButton: true,
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText: 'Cue löschen',
                    cancelButtonText: 'Änderungen verwerfen',
                    customClass: {
                        actions: 'actions-class'
                    },
                }).then((result) => {
                    if (result.value && plaigraph.level.getCue(cuename)) {
                        plaigraph.level.removeCue(cuename);
                        $paper_container.trigger('change') // CHANGE EVENT
                    }
                });
            }
            cueCopy.onclick = function (event) {
                const cuename = event.target.dataset.cuename;
                // ask before cue/action deletion
                Swal2.fire({
                    type: 'question',
                    title: 'CUE KOPIEREN',
                    text: `Willst du den Cue ${cuename} kopieren?`,
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Cue kopieren',
                    // timer: 1000
                    preConfirm: () => {
                        new Promise((resolve, reject) => {
                            const errors = editor.validate();
                            const name = editor.getValue()['name'];
                            if (errors.length) {
                                reject(errors);
                            }
                            else if (plaigraph.level.getCue(name) && cue.name != name) {
                                reject("Cue Name bereits vergeben");
                            }
                            else {
                                resolve();
                            }
                        }).catch(error => {
                            Swal2.showValidationMessage(`Da ist noch ein Fehler im Formular. ${JSON.stringify(error)}`)
                        })
                    }
                }).then((result) => {
                    if (result.value) {
                        const copyPosition = {x: position.x + 50, y: position.y - 50};
                        const cloeCue = plaigraph.level.addCue(Object.assign({}, editor.getValue(), {name: `${editor.getValue()['name']}_COPY`}), copyPosition);
                        $paper_container.trigger('change') // CHANGE EVENT
                    }
                    else if (result.dismiss == "close") {
                        console.log(result);
                    }
                    else {
                        console.log(result);
                    }
                });
            }
            div.onkeydown = function (event) {
                // Press 'F2' to save JSON EDITOR
                if (event.keyCode === 113 || (event.ctrlKey && event.keyCode == 83) || (event.keyCode == 91 && event.keyCode == 83)) {
                    event.preventDefault();
                    Swal2.clickConfirm();
                }
            };
            const container = document.getElementById('form-container');
            editor = new JSONEditor(container, plaigraph.form);
            editor.on('ready', () => {
                if (cue) { editor.setValue(cue.data); }
                if(plaigraph.locked) {editor.disable()}
            });
        },
        html: '<div id="form-container">',
        width: '80%',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: true,
        allowOutsideClick: false,
        allowEnterKey: true,
        confirmButtonText: 'Cue speichern',
        cancelButtonText: 'Eingaben verwerfen',
        footer: `<button style="color:darkred; background-color:white; border:none;" onclick='deleteCue(${cue.name})' data-cuename=${cue.name}>Cue löschen</button> <button style="color:darkgreen; background-color:white; border:none;" onclick='copyCue(${cue.name})' data-cuename=${cue.name}>Cue kopieren</button>`,
        preConfirm: () => {
            new Promise((resolve, reject) => {
                const errors = editor.validate();
                const name = editor.getValue()['name'];
                if (errors.length) {
                    reject(errors);
                }
                else if (plaigraph.level.getCue(name) && cue.name != name) {
                    reject("Cue Name bereits vergeben");
                }
                else {
                    resolve();
                }
            }).catch(error => {
                Swal2.showValidationMessage(`Da ist noch ein Fehler im Formular. ${JSON.stringify(error)}`)
            })
        }
    }).then((result) => {
        if (result.value) {
            // remove original cue on update and render new one
            if (cue) plaigraph.level.removeCue(cue.name);
            plaigraph.level.addCue(editor.getValue(), position);
            $paper_container.trigger('change') // CHANGE EVENT
        }
        else if (result.dismiss == "close") {
            console.log(result);
        }
        else {
            console.log(result);
        }
    });

}