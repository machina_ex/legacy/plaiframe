/** 
 * @file cue
 * @copyright Benedikt Kaffai 2020
 * @license GPLv3
 */

class Cue {
    constructor(data, position) {
        this.name = data.name;
        this.actions = this.getActions(data);
        this.colorTable = 
        {
            teal: "rgba(50, 195, 185, .9)",
            tomato:"rgba(220, 39, 30, .9)",
            peru: "rgba(215, 78, 9, .9)",
            sapphireblue:"rgba(27, 115, 177, .9)",
            pearlypurple: "rgba(178, 108, 150, .9)",
            orangeyellow: "rgba(242, 187, 5, .9)",
            dimgray:"rgba(85,98,112, .6)",
        };
        this.color = "teal",
        this.style = { padding: { top: 40, left: 10, right: 15, bottom: 10 } };
        this.data = data;
        this.cell;
        this.position = position;
    }
    getActions(data) {
        let actions = [];
        Object.keys(data).forEach((key, index) => {
            if (["name", "path", "view", "color"].includes(key))
                return; // ignore this property
            else if (Array.isArray(data[key]) && data[key].length == 0)
                return; // ignore empty actions
            else if (Array.isArray(data[key]) && data[key].length >= 1) {
                data[key].forEach(item => {
                    actions.push(new Action(key, item));
                });
            }
            else
                actions.push(new Action(key, data[key]));
        });
        return actions;
    }
    getActionsCells(){
        return this.actions.map(a => a.cell)
    }
    create() {
        this.cell = new joint.shapes.plaiframe.CUE();
        this.cell.attr('headerText/text', this.name);
        this.cell.attr('name', this.name);
        this.cell.position(this.position.x, this.position.y);
        
        if (this.data.color in this.colorTable) {
            this.cell.attr('header/fill', this.colorTable[this.data.color]);
            this.cell.attr('header/stroke', this.colorTable[this.data.color]);
            this.cell.attr('body/stroke', this.colorTable[this.data.color]);

        }

        this.actions.forEach((action, index) => {
            let yOffset = this.position.y + this.style.padding.top;
            if (index != 0) {
                const before = this.actions[index - 1].cell.attributes;
                yOffset = 10 + (before.position.y + before.size.height);
            }
            let position = {
                x: this.position.x + this.style.padding.left,
                y: yOffset
            };
            action.create(position);
            this.cell.embed(action.cell);
        });
        return this.name
    }
    render(){
        const actionCells = this.actions.map(a => a.cell);
        graph.addCells(actionCells);
        graph.addCell(this.cell);
        this.cell.fitEmbeds(this.style); // change container size based on children
    }
    setPosition(position) {
        this.cell.attributes.position = position;
        this.update();
    }
    update() {
        this.id = this.cell.id;
        this.position = this.cell.attributes.position;
    }
    getData() {
        let view = {
            view: {
                id: this.cell.id,
                position: this.cell.attributes.position
            }
        };
        let data = Object.assign(this.data, view);
        return data;
    }
    /*
    * adds arrays or strings to path list; ignores duplicates
    */
    addPath(path){
        if (Array.isArray(path)) {
            this.data.path = [...new Set([...this.data.path, ...path])];
            return true
        } 
        else if (typeof path === 'string') {
            if (this.data.path.indexOf(path) === -1) {
                this.data.path.push(path);
                return true
            }
        }
        else return false
    }
    static getNext(obj = this.data, keyword = "next", result = []) {
        for (let key in obj) {
            let val = obj[key];
            if (key == keyword) result.push(val);
            if (val !== null && typeof (val) == 'object') {
                Cue.getNext(val, keyword, result); // traverse deeper in obj
            }
        }
        return result
    }
}


class Action {
    constructor(type, data) {
        this.type = type;
        this.ports = this.getPorts(data)
        this.data = data;
        this.cell;
    }
    create(position) {
        let header = "";
        let body = "";
        const data = this.data;

        switch (this.type.toUpperCase()) {
            case 'SMSOUT':
                this.cell = new joint.shapes.plaiframe.ACTION();
                body = data.text;
                header = `SMSOUT to: ${data.to}`;
                break;
            case 'TELEGRAM':
                this.cell = new joint.shapes.plaiframe.ACTION();
                body = data.send.text;
                header = `TELEGRAM to: ${data.send.to}`;
                break;
            case 'SESSION':
                this.cell = new joint.shapes.plaiframe.ACTION();
                header = `SESSION`;
                if ('split' in data)
                    body += `split: ${data.split.name}\n`
                else if ('join' in data)
                    body += `join: ${data.join}\n`
                else if ('next' in data)
                    body += `next: ${data.next}\n`
                else if ('quit' in data)
                    body += `quit session\n`
                else if ('start' in data)
                    body += `start: ${data.start.level} with ${data.start.player}\n`
                else if ('launch' in data)
                body += `launch: ${JSON.stringify(data.launch)}\n`
                else 
                body += `${JSON.stringify(data.launch)}\n`
                break;
            case 'FLOW':
                this.cell = new joint.shapes.plaiframe.ACTION();
                header = `FLOW`;
                body = `Starte Studio Flow: ${data.execute}\nwith: ${data.agent} and ${data.to}`;
                break;
            case 'CALLIN':
                this.cell = new joint.shapes.plaiframe.ACTION();
                header = `CALLIN from ${data.from}`;
                if ('sayplay' in data) {
                    data.sayplay.forEach((obj) => {
                        Object.keys(obj).forEach((key) => {
                            const dots = (obj[key].length > 20) ? '...' : ''
                            body += `${key}: ${obj[key].substring(0, 20)}${dots}\n`
                        });
                    });
                } else if ('flow' in data) {
                    body = `Weiterleitung an Studio Flow:\n${data.flow}\nwith: ${data.agent}`;
                } else if ('reject' in data) {
                    body = `Anruf bei ${data.agent} ablehnen`;
                }
                break;
            case 'TIME':
                this.cell = new joint.shapes.plaiframe.TIMER();
                if (data.hasOwnProperty('timeout'))
                    header = `TIMEOUT: ${data.timeout}`;
                break;
            case 'COMMENT':
                this.cell = new joint.shapes.plaiframe.COMMENT();
                header = 'KOMMENTAR'
                body = data;
                break;
            case 'SMSIN':
                this.cell = new joint.shapes.plaiframe.ACTION();
                let obj = data;
                if ('if' in obj) {
                    if (obj['if'].length == 0) {
                        body += '(if but empty)'
                    }
                    obj['if'].forEach(element => {
                        if ('contains' in element) {
                            body += `cont: ${element.contains} ${('next' in element) ? '' : '(no next)'}\n`
                        } else if ('equals' in element) {
                            body += `== ${element.equals} ${('next' in element) ? '' : '(no next)'}\n`
                        } else if ('query' in element) {
                            body += `query: ${element.query} ${('next' in element) ? '' : '(no next)'}\n`
                        } else if ('regex' in element) {
                            body += `regex: ${element.regex} ${('next' in element) ? '' : '(no next)'}\n`
                        } else {
                            body += '(if but no condition)'
                        }
                    });
                }
                if ('else' in obj) {
                    if (obj['else'] == 0) {
                        body += '(else but empty)'
                    }
                    if ('next' in obj['else']) {
                        body += `else: ${obj.else.next}\n`
                    }
                    else if (!('next' in obj['else'])
                        && !('response' in obj['else'])) {
                        body += `(else: response but no next)`
                    }
                    else {
                        body += '(else but no response & next)'
                    }
                }
                if ((!('if' in obj) && !('else' in obj))) {
                    body = '(no reaction)'
                }
                header = `SMSIN from: ${obj.from}`;
                break;
            case 'STORE':
                this.cell = new joint.shapes.plaiframe.ACTION();
                if ('update' in data) {
                    body += `${data.document}: ${data.update}\n`;
                }
                else if ('set' in data) {
                    body += `in "${data.document}": `;
                    body += JSON.stringify(data['set']);
                }
                header = `STORE`;
                break;
            case 'LISTEN':
                this.cell = new joint.shapes.plaiframe.ACTION();
                if ('if' in this.data) {
                    this.data['if'].forEach(element => {
                        if ('next' in element && 'contains' in element) {
                            body += `${element.value} contains: ${element.contains}\n`
                        } else if ('next' in element && 'equals' in element) {
                            body += `${element.value} == ${element.equals}\n`
                        } else if ('next' in element && 'query' in element) {
                            body += `${element.value} query: ${element.query}\n`
                        } else if ('next' in element && 'regex' in element) {
                            body += `${element.value} regex: ${element.regex}\n`
                        } else if ('next' in element && 'find' in element) {
                            body += `listen for ${element.value} with: ${JSON.stringify(element.find)}\n`
                        } else {
                            body += `${JSON.stringify(element)}\n`
                        }
                    });
                }
                if ('else' in this.data) {
                    if ('next' in this.data['else']) {
                        body += `else: ${this.data.else.next}`
                    }
                }
                header = `LISTEN`;
                break;
            case 'SWITCH':
                this.cell = new joint.shapes.plaiframe.ACTION();
                if ('if' in this.data) {
                    this.data['if'].forEach(element => {
                        if ('next' in element && 'contains' in element) {
                            body += `${element.value} contains: ${element.contains}\n`
                        } else if ('next' in element && 'equals' in element) {
                            body += `${element.value} == ${element.equals}\n`
                        } else if ('next' in element && 'query' in element) {
                            body += `${element.value} query: ${element.query}\n`
                        } else if ('next' in element && 'regex' in element) {
                            body += `${element.value} regex: ${element.regex}\n`
                        } else {
                            body += `${JSON.stringify(element)}\n`
                        }
                    });
                }
                if ('else' in this.data) {
                    if ('next' in this.data['else']) {
                        body += `else: ${this.data.else.next}`
                    }
                }
                header = `SWITCH`;
                break;
            case 'SEND':
                this.cell = new joint.shapes.plaiframe.ACTION();
                const key = Object.keys(data)[0];
                body = `to device "${key}": \n `;
                body += `${JSON.stringify(data[key])}`;
                header = `SEND`;
                break;
            case 'SEND_MESSAGE':
                this.cell = new joint.shapes.plaiframe.ACTION();
                body = `${data.agent}: ${data.text}`;
                header = `TELEGRAM to: ${data.to}`;
                break;
            case 'SEND_FILE':
                this.cell = new joint.shapes.plaiframe.ACTION();
                body = `${data.agent}: ${data.file}`;
                header = `TELEGRAM FILE to: ${data.to}`;
                break;
            case 'DIALOGUE':
                this.cell = new joint.shapes.plaiframe.ACTION();
                if ('if' in data) {
                    if (data['if'].length == 0) {
                        body += '(if but empty)'
                    }
                    data['if'].forEach(element => {
                        if ('contains' in element) {
                            body += `cont: ${element.contains} ${('next' in element) ? '' : '(no next)'}\n`
                        } else if ('equals' in element) {
                            body += `== ${element.equals} ${('next' in element) ? '' : '(no next)'}\n`
                        } else if ('query' in element) {
                            body += `query: ${element.query} ${('next' in element) ? '' : '(no next)'}\n`
                        } else if ('regex' in element) {
                            body += `regex: ${element.regex} ${('next' in element) ? '' : '(no next)'}\n`
                        } else {
                            body += '(if but no condition)'
                        }
                    });
                }
                if ('else' in data) {
                    if (data['else'] == 0) {
                        body += '(else but empty)'
                    }
                    if ('next' in data['else']) {
                        body += `else: ${data.else.next}\n`
                    }
                    else if (!('next' in data['else'])
                        && !('response' in data['else'])) {
                        body += `(else: response but no next)`
                    }
                    else {
                        body += '(else but no response & next)'
                    }
                }
                if ((!('if' in data) && !('else' in data))) {
                    body = '(no reaction)'
                }
                header = `TELEGRAM from: ${data.chat}`;
                break;
            default:
                this.cell = new joint.shapes.plaiframe.ACTION();
                header = this.type.toUpperCase();
                body = JSON.stringify(data);
                break;
        }

        body = joint.util.breakText(body, { width: 275 });
        let lines = body.split('\n')
        if (lines.length > 5) {
            lines.length = 5;
            lines.push('(...)')
        }
        this.cell.attr('bodyText/text', lines.join('\n'));
        this.cell.attr('headerText/text', header.substring(0, 32));
        // this.cell.attr({rect:{style:{'pointer-events':'none'}}});
        this.cell.position(position.x, position.y);
        if (this.type.toUpperCase() != 'TIME') {
            this.cell.resize(275, 37 + (18 * lines.length));
        }
        // canvas.addCell(this.cell);
        this.cell.addPorts(this.ports);
        return this.cell;
    }
    getPorts(data) {
        let ports = [];
        Cue.getNext(data).forEach((next, index) => {
            let port = {
                group: 'next',
                args: {},
                label: {},
                attrs: { text: { text: '' }, info: { target: next } }
            };
            ports.push(port);
        });
        return ports;
    }
}

// module.exports = Cue; // RM
