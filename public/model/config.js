/** 
 * @file config
 * @copyright Benedikt Kaffai 2020
 * @license GPLv3
 */

gConfig = {
    "schema": {
        "title": "Cue",
        "headerTemplate": "{{ self.path }} {{ self.name }}",
        "type": "object",
        "id": "a_cue",
        "properties": {
            "name": {
                "required": true,
                "type": "string",
                "description": "Cue name",
                "propertyOrder": 1
            },
            "path": {
                "required": true,
                "readOnly": true,
                "propertyOrder":2000,
                "type": "array",
                "options": {
                    "hidden": true
                },
                "items": {
                    "type": "string"
                }
            },
            "comment": {
                "type": "string",
                "propertyOrder": 2
            },
            "view": {
                "type": "object",
                "propertyOrder":2001,
                "readOnly": true,
                "required": false,
                "options": {
                    "hidden": true
                },
                "descripton": "settings for jointjs view of cue",
                "properties": {
                    "id": {
                        "type": "string"
                    },
                    "position": {
                        "type": "object",
                        "properties": {
                            "x": {
                                "type": "number"
                            },
                            "y": {
                                "type": "number"
                            }
                        }
                    },
                    "style": {
                        "type": "string",
                        "propertyOrder": 3,
                        "descripton": "add css style to the cue to give it the looks"
                    }
                }
            },
            "smsin": {
                "type": "array",
                "listener": true,
                "items": {
                    "type": "object",
                    "default": {
                        "if": [
                            {}
                        ],
                        "priority": "high",
                        "else": {}
                    },
                    "properties": {
                        "agent": {
                            "type": "string",
                            "required": true
                        },
                        "from": {
                            "type": "string",
                            "required": true
                        },
                        "priority": {
                            "type": "string",
                            "required": true,
                            "enum": [
                                "high",
                                "low"
                            ],
                            "description": "on high, prefer over smsin of other paths"
                        },
                        "if": {
                            "type": "array",
                            "items": {
                                "oneOf": [
                                    {
                                        "title": "Contains",
                                        "description": "Trigger next if sms contains value",
                                        "type": "object",
                                        "additionalProperties": false,
                                        "properties": {
                                            "contains": {
                                                "type": "string",
                                                "required": true
                                            },
                                            "respond": {
                                                "type": "array",
                                                "format": "table",
                                                "description": "Reagiere auf die eingangene sms die die match Bedingung erfüllt mit einer Antwort",
                                                "items": {
                                                    "type": "string",
                                                    "format": "textarea",
                                                    "description": "antwort text",
                                                    "required": false
                                                },
                                                "default": [
                                                    ""
                                                ]
                                            },
                                            "next": {
                                                "required": false,
                                                "descripton": "To trigger a cue in a different chapter, use dot notation",
                                                "type": "string"
                                            }
                                        }
                                    },
                                    {
                                        "title": "Equals",
                                        "description": "Comming Soon! (Trigger next if value equals)",
                                        "type": "object",
                                        "additionalProperties": false,
                                        "properties": {
                                            "equals": {
                                                "type": "string",
                                                "required": true
                                            },
                                            "respond": {
                                                "type": "array",
                                                "format": "table",
                                                "description": "Reagiere auf die eingangene sms die die match Bedingung erfüllt mit einer Antwort",
                                                "items": {
                                                    "type": "string",
                                                    "format": "textarea",
                                                    "description": "antwort text",
                                                    "required": false
                                                },
                                                "default": [
                                                    ""
                                                ]
                                            },
                                            "next": {
                                                "required": false,
                                                "descripton": "To trigger a cue in a different chapter, use dot notation",
                                                "type": "string"
                                            }
                                        }
                                    },
                                    {
                                        "title": "Mongo Query",
                                        "description": "define query that triggers followcue if it returns something",
                                        "type": "object",
                                        "additionalProperties": false,
                                        "properties": {
                                            "query": {
                                                "type": "string",
                                                "required": true
                                            },
                                            "respond": {
                                                "type": "array",
                                                "format": "table",
                                                "description": "Reagiere auf die eingangene sms die die match Bedingung erfüllt mit einer Antwort",
                                                "items": {
                                                    "type": "string",
                                                    "format": "textarea",
                                                    "description": "antwort text",
                                                    "required": false
                                                },
                                                "default": [
                                                    ""
                                                ]
                                            },
                                            "next": {
                                                "required": false,
                                                "cue": "positive",
                                                "descripton": "To trigger a cue in a different chapter, use dot notation",
                                                "type": "string"
                                            }
                                        }
                                    },
                                    {
                                        "title": "Regular Expression",
                                        "description": "define query that triggers followcue if it returns something",
                                        "type": "object",
                                        "additionalProperties": false,
                                        "properties": {
                                            "regex": {
                                                "type": "string",
                                                "required": true
                                            },
                                            "respond": {
                                                "type": "array",
                                                "format": "table",
                                                "description": "Reagiere auf die eingangene sms die die match Bedingung erfüllt mit einer Antwort",
                                                "items": {
                                                    "type": "string",
                                                    "format": "textarea",
                                                    "description": "antwort text",
                                                    "required": false
                                                },
                                                "default": [
                                                    ""
                                                ]
                                            },
                                            "next": {
                                                "required": false,
                                                "descripton": "To trigger a cue in a different chapter, use dot notation",
                                                "type": "string"
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        "else": {
                            "type": "object",
                            "properties": {
                                "next": {
                                    "type": "string",
                                    "descripton": "wird ausgelöst, wenn die letzte response verschickt wird."
                                },
                                "respond": {
                                    "type": "array",
                                    "format": "table",
                                    "description": "Reagiere auf die eingangene sms die die match Bedingung erfüllt mit einer Antwort",
                                    "items": {
                                        "type": "string",
                                        "format": "textarea",
                                        "description": "antwort text",
                                        "required": false
                                    },
                                    "default": [
                                        ""
                                    ]
                                },
                                "loop": {
                                    "type": "number",
                                    "min": 0,
                                    "required": false
                                }
                            }
                        },
                        "timeout": {
                            "type": "object",
                            "properties": {
                                "time": {
                                    "type": "array",
                                    "required": true,
                                    "minItems": 1,
                                    "format": "table",
                                    "items": {
                                        "type": "number",
                                        "minimum": 0
                                    }
                                },
                                "respond": {
                                    "type": "array",
                                    "format": "table",
                                    "description": "Reagiere auf jeden Timeout mit einer Nachricht",
                                    "items": {
                                        "type": "string",
                                        "format": "textarea",
                                        "description": "sms text",
                                        "required": false
                                    },
                                    "default": [
                                        ""
                                    ]
                                },
                                "next": {
                                    "type": "string",
                                    "descripton": "wird ausgelöst, wenn die letzte response verschickt wird."
                                },
                                "loop": {
                                    "type": "number",
                                    "min": 0,
                                    "required": false
                                }
                            },
                            "default": {
                                "time": 0
                            }
                        }
                    }
                }
            },
            "smsout": {
                "type": "array",
                "default": [],
                "items": {
                    "type": "object",
                    "properties": {
                        "agent": {
                            "type": "string",
                            "required": true,
                            "description": "name of agent sending"
                        },
                        "to": {
                            "type": "string",
                            "required": true,
                            "description": "role name of target player"
                        },
                        "text": {
                            "type": "string",
                            "format": "textarea",
                            "required": true
                        },
                        "next": {
                            "type": "string",
                            "required": false
                        }
                    }
                }
            },
            "callin": {
                "type": "array",
                "listener": true,
                "items": {
                    "type": "object",
                    "default": {
                        "agent": "",
                        "from": "",
                        "priority": "high",
                        "flow": ""
                    },
                    "properties": {
                        "agent": {
                            "type": "string",
                            "description": "name of agent listening"
                        },
                        "from": {
                            "type": "string",
                            "description": "role name of target player who is expected to call"
                        },
                        "priority": {
                            "type": "string",
                            "required": true,
                            "enum": [
                                "high",
                                "low"
                            ],
                            "description": "on high, prefer over callin of other paths"
                        },
                        "flow": {
                            "type": "string",
                            "required": true,
                            "description": "Friendly name of Studio Flow that will be executed"
                        }
                    }
                }
            },
            "flow": {
                "type": "array",
                "listener": true,
                "items": {
                    "type": "object",
                    "properties": {
                        "execute": {
                            "type": "string",
                            "required": true,
                            "description": "Friendly name of Studio Flow that will be executed"
                        },
                        "agent": {
                            "type": "string",
                            "required": true
                        },
                        "to": {
                            "type": "string",
                            "required": true
                        },
                        "parameters": {
                            "type": "object",
                            "description": "forward parameters to flow execution. Access them in the flow with {{flow.data.<parameter>}}",
                            "additionalProperties": {
                                "type": "string"
                            }
                        }
                    }
                }
            },
            "switch": {
                "type": "object",
                "default": {
                    "if": [
                        {}
                    ],
                    "else": {}
                },
                "properties": {
                    "if": {
                        "title": "if",
                        "required": true,
                        "type": "array",
                        "items": {
                            "title": "switch condition",
                            "oneOf": [
                                {
                                    "title": "Contains",
                                    "description": "Trigger next if value contains",
                                    "type": "object",
                                    "additionalProperties": false,
                                    "properties": {
                                        "value": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "contains": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "next": {
                                            "required": true,
                                            "descripton": "To trigger a cue in a different chapter, use dot notation",
                                            "type": "string"
                                        }
                                    }
                                },
                                {
                                    "title": "Equals",
                                    "description": "Trigger next if value equals",
                                    "type": "object",
                                    "additionalProperties": false,
                                    "properties": {
                                        "value": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "equals": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "next": {
                                            "required": true,
                                            "type": "string"
                                        }
                                    }
                                },
                                {
                                    "title": "Mongo Query",
                                    "description": "define query that triggers followcue if it returns something",
                                    "type": "object",
                                    "additionalProperties": false,
                                    "properties": {
                                        "query": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "next": {
                                            "required": true,
                                            "cue": "positive",
                                            "descripton": "To trigger a cue in a different chapter, use dot notation",
                                            "type": "string"
                                        }
                                    }
                                },
                                {
                                    "title": "Regular Expression",
                                    "description": "define query that triggers followcue if it returns something",
                                    "type": "object",
                                    "additionalProperties": false,
                                    "properties": {
                                        "value": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "regex": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "next": {
                                            "required": true,
                                            "descripton": "To trigger a cue in a different chapter, use dot notation",
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "else": {
                        "properties": {
                            "next": {
                                "required": true,
                                "descripton": "To trigger a cue in a different chapter, use dot notation",
                                "type": "string"
                            }
                        }
                    }
                }
            },
            "listen": {
                "listener": true,
                "type": "object",
                "default": {
                    "if": [
                        {}
                    ]
                },
                "properties": {
                    "if": {
                        "type": "array",
                        "items": {
                            "title": "if condition",
                            "oneOf": [
                                {
                                    "title": "Contains",
                                    "description": "Trigger next if value contains",
                                    "type": "object",
                                    "additionalProperties": false,
                                    "properties": {
                                        "value": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "contains": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "next": {
                                            "required": true,
                                            "descripton": "To trigger a cue in a different chapter, use dot notation",
                                            "type": "string"
                                        }
                                    }
                                },
                                {
                                    "title": "Equals",
                                    "description": "Trigger next if value equals.",
                                    "type": "object",
                                    "additionalProperties": false,
                                    "properties": {
                                        "value": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "equals": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "next": {
                                            "required": true,
                                            "descripton": "To trigger a cue in a different chapter, use dot notation",
                                            "type": "string"
                                        }
                                    }
                                },
                                {
                                    "title": "Mongo Query",
                                    "description": "define query that triggers followcue if it returns something",
                                    "type": "object",
                                    "additionalProperties": false,
                                    "properties": {
                                        "query": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "next": {
                                            "required": true,
                                            "cue": "positive",
                                            "descripton": "To trigger a cue in a different chapter, use dot notation",
                                            "type": "string"
                                        }
                                    }
                                },
                                {
                                    "title": "Regular Expression",
                                    "description": "define query that triggers followcue if it returns something",
                                    "type": "object",
                                    "additionalProperties": false,
                                    "properties": {
                                        "value": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "regex": {
                                            "type": "string",
                                            "required": true
                                        },
                                        "next": {
                                            "required": true,
                                            "descripton": "To trigger a cue in a different chapter, use dot notation",
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            },
            "session": {
                "type": "array",
                "default": [
                    {}
                ],
                "items": {
                    "type": "object",
                    "properties": {
                        "next": {
                            "type": "string",
                            "description": "dispatch next cue in current session. This will immediatly close all listeners you might install in this cue."
                        },
                        "quit": {
                            "type": "boolean",
                            "enum": [
                                true
                            ],
                            "descripton": "stop this session and cancel all its open listeners"
                        },
                        "split": {
                            "type": "object",
                            "properties": {
                                "name": {
                                    "type": "string",
                                    "description": "name the path so its easier to identify it."
                                },
                                "next": {
                                    "type": "string",
                                    "description": "the initial cue of the new path. Is dispatched immediatly"
                                }
                            }
                        },
                        "start": {
                            "type": "object",
                            "description": "start a new session",
                            "required": [
                                "level"
                            ],
                            "properties": {
                                "level": {
                                    "type": "string",
                                    "description": "name of level the new session is based on"
                                },
                                "name": {
                                    "type": "string",
                                    "description": "name the new session so it can be accessed and identified. Be aware that any other session with the same name will be canceled!"
                                },
                                "reference": {
                                    "type": "string",
                                    "description": "allows you to reference the new session from only inside this level once its started."
                                },
                                "player": {
                                    "required": true,
                                    "descripton": "assign players to the session e.g. forward this sessions player.",
                                    "oneOf": [
                                        {
                                            "type": "string"
                                        },
                                        {
                                            "type": "object",
                                            "properties": {
                                                "_id": {
                                                    "type": "string"
                                                },
                                                "name": {
                                                    "type": "string"
                                                },
                                                "telnumber": {
                                                    "type": "string"
                                                }
                                            }
                                        }
                                    ]
                                },
                                "role": {
                                    "type": "string",
                                    "description": "set a role, the addressed session allows"
                                }
                            }
                        },
                        "cancel": {
                            "type": "string",
                            "description": "Cancel a remote session. Address session by defining a query searching in session collection"
                        },
                        "assign": {
                            "type": "object",
                            "properties": {
                                "session": {
                                    "required": true,
                                    "description": "address session by defining a query searching in session collection",
                                    "oneOf": [
                                        {
                                            "type": "string"
                                        },
                                        {
                                            "type": "object",
                                            "properties": {
                                                "_id": {
                                                    "type": "string"
                                                },
                                                "name": {
                                                    "type": "string"
                                                },
                                                "level": {
                                                    "type": "string"
                                                }
                                            }
                                        }
                                    ]
                                },
                                "player": {
                                    "required": true,
                                    "descripton": "assign players to the session e.g. forward this sessions player.",
                                    "oneOf": [
                                        {
                                            "type": "string"
                                        },
                                        {
                                            "type": "object",
                                            "properties": {
                                                "_id": {
                                                    "type": "string"
                                                },
                                                "name": {
                                                    "type": "string"
                                                },
                                                "telnumber": {
                                                    "type": "string"
                                                }
                                            }
                                        }
                                    ]
                                },
                                "role": {
                                    "type": "string",
                                    "description": "set a role, the addressed session allows"
                                }
                            }
                        }
                    }
                }
            },
            "time": {
                "listener": true,
                "oneOf": [
                    {
                        "type": "object",
                        "title": "Timeout",
                        "description": "Zeit bis next in Sekunden oder: hh:mm:ss",
                        "additionalProperties": false,
                        "properties": {
                            "timeout": {
                                "type": "string",
                                "required": true
                            },
                            "next": {
                                "type": "string",
                                "required": true
                            }
                        }
                    },
                    {
                        "type": "object",
                        "title": "Date Time",
                        "additionalProperties": false,
                        "properties": {
                            "datetime": {
                                "type": "string",
                                "format": "datetime-local",
                                "required": true
                            },
                            "next": {
                                "type": "string",
                                "required": true
                            }
                        }
                    }
                ]
            },
            "store": {
                "type": "array",
                "default": [
                    {
                        "document": ""
                    }
                ],
                "items": {
                    "oneOf": [
                        {
                            "title": "Set Values",
                            "type": "object",
                            "additionalProperties": false,
                            "properties": {
                                "document": {
                                    "type": "string",
                                    "required": true,
                                    "description": "Lokal abspeichern: 'level', query formulieren mit '{}'"
                                },
                                "set": {
                                    "type": "object",
                                    "default": {},
                                    "format": "grid",
                                    "required": true,
                                    "additionalProperties": {
                                        "oneOf": [
                                            {
                                                "type": "string"
                                            },
                                            {
                                                "type": "number"
                                            }
                                        ]
                                    },
                                    "description": "Einträge in Dokument anlegen oder ändern. Verschachtelte Einträge mit '.' notation hinzufügen (add property => deeply.nested.entry). Wert kann [[var]] variablen enthalten (string nutzen)"
                                }
                            }
                        },
                        {
                            "title": "Mongo update",
                            "type": "object",
                            "additionalProperties": false,
                            "properties": {
                                "document": {
                                    "type": "string",
                                    "required": true,
                                    "description": "Lokal abspeichern: 'level', query formulieren mit '{}'"
                                },
                                "update": {
                                    "type": "string",
                                    "default": "{}",
                                    "required": true,
                                    "description": "Update query, z.B.: {$set:{value:'new value'}} oder {'value.one':'[[level.CUENAME.smsin.match]]'} oder {$inc: {score:2}}"
                                }
                            }
                        }
                    ]
                }
            }
        }
    },
    "jsoneditor": {
        "show_errors": "change",
        "disable_collapse": false,
        "disable_edit_json": false,
        "disable_properties": false,
        "remove_empty_properties": true,
        "required_by_default": false,
        "display_required_only": true
    },
    "paper": {
        "width": "10000",
        "height": "3000",
        "scaling": 0.05
    },
    "dragging": {
        "x": 0,
        "y": 0,
        "panning": false
    },
    "cues": [
        {
            "content": {
                "name": "START",
                "path": "main",
                "path": [
                    "main"
                ]
            },
            "position": {
                "x": 50,
                "y": 50
            }
        },
        {
            "content": {
                "name": "END",
                "path": [
                    "main"
                ]
            },
            "position": {
                "x": 2000,
                "y": 50
            }
        }
    ]
}
