/**
* ask user for text input, then call function that handles input
*
* callback is called with text input value
*
* @param {string} msg - what to write above the text input
* @param {string} defaultValue - what text to put in input as default
* @param {function} callback - what function to call with user input
*/
function inputPrompt(msg, defaultValue, callback) {
  var input = document.createElement('input')
  input.defaultValue = defaultValue

  Swal(msg, {
    content: input,
    buttons: {
      cancel: true,
      confirm: true,
    },
  })
  .then((value) => {
    if(value && input.value) {
      callback(input.value)
    } else {
      callback('')
    }
  })
}

/**
* check if user is sure to do what she is asked
* not to be confused with window function confirm() (does similar thing though)
*
* @param {string} msg - will be shown in confirmationn window.
*
* @returns {Promise} result true if user didn't cancel
*/
function confirmation(msg) {
	return new Promise(function(resolve, reject) {
		Swal({
			title:"Proceed?",
			text: msg,
			icon: "warning",
			buttons: true,
		})
		.then(function(confirmed) {
			resolve(confirmed)
		})
	});
}

/**
 * use userAgent to find out what Browser is in use (unreliable)
 * 
 * see https://code-boxx.com/detect-browser-with-javascript/
 */
function identifyBrowser() {
    // CHROME
    if (navigator.userAgent.indexOf("Chrome") != -1 ) {
      return("Google Chrome");
    }
    // FIREFOX
    else if (navigator.userAgent.indexOf("Firefox") != -1 ) {
      return("Mozilla Firefox");
    }
    // INTERNET EXPLORER
    else if (navigator.userAgent.indexOf("MSIE") != -1 ) {
      return("Internet Explodrer");
    }
    // EDGE
    else if (navigator.userAgent.indexOf("Edge") != -1 ) {
      return("Internet Explorer");
    }
    // SAFARI
    else if (navigator.userAgent.indexOf("Safari") != -1 ) {
      return("Safari");
    }
    // OPERA
    else if (navigator.userAgent.indexOf("Opera") != -1 ) {
      return("Opera");
    }
    // YANDEX BROWSER
    else if (navigator.userAgent.indexOf("YaBrowser") != -1 ) {
      return("YaBrowser");
    }
    // OTHERS
    else {
      return("Unknown");
    }
}

/**
* Generate Unique Identifier String
*
* Happily copied from
* https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript/8809472#8809472
* (Public Domain/MIT)
*/
function generateUUID() {
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

/**
* @function
* @todo The following two functions are not recognized by JSDoc!!!  
* address object property with '.' seperated string   
* Might be handy especially when working with Mongodb and nested docuemnt find queries.  
* Mongo uses [dot notation]{@link https://docs.mongodb.com/manual/core/document/#document-dot-notation}
* from:
* https://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key/6491621#6491621
*
* @param {Object} o - some Object
* @param {strin} s - some string that finds nested properties with '.' notation
*/
Object.getPath = function(o, s) {
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    var a = s.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
}

/**
* assign object property with '.' seperated string path
*
* from:
* https://stackoverflow.com/questions/13719593/how-to-set-object-property-of-object-property-of-given-its-string-name-in-ja/13719799#13719799
*
* @param {Object} obj - object to assign value to
* @param {string} prop - path
* @param {*} value - value to assign to object at path
*/
Object.assignPath = function(obj, prop, value) {
    if (typeof prop === "string")
        prop = prop.split(".");

    if (prop.length > 1) {
        var e = prop.shift();
        Object.assignPath(obj[e] =
                 Object.prototype.toString.call(obj[e]) === "[object Object]"
                 ? obj[e]
                 : {},
               prop,
               value);
    } else {
			obj[prop[0]] = value;
		}
}
