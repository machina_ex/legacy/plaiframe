Playful Interaction Framework
=============================

* [About](#about)
* [Install](#install)
* [Getting Started](#getting-started)
* [Settings](#settings)
* [Commands](#commands)
* [MongoDB](#mongodb)
* [License and Copyright](#license-and-copyright)

![https://xkcd.com/2054/](https://imgs.xkcd.com/comics/data_pipeline.png)
xkcd.com

About
-------

Plaiframe is the core piece of a set of Tools that are made for and whilst creating Game Theater Projects. With plaiframe you can connect all kinds of hard- and software that might come to play in a game installation. Interactions can be automated in a multilinear, multiplayer setup or be controlled in oldschool theatre cue manner. 

This project consists of a server script that runs processes and manages data and of a webview that enables you to create and manage your Game Installation. The Server is made with the famous [node.js](https://nodejs.org/en/) javascript runtime and several of it's community driven libraries. Webview runs in the latest firefox and chrome browser and connects to the server using [socket.io](https://socket.io/).

In the near future a wiki will tell you in detail how to build game installations with plaiframe. Since the framework itself is under heavy development, it's not yet sufficent.

If you want to dive deeper into how this was built, checkout the documentation. It was created using [jsdocs](https://jsdoc.app/) and is seperated into the [server code documentation](https://machina_ex.gitlab.io/legacy/documentation/plaiframe/), a [client webview code documentation](https://machina_ex.gitlab.io/legacy/documentation/plaiframe/webview) and a collection of all [plugin code documentations](https://machina_ex.gitlab.io/legacy/documentation/plaiframe/plugins).

There are several plugins, that expand what can be build and what software, APIs or hardware may be interconnected. The [plugin readme and documentation](https://gitlab.com/machina_ex/legacy/plaiframe/-/tree/master/plugins) provide a closer look on how plugins are integrated into plaiframe.

The following chapters will help you install plaiframe and maintain it's basic features.

Install
------------

If you haven't already, install the latest version of [nodejs](https://nodejs.org) for your operation system which includes the nodejs package manager npm.

Use [git](https://git-scm.com) to fetch the latest version of plaiframe. Open a terminal (Linux/MacOS), command line window or power shell (Windows) to clone the plaiframe git repository.

Clone with SSH:

`$ git clone git@gitlab.com:plaitools/plaiframe.git`

Or clone with HTTPS:

`$ git clone https://gitlab.com/plaitools/plaiframe.git`

cd into the plaiframe folder:

`$ cd plaiframe/`

then do

`$ npm install`

to install all node.js dependencies.  

> If you install on a server or don't want to use midi, serial or osc plugins, you can ignore error messages addressing these modules.

### install plugins

As for now all plugins are packaged with the plaiframe git repositiory. All node dependencies for all plugins are included with npm install. Some are declared optional so if a plugin is missing a dependency, npm install might have skipped installing it because of an unresolved error.

However the telegram plugin requires you to install python 3. Have a look at the [telegram plugin readme](./plugins/telegram/README.md) for further instructions. 

### install using docker

Clone the repository and cd into the plaiframe folder (see above). Then build the container

Linux/MacOS

`$ sudo docker build -t $USER/plaiframe .`

Windows

`$ docker build -t plaiframe .`

and start the container

linux/MacOS

`$ sudo docker run -it -p 8080:8080 -d $USER/plaiframe`

Windows

`$ docker run -it -p 8080:8080 -d plaiframe`


Getting started
---------------

once installation is done you can do

`$ node index.js` 

to run plaiframe with default settings. 

Acces the user interface by opening http://localhost:8080/ui in your webbrowser, create a new game and start connecting your things.

By default plaiframe will create a data folder in the current directory. See [Settings](#markdown-header-settings) if you want to store data to a different directory.

Instead of the embedded database [NeDB](https://www.npmjs.com/package/nedb) you can also use plaiframe with [mongodb](#markdown-header-mongodb).

Read further for more information on how to customize and control plaiframe.

Settings
--------

To change basic settings you can start plaiframes `index.js` with additional options. 

For example, to run plaiframe with a web interface on port 7070 instead of 8080 and a detailed debug log, call `index.js` in your plaiframe directory and append the port and level options:

`$ node index.js --port 7070 --level debug`

All options you provide are stored to a config file. So index.js will start based on  previously appended options.

By default plaiframe creates a `config.json` file in the current directory. Use the config option to use or create a different config file:

`$ node index.js --config /path/to/my/config.json`

More options are listed below.

| opt | long option    | param      |default          | description    
|-----|----------------|------------|-----------------|--------------
|-c   |--config        |[filename]  |./config.json    |config file you want to use instead of default ./config.json
|-p   |--port          |[portno]    |8080             |port number for web interface  
|-a   |--address       |[webaddress]|http://localhost |address that enables webplugins to reach your plai webserver remotely 
|-l   |--level         |[loglevel]  |info             |level of console output can be set to "trace", "debug","info","warn", "error" or "silent"
|-d   |--database      |[db type]   |nedb             |Change database to mongodb (external) or NeDB (embedded)
|-f   |--files         |[dir path]  |./data           |Directory to store and load NeDB data files. Use with `-d nedb`
|-u   |--url           |[db url]    |mongodb://localhost:27017?replicaSet=plait-repl |url to connect to mongodb. Use with `-d mongodb`
|     |--login         |[username]  |off              |secure the web ui with a login. Combine with --passwort option. Set to "off" to disable login
|--pw |--password      |[password]  |                 |set a password for the webview login. 

Besides these options, the config file stores what games to load on startup. Whenever you load or remove a game, config is adjusted accordingly.

Commands
------------

Basic functionalities of the plaiframe can be controlled by command line.

If there are more than one games loaded, you have to address the game you want to interact with by name. Do

`> testgame sessions`

to get a list of all sessions in testgame. If there is only one game active, you address it automatically when not using main commands.

To forward commands to plugins or sessions, address them by name, prepended by the game name, separated by a blank. Sessions can also be addressed by their index. Get all sessions to see their index using:

`> sessions`

Be aware that indexes of active sessions change when other sessions are canceled.

If the game has only one session running, you don't have to address the session by name. Simply do

`> listener`

or, if there is more than one game:

`> testgame listener`

to get a list of all active listeners in testsession.

To address elements that contain space characters, wrap them in quotes (" or ')

Use the following commands to control plaiframe from command line. 

##### main

|cmd        |args             |description
|-----------|-----------------|--------------
|quit       |-                |close running plai framework. same as `ctrl+c`
|games      |-                |list currently loaded games
|load       |[game][template] |load from DB or create new game. Optionally choose template for quick setup.
|unload     |[game]           |remove game from plai server. Keep it in Database
|del/delete |[game]           |delete game, including database (!)
|level      |[log level]      |change log level to "trace" (everything), "debug", "info", "warn", "error" or "silent" (off)
|plugins    |-                |list all plugins that can be added to games

##### game

If you have more then one game loaded, prepend commands with game name.

|cmd        |args                 |description
|-----------|---------------------|--------------
|sessions   |                     |list currently running sessions
|launch     |[level] [name]       |start session with [name] based on [level]
|cancel     |[session]            |delete session and all its listeners and timers
|plugin     |list                 |list active plugins
|plugin     |add [plugin]         |activate plugin
|plugin     |remove [plugin]      |deactivate plugin and delete all items
|collection |list                 |list custom collections
|collection |create [collection]  |create new custom collection
|collection |delete [collection]  |delete existing custom collection
|import     |[type] [file]        |import device, stencil, session, level or player
|export     |[path] [collections] |export game data as json files. If you do not define parameters, all collections are exported to ./export. Customize by defining a path and optionally list collections you want to export separated by blank.

##### session

If you have more then one session running, prepend commands with session name or index.

|cmd       |args                                      |description
|----------|------------------------------------------|--------------
|cue       |[name]                                    |dispatch cue in default chapter
|cue       |[chapter] [name]                          |dispatch cue in specific chapter
|listener  |                                          |list currently active listener
|listener  |cancel [name/index]                       |cancel listener by name or index
|reference |list                                      |show all references assigned to this session
|reference |assign [collection] [document name] [name]|create new reference [name], linking to document with [document name] in [collection]
|player    |                                          |list refrences in player collection that are assigned to this session
|remove    |[reference] / [collections] [key] [value] |remove existing reference using [reference] name or specifiing a query ([key] [value]) in [collection]

##### plugin

send commands to plugins you added to your game.

Prepend commands with plugin name.

plugin collection items can also be addressed by name property.

use collections command to show available collections for plugin.

|cmd        |args                |description
|-----------|--------------------|--------------
|collections|                    |list available collections

If plugin has multiple collections prepend collection to address items.

To look into item properties like `settings` use

|cmd        |args                |description
|-----------|--------------------|--------------
|[property] |                    |show property


###### time

|cmd       |args                |description
|----------|--------------------|--------------
|list      |                    |list all running timer

###### devices

General commands

|cmd       |args                |description
|----------|--------------------|--------------
|list      |                    |list all devices
|ports     |                    |get list of midi and serial ports
|midi      |on/off              |enable (on) or disable (off) midi

Commands adressing specific device. Prepend device name

|cmd       |args                |description
|----------|--------------------|--------------
|send      |[json message]      |Send message to device. E.g.: `{message:"content"}`
|settings  |                    |show device settings
|reconnect |                    |reconnect device with current settings

> For example to send a message to your TCP Device 'lightbulb' telling it to go off do:  
> `> devices lightbulb send '{switch:"off"}'` 

###### twilio

Commands adressing specific twilio phone. Prepend phone name

|cmd       |args                |description
|----------|--------------------|--------------
|routing   |sms/answer/status   |show current call and/or sms routing. 
|cancel    |sms/answer [number] |cancel routing for incoming sms or call for Telnumber [number]
|pending   |                    |Show incoming call or sms events from unregistered telnumbers that were not yet forwarded to a session
|lock      |                    |Disable requests to twilio for this phone (E.g. to save costs when testing)
|unlock    |cancel [name/index] |Enable requests to twilio for this phone

###### telegram

Comands addressing clients. Use like this:

`> telegram client <clientname> <command>`

|cmd        |args                     |description
|-----------|-------------------------|--------------
|routing    |-/message                |show current message routing. 
|cancel     |message [id] [index]     |cancel message routing at [index] for messages from user [id]
|pending    |                         |Show incoming messages from unregistered peers that were not yet forwarded to a session
|user       |[entity]                 |get user info through telegram API. Use '+' format number to get user by telnumber. See also [telethon get_entity](https://docs.telethon.dev/en/latest/modules/client.html?highlight=get_entity#telethon.client.users.UserMethods.get_entity)
|add_contact|[phone/id] [first name]/-|add user to client contacts. Use [phone] and [first_name] or get user by [id].

###### ableton

|cmd       |args                |description
|----------|--------------------|--------------
|livesets  |                    |List livesets

###### d-pro

|cmd       |args                |description
|----------|--------------------|--------------
|shows     |                    |List D-Pro Shows

###### tradfri (experimental)

|cmd       |args                |description
|----------|--------------------|--------------
|discover  |                    |Discover Tradfri Gateway and show devices 



MongoDB
-------

By default plaiframe uses the embedded [NeDB](https://www.npmjs.com/package/nedb) Database. For larger projects with multiple sessions it is recommended to use [mongodb](https://www.mongodb.com/) instead. It's a scalable NoSQL database that is used in many Web Applications.

NeDB was actually built to replace mongodb for smaller, local projects. It has a similar query language, but it is not as advanced as mongodb itself. When you connect plaiframe with mongodb you can use it's extensive query language when developing Game Installations. 

Start by following the instructions on how to [install mongodb community edition](https://docs.mongodb.com/manual/administration/install-community/).

Plaiframe comes with a shell file `mongo.sh` to setup a database with a so called 'replica set' that is needed for use with plaiframe.

> If you run plaiframe on MacOS, use `mongo_macos.sh` instead.

Use it once for setup like this:

`$ sh mongo.sh setup`

and follow the instructions.

To create a default mongodb setup that works well on your local machine press enter for the default value on all input prompts.

this creates required database folders, config files and initiates mongo replica sets which are obligatory for change listeners.

once it is set up, you can always do

`$ sh mongo.sh start`

to start mongodb before you run the node script and

`$ sh mongo.sh stop`

to stop the mongodb process.

Once you are done setting up mongo db, tell plaiframe how to access it.
Run plaiframe using the following options:

`$ node index.js --database mongodb --url mongodb://localhost:27017?replicaSet=plait-repl`

If needed replace `27017` with the actual port of your mongo database.

> Replika setup is based on [this tutorial](https://medium.com/@thakkaryash94/mongodb-3-6-change-streams-example-with-node-js-2b9a85652c50)

### Access from a remote location 

To access the plait game databases from a remote location, it is recommended to enable authentification.

You can use `mongo.sh setup` to create a root user and enable authentification for mongodb.

> When hosting plaiframe with remote access you should also secure the webview. See [settings](#markdown-header-settings) on how to secure the web editor with login and password.

Once `mongo.sh setup` asks you to create a user, type a username and press enter.
`mongo.sh setup` will now ask you to choose a password for this user and if you want to allow remote access to your database.
If you confirm, the mongodb will be bound to ip 0.0.0.0 instead of 127.0.0.1 which allows to access the database on the given port using your server ip or domain.

To allow the plaitools node script to access the secured database, run index.js with the following options. Replace `<username>` and `<password>` with your mongodb user and password and `<port>` with your primary replica set port (default would be 27017):

```
node index.js --database mongodb --url mongodb://<username>:<password>@localhost:<port>/admin?replicaSet=plait-repl
```

> For a quick answer on how `mongo.sh setup` creates authentification for mongodb replica sets see [this helpful post](https://stackoverflow.com/questions/38524150/mongodb-replica-set-with-simple-password-authentication)


By reusing `mongo.sh setup` you can allways disable authentification or create a new root user for mongodb. Though it is not possible to change the password for an existing user. When creating the same user with a new password your changes to the user will be ignored. See [the mongodb user management manual](https://docs.mongodb.com/manual/tutorial/manage-users-and-roles/) on how to use the mongo shell to change user settings.

Config files for all replica sets are found in the mongodb folder. If you make changes to the files be aware that using `mongo.sh setup`, will overwrite these changes.

### Backups

#### Create

When using mongodb the easiest way to create full backups of your games is using [mongodump](https://docs.mongodb.com/manual/reference/program/mongodump/).

Make a command like this, to export a games full database:

`$ mongodump --db <mygame> --username <myuser> --password <mypassword> --authenticationDatabase admin -o /path/to/backupfolder`

#### Schedule

Creating a bash script when you automate the backup process helps keeping your backups in order. Make a new file called something like run_backup.sh with the following content:

``` bash
#!/bin/sh
DIR=`date +%Y-%m-%d_%H:%M:%S`
DEST=./patrol/backup/$DIR
mkdir $DEST
mongodump --db <mydatabase> --username <myuser> --password <mypassword> --authenticationDatabase admin -o $DEST
```

To create backups regularly you can make a cron job that executes the above file in specific time intervals.

Open crontab editor to add a new cronjob:

`$ crontab -u myuser -e`

add the following line at the bottom of the crontab editor to make a backup once per hour from 9-22 h:

`0 9-22 * * * ./path/to/file/run_backup.sh`

#### Restore

Restore a game using [mongorestore](https://docs.mongodb.com/manual/reference/program/mongorestore/).

To restore all games you dumped into a folder do:
`$ mongorestore /path/to/backup/`

To restore a specific backup version do:
`$ mongorestore --db <mygame> /path/to/backup/mygame`

If authentification is enabled, add authentification credentials:

`$ mongorestore --db <mygame> --username <myuser> --password <mypassword> --authenticationDatabase admin /path/to/backup`

You can use the web ui to restore the game by adding a new game with the same name. To use command line, start the plaiframe server and load your game from the database:

`> load mygame`

License and Copyright
---------------------

Copyright 2020 Lasse Marburg, Benedikt Kaffai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
