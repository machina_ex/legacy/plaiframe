/**
* Structures access to the embedded [NeDB database](https://www.npmjs.com/package/nedb)
*
* extends mongo.js collection and document class to allow nedb and mongodb to be interchangeable
*
* this module (unlike the mongo module) does not provide a client variable!
* 
* @requires nedb-promises
* @requires events
*
* @requires mongo
* @requires file
*
* @module ne
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

var NeDB = require('nedb-promises')
const events = require('events')

const mongo = require('./mongo.js')
const file = require('./file.js')

/** nedb data directory 
 * @default 
 */
var path = "./data"

/**
* get nedb data path from config and create nedb data directory
*/
function start(config) {
	return new Promise((resolve, reject) => {
		if(config.files) {
			path = config.files
		}
		
		if(!file.exists(path)) {
			log.info("NeDB", "Create data directory: " + path)
			file.mkdir(path)
		}
		
		log.info("NeDB", "Accessing Data files in " + path)
		return resolve("started")
	})
}

/**
* abstract function. No need to stop NeDB
*/
function stop() {
  
}

/**
* Allows access to a Database.
*
* For the NeDB module a Database is represented by a folder that contains data files for each collection
*
* @param {Object} config
* @param {String} config.name - database ID
* @param {number} [config.url] - host and port of mongoDB and replikas. Optional if start() function was called
*
*/
class Database extends mongo.Database {
  constructor(config) {
		super(config)
		
		this.type = "nedb"
		
		this.path = path + '/' + this.name
  }
  
  /**
  * check if database folder already exists, then create collection objects of each file
  * else return that database is new
  */
  connect() {
    return new Promise((resolve, reject) => {
      this.db = {}
      
      if(file.mkdir(this.path)) {
        file.ls(path + '/' + this.name).forEach(file => {
          var connector = new NeDB({filename: this.path + '/' + file})
          this[file] = new Collection(file, connector, this.name)
          log.debug(this.name, "Created collection Object of: " + file)
      	})
        return resolve("connected")
      } else {
        return resolve("created")
      }
    })
  }
	
	/**
	* return all collections in this databse as a list of strings based on files in the database folder
	*
	* @returns {Promise} array of Collection names (string).
	*/
	listCollections() {
		return new Promise((resolve, reject) => {
			return resolve(file.ls(this.path))
		})
	}
  
  /**
	* adds NeDB connector and collection to Database object
	*
	* @param {String} name - new collections name
	*
	* @returns {Promise} forwards response when done
	*/
	addCollection(name) {
		return new Promise((resolve, reject) => {
      var connector = new NeDB({filename: path + '/' + this.name + '/' + name})
      connector.load()
      this[name] = new Collection(name, connector, this.name)
      log.debug(this.name, "Created collection Object of: " + name)
      return resolve("created")
		})
	}
	
	/**
	* drop the collection, delete the reference 
	* keeps the file (will be overwritten if collection with same name is created)
	*
	* @param {String} name - to be dropped collections name
	*
	* @returns {Promise} forwards response when done
	*/
	dropCollection(name) {
		return new Promise((resolve, reject) => {
			this[name].c.remove({ }, { multi: true })
			.then(result => {
				return this[name].c.load()
			})
			.then(result => {
				if(result) {
					delete this[name]
				}
				resolve(result)
			})
			.catch(error => {
				log.error(this.name, error)
			})
		})
	}
	
	/**
	* Drop Database (Delete folder and all files)
	*/
	drop() {
		let name = this.name
		if(file.rm(this.path)) {
			log.info(0, "Database " + name + " deleted")
		} else {
			log.error(0, "Failed to delete Database: " + name)
		}
	}
	
}

/**
* Collection Object represents and gives access to a NeDB file. Inherits a NeDB connector.
*
* @param {String} name - name of collection. Same as object instance name in Database Object
* @param {Object} col - Collection Object created from db connection
*/
class Collection extends mongo.Collection {
  constructor(name, nedb_connector, database_name) {
    super(name, nedb_connector)
		
		this.event = new events.EventEmitter()
		this.event_subscription = false
		
		this.database = database_name
  }
	
	/**
	* @param {Object} query - json to identify Document
	*
	* @returns {Document} easier access to first document in this collection the query returns
	*/
	document(query) {
		return new Document(query, this)
	}
	
	/**
	* start the simulation of a mongo change stream
	* dispatch change event on every (successfull) update/set/insert function call
	*
	* @returns {Object} a mongodb Change stream (https://docs.mongodb.com/manual/changeStreams/)
	*/
	changes() {
		this.event_subscription = true
		return this.event
	}
	
	/**
	* Dispatch change event with additional information on the event type and data.
	*/
	dispatchChange(changes) {
		this.event.emit("change", changes)
	}
	
	/**
	* creates Change Event Object with basic data
	*
	* similar to mongo change event:
	* https://docs.mongodb.com/manual/reference/change-events/
	*
	* Though the information about what data was updated may be inaccurate!
	* updateDescription is a simple copy of the update data Object
	*
	*/
	changeEvent(type) {
		return {
			operationType:type,
			fullDocument:{},
			updateDescription:{},
			ns : {
      	db : this.database,
      	coll : this.name
   		},
   		documentKey : { _id : undefined },
   		updateDescription : {
      	updatedFields : {},
      	removedFields : [] // should actually be an array
   		}
		}
	}
	
	/**
	* dispatch a notification about updates in the collection.
	*
	* ignore if never any module subscribed to the change event.
	*
	* @param {String} type - either update or remove
	* @param {Object} query - query of original update command
	* @param {Object} before - document before update command
	* @param {Object} after - document after update command
	*/
	updateEvent(type, query, before, after) {
		if(!after) { return }
		if(!this.event_subscription) { return }
		
		let changes = this.changeEvent(type)
		
		changes.fullDocument = after
		changes.documentKey._id = after._id
		
		let diff = Object.changes(before, after)
		changes.updateDescription.updatedFields = diff.updated
		changes.updateDescription.removedFields = diff.removed
				
		this.dispatchChange(changes)
	}
	
	/**
	* compare two _ids and return if they are equal
	* Dummy function to keep mongo and NeDB interchangeable
	* mongo creates _id Objects that can not be compared with ==
	*
	* @param {String} idA - id to be checked against idB
	* @param {String} idB - id to be checked against idA
	*
	* @returns {boolean} true if ids are equal
	*/
	compareIDs(idA, idB) {
		return (idA == idB)
	}

	/**
	* Dummy function to keep mongo and NeDB interchangeable
	*
	* @param {string} id - id to be transformed
	* @returns {string} unchanged id string
	*/
	makeID(id) {
		return id
	}
	
  /**
	* get list of Documents based on query
	*
	* @param {Object} query - check docuemnts against query
	*
	* @returns {Promise} forwards an array of all found documents. Returns no rejection!
	*/
	find(query) {
		return new Promise((resolve, reject) => {
			this.c.find(query)
				.then(result => {
					resolve(this.decode(result))
				})
				.catch(error => {
					log.error(this.name, error)
				})
		});
	}
	
	/**
  * get specific element of Documents based on query.
  *
	* @param {Object} key - define key in documents you want to have returned
  * @param {Object} query - check documents against query
  *
  * @returns {Promise} forwards an array of all found document keys. No rejection!
  */
	distinct(key, query) {
		return new Promise((resolve, reject) => {
			this.find(query)
				.then(result => {
					if(!result.length) {return resolve([])}
					
					let dist = result.map(r => {return Object.getPath(r, key)})
					
					if(!dist) {return resolve([])}
					
					return resolve(dist)
				})
				.catch(error => {
					log.error(this.name, error)
				})
		});
	}
  
  /**
	*
	*  
	* @param {Object} query - check documents against query
	* @param {Object} sortBy - sort returned documents based on this object.
	*/
	sorted(query, sortBy) {
		return new Promise((resolve, reject) => {
			this.c.find(query).sort(sortBy)
				.then(result => {
					resolve(this.decode(result))
				})
				.catch(error => {
					log.error(this.name, error)
				})
		})
	}
	
	pick(query, path, sub_query) {
		return new Promise((resolve, reject) => {
			this.distinct(path, query)
			.then(result => {
				if(!result.length) {
						return resolve(undefined)
				}
				if(!Array.isArray(result[0])) {return resolve(undefined)}
				
				return this.temp(result[0])
			})
			.then(result => {
				if(!result) {return resolve(undefined)}
				return result.find(sub_query)
			})
			.then(result => {
				if(!result) {return resolve({})}
				if(!result.length) {return resolve({})}
				return resolve(result[0])
			})
			.catch(error => {
				log.error(this.name, error)
			})
		})
		
	}
	
	/**
	* create a temporary NeDatabase filled with a list of initial documents.
	*
	* @returns {Promise} NeDB connector 
	*/
	temp(list) {
		return new Promise((resolve, reject) => {
			if(!Array.isArray(list)) {return reject("temp() needs an array as argument")}
			let temp_connector = NeDB.create()
      temp_connector.load()
			.then(result => {
				temp_connector.insert(list,{multi:true})
			})
			.then(result => {
				return resolve(temp_connector)
			})
			.catch(error => {
				log.error(this.name, error)
			})
		})
	}
	
	/**
	* add new document to collection
	*
	* @returns {Promise} data about the inserted document including _id property
	*/
	insert(data) {
		return new Promise((resolve, reject) => {
			this.c.insert(this.encode(data))
				.then(res => {
					if(res) {
						let changes = this.changeEvent("insert")
						changes.fullDocument = res
						changes.documentKey._id = res._id
						this.dispatchChange(changes)
						
						res["insertedId"] = res._id
						this.feedback({insertedCount:1})
						return resolve(res)
					}
				})
				.catch(err => {
					log.error(this.name, err)
					return reject(err)
				})
		})
	}
	
	/**
	* Replace the first Document returned by query.
	* For NeDB unlike mongodb, using update without any operators replaces the queried document by the given data.
	*
	* since it calls NeDB update function it dispatches an 'update', not a 'replace' change event like mongo does
	* change event properties provide informations about what is different between the orinal and the replacing document.
	*
	* @param {Object} query - query what documents to replace
  * @param {data} data - json of how the replaced document looks like
	* @param {data} options - see replaceOne options: https://docs.mongodb.com/manual/reference/method/db.collection.replaceOne/
	*/
	replace(query, data, options) {
		return new Promise((resolve, reject) => {
			this.update(query, data)
				.then(res => {
					this.feedback(res)
					return resolve(res)
				})
				.catch(err => {
					log.error(this.name, new Error(err))
					return reject(err)
				})
		})
	}
	
	/**
	* make an update on selected documents.
	*
	* buffer the original document to allow creating a detailed change event
	*
	* @param {Object} query - query what documents to update
  * @param {data} data - json of what is to be updated
	*
	*
	* @returns {Promise} matchedCount - number of matched documents, modifiedCount - number of modified documents
	*/
	update(query, data) {
		return new Promise((resolve, reject) => {
			let before = {}
			this.find(query)
			.then(result => {
				before = result
				return this.c.update(query, this.encode(data), {multi:true, returnUpdatedDocs:true})
			})
			.then(result => {
				for(let i = 0; i< result.length; i++) {
					this.updateEvent("update", query, before[i], result[i])
				}
					
				return resolve({modifiedCount:result.length, matchedCount:before.length})
			})
			.catch(err => {
					return reject(err)
			})
		})
	}
	
	/**
	* change existing document. Only objects mentioned in query are updated.
	* If you don't want to rewrite the object as a whole, use string dot notation
	* to specify the nested object
	*
	* prepends $set to updated data
	*
	* @param {Object} query - query what documents to update
	* @param {data} data - json of what is to be updated
	*
	* @returns {Promise} number of documents that where udated
	*/
	set(query, data) {
		return new Promise((resolve, reject) => {
			this.update(query, { $set: data })
				.then(result => {
					return resolve(result)
				})
				.catch(err => {
					log.error(err)
					return reject(err)
				})
		})
	}
	
	/**
	* delte documents
	*
	* @param {Object} query - documents to be deleted
	*/
	delete(query) {
		return new Promise((resolve, reject) => {
			this.slice(query)
			.then(result => {
				return resolve(result.length)
			})
			.catch(error => {
				log.error(this.name, error)
			})
		})
	}
	
	/**
	* delete documents based on query and return them
	*
	* @param {Object} query - what entries to cut out
	*
	* @returns {Promise} deleted documents as array
	*/
	slice(query) {
		return new Promise((resolve, reject) => {
			let docs = []
			this.find(query)
				.then(result => {
					docs = result
					
					return this.c.remove(query,{multi:true})
				})
				.then(result => {
					if(result) {
						let changes = this.changeEvent("delete")
						changes.documentKey._id = docs[0]._id
						this.dispatchChange(changes)
					}
					return resolve(docs)
				})
				.catch(error => {
					log.error(this.name, error)
				})
		})
	}
	
	/**
	* append item to array in object. Don't allow duplicates
	*
	* @param {Object} data - Array name/path and values to append
	*												use string with dot notation for nested array (e.g.: {'object.array':{key:value}})
	* @returns {Promise} matchedCount - number of matched documents, modifiedCount - number of modified documents
	*/
	push(query, data) {
		return new Promise((resolve, reject) => {
			let documents = 0
			this.find(query)
			.then(result => {
				documents = result	
				return this.update(query, { $addToSet: data })
			})
			.then(res => {
				resolve(res)
			})
			.catch(err => {
				log.error(this.name, err)
			})
		})
	}
}

/**
* Narrows any query down to the same document.
*
* @todo the document should not rely on the query that defined in in the init query  
* it should rather be reffered to by _id once the init query returned a document.
* But this seems to be a problem with the pick function
*
* @param {ObjectId} query - query to address one single record in mongodb
* @param {ne.Collection} collection - collection that inherits this Document
*/
class Document extends mongo.Document{
	constructor(query, collection) {
		super(query, collection)
	}
}

if (require.main === module) {
    database = new Database({name:'testNeDB'})
    database.connect()
    .then(result => {
      database.db.update({"test":0},{$set:{"yes":111}})
    })
    .catch(error => {
      console.log(error)
    })
}

module.exports = {
	Database: Database,
  Colleciton: Collection,
  start:start,
  stop:stop
}
