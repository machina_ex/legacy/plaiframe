/**
* provides functions to scan strings or objects for variables and replaces them
* 
* @requires json5
* @requires functions
* @requires medley
*
* @module variables
* @copyright Lasse Marburg 2020
* @license GPLv3
*/
const JSON5 = require('json5')
const functions = require('./plugins/logic/functions.js')

const medley = require('./medley.js')

/**
*
* resolve variables using session references.
*
* Each Variable instance is assign to a cue context of a certain session
*
* @todo add chapter reference to cue
*
* @param {Object} databse - MongoDB Game database interface
* @param {Object} references - current session variable references
* @param {ObjectID} session - _id property of session 
* @param {string} cuename - name of cue that forwarded this variable instance to plugin
* @param {string} action - name of action that uses this variables instance
*/
class Variables {
  constructor(database, references, session, cuename, action) {
    this.db = database
    this.session = session
    this.name = session + " " + cuename + " variables"
    this.cue = cuename
    this.action = action
    
    this.references = references
  }
  
  /**
  * write data locally to session referenced by cue and action type
  *
  * @returns {Promise} mongodb update result
  */
  store(data) {
    return new Promise((resolve, reject) => {
      let updt = {}
      let path = this.cue + '.' + this.action
      updt[path] = data
      this.db.sessions.set({_id:this.session}, updt)
      .then(result => {
        return resolve(result)
      })
      .catch(error => {
        log.error(this.name, "couldn't store data for " + this.action + "." + this.cue + " in session " + this.session)
        return reject(error)
      })
    })
  }
  
  
  /**
  * search for document based on variable reference or query
  *
  * @example
  * // returns document that references to the curent session as "teamcaptain"
  * get("teamcaptain")
  *
  * @param {string|Object} variable - value to check against being a reference variable or db query
  * @param {boolean} [return_empty=false] - reject promise if no document can be resolved. If false, resolves undefined
  * 
  * @returns {Promise} - resolves with (first) refered document. rejects if no document could be found.
  */
  get(variable, return_empty) {
    return new Promise((resolve, reject) => {
      let ref = this.getReference(variable)
      if(!ref) {
        if(return_empty) {
          return resolve()
        } else {
          return reject("reference couldn't be resolved: " + variable)
        }
      }
      if(this.db.hasOwnProperty(ref.collection)) {
        this.db[ref.collection].find(ref.query)
        .then(result => {
          if(result.length) {
            return resolve(result[0])
          } else if(return_empty) {
            return resolve()
          } else {
            return reject("no document found with reference: " + variable)
          }
        })
        .catch(error => {
          return reject(error)
        })
      } else {
        return reject("not a collection: " + ref.collection)
      }
    })
  }

  /**
   * search for documents based on variable reference or query
   *
   * @param {*} variable - value to check against. Can be a reference variable or db query
   * @returns {Promise} - list of all documents refered to by variable. rejects if none is found
   */
  getAll(variable) {
    return new Promise((resolve, reject) => {
      let ref = this.getReference(variable)
      if(!ref) {return reject("reference couldn't be resolved: " + variable)}
      if(this.db.hasOwnProperty(ref.collection)) {
        this.db[ref.collection].find(ref.query)
        .then(result => {
          if(result.length) {
            return resolve(result)
          } else {
            return reject("no documents found with reference: " + variable)
          }
        })
        .catch(error => {
          return reject(error)
        })
      } else {
        return reject("not a collection: " + ref.collection)
      }
    })
  }
  
  /**
  * resolve query based on variable. Might be a parsable json string or a local session variable
  * refering to a document in a collection
  *
  * if variable is a string, try to parse JSON, else try to match a session variable
  *
  * @param {string|Object} variable - value to check against being a variable or db query
  *
  * @returns {Object} collection name and Mongo find query (might still leed to no docuemnt!) or undefined (definetly no document)
  */
  getReference(variable) {
    
    if(variable == "level") {
      return {collection:"sessions",query:{_id:this.session}}
    }
    
    for(let ref of this.references) {
      if(ref.name == variable) {
        let query = {}
        query['sessions'] = {}
        query.sessions['_id'] = this.session 
        query.sessions['reference'] = variable
        return {collection:ref.collection, query:query}
      }
    }
    if(typeof(variable) === "string") {
      if(variable[0] == '{') {
        try {
          let query = JSON5.parse(variable)
          for(let col in query) {
            return {collection:col, query:query[col]}
          }
        } catch(err) {
          if(err instanceof SyntaxError) {
            log.warn(this.name, 'syntax error in query "' + variable + '"')
          } else {
            log.error(this.name, 'Error when parsing to json:')
            log.error(this.name, err)
          }
          return undefined
        }
      }
    }
    log.warn(this.name, "couldn't resolve reference " + variable)
    return undefined
  }

  /** 
  * address object property with '.' seperated string   
  * Might be handy especially when working with Mongodb and nested docuemnt find queries.  
  * Mongo uses [dot notation]{@link https://docs.mongodb.com/manual/core/document/#document-dot-notation}
  * from:
  * https://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key/6491621#6491621
  *
  * @param {Object} o - some Object
  * @param {strin} s - some string that finds nested properties with '.' notation
  */
  getPath(o, s) {
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    var a = s.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
  }
  
  /**
  * search for variables in first argument before '.'
  *
  * @todo fails if there is '.' notation in docuemnt query/reference (first element of path)
  * needs to filter '{ }' regular mongo query (parse json)
  *
  * @returns {Object} mongo find query object
  */
  parseQuery(data){
    if(typeof data === "string") {
      let first = data.split('.', 1)
      let doc = this.getReference(first[0])
      
      let second = data.slice(first[0].length+1)
      let path = second.split(':', 1)
      let value = second.slice(path[0].length+1)
      
      let property
      try {
        property = JSON5.parse(value)
      } catch(err) {
        if(err instanceof SyntaxError) {
          property = value
        } else {
          log.warn(this.name, 'Error when parsing to json:')
          log.warn(this.name, err)
        }
      }
      let assignment_query = {}
      assignment_query[path] = property
      
      let query = Object.assign(doc, assignment_query)
      //log.info("PATHTOQUERY", query)
      return(query)
    }
  }
  
  /**
  * analyse dot notated path.
  * part before first dot references document.
  * Checks if path begins with a query `{`. In that case the query is used as document reference
  * check for references in first part and make it a query
  * find document
  * get value referenced by 2nd part and return it
  *
  * first part of path can be 'function'. This makes getPath forward the following to the logic function module.
  * Format needs to be:
  * function.<function_name>(<arg1>,<arg2>,...)
  * function args are traeted as references, values, strings or numbers
  *
  * @todo zuerst mongo querys in 1st part herausfiltern `{ }`
  * 
  * @returns {Promise} if found, value is returned. Else undefined is returned
  */
  getValue(path) {
    return new Promise((resolve, reject) => {
      if(typeof path === "string") {
        let first
        if(path[0] == '{') {
          //let end_index = path.indexOf("}.")
          first = path.slice(0, path.indexOf("}.") + 1)
        } else {
          first = path.split('.', 1)[0]
        }
        
        let second = path.slice(first.length+1)
        
        if(first == "function") {
          let function_name = second.substring(0, second.indexOf('('))
          let function_args = second.substring(second.indexOf('(')+1, second.indexOf(')'))
          //function_args = this.review(function_args) // recursive!! Do I need to forbid certain things here?
          log.debug(this.name, "resolve function " + function_name + " with args: " + function_args)
          
          if(typeof functions[function_name] === "function") {
            functions[function_name](function_args.split(','), {db:this.db, session:this.session, variables:this})
            .then(result => {
              return resolve(result)
            })
            .catch(error => {
              log.error(this.name, error)
              return resolve(undefined)
            })
          } else {
            log.error(this.name, "no such function " + second)
            return resolve(undefined)
          }
        } else {
          let ref = this.getReference(first)
          
          if(ref) {
            this.db[ref.collection].find(ref.query)
            .then(result => {
              if(result.length) {
                let value = this.getPath(result[0], second)
                if(typeof value === 'undefined') {
                  log.warn(this.name, second + " was not found in document " + JSON.stringify(ref.query) + " of collection " + ref.collection)
                }
                return resolve(value)
              } else {
                log.error(this.name, "document '" + first + "' not found: " + JSON.stringify(ref.query) + " in " + ref.collection)
                return resolve(undefined)
              }
            })
            .catch(error => {
              log.error(this.name, error)
              return resolve(undefined)
            })
          } else {
            return resolve(undefined)
          }
        }
        
      } else {
        return reject("variable query from path only allows string path.")
      }
    })
  }
  
  /**
  * find variables and replace them with value from database. Takes diferent types:  
  * - String: review string
  * - Array: walk through all elements of array and review them  
  * @todo will only replace variables in strings on first array level
  * - Object: parses JSON to string and replaces [[]] variables using review funciton
  *
  * @todo iterate through js object and identify string. Then review and replace them
  *
  * @param {Object} element - js Object. May be string or array
  *
  * @returns {Promise} JSON with replaced variables
  */
  findAndReplace(element) {
    return new Promise((resolve, reject) => {
      let type = ""
      let review_promises = [] 
      if(typeof element === "string") {
        review_promises.push(this.review(element))
        type = "string"
      } else if(Array.isArray(element)) {
        for(let elem of element) {
          review_promises.push(this.review(elem))
        }
        type = "array"
      } else {
        let str_element = JSON5.stringify(element)
        
        review_promises.push(this.review(str_element))
        type = "object"
      }
      
      Promise.all(review_promises)
      .then(result => {
        switch (type) {
          case "string":
          return resolve(result[0])
          
          case "array":
          return resolve(result)
          
          case "object":
          let json_element = JSON5.parse(result)
          return resolve(json_element)
        }
      })
      .catch(error => {
        return reject(error)
      })
    })
  }
  
  /**
  * check string for `[[ ]]` and replace the content in case.
  * if `[[ ]]` embraces the whole string, it resolves to its original data type of the variable and will not be parsed to string!
  * 
  * if review is supposed to check anything thats not a string it returns the argument itself
  *
  * @example
  * // resolve player variables using the player reference, a database query and the level reference.
  * this.review("your score is: [[plyr.score]] and your telnumber is [[{player:{'friends.good':'tom'}}.telnumber]] and your level is [[level.level]]")
  * .then(res => {
  *   log.info("VARIABLES EXAMPLE", res)
  * })
  *
  *
  * @param {string} str - string that might contain replacable values. Other datatypes are allowed and simply returned the way they came in.
  *
  * @returns {Promise} the string with replaced variables.
  */
  review(str) {
    return new Promise((resolve, reject) => {
      if(typeof str === "string") {
        var regex = /\[\[\s*([\w\.\,\{\}\(\)\$\:\'\"\\]+)\s*\]\]/g
        
        var matches = str.match(regex)
        
        if(!matches) {
          return resolve(str)
        }
        
        let values = matches.map( (match, index) => {
          return this.getValue(match.slice(2,-2))
        })
        Promise.all(values)
        .then(result => {
          if(matches[0] == str) {
            return resolve(result[0])
          }
          
          var i = -1
          let replaced_str = str.replace(regex, (tag, match) => {
            i++
            return result[i]
          })
          return resolve(replaced_str)
        })
        .catch(err => {
          log.error(this.name, err)
          return resolve(undefined)
        })
      } else {
        return resolve(str)
      }
      
    })
  }
}

module.exports = {
  Variables:Variables
}
