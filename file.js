/**
* read and write files
*
* @requires fs-extra
* @requires path
* @requires chokidar
* 
* @module file
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

const fs = require('fs-extra')
const chokidar = require("chokidar")
const path = require('path')

/**
* write data to file
*/
function toFile(data, file, create_dirs)
{
	return new Promise(function(resolve, reject) {
		fs.writeFile(file, data, 'utf8', function (err) {
	    if (err) {
	    	log.error(0, "An error occured while writing to "+file+"\n"+err)
	      reject()
	    }
	    log.info(0, file + " saved")
			resolve(file + " saved")
		})
	})
}

/**
* create folder if it doesnt exist
*
* @returns {boolean} true if folder already existed, false if it was created
*/
function mkdir(path) {
	if (!fs.existsSync(path)){
		fs.mkdirSync(path)
		return(false)
	}
	return(true)
}

/**
 * report changes on directory. Uses [chokidar](@link https://github.com/paulmillr/chokidar).
 * 
 * @param {string} path 
 * @param {function} callback 
 */
function watchDir(path, callback) {
	chokidar.watch(path, {ignoreInitial:true, depth:10}).on('all', callback)
}

/**
* remove folder and content
*
*/
function rm(path) {
	if (fs.existsSync(path)) {
		fs.removeSync(path)
		return(true)
	}
	return(false)
}

/**
 * move or rename a file. 
 * @param {string} orig - file to be moved
 * @param {string} dest - move file to destination
 */
function mv(orig, dest) {
	return new Promise((resolve, reject) => {
		if(dest[dest.length-1] == "/") {
			orig_file = orig.substr(orig.lastIndexOf("/")+1)
			dest = dest + orig_file
		}
		fs.rename(orig, dest, err => {
			if (err) return reject(err);
			log.info("file", "moved from " + orig + " to " + dest)
			return resolve()
		})
	})
}

/**
* check if file or folder exists
*
* @returns {boolean} - true if folder/file exists
*/
function exists(path) {
	return fs.existsSync(path)
}

/**
* get a list of all files in a folder (syncronous)
*
* @example
* // returns list of image files
* ls("/path/to/my/pictures", [".jpg",".jpeg",".png"])
* @example
* // returns only directories
* ls("/path", "directories")
*
* @param {string|array} filter - filter "directories" (only directories) or filenames
*
* @returns {array} list of directories and/or filenames
*/
function ls(dir, filter) {
	if(filter == "directories") {
		return fs.readdirSync(dir)
		.filter(name => {
			let p = path.join(dir, name)
			return fs.lstatSync(p).isDirectory()
		})
	} else if(Array.isArray(filter)) {
		let all = fs.readdirSync(dir)
		let show = []
		for(let a of all) {
			filter.forEach(elem => {
				if(a.includes(elem)) {
					show.push(a)
				}
			})
		}
		return show
	} else if(typeof filter === "string") {
		let all = fs.readdirSync(dir)
		let show = []
		for(let a of all) {
			if(a.includes(filter)) {
				show.push(a)
			}
		}
		return show
	}
	return fs.readdirSync(dir)
}


/**
* load json data from file
*/
function loadJSON(filename)
{
	return new Promise(function(resolve, reject) {
		fs.readFile(filename, 'utf8', function (err,data) {
		if (err) {
			log.error(0, "An error occured while reading "+filename+"\n"+err)
			//reject()
		} else {
			try {
				json_data = JSON.parse(data)
				resolve(json_data)
			} catch(e) {
				log.error(0, "An error occured while reading "+filename+"\n"+e)
				//reject()
			}
		}
		});
	});
}

/**
* save json data to file
*/
function saveJSON(data, file) {
	return new Promise(function(resolve, reject) {
		save_data = JSON.stringify(data, null, 2)
		toFile(save_data, file, true)
		.then(function(result) {
			resolve(result)
		})
		.catch(function(error){
			log.error(this.name, error)
			reject(error)
		})
	});
}

module.exports = {
	loadJSON:loadJSON,
	saveJSON:saveJSON,
	mkdir:mkdir,
	watchDir:watchDir,
	exists:exists,
	ls:ls,
	rm:rm,
	mv:mv
}
