/**
* creates framework for starting, creating, loading, editing sessions.
*
* Uses minimist to parse optional arguments
*
* provides webside to mess with it.
*
* @requires mongo
* @requires ne
* @requires game
* @requires medley
* @requires file
*
* @requires express
* @requires http
* @requires socket.io
* @requires path
* @requires readline
*
* @requires minimist
*
* @module index
* @copyright Lasse Marburg 2020
* @license GPLv3
*/

const mongo = require("./mongo.js")
const ne = require("./ne.js")
const medley = require("./medley.js")
const game = require("./game.js")
const file = require("./file.js")

const readline = require('readline')

var argv = require('minimist')(process.argv.slice(2));

const cors = require('cors')
const path = require('path')
const express = require("express")
var app = express()
var secured = express.Router()
var server = require('http').createServer(app);
io = require('socket.io')(server)

/** list of game Objects */
var games = {}

/** all plugin modules that were required */
var plugin_modules = {}

/** Database module depends on selection in config */
var database

/**
* initiate plait based on config
*
* - create login security middleware. Code snippet from:
* https://stackoverflow.com/questions/23616371/basic-http-authentication-with-node-and-express-4/33905671#33905671 
*
* - provide static route to javascript and css in node_modules folder
* - provide static route for website
* - connect to Database
* - instantiate games that are listed in config
*
* @param {Object} config - Contains parameters to load existing games
* @param {Object} config.database - Database connection parameters
* @param {Array} [config.games] - List of games that will be loaded
*/
function init(config) {
	if(config.hasOwnProperty("login")) {
		if(config.login.hasOwnProperty("user") && config.login.hasOwnProperty("password")) {
			enableAuthentication(config.login)
		} else {
			log.warn(false, "Webview authentification disabled. No user and/or password was set.")
		}
	} else if (config.hasOwnProperty("user")) {
		enableAuthentication(config.user)
		if(!config.user.length) {
			log.warn(false, "Webview authentification enabled but user list has no entries.")
		}
	} else if (config.webaddress){
		log.warn(false, "Webview authentification disabled")
	} else {
		log.info(false, "Webview authentification disabled")
	}
	
	app.use('/ui', secured)
	
	app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist'))
	app.use('/sweetalert', express.static(__dirname + '/node_modules/sweetalert/dist'))
	app.use('/sweetalert2', express.static(__dirname + '/node_modules/sweetalert2/dist')) // Only for Model Editor
	app.use('/jointjs', express.static(__dirname + '/node_modules/jointjs/dist'))					// Only for Model Editor
	app.use('/lodash', express.static(__dirname + '/node_modules/lodash'))					// Only for Model Editor
	app.use('/backbone', express.static(__dirname + '/node_modules/backbone'))					// Only for Model Editor
	app.use('/json-editor', express.static(__dirname + '/node_modules/@json-editor/json-editor/dist'))
	app.use('/font-awesome', express.static(__dirname + '/node_modules/@fortawesome/fontawesome-free'))
	app.use('/select2', express.static(__dirname + '/node_modules/select2'))
	
	// Host Public files
	app.use('/files', express.static(__dirname + '/public/files'))

	app.use(express.json());
	secured.use('/public', express.static(__dirname + '/public'))

	server.listen(config.webport)
	
	server.on('error', (e) => {
	  if (e.code === 'EADDRINUSE') {
	    log.error("http server", "Port " + config.webport + " is already in use. Close any other plait instance or other server that uses the same port and start again.")
			process.exit(0)
	  }
	})
	
	server.on('listening', (res) => {
	  log.info(0, "Open PLAIT Webview on " + config.webaddress + ":" + config.webport + "/ui")
	})

	secured.get('/', function (req, res) {
	   res.sendFile(path.join(__dirname,'public', 'index.html'))
	});
	
	let homesocket = io.of('/ui')
	homesocket.on('connection', client)

  if(config.hasOwnProperty('database'))
  {
		if(config.database["type"] == "mongodb") {
			database = mongo
		} else if (config.database["type"] == "nedb") {
			database = ne
		} else {
			log.error(0,"Not a valid databas type: " + config.database["type"] + "\n. Options: mongodb, nedb ")
			process.exit(0)
		}
  } else {
		database = ne
		config.database["files"] = "./data"
	}
	
	database.start(config.database)
	.then(result => {
		let load_game_promises = []
		if(config.hasOwnProperty('games'))
		{
			for(let game in config.games) {
				load_game_promises.push(loadGame(game, config.games[game]))
			}
			return Promise.all(load_game_promises)
		}
		return
	})
	.then(result => {
		rl.prompt()
	})
	.catch(error => {
		log.error(0, error)
		process.exit(0)
	})
}

function client(socket) {
	log.debug("home", "client connected on " + socket.handshake.headers.host)

	socket.on('games', function(msg) {
		log.debug("home", msg)
		let returnmsg = []
		for(let game in games) {
			returnmsg.push(game)
		}
		socket.emit(msg.id, returnmsg)
	})

	socket.on('create', function(msg) {
		if(msg.data.title) {
			if(msg.data.template) {
				var data = {template:msg.data.template}
			} else {
				var data = undefined
			}
			loadGame(msg.data.title, data)
			.then(function(result) {
				socket.emit(msg.id,result)
			})
			.catch(function(err) {
				log.error(0, err)
			})
		} else {
			log.error('socket', 'couldn`t load game. Name property missing.')
		}
	})

	socket.on('unload', function(msg) {
		if(msg.data.title) {
			let deleted = unloadGame(msg.data.title)
			socket.emit(msg.id,deleted)
		} else {
			log.error('socket', 'couldn`t unload game. Name property missing.')
		}
	})
}

/**
 * create authentication middleware function for secure subdomains.
 * 
 * respond with www-authenticate necessity. Only pass if reqest header contains valid authentication
 * 
 * parse login and password from headers
 * match against user list or single login user
 * Verify login and password are set and correct
 *
 * @param {Object|Array} user - list of users or single user object
 */
function enableAuthentication(user) {
	secured.use((req, res, next) => {
		const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
		const [login, password] = new Buffer(b64auth, 'base64').toString().split(':')

		let auth = {}

		if(Array.isArray(user)) {
			for(let u of user) {
				if(u.login == login) {
					auth = {login: u.login, password: u.password}
					break
				}
			}
		} else {
			auth = {login: user.user, password: user.password}
		}
		
		if (!login || !password || login !== auth.login || password !== auth.password) {
			log.warn(0, "remote auth failed " + JSON.stringify(req.query) + " auth " + JSON.stringify(req.headers.authorization + " user " + login + " pw " + password))
			res.set('WWW-Authenticate', 'Basic realm="401"')
			res.status(401).send('Authentification enabled. Check Readme on how to change authentification for your plaitools server.')
			return
		}
		next()
	})
}

/**
* - create Game Object with provided Database Object
* - resume if there is an existing database
* else build default game
* - open route to Game editing Website
* - note game in config file
*
* @param {string} name - name of game to be created or continued
* @param {Object} [game_data] - game specifif config parameters or template name if its a new game
*/
function loadGame(name, game_data) {
	if(!game_data) {
		game_data = {}
	}
	return new Promise(function(resolve, reject) {
		let name_convert_spaces = name.replace(/ /g, '%20')
		var game_db = new database.Database({"name":name_convert_spaces})
		game_db.connect()
		.then(result => {
			let client = {}
			client['home'] = io.of('/ui/' + name_convert_spaces)
			client['editor'] = io.of('/ui/' + name_convert_spaces + '/editor')
			client['control'] = io.of('/ui/' + name_convert_spaces + '/control')
			
			if(config.hasOwnProperty("weburl")) {
				game_data['files'] = config.weburl + '/files'
				game_data['url'] = config.weburl + '/' + name_convert_spaces
			} else {
				game_data['files'] = config.webaddress + ':' + config.webport + '/files'
				game_data['url'] = config.webaddress + ':' + config.webport + '/' + name_convert_spaces
			}
			game_data['port'] = config.webport
			
			game_data['name'] = name
			
			let framework = {
				getPlugin:getPlugin,
				getAvailablePlugins:getAvailablePlugins
			}
			
			let newgame = new game.Game(game_data, app, client, framework)
			games[name] = newgame

			app.use('/' + name_convert_spaces + '/data', cors(), secured)
			app.use('/' + name_convert_spaces, cors())
			
			secured.get('/' + name_convert_spaces + '/editor', function (req, res) {
				 res.sendFile(path.join(__dirname,'public', '/editor/index.html'))
			})
			secured.get('/' + name_convert_spaces + '/editor/*', function (req, res) {
				 res.sendFile(path.join(__dirname,'public', '/editor/index.html'))
			})
			
			secured.get('/' + name_convert_spaces + '/control', function (req, res) {
				 res.sendFile(path.join(__dirname,'public', '/control/index.html'))
			})
			secured.get('/' + name_convert_spaces + '/control/*', function (req, res) {
				 res.sendFile(path.join(__dirname,'public', '/control/index.html'))
			})
			
			if(result == "created") {
				if(game_data.hasOwnProperty("template")) {
					return newgame.build(game_db, game_data.template)
				} else {
					return newgame.build(game_db, "basic")
				}
			} else if (result == "connected") {
				return newgame.resume(game_db)
			} else {
				throw new Error(name + " could neither be resumed or created.")
			}
		})
		.then(result => {
			if(config_file) {
				if(! config.hasOwnProperty('games')) {
					config['games'] = {}
				}
				config.games[name] = game_data
				file.saveJSON(config, config_file)
			}
			
			log.info(0, "Successfully " + result + " " + name)
			return resolve(result)
		})
		.catch(error => {
			log.error(0, "Error when loading game " + name + ":")
			log.error(0, error)
		})
	});
}

/**
* remove game but keep its data in database
*/
function unloadGame(name) {
	if(games.hasOwnProperty(name)) {
		if(config_file) {
			console.log("unload ")
			console.log(config.games[name])
			if(config.games.hasOwnProperty(name)) {
				delete config.games[name]
				file.saveJSON(config, config_file)
			}
		}
		games[name].unload()
		delete games[name]
		log.info('game', name + ' unloaded')
		return true
	} else {
		log.error('unload', "No such Game '" + name + "'")
		return false
	}
}

/**
* unload and remove game and its Database
* does not create backup! Handle with care
*/
function deleteGame(name) {
	if(games.hasOwnProperty(name)) {
		games[name].delete()
		unloadGame(name)
	} else {
		log.error('delete', "No such Game '" + name + "'")
		return false
	}
}

/**
* Games that load a plugin, use this function, so that all plugin modules are only required ones.
* If another game added the same plugin already use the already required module.
*
* plugin has to be in plugin folder and either
* - be a js file called like plugin name and/or be in a folder called like plugin name
* - be a js file called index.js in a folder called like plugin name
*
* @returns {Object} new instance of the plugins main class
*/
function getPlugin(plugin) {
	if(!plugin_modules.hasOwnProperty(plugin)) {
		if(file.exists('./plugins/' + plugin + '.js')) {
			plugin_modules[plugin] = require('./plugins/' + plugin + '.js')
		} else if(file.exists('./plugins/' + plugin + '/' + plugin + '.js')) {
			plugin_modules[plugin] = require('./plugins/' + plugin + '/' + plugin + '.js')
		} else if(file.exists('./plugins/' + plugin + '/index.js')) {
			plugin_modules[plugin] = require('./plugins/' + plugin + '/index.js')
		} else {
			throw new Error("couldn't find " + plugin + " in plugins directory")
		}
		
		log.debug(0, "load plugin module " + plugin)
	}
	
	let plugin_object
	
	if(plugin_modules[plugin].hasOwnProperty("Plugin")) {
		 plugin_object = new plugin_modules[plugin].Plugin()
		plugin_object.name = plugin
	} else {
		plugin_object = plugin_modules[plugin]
	}
	
	
	return plugin_object
}

/**
* list all plugins found in plugins directory
*
* @returns {Array} name of all plugins available
*/
function getAvailablePlugins() {
	return file.ls("./plugins", "directories")
}

/**
 * give all games and plugins the chance to tidy up if necessary then exit node process
 * 
 */
function quit() {
	let promises = []
	for(let g in games) {
		promises.push( games[g].quit() )
	}
	Promise.all(promises)
	.then((result) => {
		log.info('',"C'était un plaisir!")
		process.exit(0)
	}).catch((err) => {
		log.error('',err)
		process.exit(0)
	})
}

/********************************************************************************************
* plaiframe index.js script begins below
*
*********************************************************************************************/

/** global variable. That should be the one and last global var!
* Use log insead of console.log whenever a module
* is logging something.
*/
global.log = new medley.Log()

global.Queue = medley.Queue

/** path to and name of config file. If file is not found config will be created based on options
* @default
*/
var config_file = './config.json'

/** content of config file.
* @default
*/
var config = {webaddress:"http://localhost", webport:8080, database:{type:"nedb",files:"./data"}, level:"info"}

if(argv.hasOwnProperty('c')) {
	setConfig(argv.c)
} else if(argv.hasOwnProperty('config')) {
	setConfig(argv.config)
} else {
	setConfig(config_file)
}

function setConfig(filename) {
	config_file = filename
	try {
		config = require(filename)
	} catch (e) {
		if(e.code !== 'MODULE_NOT_FOUND') {
			log.error(0, e)
		}
		log.warn(0, 'config file missing. Will create ' + filename)
	}
}

for(let arg in argv) {
	if(argv[arg] == true) {
		log.warn(0, "parameters missing for option '" + arg + "'")
	} else {
		switch(arg) {
			case 'level':
				config['level'] = argv.level
				break
			case 'l':
				config['level'] = argv.l
				break
			case 'p':
				config.webport = argv.p
				break
			case 'port':
				config.webport = argv.port
				break
			case 'address':
				config['webaddress'] = argv.address
				break
			case 'a':
				config['webaddress'] = argv.a
				break
			case 'weburl':
				config['weburl'] = argv.weburl
				break
			case 'd':
				databaseConfig(argv.d)
				break
			case 'database':
				databaseConfig(argv.database)
				break
			case 'login':
				loginConfig()
				break
			case 'password':
				loginConfig()
				break
			case 'pw':
				loginConfig()
				break
			case '_':
				break
			
	  }
	}
}

function loginConfig() {
	if(!config.hasOwnProperty("login")) {
		config["login"] = {}
		log.info(0, "Create Webview login credentials")
	} else {
		log.info(0, "Change Webview login credentials")
	}
	
	if(argv.hasOwnProperty("login")) {
		if(argv.login == "off") {
			delete config.login["user"]
			delete config.login["password"]
			log.info(0, "Webview login deactivated. Reactivate by setting login and password.")
			return
		}
		config.login["user"] = argv.login
	}
	if(argv.hasOwnProperty("password")) {
		config.login["password"] = argv.password
	}
	if(argv.hasOwnProperty("pw")) {
		config.login["password"] = argv.pw
	}
}

/**
* set url for database access (only mongodb)
* -d mongodb --url mongodb://<username>:<password>@localhost:<port>?replicaSet=plait-repl
* with authentification enabled use:
* -d mongodb --url mongodb://<username>:<password>@localhost:<port>/admin?replicaSet=plait-repl
*/
function databaseConfig(db_type) {
	if(!config.hasOwnProperty("database")) {
		config['database'] = {type:db_type}
	} else {
		config.database["type"] = db_type
	}
	
	if(argv.hasOwnProperty('u')) {
		config.database['url'] = argv.u
	} else if (argv.hasOwnProperty('url')) {
		config.database['url'] = argv.url
	}
	
	if(argv.hasOwnProperty('f')) {
		config.database['files'] = argv.f
	} else if (argv.hasOwnProperty('files')) {
		config.database['files'] = argv.files
	}
}

file.saveJSON(config, config_file)
.then(result => {
	return log.init(config.level, rl)
})
.then(result => {
	init(config)
})
.catch(error => {
	log.error("config", error)
})

/** global object that provides some useful input functions */
global.Input = {}

Input.confirmation = function(action, element, callback) {
	rl.question("are you sure you want to " + action + " " + element + "? (y/n) ", answ => {
		if(answ == "y" || answ == "yes") {
			callback(element)
		}
		rl.prompt()
	})
}

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
  	prompt: '> '
})

rl.on('line', (input) => {
	if(!input) {
		return
	}
	/*
	* split input by space character but ignore spaces in quotes (" or ')
	*
	* regex for only double quotes: /(?:[^\s"]+|"[^"]*")+/g
	* source:
	* https://stackoverflow.com/questions/16261635/javascript-split-string-by-space-but-ignore-space-in-quotes-notice-not-to-spli/16261693#16261693
	*
	* remove quotes consequentially
	*/
	input = input.match(/(?:[^\s"']+|['"][^'"]*["'])+/g)
	for(var i = 0; i < input.length; i++) {
		input[i] = input[i].replace(/["']+/g, '')
	}
	
  switch (input[0]) {
		case "quit":
		quit()
		break
		case "games":
		for(let game in games) {
			log.info(0, game)
		}
		case "del":
		case "delete":
			if(input.length > 1){
				Input.confirmation("delete",input[1],deleteGame)
			}
			break
		case "load":
			if(input.length == 2) {
				loadGame(input[1]) 
			}
			else if(input.length > 2) {
				loadGame(input[1], {template:input[2]})
			}
			break
		case "unload":
			if(input.length > 1){unloadGame(input[1])}
			break
		case "plugins":
			log.info(0, "List of available Plugins:\n")
			for(let t of getAvailablePlugins()) {
				log.info(0, t)
			}
			break
		case "level":
			if(input.length == 2) {
				log.setLevel(input[1])
				config['level'] =input[1]
				file.saveJSON(config,config_file)
				break
			}
			log.info(0, "wrong number of arguments for level command")
			break
		default:
		if(games.hasOwnProperty(input[0])) {
			let game = input.shift()
			games[game].command(input)
		} else {
			if(Object.keys(games).length == 1) {
				for(let game in games) {
					games[game].command(input)
				}
			} else {
				log.info("", "No such game or command '" + input[0] + "'")
			}
		}
		break
	}
	rl.prompt();
}).on('close', () => {
  quit()
})