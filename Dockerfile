# Following the guidlines on https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

FROM node:current-alpine 

RUN apk add --no-cache --virtual .build-deps python g++ make gcc linux-headers udev alsa-lib-dev 

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
RUN apk add python3 py3-pip
RUN pip3 install telethon quart requests

RUN apk del .build-deps
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 8080

CMD [ "node", "index.js" ]
