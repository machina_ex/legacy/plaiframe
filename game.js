/**
 * allows index.js to create Game Object instances
 * 
 * @requires path
 * @requires events
 *
 * @requires file
 * @requires session
 *
 * @module game
 * @copyright Lasse Marburg 2020
 * @license GPLv3
 */

const path = require('path')
const events = require('events')

const file = require("./file.js")
const session = require("./session.js")
const medley = require("./medley.js")

/** objets that define what plugins and collections are part of which game template. All other templates inherit the default plugins and collections */
const templates = {
  "default":{
    "plugins":["logic","data","time"],
    "collections":["plugins","level","sessions","listener","setup"]
  },
  "basic":{
    "plugins":[],
    "collections":[]
  },
  "installation":{
    "plugins":["devices"],
    "collections":[]
  },
  "multiplayer":{
    "plugins":["devices","control"],
    "collections":["player"]
  },
  "open-world":{
    "plugins":["devices","control"],
    "collections":["player","places"]
  }
}

/**
* Main Framework class that inherits sessions and plugins and serves editor webpage
*
* host global (inter session game wide) events 
*
* @param {Object} preferences - config the game is based on
* @param {Object} app - Express app
* @param {Object} client - io socket Namespace Connections
* @param {Object} client.home - connection to game main page
* @param {Object} client.editor - connection to game editor page
* @param {Object} framework - access to some index.js functions. E.g. getPlugin
*/
class Game {
  constructor(preferences, app, client, framework) {
    Object.assign(this, preferences)
    this.app = app
    this.framework = framework
    
    /** list of sessions */
    this.sessions = []
    
    /** counts how many sessions were created since game started to allow individual session names */
    this.session_count = 0
    
    /** dictionary of enabled plugins */
    this.plugins = {}

    /**
     * event emitter for relevant game incidents or changes
     */
    this.status = new events.EventEmitter()

    /** dictionary of DB change stream listeners. The stream listener for each collection 
     * is stored under its respective name.
     */
    this.changes = {}
    
    /** setup information about this game collections and plugins */
    this.setup = {
      collections:[],
      plugins:[]
    }
    
    /** eventemitter instance is forwarded to plugins on creation to interact with other sessions */
    this.event = new events.EventEmitter()
    
    /** socket namespace connection to editor webpage */
    this.client = client

    /** sockets editing the same document are assorted into room groups, much like socketio rooms, to avoid conflicts */
    this.rooms = {}

    /** collects callbacks for responses on editor alerts */
    this.alerts = {}
    
    this.client.editor.on('connection', this.clientConnection.bind(this))
    this.client.control.on('connection', this.clientConnection.bind(this))

    this.client.editor.on('connect', socket => {
      // 
    })

    this.client.control.on('connect', socket => {
      // 
      log.info(this.name, "control view socket connected " + socket.id)
    })
  }

  /**
   * call quit function on all plugins that have one
   * 
   * @returns {Promise} - resolves once all plugin quit functions returned
   */
  quit() {
    return new Promise((resolve, reject) => {
      let promises = []
      for(let p in this.plugins) {
        if(typeof this.plugins[p].quit === "function") {
          promises.push(this.plugins[p].quit())
        }
      }
      Promise.all(promises)
      .then((result) => {
        return resolve()
      }).catch((err) => {
        return reject(err)
      })
    })
  }

  /**
  * create a new (empty) default Game
  * - assign database
  * - create collections based on template
  * - add plugins based on template
  * - store setup
  *
  * @param {Database} database - refers to database with this.name
  * @param {string} template - template name, the game build is based on
  */
  build(database, template) {
    return new Promise((resolve, reject) => {
      this.db = database
      
      let all_collections = []
      let all_plugins = []
      
      if(templates.hasOwnProperty(template)) {
        this.setup = templates[template]
        this.setup["assets"] = "public/files"
        
        all_collections.push(...templates.default.collections)
        all_collections.push(...templates[template].collections)
        all_plugins.push(...templates.default.plugins)
        all_plugins.push(...templates[template].plugins)
      } else {
        throw new Error(template + " is not a valid game template")
      }
      
      let create_collections = all_collections.map((coll, index) => {
        return this.db.addCollection(coll)
      })
      
      Promise.all(create_collections)
      .then(result => {
        return this.db.setup.insert(this.setup)
      })
      .then(result => {
        this.setup_doc = this.db.setup.document({})
        let add_plugins = all_plugins.map((plugin, index) => {
          return this.addPlugin(plugin)
        })
        
        return Promise.all(add_plugins)
      })
      .then(result => {
        this.status.emit("plugins_loaded")
        this.changes["plugins"] = this.db.plugins.changes({ fullDocument: 'updateLookup' })
        this.changes["plugins"].on("change", this.reviewPlugins.bind(this))
      
        log.info(this.name, "created")
        
        return resolve("built")
      })
      .catch(error => {
        return reject(error)
      })
    })
  }

  /**
  * resume game, based on existing data on database
  * - enable plugins stored in plugins collection
  * - load plugins that were added previously
  * - add local session objects list based on session Collection entries
  * - connect to host website for edit and control
  * @todo wait until all plugins are definetly loaded and only then start loading sessions
  *
  * @param {Database} database - refers to database with this.name
  */
  resume(database) {
    return new Promise((resolve, reject) => {
      this.db = database

      log.info(this.name, "resume ...")
      
      for(let coll of templates.default.collections) {
        if(!this.db.hasOwnProperty(coll)) {
          return reject("Game data is incomplete: " + coll + " collection missing")
        }
      }

      this.setup_doc = this.db.setup.document({})
      
      this.setup_doc.get()
      .then(result => {
        this.setup = result
        if(!this.setup.assets) {
          
          this.setup["assets"] = "public/files"
        }
        return this.db.plugins.find({})
      })
      .then(plugins => {
        let plugins_added = plugins.map((plugin, index) => {
          return this.addPlugin(plugin.name)
        })

        return Promise.all(plugins_added)
      })
      .then(plugins => {
        this.status.emit("plugins_loaded")
        this.changes["plugins"] = this.db.plugins.changes({ fullDocument: 'updateLookup' })
        this.changes["plugins"].on("change", this.reviewPlugins.bind(this))
        return this.db.sessions.find({})
      })
      .then(sessions => {
        let add_session_promises = []
        for(let i = 0;i<sessions.length;i++)
        {
          let new_session = this.addSession(sessions[i])
          add_session_promises.push( new_session.reload() )
        }
        return Promise.all(add_session_promises)
      })
      .then(result => {
        return resolve("reloaded")
      })
      .catch(err => {
        return reject(err)
      })
    })
  }
  
  /**
  * writes current game data to file
  * creates foler for game and for each collection.
  *
  * @param {string} [path='./export'] - folder to export game files into. Will be created if it doesnt exist.
  * @param {array} [exp_coll] - what collections to export. ALl collections are exported if not provided
  */
  export(path, exp_coll) {
    return new Promise((resolve, reject) => {
      if(!path) {
        path = './export'
      }
      if(!exp_coll) {
        this.db.listCollections()
        .then(result => {
          this.writeCollections(path, result)
        })
      } else {
        this.writeCollections(path, exp_coll)
      }
    })
  }
  
  /**
  * follows export function
  *
  * @param {string} path - folder to export game to
  * @param {array} collections - list of collection names
  */
  writeCollections(path, collections) {
    let get_collections = collections.map((coll, index) => {
      if(this.db.hasOwnProperty(coll)) {
        return this.db[coll].find({})
      } else {
        throw new Error(this.name + " has no collection with name " + coll)
      }
    })

    Promise.all(get_collections, path)
    .then(res => {
      file.mkdir(path)
      file.mkdir(path + '/' + this.name)
      for(let i = 0; i<res.length; i++) {
        file.mkdir(path + '/' + this.name + '/' + collections[i])
        for(let doc of res[i]) {
          file.saveJSON(doc, path + '/' + this.name + '/' + collections[i] + '/' + doc.name + '.json')
        }
      }
    })
    .catch(err => {
      log.error(this.name, err)
    })
  }

  /**
   * get the correct file path relative to the game asset directory.
   * Will return unchanged if file has a leading '/' as in an absolute path
   * @param {string} file 
   * @returns {string} file path relative to assets path
   */
  getAssetsPath(file) {
    if(file[0] == "/") {
      return file
    } else if (this.setup.assets[this.setup.assets.length -1] == "/") {
      return this.setup.assets + file
    } else {
      return this.setup.assets + "/" + file
    }
  }
  
  /**
  * add new custom collection to database and store it in setup
  */
  async addCollection(coll_name) {
    let existed = false
    if(!this.db.hasOwnProperty(coll_name)) {
      await this.db.addCollection(coll_name)
      existed = true
    }
    if(!this.setup.collections.includes(coll_name)) {
      await this.setup_doc.push({'collections':coll_name})
      this.setup.collections.push(coll_name)
      log.info(this.name, "collection added " + coll_name)
      this.status.emit('collection_added',{name:coll_name, existed:existed})
    }
    return
  }
  
  /**
  * remove custom collection from game database
  * remove collection from setup
  */
  removeCollection(coll_name) {
    if(this.setup.collections.includes(coll_name)) {
      this.db.dropCollection(coll_name)
      .then(result => {
        this.setup.collections = this.setup.collections.filter(coll =>{
          return coll != coll_name
        })
        return this.setup_doc.remove({"collections":coll_name})
      })
      .then(result => {
        log.info(this.name, "Custom collection removed: " + coll_name)
      })
      .catch(error => {
        log.error(this.name, error)
      })
    } else {
      log.warn(this.name, "Can not remove " + coll_name + ". No such custom collection")
    }
  }

  /**
   * emit alert to editor via socket to all web clients that are editing this game.
   * 
   * Stores the callback in a dictionary. The alert response by socket will then call it {@see clientConnection}
   * 
   * @param {*} alert - can be anything that can be displayed by sweet alert
   */
  alert(msg, callback) {
    let uuid = medley.generateUUID()
    if(callback) {
      this.alerts[uuid] = callback
    } else {
      this.alerts[uuid] = res => {
        log.debug(this.name, 'alert response ' + res)
      }
    }
    this.client.editor.emit('alert', msg, uuid)
  }
  
  /**
  * add a plugin to the pallette of this game
  * get it from index.js with getPlugin function
  *
  * @param {string} plugin - name of plugin
  *
  * @returns {Promise} schema the plugin module returns on setup
  */
  addPlugin(plugin) {
    return new Promise((resolve, reject) => {
      if(this.plugins.hasOwnProperty(plugin)) {
        return reject("Plugin is already active")
      }
      let new_plugin = this.framework.getPlugin(plugin)
      
      this.plugins[plugin] = new_plugin

      let config = {
        game:{
          name:this.name,
          event:this.event,
          changes:this.changes,
          status:this.status,
          assets:this.setup.assets,
          getAssetsPath:file => this.getAssetsPath(file),
          alert:this.alert.bind(this),
          addCollection:name => this.addCollection(name),
          deleteSession:query => this.deleteSession(query),
          createSession:args => this.createSession(args)
        },
        app:this.app,
        socket:global.io.of('/' + this.name + '/' + plugin),
        db:this.db,
        port:this.port,
        url:this.url,
        files:this.files,
        custom_collections:this.setup.collections,
        name:plugin
      }
      
      this.db.plugins.find({name:plugin})
      .then(result => {
        if(result.length) {
          config['settings'] = result[0]['settings']
          this.plugins[plugin]._id = result[0]._id
          return 'done'
        } else {
          config['settings'] = {}
          return this.db.plugins.insert({name:plugin, settings:{}})
        }
      })
      .then(result => {
        if(result != 'done') {
          this.plugins[plugin]._id = result.insertedId
        }
        if(typeof this.plugins[plugin].setup === "function") {
          return Promise.resolve(this.plugins[plugin].setup(config))
        } else {
          throw new Error(plugin + " is missing a setup function!")
        }
      })
      .then(schema => {
        if(schema) {
          if(!schema.hasOwnProperty('settings')) {
            schema['settings'] = {}
          }
          if(!schema.hasOwnProperty('cue')) {
            schema['cue'] = {}
            schema.cue[plugin] = {}
          }
        }
        this.plugins[plugin]._schema = schema
        return this.db.plugins.set({name:plugin}, {schema:schema})
      })
      .then(result => {
        resolve(true)
      })
      .catch(error => {
        switch (error.code) {
          case 'MODULE_NOT_FOUND':
          log.error(this.name, error.message)
          break
          default:
          log.error(this.name, error)
        }
        this.alert({
          title: "Failed to add plugin",
          text: error,
          icon: "error"
        })
        return reject(error)
      }) 
    })
  }
  
  /**
  * walk through local plugin object and look for matching key
  *
  * @param {string} key - key to look up
  * @param value - value in key to match
  *
  * @returns {Plugin} first plugin Object that has key:value. undefined if no plugin was found.
  */
  findPlugin(key, value) {
    for(let plugin in this.plugins) {
      if(this.plugins[plugin].hasOwnProperty(key)) {
        if(this.plugins[plugin][key] == value) {
          return this.plugins[plugin]
        } else if (key == "_id") {
          if(this.db.plugins.compareIDs(this.plugins[plugin]._id, value)) {
            return this.plugins[plugin]
          }
        }
      }
    }
    log.warn(this.name, "no plugin found with " + key + ": '" + value + "'")
    return undefined
  }
  
  /**
  * get list of plugins that are not enabled for this game
  */
  getInactivePlugins() {
    let i_plugins = this.framework.getAvailablePlugins().filter(a_t => !this.plugins.hasOwnProperty(a_t))
    
    return i_plugins
  }
  
  
  /**
  * forward changes in plugin preferences to plugin module
  *
  * insert adds the inserted plugin if it is not existing
  * replace and update forward changes to concerned plugin
  * delete removes concerned plugin module
  *
  * @todo Change of name might be obsolte for plugins
  * delete does not work whenever more than one game requires the deleted plugin
  * because plugin._id is overwritten by latest require
  */
  reviewPlugins(change) {
    switch(change.operationType) {
      case 'insert':
        if(!this.plugins.hasOwnProperty(change.fullDocument.name)) {
          this.addPlugin(change.fullDocument.name)
          .catch(err => {
            log.error(this.name, err)
          })
        }
        break
        
      case 'replace':
      case 'update':
        let plugin = this.findPlugin("_id", change.documentKey._id)
        
        if(!plugin) {
          log.error(this.name, "Failed to update. Plugin missing: " + change.fullDocument.name + ", _id: " + change.documentKey._id)
          return
        }
        
        let changes = {updated:{},removed:[]}
        
        if(change.hasOwnProperty("updateDescription")) {
          if(change.updateDescription.hasOwnProperty("updatedFields")) {
            changes.updated = change.updateDescription.updatedFields
          }
          if(change.updateDescription.hasOwnProperty("removedFields")) {
            changes.removed = change.updateDescription.removedFields
          }
        } else {
          changes = Object.changes(plugin, change.fullDocument)
        }
        
        changes["document"] = change.fullDocument
        
        if(changes.updated.hasOwnProperty("settings")) {
          if(typeof plugin.update === "function") {
            log.debug(plugin.name, "settings changed: ")
            log.debug(plugin.name, changes.document.settings)
            plugin.update(changes.document.settings)
          } else {
            log.warn(this.name, plugin.name + " settings have changed but plugin does not own an update function to apply them.")
          }
        }
        
        if(changes.updated.hasOwnProperty("schema")) {
          changes.document = this.db.plugins.decode(changes.document)
          let push_msg = {}
          push_msg[plugin.name] = changes.document.schema
          this.client.editor.emit('schema', push_msg)
          plugin._schema = changes.document.schema
        }
        
        if(plugin.name != changes.document.name) {
          log.debug(plugin.name, " name changed to " + changes.document.name)
          this.plugins[changes.document.name] = plugin
          plugin.name = changes.document.name
          delete this.plugins[plugin.name]
        }
        break
        
      case 'delete':
        let d_plugin = this.findPlugin("_id", change.documentKey._id)
        
        if(d_plugin.hasOwnProperty('schema')) {
          if(d_plugin.schema.hasOwnProperty('collections')) {
            for (let item in d_plugin.schema.collections){
              this.db.dropCollection(item)
              log.info(this.name, item + " collection removed")
            }
          }
        } else {
          log.warn(this.name, "tried to remove " + d_plugin.name + " but it has no schema")
        }
        
        delete this.plugins[d_plugin.name]
        log.info(this.name, "plugin removed: " + d_plugin.name)
        break
    }
  }
  
  /**
  * remove plugin from plugins collection.
  *
  * @param {string} plugin - plugin name
  */
  deletePlugin(plugin) {
    if(this.plugins.hasOwnProperty(plugin)) {
      this.db.plugins.delete({name:plugin})
      .then(res => {
        if(!res) {
          log.error(this.name, "Failed to delete " + plugin + " document from plugins collection")
        }
      })
    } else {
      log.info(this.name, "Could not remove. " + plugin + " is not an active plugin")
    }
  }

  /**
  * make session based on existing db entry
  * route webview for session control
  * forward io socket namespace for communication with session webview client
  * store new session in games session array
  * 
  * @param {Object} data - session relevant data to make it work
  * 
  * @returns {session.Session} - the newly created session Instance.
  */
  addSession(data) {
    let doc = this.db.sessions.document({"_id":data._id})
    let level = this.db.level.document({"name":data.level})
    
    if(data.name) {
      var name_convert_spaces = data.name.replace(/ /g, '%20')
    } else {
      var name_convert_spaces = data.level + "_" + this.session_count
      this.session_count ++
      data.name = name_convert_spaces
    }
    
    let client = io.of('/ui/' + this.name + '/control/sessions/' + name_convert_spaces)
    
    data["custom_collections"] = this.setup.collections
    
    let new_session = new session.Session(data, this.db, client, this.plugins)
    this.sessions.push(new_session)
    
    this.app.get('/ui/' + this.name + '/' + name_convert_spaces, function (req, res) {
       res.sendFile(path.join(__dirname,'public', '/session/index.html'))
    })

    return new_session
  }

  /**
  * launch a new session based on level
  * 
  * - fetch level data
  * - create entry in session collection
  * - create session class instance with level data and db document instance
  * - optional: assign arguments to session once its created
  * - dispatch start cue
  * - socket emit to web editor that a session was created
  *
  * @param {Object|string} properties.level - find query object or name of level the session is based on
  * @param {string} [properties.name] - session name. Has to be unique otherwise createSession rejects. Defaults to unique id
  * @param {Object|array} [properties.arguments] - list or object of arguments that are assigned to the session [session createReference function]{@link Session#createReference }
  *
  * @returns {Promise} "exists" if session with name exists. else the session instance is returned
  */
  createSession(properties) {
    return new Promise((resolve, reject) => {
      if(properties['name']) {
        if(this.findSession("name", properties.name)) {
          return reject("session exists")
        }
      } else {
        properties['name'] = medley.generateUUID()
      }

      let level_query = properties.level

      if(typeof properties.level === 'string') {
        level_query = {"name":properties.level}
      }
      
      let new_session
      var session_data = null
      this.db.level.find(level_query)
      .then(lvldata => {
        if(lvldata.length) {
          session_data = {"name":properties.name,"level":lvldata[0].name,"chapter":lvldata[0].chapter,"config":lvldata[0].config, "game":this.name, "date":new Date(Date.now())}
          return this.db.sessions.insert(session_data)
        } else {
          throw new Error("level not found: " + JSON.stringify(level_query))
        }
      })
      .then(result => {
        session_data['_id'] = result.insertedId
        new_session = this.addSession(session_data)
        return new_session.setup(properties["arguments"])
      })
      .then(result => {
        new_session.next("chapter.START")
        return this.db.level.update(level_query,{$inc:{instances:1}})
      })
      .then(result => {
        this.client.control.emit('session', 'created')
        return resolve(new_session)
      })
      .catch(error => {
        return reject(error)
      })
    })
  }

  /**
  * delete session with same name first, then create
  *
  * @returns {Promise} forwarded createSession Promise result
  */
  overwriteSession(args) {
    return new Promise((resolve, reject) => {
      this.deleteSession(args.name)
      .then(res => {
        return this.createSession(args)
      })
      .then(res => {
        return resolve(res)
      })
      .catch(err => {
        return reject(err)
      })
    })
  }

  /**
  * delete session object and its database entry
  *
  * @param {Object|string} query - query to find sessions that should be removed. If its a string its queried by 'name' property
  *
  * @returns {Promise} true if session was found and deleted. false if not found
  */
  deleteSession(query) {
    return new Promise((resolve, reject) => {
      if(typeof query === 'string') {
        query = {"name":query}
      }
      this.db.sessions.slice(query)
      .then(result => {
        if(result.length) {
          let canceled_sessions = result.map((res, index) => {
            for(let i = 0; i < this.sessions.length; i++) {
              if(this.db.sessions.compareIDs(this.sessions[i]._id, res._id)) {
                //log.info(this.name, "Session " + i + " " + res._id + " " + this.sessions[i].name + " deleted")
                return this.sessions.splice(i, 1)[0].quit()
              }
            }
          })
          return Promise.all(canceled_sessions)
        } else if (Object.isEmpty(query)) {
          log.info(this.name, "No session to delete")
          return []
        } else {
          log.error(this.name, "Couldn't find session to delete:")
          log.error(this.name, query)
          throw new Error("session not found")
        }
      })
      .then(result => {
        let dec_instances = result.map((session, index) => {
          log.info(this.name, "deleted session " + session.name + " based on level " + session.level)
          return this.db.level.update({"name":session.level},{$inc:{instances:-1}})
        })
        
        return Promise.all(dec_instances)
      })
      .then(result => {
        this.client.control.emit('session', 'deleted')
        return resolve(result)
      })
      .catch(error => {
        return reject(error)
      })
    })
  }
  
  /**
  * iterate through local list of sessions and look for matching _id
  *
  * @param {mongodb.ObjectID} id - mongo Object id matching the session _id property
  * 
  * @return {session.Session|undefined} - Session Object on match. Otherwise undefined.
  */
  getSession(id) {
    for(let i = 0; i < this.sessions.length; i++) {
      if(this.db.sessions.compareIDs(this.sessions[i]._id, id)) {
        return this.sessions[i]
      }
    }
    log.warn(this.name, "no session matching id " + id)
    return undefined
  }
  
  /**
  * iterate through local list of sessions and look for matching key
  *
  * @param {string} key - key to look up
  * @param {*} property - property to match
  * 
  * @return {session.Session|undefined} - Session Object on match. Otherwise undefined.
  */
  findSession(key, property) {
    for(let i = 0; i < this.sessions.length; i++) {
      if(this.sessions[i][key] == property) {
        return this.sessions[i]
      }
    }
    return undefined
  }
  
  /**
  * find sessions by query in session collection and return corresponding session object
  * 
  * @param {Object} query - mongo find query
  * 
  * @return {Promise} - resolves with Session Object on match. Otherwise undefined.
  */
  querySession(query) {
    return new Promise((resolve, reject) => {
      if(typeof query === 'string') {
        query = {"name":query}
      } 
      
      this.db.sessions.find(query)
      .then(result => {
        for(let res of result) {
          return resolve(this.getSession(res._id))
        }
        return resolve(undefined)
      })
    })
  }
  
  /**
  * communication with webview
  * 
  * extract login credentials from request header to identify user
  *
  * @param {Object} socket - io socket connection accessing game editor
  */
  clientConnection(socket) {
    let browser = socket.request._query["browser"]
    
    const b64auth = (socket.request.headers.authorization || '').split(' ')[1] || ''
    const [login, password] = new Buffer(b64auth, 'base64').toString().split(':')

    log.debug(this.name, `webclient socket request. user: ${login}, browser: ${browser}, id: ${socket.id}`)

    let user = {name:login, login:login, browser:browser, socket_id:socket.id}

    socket.on('user', msg => {
      socket.emit(msg.id, user)
    })

    socket.on('rooms', msg => {
      if(msg.data) {
        socket.emit(msg.id, this.rooms[msg.data])
      } else {
        socket.emit(msg.id, this.rooms)
      }
    })

    socket.on('alert_response', msg => {
      if(this.alerts.hasOwnProperty(msg.id)) {
        this.alerts[msg.id](msg.data)
        delete this.alerts[msg.id]
      }
    })

    socket.on('session', msg => {
      if(msg.hasOwnProperty('id')) {
        switch(msg.data.cmd) {
          case 'create':
          let create_params = {}
          
          if(!msg.data.hasOwnProperty("arguments")) {
            create_params = {name:msg.data.session, level:msg.data.level}
          } else if(msg.data.arguments.length == 0) {
            create_params = {name:msg.data.session, level:msg.data.level}
          } else {
            create_params = {name:msg.data.session, level:msg.data.level, "arguments":msg.data.arguments}
          }
          this.createSession(create_params)
          .then(value => {
            socket.emit(msg.id, "created")
          })
          .catch(err => {
            if(err == "session exists") {
              socket.emit(msg.id, "exists")
            }
            socket.emit(msg.id, "failed")
            log.error(this.name, err)
          })
          break
          case 'overwrite':
          let overwrite_params = {}
          if(!msg.data.hasOwnProperty("arguments")) {
            overwrite_params = {name:msg.data.session, level:msg.data.level}
          } else if(msg.data.arguments.length == 0) {
            overwrite_params = {name:msg.data.session, level:msg.data.level}
          } else {
            overwrite_params = {name:msg.data.session, level:msg.data.level, "arguments":msg.data.arguments}
          }
          this.overwriteSession(overwrite_params)
          .then(value => {
            if(value.modifiedCount) {
              socket.emit(msg.id, "created")
            } else {
              socket.emit(msg.id, "failed")
            }
          })
          break
          case 'delete':
          if(msg.data.session.hasOwnProperty('_id')) {
            msg.data.session._id = this.db.sessions.makeID(msg.data.session._id)
          }
          this.deleteSession(msg.data.session)
          .then(result => {
            socket.emit(msg.id, "deleted")
          })
          .catch(err => {
            log.error(err)
          })
          break
          case 'get_references':
            if(msg.data.hasOwnProperty('_id')) {
              let session = this.getSession(msg.data._id)
              if(session) {
                session.getAllReferences()
                .then((result) => {
                  socket.emit(msg.id, result)
                })
                .catch((err) => {
                  log.error(this.name, err)
                  socket.emit(msg.id, {})
                })
              } else {
                socket.emit(msg.id, {})
              }
            }
            break
        }
      }
    })
    
    socket.on('game', msg => {
      if(this.hasOwnProperty(msg.data)) {
        try {
          socket.emit(msg.id,this[msg.data])
        } catch (e) {
          log.error(this.name + " socket", e)
        }
      } else {
        socket.emit(msg,undefined)
      }
    })
    
    socket.on('plugin', msg => {
      if(msg.data.hasOwnProperty("list")) {
        if(msg.data.list == "disabled") {
          try {
            socket.emit(msg.id,this.getInactivePlugins())
          } catch (e) {
            log.error(this.name + " socket", e)
          }
        }
      } else if (msg.data.hasOwnProperty("add")) {
        this.addPlugin(msg.data.add)
        .then(result => {
          log.info(this.name, "Plugin added: " + msg.data.add)
          socket.emit(msg.id,"added")
        })
        .catch(err => {
          switch (err.name) {
            case 'Error':
            log.error(this.name, err.message)
            break
            default:
            log.error(this.name, err)
          }
        })
      }
    })

    socket.on('find', msg => {
      let query = msg.data
      for(let key in query) {
        if(this.db.hasOwnProperty(key)) {
          this.db[key].find(query[key])
          .then(result => {
            socket.emit(msg.id, result)

            if(result.length && !["plugins","player","setup","listener", "sessions"].includes(key)) {
              if(result[0].hasOwnProperty("name")) {
                return this.changeRoom(socket, user, key + "-" + result[0].name)
              }
            }
            return
          })
          .then((result) => {
            if(result) {
              log.debug(this.name, user.name + " join room " + result.room + ". Is Occupied: " + result.occupied)
            }
          })
          .catch(err => {
            log.error(this.name, err)
          })
        } else {
          socket.emit(msg.id, undefined)
          log.warn(this.name, 'socket tried fetching from db collection that doesnt exist: ' + key)
        }
      }
    })
    
    socket.on('sorted', msg => {
      let query = msg.data
      if(query.hasOwnProperty("sort")) {
        var sort_by = query.sort
        delete query.sort
      }
      for(let key in query) {
        if(this.db.hasOwnProperty(key)) {
          this.db[key].sorted(query[key], sort_by)
          .then(result => {
            socket.emit(msg.id, result)
          })
        } else {
          log.error(this.name, 'socket tried fetching from db collection that doesnt exist: ' + key + ' or sort option is missing')
        }
      }
    })

    socket.on('set', msg => {
      if(msg.hasOwnProperty('data') && msg.hasOwnProperty('query') && msg.hasOwnProperty('collection')) {
        log.debug(this.name, "update set document:")
        log.debug(this.name, msg.query)
        log.debug(this.name, msg.data)
        if(msg.data.hasOwnProperty('_id')) {
          delete msg.data._id
        }
        this.db[msg.collection].set(msg.query, msg.data)
        .then(result => {
          if(msg.hasOwnProperty('id')) {
            socket.emit(msg.id, result)
          }
        })
        .catch(err => {
          if(msg.hasOwnProperty('id')) {
            socket.emit(msg.id, err)
          }
        })
      }
    })
    
    //
    // replace document in database. Delete any _id first. 
    // Will also replace string _ids in sessions property with database compatible _id.
    //
    socket.on('replace', msg => {
      if(msg.hasOwnProperty('data') && msg.hasOwnProperty('query') && msg.hasOwnProperty('collection')) {
        log.debug(this.name, "replace document:")
        log.debug(this.name, msg.query)
        if(msg.data.hasOwnProperty('_id')) {
          delete msg.data._id
        }
        if(msg.data.hasOwnProperty("sessions")) {
          if(Array.isArray(msg.data.sessions)) {
            for(let session of msg.data.sessions) {
              if(session.hasOwnProperty("_id")) {
                session._id = this.db[msg.collection].makeID(session._id)
              }
            }
          }
        }
        this.db[msg.collection].replace(msg.query, msg.data)
        .then(result => {
          if(msg.hasOwnProperty('id')) {
            socket.emit(msg.id, result)
          }
        })
        .catch(err => {
          if(msg.hasOwnProperty('id')) {
            socket.emit(msg.id, err)
          }
        })
      }
    })

    socket.on('insert', msg => {
      if(msg.hasOwnProperty('data') && msg.hasOwnProperty('collection')) {
        log.debug(this.name, "insert new document in " + msg.collection)
        if(msg.data.hasOwnProperty('_id')) {
          delete msg.data._id
        }
        this.db[msg.collection].insert(msg.data)
        .then(result => {
          if(msg.hasOwnProperty('id')) {
            socket.emit(msg.id, {insertedId:result.insertedId})
          }
        })
        .catch(err => {
          if(msg.hasOwnProperty('id')) {
            socket.emit(msg.id, err)
          }
        })
      }
    })

    socket.on('remove', msg => {
      if(msg.hasOwnProperty('query') && msg.hasOwnProperty('collection')) {
        log.debug(this.name, "remove:")
        log.debug(this.name, msg.collection)
        log.debug(this.name, msg.query)
        
        if(msg.query.hasOwnProperty('_id')) {
          msg.query._id = this.db[msg.collection].makeID(msg.query._id)
        }
        this.db[msg.collection].slice(msg.query)
        .then(result => {
          if(msg.hasOwnProperty('id')) {
            socket.emit(msg.id, result)
          }
        })
        
      }
    })

    socket.on('disconnecting', result => {
      for(let r in socket.rooms) {
        this.leaveRoom(socket, user, r)
      }
    })
    socket.on('disconnect', result => {
      log.debug(this.name, "web client disconnected: " + socket.id)
    })
  }

  /**
   * leave socket.io room. Notify first socket in same room to unlock (if any)
   *
   * @param {Object} socket - socket instance to leave room
   * @param {string} room - room name to leave
   */
  leaveRoom(socket, user, room) {
    log.debug(this.name, socket.id + " (" + user.name + ") leaves room " + room)
    socket.leave(room)
    if(room && room != user.socket_id) {
      delete this.rooms[room][socket.id]
    }
    this.client.editor.in(room).clients((error, clients) => {
      if (error) throw error;
      if(clients.length) {
        this.client.editor.to(clients[0]).emit('unlock', {})
      }
    })
  }

  joinRoom(socket, user, room) {
    socket.join(room)
    if(!this.rooms[room]) {
      this.rooms[room] = {}
    }
    this.rooms[room][socket.id] = user
  }

  /**
   * socket goto new editor room (e.g. a specific level) and leave any other editor room. 
   * Notify socket to lock editor if there are others in the same room to avoid conflicts.
   *
   * @param {*} socket - socket to switch rooms
   * @param {*} room - new room to attend. maybe a specific level or settings page.
   * @returns {Promise} - resolves with room and if it is occupied ({room:<room>, occupied:<true/false>})
   */
  changeRoom(socket, user, room) {
    return new Promise((resolve, reject) => {
      // let url = socket.handshake.headers.referer
      // let room = url.substring(url.substring(0, url.lastIndexOf("/")).lastIndexOf("/") + 1)

      for(let r in socket.rooms) {
        if(r != room && r != socket.id) {
          this.leaveRoom(socket, user, r)
        }
      }

      if(!socket.rooms.hasOwnProperty(room)) {
        this.joinRoom(socket, user, room)
      }

      this.client.editor.in(room).clients((error, clients) => {
        if (error) throw error;

        if(clients.length > 1) {
          if(socket.id == clients[0]) {
            return resolve({room:room, occupied:false})
          } else {
            log.info(this.name, "there are " + clients.length + " webclients viewing the same document " + room)
            socket.emit('lock', `${this.rooms[room][clients[0]].name} (${this.rooms[room][clients[0]].browser}) is editing ${room}.\n\n`)
            return resolve({room:room, occupied:true})
          }
        }
        return resolve({room:room, occupied:false})
      })
    })
  }

  /**
  * react on std input addressing this game
  *
  */
  command(input) {
    switch(input[0]) {
      case "sessions":
      this.sessions.forEach((session, index) => {
        if(this.db.hasOwnProperty("player")) {
          this.db.player.find({'sessions._id':session._id})
          .then(result => {
            let player = ""
            for(let plyr of result) {
              player += plyr.name + ", "
            }
            log.info(index+"", session._id + " lvl:" + session.level + " player:" + player + " Since: " + Math.round((new Date(Date.now() - session.date))/60000) + " min.")
          })
          .catch(error => {
            log.error(this.name, error)
          })
        } else {
          log.info(index+"", session._id + " lvl:" + session.level + " Since: " + Math.round((new Date(Date.now() - session.date))/60000) + " min.")
        }
      })
      break
      case "launch":
      case "create":
      case "createsession":
      if(input.length == 2) {
        this.createSession({level:input[1]})
        .catch(err => {
          log.error(this.name, err)
        })
      } else if(input.length > 2) {
        let i_args = input.slice(3)
        let args = []
        for(let a of i_args) {
          args.push({collection:"player", query:{name:a}})
        }
        log.info(this.name, "launch session with player")
        log.info(this.name, args)
        this.createSession({level:input[1], name:input[2], arguments:args})
        .catch(err => {
          log.error(this.name, err)
        })
      } else {
        log.warn(this.name, "create session takes only 1 or 2 arguments.")
      }
      break
      case "del":
      case "delete":
      case "cancel":
        let query
        if(input.length == 2) {
          if(isNaN(input[1])) {
            if(input[1] == 'all') {
              query = {}
            } else {
              query = {"name":input[1]}
            }
          } else if (Number(input[1]) < this.sessions.length){
            query = {"_id":this.sessions[Number(input[1])]._id}
          } else {
            log.error(this.name, " session index " + input[1] + " not in range")
          }
        } else if (input.length == 3) {
          query = {}
          query[input[1]] = input[2]
        } else {
          log.warn(this.name, "session cancel requires name, index or a key value pair to identify the session to be canceled. Use 'all' to cancel all sessions.")
        }
        if(query) {
          this.deleteSession(query)
          .catch(err => {
            log.error(this.name, err)
          })
        }
        break
      case "alert":
      if(input.length == 2) {
        this.alert(input[1], response => {
          log.info(this.name, response)
        })
      } else {
        log.info(0, 'client editor alert requires exactly 1 argument')
      }
      break
      case "plugins":
      case "plugin":
      if(input[1] == "add") {
        this.addPlugin(input[2])
        .then(result => {
          log.info(this.name, "Plugin added: " + input[2])
        })
        .catch(err => {
          switch (err.name) {
            case 'Error':
            log.error(this.name, err.message)
            break
            default:
            log.error(this.name, err)
          }
        })
      } else if (input[1] == "remove") {
        Input.confirmation("remove", input[2], this.deletePlugin.bind(this))
      } else if (input[1] == "settings") {
        for(let plugin in this.plugins) {
          log.info(0, plugin + ", " + JSON.stringify(this.plugins[plugin].settings))
        }
      } else if (input[1] == "list") {
        for(let plugin in this.plugins) {
          log.info(0, "[x] " + plugin)
        }
        for(let plugin of this.getInactivePlugins()) {
          log.info(0, "[-] " + plugin)
        }
      } else {
        log.info(0, 
`Please use one of these commands:

|cmd                  |description
|---------------------|--------------
|list                 |list all plugins
|add [plugin]         |activate plugin
|remove [plugin]      |deactivate plugin and delete all items`
        )
      }
      break
      case "collection":
      if (input.length > 1) {
        switch(input[1]) {
          case "list":
          for(let coll of this.setup.collections) {
            log.info(0, coll)
          }
          break
          case "create":
          this.addCollection(input[2])
          break
          case "delete":
          Input.confirmation("remove",input[2],this.removeCollection.bind(this))
          break
          default:
          log.warn(this.name, "unknown collection command " + input[1])
        }
      } else {
        log.info(this.name, "wrong number of arguments")
      }
      break
      case "import":
      if(this.db.hasOwnProperty(input[1])) {
        this.db[input[1]].import(input[2])
      } else {
        log.error(this.name, input[1] + " is not a database collection")
      }
      break
      case "export":
      if(input.length == 1) {
        this.export()
      } else if(input.length == 2) {
        this.export(input[1])
      } else {
        this.export(input[1], input.slice(2))
      }
      break
      default:
      let session = this.findSession("name", input[0])
      if(session) {
  			input.shift()
  			session.command(input)
  		} else if (this.plugins.hasOwnProperty(input[0])){
        let plugin = input.shift()
        if(typeof this.plugins[plugin].command === "function") {
          this.plugins[plugin].command(input)
        } else {
          log.info(0, plugin + " doesn't take std input commands")
        }
      } else if (!isNaN(input[0])) {
        if(this.sessions.length > input[0] && input[0] >= 0) {
          this.sessions[input.shift()].command(input)
        } else {
          log.error(this.name, " session index " + input[0] + " not in range")
        }
      } else {
        if(this.sessions.length == 1) {
  				this.sessions[0].command(input)
  			} else {
          log.info(this.name,"No such session, plugin or command '" + input[0] + "'")
        }
  		}
      break
    }
  }

  /**
  * - drop database
  *
  * @todo quit all processes properly. perhabs make backup
  */
  delete() {
    this.status.removeAllListeners()
    this.event.removeAllListeners()
    for(let change_listener in this.changes) {
      this.changes[change_listener].removeAllListeners()
    }

    this.db.drop()
  }

  /**
   * @todo feedback to client that all game windows should be closed or notified
   */
  unload() {
    //this.client.emit()
    //this.editor.disconnect()
  }

}

/** Game Object */
module.exports.Game = Game;
